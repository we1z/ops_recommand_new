# coding:utf-8
"""
author: hyx
date： 2017.6.5
控制爬虫速度的装饰器
"""

import time
import random

# 设置企查查两次链接之间的最少时间
QICHACHA_TIME_DELAY = 10
# 记录上一次爬取企查查信息的时间
time_qichacha = 0

# 设置天眼查两次链接之间的最少时间
TIANYANCHA_TIME_DELAY = 5
# 记录上一次爬取天眼查信息的时间
time_tianyancha = 0


def time_delay_controller_qichacha(func):
    '''
    控制企查查爬取速度的装饰器
    :param func: 爬取企查查的函数
    :return: 爬取企查查的函数
    '''

    def _fun(*args):
        global time_qichacha
        if time.time() - time_qichacha < QICHACHA_TIME_DELAY:
            time.sleep(QICHACHA_TIME_DELAY-(time.time() - time_qichacha)+random.randint(0,4))
        time_qichacha = time.time()
        print(func)
        return func(*args)

    return _fun


def time_delay_controller_tianyancha(func):
    '''
    控制天眼查爬取速度的装饰器
    :param func: 爬取天眼查的函数
    :return: 爬取天眼查的函数
    '''

    def _fun(*args):
        global time_tianyancha
        if time.time() - time_tianyancha < TIANYANCHA_TIME_DELAY:
            time.sleep(TIANYANCHA_TIME_DELAY-(time.time() - time_tianyancha)+random.randint(0,4))
        time_tianyancha = time.time()
        print(func)
        return func(*args)

    return _fun