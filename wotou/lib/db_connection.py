# -*- coding:utf-8-*-#

"""
管理数据库链接
"""

import pymongo
from wotou.config.config import *
import time

company_db = None
corpus_db = None
news_db = None


def mongodb_connection_local(db):
    conn = pymongo.MongoClient('127.0.0.1', 27017)
    return conn[db]


def mongodb_connection(db, is_reused=False):
    """
    获取数据库链接
    :param db: 数据库名称
    :param is_reused 是否复用数据库链接，若链接长期不用，则False，若短时间内（1分钟以内）则选True
    :return: 数据库链接
    """
    global company_db
    global corpus_db
    global news_db

    if db == 'company':
        if company_db and is_reused:
            return company_db
        else:
            user = COMPANY_DB_USER
            pw = COMPANY_DB_PW
    elif db == 'corpus':
        if corpus_db and is_reused:
            return corpus_db
        else:
            user = CORPUS_DB_USER
            pw = CORPUS_DB_PW
    elif db == 'news':
        if news_db and is_reused:
            return news_db
        else:
            user = NEWS_DB_USER
            pw = NEWS_DB_PW

    conn = pymongo.MongoClient(MONGO_SERVER, MONGO_PORT, connectTimeoutMS=5000, connect=False, maxPoolSize=3000,
                               wtimeoutMS=5000)

    try:
        conn.database.authenticate(user, pw, db)
    except pymongo.errors.NetworkTimeout:
        time.sleep(1)
        return mongodb_connection(db, is_reused)
    except pymongo.errors.ServerSelectionTimeoutError:
        time.sleep(1)
        return mongodb_connection(db, is_reused)
    if db == 'company':
        company_db = conn[db]
        return company_db
    elif db == 'corpus':
        corpus_db = conn[db]
        return corpus_db
    elif db == 'news':
        news_db = conn[db]
        return news_db


def read_col(db_name, col_name, key_name):
    """
    查询数据库某表某列所有数据
    :param col_name:需要查询的collection
    :param key_name:需要查询的列
    :return:所有满足结果
    """
    db = mongodb_connection(db_name)
    collection = db[col_name]
    return [item[key_name] for item in collection.find({}, {"_id": 0, key_name: 1}) if key_name in item]
