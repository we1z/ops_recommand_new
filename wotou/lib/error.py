# coding:utf-8
"""
author: hyx
关于爬虫和网络链接的自定义错误
"""


# 当前cookies不可用时抛出
class CookiesError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value


# 全部cookies不可用时抛出
class NoUsefulCookiesError(Exception):
    def __init__(self):
        Exception.__init__(self)


# 找不到公司信息时抛出
class CompanyNotFoundError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value


# 需要验证码时输出
class NeedEnterConfirmationCodeError(Exception):
    def __init__(self):
        Exception.__init__(self)


class RetryTimesTooMuchError(Exception):
    def __init__(self):
        Exception.__init__(self)


class RegexTimeoutError(Exception):
    def __init__(self):
        Exception.__init__(self)
