# coding:utf-8
'''
调用云分词服务pullword 的api
'''
import requests
import urllib.parse
import time

get_url = "http://api.pullword.com/get.php"


class ServerError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def pullword(source="", threshold=0.9, debug=0):
    """
    请求分词结果
    :param source: 待分词的文章
    :param threshold: 准确率
    :param debug:
    :return: 分词后的list
    """

    if len(source) > 1000:
        source_list = []
        temp_source = ''
        for i in source.split("，"):
            for j in i.split(","):
                if len(temp_source) > 380:
                    source_list.append(temp_source)
                    temp_source = ''
                else:
                    temp_source += j
        if len(temp_source) > 0:
            source_list.append(temp_source)

        temp_list = []
        for i in source_list:
            if len(i) > 1000:
                # 如此可以过滤掉一些不是正文的内容
                i = i[:1000]
            temp_list.extend(pullword(i, threshold, debug))

        return temp_list
    # 由于网站的特殊性，需要按顺序添加get参数
    _source = urllib.parse.urlencode({"source": source}).replace("%0D%0A","")
    _threshold = urllib.parse.urlencode({"param1": threshold})
    _debug = urllib.parse.urlencode({"param2": debug})
    time.sleep(1.5)
    try:
        pw = requests.get(get_url + "?" + _source + "&" + _threshold + "&" + _debug, timeout=20)
    except requests.exceptions.ReadTimeout:
        return pullword(source, threshold, debug)
    except requests.exceptions.ConnectionError:
        time.sleep(10)
        return pullword(source, threshold, debug)
    except requests.exceptions.ChunkedEncodingError:
        time.sleep(10)
        return pullword(source, threshold, debug)
    if pw.status_code != 200:
        raise ServerError("server return %s" % pw.status_code)
    re = pw.content.decode('utf-8').split()
    if len(re)*2.2 > len(source) > 100:
        time.sleep(10)
        print("分词可能出现问题")
    if 'pullword' in re:
        print("分词可能出现问题")
    return re


if __name__ == '__main__':
    pullword('我要发达')