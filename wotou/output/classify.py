#首先从xls获取列表并判断
import xlrd
import xlwt
import os

def classify(filename):
    number_b =0
    file = xlrd.open_workbook(filename)
    table = file.sheets()[0]
    rows = table.nrows
    BSG = []
    UNBSG = []
    for i in range(rows):
        row = table.row_values(i)
        value = table.row_values(i)[18]
        if value in'北京上海广州深圳苏州南京杭州':
            #print(row)
            BSG.append(row)
            number_b = number_b + 1

        else:
            #print('非北上广')
            UNBSG.append(row)

    file = xlwt.Workbook()
    table = file.add_sheet('sheet name')
    for i in range(len(BSG)-1):
        for m in range(33):
            table.write(i, m, BSG[i][m])

    #table.write(0,0, BSG[0][0])
    file_name='（热门城市）'+filename
    file.save(file_name)


    file2 = xlwt.Workbook()
    table2 = file2.add_sheet('sheet name')
    for i in range(len(UNBSG)-1):
        for m in range(33):
            table2.write(i, m, UNBSG[i][m])

    #table.write(0,0, BSG[0][0])
    file2_name='（非热门城市）'+filename
    file2.save(file2_name)

if __name__ == '__main__':
    for i in [filename for filename in os.listdir('.') if (filename.endswith('xls') or filename.endswith('xlsx'))and not filename.startswith('（')]:
        classify(i)
        os.remove(i)
