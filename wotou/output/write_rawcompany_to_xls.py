# coding:utf-8
"""
从数据库中读入rawcompany数据
按企业初评表格式整理，写入excel中，输出待二评结果
"""
from wotou.lib.db_connection import mongodb_connection
import xlwt
import xlrd
from xlutils.copy import copy
import os
from wotou.spiders.company.spider_company import filter_company_by_detail_api
from wotou.spiders.company.spider_company import filter_company_by_blacklist_api
from wotou.spiders.company.spider_company import search_company_in_cluster_api
from wotou.spiders.company.spider_company import is_state_own_company, is_stock_relate_company
import datetime
from wotou.spiders.clinical.spider_yaozh import clinical_trial_filter_api
from wotou.spiders.medicine.spider_medicine import medicine_filter_api
from wotou.lib.db_connection import read_col
from wotou.spiders.patent.patexplorer import patexplorer_search_word_api
import urllib
from selenium import webdriver
from lxml import html
from selenium.common import exceptions
import time
from tqdm import tqdm
from wotou.spiders.baidu.baidu_text import search_company_baidu_info_api
from wotou.spiders.baidu.baidu_score import cal_baidu_score_api
from wotou.spiders.company.add_blacklist_from_not_found import add_not_found_blacklist_api
import multiprocessing
from multiprocessing import Pool

db = mongodb_connection('company')
collection_rawcompany = db['raw_company']
collection_investcompany = db['investment_company']
temp_blacklist = []


def get_temp_blacklist():
    """
    获取临时黑名单,即获取目录上，刚刚生成的xls文件中的企业，避免重复推荐
    :return:
    """
    global temp_blacklist
    for filename in os.listdir('.'):
        if filename.endswith(".xls") or filename.endswith(".xlsx"):

            workbook = xlrd.open_workbook(filename)
            table = workbook.sheets()[0]
            nrows = table.nrows

            for i in range(1, nrows):
                temp_blacklist.append(table.cell(i, 2).value)


def create_xls(filename, keys):
    """创建工作表
       输入:工作表的name,表的字段
    """
    f = xlwt.Workbook(encoding='utf-8')
    sheet = f.add_sheet("1", cell_overwrite_ok=True)
    for i in range(len(keys)):
        sheet.write(0, i, keys[i])

    f.save(filename + '.xls')


def write_to_excel(rawcompany, filename):
    """写入新的数据
        输入：待写入公司列表，字段名称，打开工作表名称
    """
    keys = ['_id', '公司地址', '企业全称', '行业', '曾用名', '网站', '企业资质（专利、著作权、临床药物等）', '初评时间', '成立日期', '董事长/其他联系人', '注册资本',
            '注册资本1',
            '公司结构/股东状况', '天眼查链接', '是否上市', '联系方式', '主营业务', '省份', '地区', '负责人', '股东信息', '知识产权', '价值', '初评人', '推荐人', '是否初评',
            '企业来源ly',
            '二评', '财务数据', '过往融资/上市情况', '备注', '称呼', '前置二评分数']
    if not os.path.exists(filename + '.xls'):
        create_xls(filename, keys)
    oldwb = xlrd.open_workbook(filename + '.xls')
    newwb = copy(oldwb)
    sheet = newwb.get_sheet(0)
    nrows = len(sheet.rows)
    for i in range(len(rawcompany)):
        for j in range(len(keys)):
            try:
                # 按目前crm系统的格式输入，会有冗余
                if keys[j] == '注册资本1':
                    sheet.write(nrows + i, j, str(rawcompany[i]['注册资本']) + '万人民币')
                elif keys[j] == '初评时间':
                    sheet.write(nrows + i, j, str(rawcompany[i]['爬取时间']))
                elif keys[j] == '董事长/其他联系人':
                    sheet.write(nrows + i, j, str(rawcompany[i]['主要负责人']))
                elif keys[j] == '公司结构/股东状况':
                    sheet.write(nrows + i, j, str(rawcompany[i]['股东状况']))
                elif keys[j] == '天眼查链接':
                    sheet.write(nrows + i, j, str(rawcompany[i]['链接']))

                elif keys[j] == '企业资质（专利、著作权、临床药物等）' or keys[j] == '知识产权':
                    if rawcompany[i]['企业资质']['专利数'] > 0 and rawcompany[i]['企业资质']['著作权数'] > 0:
                        sheet.write(nrows + i, j, str(rawcompany[i]['企业资质']['专利数']) + '个专利' + str(
                            rawcompany[i]['企业资质']['著作权数']) + '个著作权')
                    elif rawcompany[i]['企业资质']['专利数'] > 0:
                        sheet.write(nrows + i, j, str(rawcompany[i]['企业资质']['专利数']) + '个专利')
                    elif rawcompany[i]['企业资质']['著作权数'] > 0:
                        sheet.write(nrows + i, j, str(rawcompany[i]['企业资质']['著作权数']) + '个著作权')
                    else:
                        sheet.write(nrows + i, j, '无')
                elif keys[j] == '初评人' or keys[j] == '推荐人':
                    sheet.write(nrows + i, j, 'OPS')
                elif keys[j] == '是否初评':
                    sheet.write(nrows + i, j, '1，已初评')
                elif keys[j] in rawcompany[i]:
                    sheet.write(nrows + i, j, str(rawcompany[i][keys[j]]))
                else:
                    pass
            except Exception as e:
                print(str(rawcompany[i]) + '公司信息输出时出错')

    # 调整输出格式
    sheet.col(0).width = 256 * 1
    sheet.col(1).width = 256 * 30
    sheet.col(2).width = 256 * 25
    sheet.col(3).width = 256 * 15
    sheet.col(4).width = 256 * 1
    sheet.col(5).width = 256 * 15
    sheet.col(6).width = 256 * 10
    newwb.save(filename + '.xls')


def _find_non_investment_company_for_single_investment_company(id):
    """
        生成non_invest_company列表，机构跟踪法专用
    """
    non_investment_company = []
    ret = collection_investcompany.find_one({'_id': id})
    for raw_company in ret['公司列表']:
        non_investment_company.append(collection_rawcompany.find_one({'_id': raw_company.id}))

    return non_investment_company


def _find_investment_company_for_single_investment_company(id):
    """
    返回公司名下所有投资公司，机构跟踪法专用
    :param id: 母投资公司在数据库里的id
    :return: 母投资公司投资的子投资公司id列表
    """
    ret = collection_investcompany.find_one({'_id': id})
    return [item.id for item in ret['基金公司列表']]


def _find_interation_non_investment_company(investment_company_name=None, id=None, mother_investment_list=None):
    """
    递归搜索投资公司所投的非投资公司
    :param mother_investment_list:
    :param investment_company_name:投资公司名
    :param id:
    :return:
    """
    if mother_investment_list is None:
        mother_investment_list = []
    non_investment_company = []
    if investment_company_name:
        ret = mongodb_connection('company')['investment_company'].find_one({"企业全称": investment_company_name})
    else:
        if id in mother_investment_list:
            return []
        ret = mongodb_connection('company')['investment_company'].find_one({"_id": id})
        mother_investment_list.append(id)

    non_investment_company.extend(_find_non_investment_company_for_single_investment_company(ret['_id']))

    son_investment_company_id_list = _find_investment_company_for_single_investment_company(ret['_id'])
    if son_investment_company_id_list:
        for i in son_investment_company_id_list:
            non_investment_company.extend(
                _find_interation_non_investment_company(id=i, mother_investment_list=mother_investment_list))
    print(ret['企业全称'], len(non_investment_company), len(son_investment_company_id_list))
    return non_investment_company


def get_media_data_by_magazine_mark(magazine_mark):
    """
    输入：文献标识
    输出：拥有该文献标识的文献信息
    """
    l=[]
    for i in tqdm([i for i in mongodb_connection('news')['cnki'].find({'文献标识': magazine_mark, 'company': {'$exists': 1}})]):
        for j in i['company']:
            l.extend(j)
    l = list(set(l))
    return [i for i in l if i !='']


def get_data_from_db_by_date(media_name, datestr):
    """
    获取单日的媒体新闻信息
    :param media_name:媒体名称
    :param datestr: 日期
    :return: 当天的媒体新闻
    """
    collection = mongodb_connection('news')[media_name]
    return [item for item in collection.find({'time': datestr,'company':{'$exists':1}})]


def get_media_data_by_date(media_name, datestr_str, dateend_str):
    """
    获取某段日期内的所有待处理新闻
    :param :media_name:[str]:待处理新闻库
    :param :datestr_str:[str]:起始日期
    :param :dateend_str:[str]:终止日期
    :return result[list]:该段时间内的媒体新闻的信息
    """
    result = []
    datestr = datetime.datetime.strptime(datestr_str, '%Y-%m-%d')
    dateend = datetime.datetime.strptime(dateend_str, '%Y-%m-%d')
    while (dateend - datestr).days > -1:
        one_day_data = get_data_from_db_by_date(media_name, datestr.strftime('%Y-%m-%d'))  # 现在数据库中查找该日数据
        result += one_day_data
        datestr = datestr + datetime.timedelta(days=1)
    return result


def get_baidu_info_len(company_name):
    '''
    获取公司在百度搜索中信息的条数
    :param company_name:公司名称
    :return:信息条数（除去部分无用信息后）
    '''
    options = webdriver.ChromeOptions()
    # options.add_argument("--headless")
    driver = None
    try:
        driver = webdriver.Chrome(chrome_options=options)
        driver.implicitly_wait(10)
        driver.set_page_load_timeout(30)
        company_name = company_name.split('（')[0]
        url = r'https://www.baidu.com/s?wd='
        words = urllib.parse.quote('"' + company_name + '" -（招聘） -（信用） -（职友集） -（软件著作权）')

        driver.get(url + words)
        response_data = driver.page_source
        if len(response_data) < 500:
            print(response_data)

    except (exceptions.StaleElementReferenceException, exceptions.TimeoutException, ConnectionRefusedError,
            ConnectionResetError,
            exceptions.WebDriverException):
        print("ConnectionRefuse")
        time.sleep(4)
        if driver:
            driver.quit()
        return get_baidu_info_len(company_name)

    num = response_data[response_data.find('百度为您找到相关结果约') + 11:]
    num = int(num[:num.find("个")].replace(',', ''))
    driver.quit()
    return num


def whole_filter(i, temp_blacklist):
    '''
    总过滤器，根据五大维度，临时黑名单，是否上市公司子公司，是否国企子公司，是否百度信息太少，是否前置二评得分太低对企业进行过滤
    :param temp_blacklist:
    :param i:企业详细信息
    :return:企业详细信息，是否保留
    '''
    if filter_company_by_detail_api(i) and i['企业全称'] not in temp_blacklist:
        if is_stock_relate_company(i) or is_state_own_company(i):
            print(i['企业全称'], '上市公司相关或国企相关')
        else:
            # 公司有主页，或者百度信息多于6条
            if (i['网站'] and len(i['网站']) > 7) or get_baidu_info_len(i['企业全称']) > 3:
                try:
                    if os.path.exists('E:\\Work\\ops_recommand\\wotou\output\\baidu_raw_text\\' + i['企业全称'] + '.txt'):
                        with open('E:\\Work\\ops_recommand\\wotou\output\\baidu_raw_text\\' + i['企业全称'] + '.txt', encoding='UTF-8') as f:
                            baidu_score = cal_baidu_score_api(f.read(),i['成立日期'])
                    else:
                        text = search_company_baidu_info_api(i['企业全称'])
                        print("获取写入文件内容"+str(i['企业全称']))
                        with open('E:\\Work\\ops_recommand\\wotou\output\\baidu_raw_text\\' + i['企业全称'] + '.txt', mode='w', encoding='UTF-8') as f:
                            print("写入文件")
                            f.write(text)
                            print("已写入")
                        print("计算百度分数")
                        baidu_score = cal_baidu_score_api(text,i['成立日期'])
                except FileNotFoundError as e:
                    print(e)
                    return whole_filter(i, temp_blacklist)
                except FileExistsError as e:
                    print(e)
                    return whole_filter(i, temp_blacklist)
                # 设置前置二评分数下限
                # if baidu_score > 99.99:
                #     print(i['企业全称'], '前置二评分数 ' + str(baidu_score))
                #     return i, True
                # else:
                #     print(i['企业全称'], '前置二评分数太低')
                i['前置二评分数'] = baidu_score
                return i, True
            else:
                print(i['企业全称'], '网上信息太少')
    elif i['企业全称'] in temp_blacklist:
        print(str(i['企业全称'])+"在黑名单中")
        pass
    return i, False


def write_rawcompany_to_xls_api(method, *args):
    """
    输出到excel表的api
    :param method:所用方法，包括媒体跟踪法（media），机构跟踪法(organization)，临床法(clinical)，药物法(medicine)，关键词专利法（keywordpatent），名单法(list),期刊跟踪法（magazine）
    :param args: 所用方法的参数 媒体跟踪法（媒体名称，开始日期，结束日期），机构跟踪法（机构名），临床法（开始日期，结束日期），药物法（开始日期，结束日期），关键词专利法（关键词列表）,名单法（名单文件路径）
    :return: None
    """
    get_temp_blacklist()
    filtered_company = []

    if method == 'organization':
        company_list = []
        filename = "机构跟踪法" + args[0]
        non_invest_company = _find_interation_non_investment_company(args[0])

        raw_company_list = filter_company_by_blacklist_api(non_invest_company)
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

    elif method == 'clinical':
        company_list = []
        filename = "临床法" + args[0] + args[1]
        company_name_list = clinical_trial_filter_api(args[0], args[1])

        company_list.extend(search_company_in_cluster_api(list(set(company_name_list))))

        raw_company_list = filter_company_by_blacklist_api(company_list)
        company_list = []
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

    elif method == 'medicine':
        company_list = []
        filename = "药物法" + args[0] + args[1]
        company_name_list = medicine_filter_api(args[0], args[1])

        # 找到company数据库中没有处理的企业
        company_list.extend(search_company_in_cluster_api(list(set(company_name_list))))

        raw_company_list = filter_company_by_blacklist_api(company_list)
        company_list = []
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

    elif method == 'list':
        filename = "名单法" + args[0]
        company_list = []
        with open(args[0], encoding="utf-8") as f:
            company_list.extend(
                search_company_in_cluster_api(list(([a.replace('\n', '') for a in f.readlines()]))))

        raw_company_list = filter_company_by_blacklist_api(company_list,True)
        company_list = []
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

    elif method == 'media':
        print('进入媒体跟踪法')

        company_list = []
        company_name_list=[]
        raw_company_list = []
        filename = '媒体跟踪法' + args[0] + args[1] + args[2]
        # news_output = open('媒体详情' + args[0] + args[1] + args[2] + '.txt', mode='a', encoding='UTF-8')
        # 获取媒体
        news_list = get_media_data_by_date(args[0], args[1], args[2])
        for i in news_list:
            for j in i['company']:
                company_name_list.extend(j)
        company_name_list=list(set(company_name_list))
        mission_list = [company_name_list[i:i + 100] for i in range(0, len(company_name_list), 100)]
        print('任务列表数：', len(mission_list))
        #pool = Pool(processes=40)
        pool = Pool(processes=6)
        for i in pool.map(search_company_in_cluster_api, mission_list):
            raw_company_list.extend(i)

        raw_company_list = filter_company_by_blacklist_api(raw_company_list)
        print(raw_company_list)
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

        # news_output.close()


    elif method == 'magazine':
        print('进入期刊跟踪法')
        company_list = []
        raw_company_list = []
        filename = '期刊跟踪法' + args[0]
        # news_output = open('期刊详情' + args[0] + '.txt', mode='a', encoding='UTF-8')
        news_list = get_media_data_by_magazine_mark(args[0])
        mission_list=[news_list[i:i + 100] for i in range(0, len(news_list), 100)]
        print('任务列表数：',len(mission_list))
        pool=Pool(processes=6)
        for i in pool.map(search_company_in_cluster_api,mission_list):
            raw_company_list.extend(i)
        raw_company_list = filter_company_by_blacklist_api(raw_company_list)
        print('生成过滤后的初始名单')
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

        # news_output.close()

    elif method == 'keywordpatent':
        print("进入关键词专利法")
        filename = "关键词专利法" + str(args[0])
        company_list = []
        for key_word in args[0]:
            company_list.extend(search_company_in_cluster_api(patexplorer_search_word_api(key_word, True, 2)[1]))
        raw_company_list = filter_company_by_blacklist_api(company_list)
        company_list = []
        for i in raw_company_list:
            if i not in company_list:
                company_list.append(i)

    else:
        print("方法名错误，结束运行")
        return

    # 多进程过滤公司
    print('开始过滤公司')
    pool = multiprocessing.Pool(processes=10)
    process_list = []

    for i in company_list:
        # 前置二评算法
        process_list.append(pool.apply_async(whole_filter, args=(i, temp_blacklist)))
    pool.close()
    pool.join()

    for p in process_list:
        i, b = p.get()
        print(str(i)+str(b))
        if b and i not in filtered_company:
            filtered_company.append(i)
    if method=='magazine':
        a=len(filtered_company)//3
        print('列表1')
        filtered_company0=filtered_company[:a]
        print('列表2')
        filtered_company1=filtered_company[a:2*a]
        print('列表3')
        filtered_company2=filtered_company[2*a: ]
        filtered_company0.sort(key=lambda a: float(a['前置二评分数']), reverse=True)
        print('列表1排序')
        filtered_company1.sort(key=lambda a: float(a['前置二评分数']), reverse=True)
        print('列表2排序')
        filtered_company2.sort(key=lambda a: float(a['前置二评分数']), reverse=True)
        print('列表3排序')
        print('开始写xls')
        if filtered_company0:
            write_to_excel(filtered_company0, filename+'上')
        if filtered_company1:
            write_to_excel(filtered_company1, filename+'中')
        if filtered_company2:
            write_to_excel(filtered_company2, filename+'下')

    else:
        filtered_company.sort(key=lambda a: float(a['前置二评分数']), reverse=True)
        if filtered_company:
            print("写入excel")
            write_to_excel(filtered_company, filename)
        add_not_found_blacklist_api()
    add_not_found_blacklist_api()


if __name__ == '__main__':
    # write_rawcompany_to_xls_api("clinical","2017-11-10","2018-06-01")
    # write_rawcompany_to_xls_api("keywordpatent", ['impella系统', 'Brentuximab Vedotin','甲状腺髓样癌','等离子体发射光谱仪','傅立叶变换红外光谱仪'])
    # write_rawcompany_to_xls_api("list", "111.txt")
    # l=['北京风云际会投资管理有限公司', '北京启赋投资咨询中心（有限合伙）', '北京信中利股权投资管理有限公司', '北京执一资本投资管理有限公司', '北京中海长益投资管理中心（有限合伙）', '北京中海创业投资有限公司', '成都高投创业投资有限公司', '广州海汇投资管理有限公司', '湖北省高新技术产业投资有限公司', '湖南高新创业投资集团有限公司', '华创汇才投资管理（北京）有限公司', '华映光辉投资管理（苏州）有限公司', '华映光辉投资管理(无锡)有限公司', '江苏达泰股权投资基金管理有限公司', '江苏金茂投资管理股份有限公司', '江苏毅达股权投资基金管理有限公司', '君盛投资管理有限公司', '昆仲（深圳）股权投资管理有限公司', '宁波君润创业投资管理有限公司', '宁波杉杉创业投资有限公司', '清控银杏创业投资管理（北京）有限公司', '赛富复兴（深圳）投资管理中心（有限合伙）', '上海久奕投资管理有限公司', '上海领庆创业投资管理有限公司', '上海武岳峰高科技创业投资管理有限公司', '上海新中欧创业投资管理有限公司', '深圳东方赛富投资有限公司', '深圳清源时代投资管理控股有限公司', '深圳市创东方投资有限公司', '深圳市创新投资集团有限公司', '深圳市东方汇富创业投资管理有限公司', '深圳市富坤创业投资集团有限公司', '深圳市启赋资本管理有限公司', '深圳市深商富坤兴业基金管理有限公司', '深圳同创伟业资产管理股份有限公司', '苏州高新创业投资集团有限公司', '苏州元禾控股股份有限公司', '天津创业投资管理有限公司', '天津赛富盛元投资管理中心（有限合伙）', '乌鲁木齐凤凰基石股权投资管理有限合伙企业', '西安高新技术产业风险投资有限责任公司', '西安鲁信股权投资管理有限公司', '盈富泰克创业投资有限公司', '浙江如山汇金资本管理有限公司', '浙江赛伯乐投资管理有限公司', '浙江省创业投资集团有限公司', '浙江银江股权投资管理有限公司', '浙江浙科投资管理有限公司', '浙商创投股份有限公司', '中国风险投资有限公司']
    # for i in l:
    #     try:
    #         write_rawcompany_to_xls_api("organization", i)
    #     except:
    #         pass
    write_rawcompany_to_xls_api('media',"shengwugu", "2018-11-14", "2018-11-14")
    #write_rawcompany_to_xls_api('organization','北京风云际会投资管理有限公司')
    # write_rawcompany_to_xls_api('magazine', 'ZCYO')
    # write_rawcompany_to_xls_api('media','weixin_ai_era','2018-06-01','2018-08-01')
    # write_rawcompany_to_xls_api('medicine','2017-11-01','2018-06-01')
