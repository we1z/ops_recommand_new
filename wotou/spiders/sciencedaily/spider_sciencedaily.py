# coding:utf-8
"""
从sciencedaily_computers_math中爬取新闻
"""
import time
import pymongo
from wotou.lib.db_connection import mongodb_connection
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from wotou.spiders.util.proxies import service_args
from selenium.webdriver import DesiredCapabilities
from selenium import webdriver
from selenium.common import exceptions


def modify_time(time_str):
    """
    时间处理函数
    :param time_str: 发表日期，格式为:August 2, 2017
    :return:符合格式的日期2017-08-02
    """
    month_dict = {"January": '01', "February": '02', "March": '03', "April": '04',
                  "May": '05', "June": '06', "July": '07', "August": '08',
                  "September": '09', "October": '10', "November": '11', "December": '12'}
    time_str = time_str.replace(',', '')
    time_list = time_str.split(' ')
    if len(time_list[1]) == 1:
        time_list[1] = '0' + time_list[1]
    time_list[0] = month_dict[time_list[0]]
    time_str = time_list[2] + '-' + time_list[0] + '-' + time_list[1]
    return time_str


def spider_current(driver, collection):
    """
    爬取第一页的数据
    :param driver: 浏览器
    :param collection: 数据库表
    :return:
    """
    ##新闻两种格式，有图片和没图片的，xpath不同
    # 无图片的新闻
    try:
        news_without_photo = WebDriverWait(driver, 20).until \
            (EC.presence_of_all_elements_located((By.XPATH, ".//*[@id='summaries']/h3")))
        print(len(news_without_photo))
        for i in range(1, len(news_without_photo) + 1):
            new_title = WebDriverWait(driver, 20).until \
                (EC.presence_of_element_located((By.XPATH, ".//*[@id='summaries']/h3[%d]/a" % i))).text
            new_url = WebDriverWait(driver, 20).until \
                (EC.presence_of_element_located((By.XPATH, ".//*[@id='summaries']/h3[%d]/a" % i))).get_attribute('href')
            if collection.find_one({'url': new_url}):
                print('此链接已存在，不在录入！！！')
            else:
                collection.insert({'name': new_title, 'url': new_url})
                print('链接录入成功')
                print('新闻标题', new_title)
                print('新闻链接', new_url)
        # 有图片的新闻
        news_with_photo = WebDriverWait(driver, 20).until \
            (EC.presence_of_all_elements_located((By.XPATH,
                                                  ".//*[@id='summaries']/div[@class='row']/div[2]/h3/a")))
        print(len(news_with_photo))
        for i in news_with_photo:
            new_title = i.text
            new_url = i.get_attribute('href')
            if collection.find_one({'url': new_url}):
                print('此链接已存在，不在录入！！！')
            else:
                collection.insert({'name': new_title, 'url': new_url})
                print('链接录入成功')
                print('新闻链接:', new_url)
                print('新闻标题:', new_title)
    except exceptions.TimeoutException as e:
        print('爬取超时，可能是出验证码了')
        driver.save_screenshot('123.png')
        return spider_current(driver, collection)


def spider_load_more(driver, collection):
    """
    爬取load_more加载后的数据
    :param driver: 浏览器
    :param collection: 数据库表
    :return:
    """
    try:
        # load_more按键
        load_more_stories = WebDriverWait(driver, 20).until \
            (EC.presence_of_element_located((By.XPATH, ".//*[@id='load_more_stories']")))
        load_more_stories.click()
        time.sleep(10)
        more_stores = WebDriverWait(driver, 20).until \
            (EC.presence_of_element_located((By.XPATH, ".//*[@id='summaries']/div[last()]")))
        # 无图片的xpath
        news_without_photo = WebDriverWait(more_stores, 20).until \
            (EC.presence_of_all_elements_located((By.XPATH, "./h3")))
        print(len(news_without_photo))
        for i in range(1, len(news_without_photo) + 1):
            new_title = WebDriverWait(more_stores, 20).until \
                (EC.presence_of_element_located((By.XPATH, "./h3[%d]/a" % i))).text
            new_url = WebDriverWait(more_stores, 20).until \
                (EC.presence_of_element_located((By.XPATH, "./h3[%d]/a" % i))).get_attribute('href')
            if collection.find_one({'url': new_url}):
                print('此链接已存在，不在录入！！！')
            else:
                collection.insert({'name': new_title, 'url': new_url})
                print('链接录入成功')
                print('新闻标题', new_title)
                print('新闻链接', new_url)
        # 有图片的新闻
        news_with_photo = WebDriverWait(more_stores, 20).until \
            (EC.presence_of_all_elements_located((By.XPATH,
                                                  "./div[@class='row']/div[2]/h3/a")))
        print(len(news_with_photo))
        for i in news_with_photo:
            new_title = i.text
            new_url = i.get_attribute('href')
            if collection.find_one({'url': new_url}):
                print('此链接已存在，不在录入！！！')
            else:
                collection.insert({'name': new_title, 'url': new_url})
                print('链接录入成功')
                print('新闻标题:', new_title)
                print('新闻链接:', new_url)
        print('\n')
    except exceptions.TimeoutException as e:
        print('load_more加载失败！！！')
        return spider_load_more(driver, collection)


def sciencedaily_spider_page(url, driver, collection):
    """
    对新闻页面进行爬虫
    :param url:新闻的链接
    :param driver:浏览器
    :param collection:数据库表
    :return:
    """
    try:
        driver.get(url)
        # 加载时间过长，手动停止加载，直接进行爬取
    except exceptions.TimeoutException as e:
        print('休息3秒后手动停止')
        driver.execute_script('window.stop()')
    try:
        new_title = WebDriverWait(driver, 20).until \
            (EC.presence_of_element_located((By.XPATH, ".//*[@id='headline']"))).text
        new_date = WebDriverWait(driver, 20).until \
            (EC.presence_of_element_located((By.XPATH, ".//*[@id='date_posted']"))).text
        new_date = modify_time(new_date)
        new_content = WebDriverWait(driver, 20).until \
            (EC.presence_of_element_located((By.XPATH, ".//*[@id='text']"))).text
        # 爬虫时间：格式为YY-MM-DD
        spidertime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        print('新闻标题：', new_title)
        print('新闻链接：', url)
        print('发表时间：', new_date)
        print('爬虫时间：', spidertime)
        print('新闻内容：\n', new_content)
        # 更新数据库
        collection.update({'url': url}, {'url': url, 'name': new_title, 'time': new_date,
                                         'spidertime': spidertime, 'content': new_content})
    # 手动停止后页面信息未加载出来，重新加载
    except exceptions.TimeoutException as e:
        print('爬取时间过长！！！！')
        return sciencedaily_spider_page(url, driver, collection)
    # 可能出现验证码，更新几次好像就没有了
    except exceptions.NoSuchElementException as e:
        print('没爬到，是不是验证码出来了！！！！')
        return sciencedaily_spider_page(url, driver, collection)


def sciencedaily_spider(url, driver):
    """
    对sciecedaily进行首页爬虫
    :param url: computer_math的链接
    :param driver:浏览器
    :return:
    """
    # 本地数据库表
    # collection = mongodb_connection_local()
    # 远程数据库表
    collection = mongodb_connection('news')['sciencedaily']
    # 爬虫分两部分，在首页得到所有的文章链接并写入数据库，在从数据库读取链接得到内容
    # 得到文章的链接和名字
    try:
        driver.get(url)
    except exceptions.TimeoutException as e:
        print('网页加载时间过长！！！！')
        return sciencedaily_spider(url, driver)
    spider_current(driver, collection)
    # 加载5次load more stories，每次20篇文章，更新时修改for函数参数就行
    time.sleep(20)
    for i in range(1, 5):
        print('第' + str(i) + '次加载！！！！')
        spider_load_more(driver, collection)
    # 从数据库读取链接，得到内容，自动对未存在内容的链接进行爬取
    # 考虑到网速等因素，可注释上一部分单独对数据库执行操作
    url_list = []
    for i in collection.find():
        if 'spidertime' not in i:
            sciencedaily_spider_page(i['url'], driver, collection)


def sciencedaily_spider_api():

    options = webdriver.ChromeOptions()
    # options.add_argument("-headless")
    driver = webdriver.Chrome(chrome_options=options)
    # firefox浏览器，暂时没有问题
    # driver = webdriver.Chrome()
    driver.implicitly_wait(100)
    driver.set_page_load_timeout(120)  # 3秒情况下在网速正常情况等下能正常加载
    url_current = 'https://www.sciencedaily.com/news/computers_math/'
    sciencedaily_spider(url_current, driver)
    driver.quit()


if __name__ == '__main__':
    sciencedaily_spider_api()
