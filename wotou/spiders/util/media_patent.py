# coding:utf-8
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.patent.patexplorer import patexplorer_search_word_api
from multiprocessing import Pool
from wotou.spiders.patent.sooip import get_patent_company
from wotou.spiders.patent.sooip_selenium import *
from wotou.spiders.patent.zgg_patent import get_patent
from wotou.spiders.patent.patenthub import patenthub_get_patent
from tqdm import tqdm

# SOURCE='ZGG'
# SOURCE='PATENTHUB'
# SOURCE='PATEXPLORER'

SOURCE='SOOIPTEST'
# 停用词
stop_word_list = []

def get_single_patent(i):

    if 'company' in i[0]:
        pass
    elif 'keyword' not in i[0]:
        pass
    else:
        company = []
        for word in i[0]['keyword']:
            flag = True
            if ' ' in word:
                for stop_word in stop_word_list:
                    if word.startswith(stop_word + ' ') or word.endswith(' ' + stop_word):
                        company.append([])
                        flag = False
                        break
            if flag:
                if SOURCE=='PATEXPLORER':
                    _, raw_company = patexplorer_search_word_api(word, True, 2)
                elif SOURCE=='PATENTHUB':
                    _, raw_company = patenthub_get_patent(word)
                elif SOURCE == "SOOIP":
                    _, raw_company = get_patent_company(word)
                elif SOURCE == 'SOOIPTEST':
                    raw_company = get_filter_singpatent(word)
                if raw_company:
                    company.append(list(set(raw_company)))
                    if ' ' not in word:
                        stop_word_list.append(word)
                elif raw_company is None:
                    if ' ' not in word:
                        stop_word_list.append(word)
                    company.append([])
                else:
                    company.append(raw_company)

        mongodb_connection('news')[i[1]].update({"_id": i[0]['_id']}, {'$set': {'company': company}})


def get_media_patent_api(media, start_date, end_date):
    '''
    从媒体关键词，通过专利找到相关公司,并保存在数据库中
    :param media: 媒体名称
    :param start_date: 开始时间
    :param end_date: 结束时间
    :return: 无
    '''
    dbname='crawling_'+media+'_'+start_date+'_'+end_date
    if dbname in mongodb_connection('news').list_collection_names():
        print('数据库表已存在')
    else:
        move_data(media, start_date, end_date)
    l=[[i,dbname] for i in mongodb_connection('news')[dbname].find({'company':{'$exists':0}})]
    for i in range(len(l)):
        get_single_patent(l[i])

    # pool=Pool(processes=32)
    # pool.map(get_single_patent,l)
    insert_company(media, start_date, end_date)
    insert_word(stop_word_list)


def insert_word(word_list):
    stop_words = mongodb_connection('news')['stopword']
    stop_word_list = [word['stopword'] for word in stop_words.find({})]

    for word in word_list:
        if word not in stop_word_list:
            stop_words.insert_one({'stopword': word})

def move_data(media,start_date,end_date):
    k=[]
    final=[]
    for i in mongodb_connection('news')[media].find({'company':{'$exists':0},'keyword':{'$exists':1},"$and": [{"time": {"$gte": start_date}}, {"time": {"$lte": end_date}}]}):
        k.extend(i['keyword'])
        # print(i['keyword'])
    keyword=list(set(k))
    for i in keyword:
        final.append({'keyword':[i]})
    mongodb_connection('news')['crawling_'+media+'_'+start_date+'_'+end_date].insert_many(final)


def insert_company(media,start_date,end_date):
    l=[i for i in mongodb_connection('news')[media].find({'company':{'$exists':0},'keyword':{'$exists':1},"$and": [{"time": {"$gte": start_date}}, {"time": {"$lte": end_date}}]})]
    insert_list=[i for i in mongodb_connection('news')['crawling_'+media+'_'+start_date+'_'+end_date].find() if i['keyword']!=None]
    for i in tqdm(l):
        try:
            company=[]
            keyword=i['keyword']
            for k in keyword:
                print(k)
                for j in insert_list:
                    if j['keyword'][0]==k:
                        company.extend(j['company'])
            mongodb_connection('news')[media].update({'_id':i['_id']},{'$set':{'company':company}})
            print('insert completed')
        except:
            continue
    mongodb_connection('news').drop_collection('crawling_'+media+'_'+start_date+'_'+end_date)



if __name__ == '__main__':

    # get_media_patent_api('weixin_geekheal_com', '2018-02-01', '2018-11-01')
    # get_media_patent_api('weixin_mit-tr', '2018-02-01', '2018-11-01')
    # get_media_patent_api('weixin_pharmacodia', '2018-05-01', '2018-06-01')
    # get_media_patent_api('weixin_wuxiapptecchina', '2018-02-01', '2018-11-01')
    # get_media_patent_api('shengwugu', '2018-05-01', '2018-08-01')
    # get_media_patent_api('weixin_almosthuman2014', '2018-05-01', '2018-06-01')
    # get_media_patent_api('weixin_ai_era', '2018-02-01', '2018-11-01')
    get_media_patent_api('shengwugu', '2018-11-13', '2018-11-13')