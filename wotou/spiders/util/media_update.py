# coding:utf-8
'''
一键更新所有媒体资讯
author：heyuxuan
'''

from wotou.spiders.medicine.spider_medicine import update_medicine_data_api
from wotou.spiders.clinical.spider_yaozh import update_yaozhi
from wotou.spiders.shengwugu_crawl.bioon import update_shengwugu_data_api
from wotou.spiders.wechat.spider_weixin import update_weixin_data_api
from wotou.spiders.util.media_pullword import pullword_all_media_api
from wotou.spiders.fiercebiotech.spider_fiercebiotech import biotech_spider_api
from wotou.spiders.ieee.spider_ieee import update_spectrum_ieee_api
from wotou.spiders.sciencedaily.spider_sciencedaily import sciencedaily_spider_api

def update_all_media_api():
    '''
    一键更新所有媒体信息，目前有临床批文，药物批文，生物谷，生物制药观察，acta文献，并将部分资讯分词
    :return:
    '''
    # update_weixin_data_api('丁香智汇')
    # update_weixin_data_api('药渡')
    # update_weixin_data_api('DeepTech深科技')
    # update_weixin_data_api('机器之心')
    # update_weixin_data_api('大数据文摘')
    # update_weixin_data_api('新智元')
    # update_weixin_data_api('药明康德')
    # update_weixin_data_api('能源杂志')
    # update_shengwugu_data_api()
    update_weixin_data_api('奇点网')
    update_yaozhi()
    update_medicine_data_api()
    try:
        biotech_spider_api(1)
    except Exception:
        pass
    update_spectrum_ieee_api()
    sciencedaily_spider_api()
    pullword_all_media_api()


if __name__ == '__main__':
    update_all_media_api()