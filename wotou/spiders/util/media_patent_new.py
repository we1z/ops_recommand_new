# coding:utf-8
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.patent.patexplorer import patexplorer_search_word_api
from multiprocessing import Pool
from wotou.spiders.patent.zgg_patent import get_patent

# SOURCE='ZGG'
SOURCE='PATEXPLORER'

def get_single_patent(i):

    # 如果里面已经有公司和或者没有关键字则不进行提取
    if 'company' in i[0]:
        pass
    elif 'keyword' not in i[0]:
        pass
    else:
        company = []
        stop_word_list = []
        # 遍历关键词
        for word in i[0]['keyword']:
            flag = True
            # 如果关键词里有空格，即关键词是排列组合得来的
            if ' ' in word:
                # 对排列组合得到的关键字进行停顿词判断
                for stop_word in stop_word_list:
                    if word.startswith(stop_word + ' ') or word.endswith(' ' + stop_word):
                        # 有停顿词开头或者结尾的，就将公司列添加空白列表
                        company.append([])
                        flag = False
                        break
            # 如果是关键词
            if flag:
                if SOURCE=='PATEXPLORER':
                    _, raw_company = patexplorer_search_word_api(word, True, 2)
                else:
                    _, raw_company = get_patent(word)
                if raw_company:
                    company.append(list(set(raw_company)))
                    if ' ' not in word:
                        stop_word_list.append(word)
                elif raw_company is None:
                    if ' ' not in word:
                        stop_word_list.append(word)
                    company.append([])
                else:
                    company.append(raw_company)

        mongodb_connection('news')[i[1]].update({"_id": i[0]['_id']}, {'$set': {'company': company}})


def get_media_patent_api(media, start_date, end_date):
    '''
    从媒体关键词，通过专利找到相关公司,并保存在数据库中
    :param media: 媒体名称
    :param start_date: 开始时间
    :param end_date: 结束时间
    :return: 无
    '''
    dbname='crawling_'+media+'_'+start_date+'_'+end_date
    if dbname in mongodb_connection('news').list_collection_names():
        print('数据库表已存在')
    else:
        move_data(media, start_date, end_date)
    l=[[i,dbname] for i in mongodb_connection('news')[dbname].find({'company':{'$exists':0}})]
    pool=Pool(processes=1)
    pool.map(get_single_patent,l)
    insert_company(media, start_date, end_date)


def move_data(media,start_date,end_date):
    k=[]
    final=[]
    for i in mongodb_connection('news')[media].find({'company':{'$exists':0},'keyword':{'$exists':1},"$and": [{"time": {"$gte": start_date}}, {"time": {"$lte": end_date}}]}):
        k.extend(i['keyword'])
        print(i['keyword'])
    keyword=list(set(k))
    for i in keyword:
        final.append({'keyword':[i]})

    mongodb_connection('news')['crawling_'+media+'_'+start_date+'_'+end_date].insert_many(final)


def insert_company(media,start_date,end_date):
    l=[i for i in mongodb_connection('news')[media].find({'company':{'$exists':0},"$and": [{"time": {"$gte": start_date}}, {"time": {"$lte": end_date}}]})]
    insert_list=[i for i in mongodb_connection('news')['crawling_'+media+'_'+start_date+'_'+end_date].find() if i['keyword']!=None]
    for i in l:
        company=[]
        keyword=i['keyword']
        for k in keyword:
            print(k)
            for j in insert_list:
                if j['keyword'][0]==k:
                    company.extend(j['company'])
        mongodb_connection('news')[media].update({'_id':i['_id']},{'$set':{'company':company}})
        print('insert completed')
    mongodb_connection('news').drop_collection('crawling_'+media+'_'+start_date+'_'+end_date)



if __name__ == '__main__':
    # get_media_patent_api('weixin_geekheal_com', '2018-02-01', '2018-11-01')
    # get_media_patent_api('weixin_mit-tr', '2018-02-01', '2018-11-01')
    # get_media_patent_api('weixin_pharmacodia', '2018-05-01', '2018-06-01')
    # get_media_patent_api('weixin_wuxiapptecchina', '2018-02-01', '2018-11-01')
    # get_media_patent_api('shengwugu', '2018-02-01', '2018-11-01')
    get_media_patent_api('zhihu', '2018-09-10','2018-09-19')
    # get_media_patent_api('weixin_ai_era', '2018-02-01', '2018-11-01')
    # get_media_patent_api('weixin_bigdatadigest', '2018-05-01', '2018-06-01')



