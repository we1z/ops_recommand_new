# coding:utf-8
'''
将部分媒体跟踪结果分词，作为语料库保存到数据库中
author：wangdi heyuxuan
'''
from wotou.lib.db_connection import mongodb_connection
from wotou.ML.ML_pullword import pullword
import datetime
import re


def filter_word(str):
    """
    过滤分词结果中无用的词汇
    :param str:
    :return:
    """
    str = " " + str.replace(" ", "  ") + " "  # 匹配所有纯数字，英文字母和日期
    regex = re.compile("(\s((\d+年)|(\d+月)|[1234567890.]|[a-zA-Z\-])+日?)*\s")
    str = regex.sub(" ", str).replace("   ", " ").replace("  ", " ")

    return str


def pullword_single_media_api(from_media, last_segmentation_date):
    '''
    将单个媒体的资讯进行分词，并将分词结果保存到数据库中
    :return:
    '''
    db_news = mongodb_connection('news')  # 从shengwugu数据库读
    collection_media = db_news[from_media]
    papers = collection_media.find({'spidertime': {'$gt': last_segmentation_date}}, {'_id': 0, 'content': 1})
    for paper in papers:
        if len(paper.get('content')) > 500:
            pull_word = pullword(paper.get('content'))
            pull_word = ' '.join(pull_word)
            pull_word = filter_word(pull_word)
            if mongodb_connection('corpus')['segmentation'].find_one({'word': pull_word}):
                print('这个分词已经添加过了')
            else:
                mongodb_connection('corpus')['segmentation'].insert(
                    {'from': from_media, 'word': pull_word, 'time': datetime.datetime.now()})
                print(pull_word)


def pullword_zhihu_media_api(from_media, last_segmentation_date):
    '''
    对知乎的文章进行爬取，并将分词结果保存到数据库中
    :return:
    '''
    db_news = mongodb_connection('news')
    collection_media = db_news[from_media]
    papers = collection_media.find({'爬取时间': {'$gt': last_segmentation_date}}, {'_id': 0, 'content': 1})
    for paper in papers:
        if len(paper.get('content')) > 500:
            pull_word = pullword(paper.get('content'))
            pull_word = ' '.join(pull_word)
            pull_word = filter_word(pull_word)
            if mongodb_connection('corpus')['segmentation'].find_one({'word': pull_word}):
                print('这个分词已经添加过了')
            else:
                mongodb_connection('corpus')['segmentation'].insert(
                    {'from': from_media, 'word': pull_word, 'time': datetime.datetime.now()})
                print(pull_word)


def pullword_all_media_api():
    '''
    将新爬下来的，需要分词的媒体信息进行分词并保存到数据库中
    目前需要分词的媒体为生物谷，生物制药观察，acta
    :return: 无
    '''
    db_corpus = mongodb_connection('corpus')  # 写入segmentation表中
    collection_segmentation = db_corpus['segmentation']
    last_sementation_time = collection_segmentation.aggregate(
        [{"$group": {'_id': '$from', 'max_value': {"$max": "$time"}}}])

    for time in last_sementation_time:

        if time['_id'] == 'shengwugu':
            pullword_single_media_api('shengwugu', time['max_value'])
        elif time['_id'] == 'weixin_ai_era':
            pullword_single_media_api('weixin_ai_era', time['max_value'])
        elif time['_id'] == 'weixin_mit-tr':
            pullword_single_media_api('weixin_mit-tr', time['max_value'])
        elif time['_id'] == 'weixin_bigdatadigest':
            pullword_single_media_api('weixin_bigdatadigest', time['max_value'])
        elif time['_id'] == 'weixin_almosthuman2014':
            pullword_single_media_api('weixin_almosthuman2014', time['max_value'])
        elif time['_id'] == 'weixin_pharmacodia':
            pullword_single_media_api('weixin_pharmacodia', time['max_value'])
        elif time['_id'] == 'weixin_wuxiapptecchina':
            pullword_single_media_api('weixin_wuxiapptecchina', time['max_value'])
        elif time['_id'] == 'weixin_geekheal_com':
            pullword_single_media_api('weixin_geekheal_com', time['max_value'])
        elif time['_id'] == 'weixin_dxywisdom':
            pullword_single_media_api('weixin_dxywisdom', time['max_value'])
        elif time['_id'] == 'weixin_energymagazine':
            pullword_single_media_api('weixin_energymagazine', time['max_value'])

        # 分词知乎
        elif time['_id'] == 'zhihu':
            pullword_zhihu_media_api('zhihu', time['max_value'])
if __name__ == '__main__':
    # pullword_all_media_api()
    # pullword_single_media_api('zhihu', datetime.datetime(2018,10,8))
    pullword_zhihu_media_api('zhihu','2018-10-09')
    # pullword_single_media_api('weixin_energymagazine', datetime.datetime(2017, 1, 1))
