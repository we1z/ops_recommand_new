import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import gc
import datetime
import math
from wotou.ML.ML_pullword import *
from wotou.lib.db_connection import *
from functools import wraps
from wotou.lib.error import *
import time
from wotou.lib.db_connection import mongodb_connection, mongodb_connection_local
from wotou.config.config import *
import jieba

# 专业语料在总语料中的占比 这个值的范围是：0-100
PROFESSIONAL_CORPUS_PERCENT = 40
corpus = mongodb_connection_local('corpus')
news_db = mongodb_connection_local('news')
cn_segmentation = []
en_segmentation = []


def last_time(func):
    """
    函数装饰器：计时器
    @:param func
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        res = func(*args, **kwargs)
        end_time = time.time()
        print(func.__name__, ':', end_time - start_time)
        return res

    return wrapper


@last_time
def tf_idf_cal(content_train_set):
    """
    计算tf_idf值
    :param :content_train_set :[list]:文档集
    :return ：词典以及每篇文章的tfidf值列表
    """
    vectorizer = CountVectorizer()  # 该类会将文本中的词语转换为词频矩阵，矩阵元素a[i][j] 表示j词在i类文本下的词频
    transformer = TfidfTransformer()  # 该类会统计每个词语的tf-idf权值
    # 第一个fit_transform是计算tf-idf，第二个fit_transform是将文本转为词频矩阵
    tfidf = transformer.fit_transform(vectorizer.fit_transform(content_train_set))
    word = vectorizer.get_feature_names()  # 获取词袋模型中的所有词语
    del vectorizer
    gc.collect()
    weight = tfidf.toarray()  # 将tf-idf矩阵抽取出来，元素a[i][j]表示j词在i类文本中的tf-idf权重

    del tfidf
    gc.collect()
    return word, weight, transformer.idf_


def getLengthOfDoc(doc_list):
    """
    计算一个文档集合中一篇文档的词数
    :param doc_list  [list]      :文档集合
    :return          [generator] :可遍历对象，其中每一个值对应每一篇文档的词数,为节省内存，这里用了生成器
    """
    for item in doc_list:
        yield len(item)


def source_of_media(name):
    """
    根据录入的文档集选择对应领域的语料库
    :param   name [str]:输入文档集的名称
    :return  professional_source [list]:对应领域语料库所在collection的名称集合
    """
    if name in TECHNOLOGY:
        professional_source = [{"from": "weixin_ai_era"}, {'from': 'weixin_mit-tr'}, {'from': 'weixin_almosthuman2014'},
                               {'from': 'weixin_bigdatadigest'}]
        return professional_source
    elif name in MEDICAL:
        professional_source = [{"from": "shengwugu"}, {'from': 'weixin_wuxiapptecchina'},
                               {'from': 'weixin_pharmacodia'},
                               {"from": "weixin_dxywisdom"}, {'from': 'weixin_geekheal_com'}]
        return professional_source
    else:
        pass


def filter_word(str):
    """
    过滤分词结果中无用的词汇
    :param [str]:一篇文档的分词结果
    :return [str]:分词结果
    """
    # 匹配所有纯数字，英文字母和日期
    str = " " + str.replace(" ", "  ") + " "
    regex = re.compile("(\s((\d+年)|(\d+月)|[1234567890.]|[a-zA-Z\-])+日?)*\s")
    return regex.sub(" ", str).replace("   ", " ").replace("  ", " ")


def _deal_with_en_professional_news(body):
    """
    处理专业领域的英语文章，去除标点符号，转化为小写，将'-'号转化为'zzuzz'（为了避免sklearn库在计算idf时将其分开）
    :param body[str]:文章内容
    :return    [str]: 处理完的文章内容
    """
    body = body.replace("-", "zzuzz")
    strinfo = re.compile('\W')
    segmentation = strinfo.sub(' ', body)
    strinfo = re.compile('\\s+')
    segmentation = strinfo.sub(' ', segmentation).lower()
    return segmentation


def get_media_data_by_date(media_name, datestr_str, dateend_str):
    """
    获取某段日期内的所有待处理新闻
    :param :media_name:[str]:待处理新闻库
    :param :datestr_str:[str]:起始日期
    :param :dateend_str:[str]:终止日期
    :return result[list]:该段时间内的媒体新闻的信息
    """
    result = []
    datestr = datetime.datetime.strptime(datestr_str, '%Y-%m-%d')
    dateend = datetime.datetime.strptime(dateend_str, '%Y-%m-%d')
    while (dateend - datestr).days > -1:
        one_day_data = get_data_from_db_by_date(media_name, datestr.strftime('%Y-%m-%d'))  # 现在数据库中查找该日数据
        result += one_day_data
        datestr = datestr + datetime.timedelta(days=1)
    return result


def get_data_from_db_by_date(media_name, datestr):
    """
    获取单日的媒体新闻信息
    :param media_name:媒体名称
    :param datestr: 日期
    :return: 当天的媒体新闻
    """
    collection = news_db[media_name]
    return [item for item in collection.find({'time': datestr})]


import numpy as np


def get_washed_words(sour):
    useless_word = []
    words_list = []
    res = tf_idf_cal(sour)
    tf_1 = []
    for i in range(len(res[1])):
        tf_line = []
        for j in range(len(res[0])):
            tf_line.append((res[1][i][j] / res[2][j]))
        tf_1.append(tf_line)
    tf_2 = np.array(tf_1)
    # var_tf=tf_2.var(axis=0)
    # mean_tf = tf_2.mean(axis=0)
    sum_tf = tf_2.sum(axis=0)

    # 需要剔除的词无非两种：一是常用词这种词的特征是词频非常高，idf非常小，综合两个特征，可以用tf/idf表示，越大则该词越普通
    #                      一是分错的词，这种词的特征表现为词频低，但是idf值非常高，可以用idf/tf衡量，越大则该词的分错概率越大
    #   对于这两个标准，分别设置阈值，超过阈值则剔除该词. 下面代码分别设置为总词数的0.0001和0.001
    #   错词与关键词性质相似，但是更加偏激，所以阈值应适当设置的高一些
    for i in range(len(res[0])):
        words_list.append((res[0][i], res[2][i] / sum_tf[i], sum_tf[i] / res[2][i]))

    words_list.sort(key=lambda x: x[2], reverse=True)
    for i in range(len(res[0]) // 1000):
        useless_word.append(words_list[i][0])

    words_list.sort(key=lambda x: x[1], reverse=True)
    for i in range(len(res[0]) // 5000):
        useless_word.append(words_list[i][0])

    return useless_word


@last_time
def get_name_string(content, name_string=''):
    """
    返回待处理新闻中可能包含的人物名称，
    :param :content :[str]文本内容
    :param :name_string :[str]:
    :return :name_string :[str]:人物名称
    """
    #     def timed_out(b, c):
    #         raise RegexTimeoutError()

    englist_sen_regex = '[^\u4e00-\u9fa5]{50,}'

    content = re.sub(englist_sen_regex, '', content, flags=re.S)

    re_string = '(([A-Za-z]+)(）| |》)?)*(教授|博士|公司|执行官|期刊|主任|杂志|研究所|所长|医师|研究员|科学家|团队|实习生|实验室|研究者|医生|作者|子刊|发表在|学家|管理局|委员会|研究生|院刊|主席|集团|主编)((（| |《)?([A-Za-z]+))*'
    name_list = re.finditer(re_string, content)
    #     signal.signal(signal.SIGALRM, timed_out)
    #     signal.setitimer(signal.ITIMER_REAL, 3.1, 0)

    have_deal_index = 0

    for i in name_list:
        name_string += " " + i.group() + " "
        # have_deal_index = i.regs[0][1]
    # except RegexTimeoutError:
    #     content = re.sub(timeout_re_string, " ", content[have_deal_index:], count=1)
    #     return get_name_string(content, name_string)

    # name_string = re.sub(
    #     '教授|CEO|博士|公司|执行官|期刊|主任|杂志|研究所|所长|医师|研究员|科学家|团队|实习生|实验室|研究者|医生|作者|子刊|发表在|学家|管理局|委员会|研究生|主席|院刊|主编|集团|《|》|（|）',
    #     ' ',
    #     name_string)
    name_string = name_string.lower()
    return name_string


def lower_str(str):
    """
    将大写英文字符转化为小写
    :param :str
    """
    return str.lower()


@last_time
def extracting_text(news, media_name):
    """
    抽取待处理新闻中的正文
    :param :news[dict]:待处理新闻
    :return :extracting_list[list]:处理后的新闻
    """
    if (len(news['content'])) < 500:
        return news
    if 'keyword' in news:
        return news
    # 去除文章无用部分
    if media_name == "shengwugu":
        news['content'] = news['content'][:news['content'].find("本文系生物谷原创编译整理，欢迎个人转发， 网站转载请注明来源")]
        news['content'] = news['content'][:news['content'].find("（生物谷Bioon.com）")]
        news['content'] = news['content'][:news['content'].find("（生物谷 Bioon.com）")]
        news['content'] = news['content'][:news['content'].find("原始出处：")]
        news['content'] = news['content'][:news['content'].find("参考资料：")]

    if media_name == "weixin_mit-tr":
        news['content'] = news['content'][:news['content'].find("-End-")]

    if media_name == 'weixin_ai_era':
        news['content'] = news['content'][:news['content'].find("*新智元整理报道")]
        news['content'] = news['content'][:news['content'].find("参考文献\n")]
        news['content'] = news['content'][:news['content'].find("【号外】")]

    if media_name == 'weixin_almosthuman2014':
        news['content'] = news['content'][:news['content'].find("转载请联系本公众号获得授权。")]

    if media_name == 'weixin_wuxiapptecchina':
        news['content'] = news['content'][:news['content'].find('参考资料：\n')]

    if media_name == 'weixin_geekpark':
        news['content'] = news['content'][:news['content'].find('头图来源：')]
        news['content'] = news['content'][:news['content'].find('责任编辑：')]
        news['content'] = news['content'][:news['content'].find('本文由极客公园原创')]
        news['content'] = news['content'][:news['content'].find('插图来源：')]
        news['content'] = news['content'][:news['content'].find('图片来源：')]
        news['content'] = news['content'][:news['content'].find('头图来自 ')]

    if media_name == 'weixin_pharmacodia':
        news['content'] = news['content'][:news['content'].find('参考：')]
        news['content'] = news['content'][:news['content'].find('参考资料：')]
        news['content'] = news['content'][:news['content'].find('参考文献')]
        news['content'] = news['content'][:news['content'].find('参考资料\n')]

    if media_name == 'weixin_wuxiapptecchina':
        news['content'] = news['content'][:news['content'].find('参考资料\n')]
        news['content'] = news['content'][:news['content'].find('参考：')]
        news['content'] = news['content'][news['content'].find('\n') + 1:]

    if media_name == 'weixin_bigdatadigest':
        is_second_time_big_line = False
        raw_content_list = news['content'].split('\n')
        for i in range(0, 10):
            if len(raw_content_list[i]) < 50:
                if not is_second_time_big_line:
                    raw_content_list[i] = ''
            else:
                if is_second_time_big_line:
                    break
                else:
                    is_second_time_big_line = True
        is_second_time_big_line = False
        for i in range(len(raw_content_list) - 1, len(raw_content_list) - 30, -1):
            if len(raw_content_list[i]) < 50:
                if not is_second_time_big_line:
                    raw_content_list[i] = ''
            else:
                if is_second_time_big_line:
                    break
                else:
                    is_second_time_big_line = True
        news['content'] = '\n'.join(raw_content_list)
    news['content'] = re.sub('http(s)?:\/\/((\w+).)+', "", news['content'])
    news['content'] = re.sub('www(\.\w+)*', "", news['content'])
    news['content'] = re.sub('(\w+\.)*com', "", news['content'])
    return news


@last_time
def get_cn_corpus(media_name, flag):
    """
    生成语料库
    :param :media_name :[str] 待提取关键字的文档集名称，根据这一名称来选择相应领域的训练语料
    :param :flag :[int] 选择专业语料和新闻语料的配比方式。值为1时，专业语料词数占总词数的30%，否则专业语料文档数占总文档数的30%
    :return           [list] ：中文语料库
    """
    # 生成专业语料库
    set_of_media = source_of_media(media_name)
    cn_corpus = corpus['segmentation']
    seg=[]
    # seg = [item['word'] for item in cn_corpus.find({'$or': set_of_media})]
    len_of_professional_seg = sum([i for i in getLengthOfDoc(seg)]) if flag == 1 else len(seg)

    new_segs = []
    len_of_news_seg = 0
    sp = 0
    while len_of_news_seg < 10000:
        new_seg = [item['word'] for item in cn_corpus.find({'$or': NEWS_SOURCE}).skip(sp).limit(50)]
        len_of_news_seg = sum([i for i in getLengthOfDoc(new_seg)]) + len_of_news_seg if flag == 1 else len(
            new_seg) + len_of_news_seg
        new_segs += new_seg
        sp = sp + 50
    seg = seg + new_segs
    return seg


@last_time
def get_en_corpus(media_name):
    """
    获取英文语料库
    :param :media_name: [str]：待提取关键字的文档集名称
    :return :[list]:英文语料库
    """

    en_professional_segmentation = []
    if media_name in MEDICAL:
        en_professional_segmentation = [_deal_with_en_professional_news(item['content']) for item in
                                        news_db["fiercebiotech"].find()]

        en_professional_segmentation.extend([_deal_with_en_professional_news(item['word']) for item in
                                             corpus["segmentation_english"].find().limit(
                                                 len(en_professional_segmentation))])
    if media_name in TECHNOLOGY:
        en_professional_segmentation = [_deal_with_en_professional_news(item['content']) for item in
                                        news_db["spectrum_ieee"].find().sort('spidertime')]

        en_professional_segmentation.extend([_deal_with_en_professional_news(item['content']) for item in
                                             news_db["sciencedaily"].find().sort('spidertime')])

        en_professional_segmentation.extend([_deal_with_en_professional_news(item['word']) for item in
                                             corpus["segmentation_english"].find().limit(
                                                 len(en_professional_segmentation))])
    # for i in range(len(en_professional_segmentation)):
    #     en_professional_segmentation[i] = filter_word(en_professional_segmentation[i])

    return en_professional_segmentation


def get_en_word(new):
    """
    获取待提取关键字新闻中的英文词汇
    :param new[dict]:新闻信息
    ：return en_list[str]:新闻中的英文词汇
    """
    en_regex = re.compile(r'[a-zA-Z][a-zA-Z\d][a-zA-Z\d]+')
    en_list = en_regex.findall(new['content'].replace("  ", " ").replace("-", "zzuzz"))
    en_list = list(map(lower_str, en_list))
    en_list = " ".join(en_list)
    return en_list


def lower_str(str):
    """
    转化为小写
    :param :str:
    """
    return str.lower()


def pull_word(doc):
    return pullword(doc)


def media_keyword_api(media_name, start_date, end_date):
    news_list = get_media_data_by_date(media_name, start_date, end_date)
    global cn_segmentation
    global en_segmentation
    if not news_list:
        return
    if not cn_segmentation:
        cn_segmentation = get_cn_corpus(media_name, 0)
    # if not en_segmentation:
    #     en_segmentation = get_en_corpus(media_name)
    # stop_word_list = [item['stopword'] for item in mongodb_connection_local('news')['stopword'].find()]
    # frequency_word_list = [item['词条'] for item in mongodb_connection_local('news')['word_frequency'].find()]

    content_train_list = []
    en_content_train_list = []
    name_string_list = []
    id_list = []

    for new in news_list:
        if (len(new['content'])) < 500:
            continue
        new = extracting_text(new, media_name)
        # try:
        #     name_string = get_name_string(new['content'])
        # except RecursionError:
        #     name_string = ''

        content_train_list.append(' '.join(jieba.cut(new['content'])))
        try:
            del cn_segmentation[cn_segmentation.index(content_train_list[0])]
        except ValueError:
            pass
        id_list.append(new['_id'])
        # name_string_list.append(name_string)
        # en_list = get_en_word(new)
        # en_content_train_list.append(en_list)

    # 计算语料的tf_idf值
    content_train_list.extend(cn_segmentation[:10000])
    # en_content_train_list.extend(en_segmentation)

    word, weight, idf_list = tf_idf_cal(content_train_list)
    # en_word, en_weight, en_idf_list = tf_idf_cal(en_content_train_list)
    # print(len(en_content_train_list))
    print(len(word))
    for m in range(len(id_list)):
        tf_idf_list = []
        news_word = ' ' + content_train_list[m]
        for i in range(len(word)):
            tf_idf_list.append((word[i], math.log(weight[m][i] / idf_list[i] * 2 + 1) * idf_list[i], idf_list[i],
                                weight[m][i] / idf_list[i]))
        # for j in range(len(en_word)):
        #     tf_idf_list.append((en_word[j], math.log(en_weight[m][j] / en_idf_list[j] * 2 + 1) * en_idf_list[j],
        #                         en_idf_list[j], en_weight[m][j] / en_idf_list[
        #                             j]))  # math.log(en_weight[m][j]/en_idf_list[j] + 1)*en_idf_list[j]))
        tf_idf_list.sort(key=lambda x: x[1], reverse=True)
        flagg = 0


        keywords = []
        keywords_list = []
        index = 0
        frequency_word_num = 0
        news = news_list[m]
        while len(keywords) < 10:
            keywords.append(tf_idf_list[index][0])
            index+=1
        print(keywords)


if __name__ == '__main__':
    media_keyword_api("zhihu", "2018-10-03", "2018-10-04")

