# -*- coding:utf-8 -*-#

'''
代理池
'''

import requests
from base64 import b64encode
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import zipfile
import re

# 动态版代理服务器
# proxyHost = "http-dyn.abuyun.com"
# proxyPort = "9020"

# 动态版代理隧道验证信息
# proxyUser = "H8991CC31242JS4D"
# proxyPass = "5E673A1C27BD06B3"
# proxyUser = "H2D01EA98081RRQD"
# proxyPass = "277AE6FFBAAB564C"

# # 专业版代理服务器
# proxyHost = "http-pro.abuyun.com"
# proxyPort = "9010"
#
# # 专业版代理隧道验证信息
# proxyUser = "H733N4Y53TCLOPIP"
# proxyPass = "B1179EFFBD239496"

# # socks5代理服务器
proxyHost = "http-pro.abuyun.com"
proxyPort = "9010"

# # 专业版代理隧道验证信息   H733N4Y53TCLOPIP:B1179EFFBD239496
proxyUser = "H733N4Y53TCLOPIP"
proxyPass = "B1179EFFBD239496"

service_args = [
    "--proxy-type=http",
    "--proxy=%(host)s:%(port)s" % {
        "host": proxyHost,
        "port": proxyPort,
    },
    "--proxy-auth=%(user)s:%(pass)s" % {
        "user": proxyUser,
        "pass": proxyPass,
    },
]

requests_proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
      "host" : proxyHost,
      "port" : proxyPort,
      "user" : proxyUser,
      "pass" : proxyPass,
    }

requests_proxies = {
        "http"  : requests_proxyMeta,
}

# firefox代理
def get_firefox_profile():
    proxy = {'host': proxyHost, 'port': proxyPort, 'usr': proxyUser, 'pwd': proxyPass}

    firefox_profile = webdriver.FirefoxProfile()

    firefox_profile.add_extension(
        '/home/hins/Documents/wotoudjango/wotu/spiders/util/close_proxy_authentication-1.1-sm+tb+fx.xpi')
    firefox_profile.set_preference('network.proxy.type', 1)
    firefox_profile.set_preference('network.proxy.http', proxy['host'])
    firefox_profile.set_preference('network.proxy.http_port', int(proxy['port']))

    firefox_profile.set_preference("network.proxy.ssl", proxy['host'])
    firefox_profile.set_preference("network.proxy.ssl_port", int(proxy['port']))
    # ... ssl, socks, ftp ...
    firefox_profile.set_preference('network.proxy.no_proxies_on', 'localhost, 127.0.0.1')

    credentials = '{usr}:{pwd}'.format(**proxy)
    credentials = b64encode(credentials.encode('ascii')).decode('utf-8')
    firefox_profile.set_preference('extensions.closeproxyauth.authtoken', credentials)

    return firefox_profile


# chrome代理
def get_chrome_option():
    manifest_json = """
{
    "version": "1.0.0",
    "manifest_version": 2,
    "name": "Chrome Proxy",
    "permissions": [
        "proxy",
        "tabs",
        "unlimitedStorage",
        "storage",
        "<all_urls>",
        "webRequest",
        "webRequestBlocking"
    ],
    "background": {
        "scripts": ["background.js"]
    },
    "minimum_chrome_version":"22.0.0"
}
"""

    background_js = """
var config = {
        mode: "fixed_servers",
        rules: {
          singleProxy: {
            scheme: "http",
            host: "%(host)s",
            port: parseInt(%(port)s)
          },
          bypassList: ["foobar.com"]
        }
      };

chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

function callbackFn(details) {
    return {
        authCredentials: {
            username: "%(user)s",
            password: "%(pass)s"
        }
    };
}

chrome.webRequest.onAuthRequired.addListener(
            callbackFn,
            {urls: ["<all_urls>"]},
            ['blocking']
);
""" % {
        "host": proxyHost,
        "port": proxyPort,
        "user": proxyUser,
        "pass": proxyPass,
    }

    pluginfile = 'proxy_auth_plugin.zip'

    with zipfile.ZipFile(pluginfile, 'w') as zp:
        zp.writestr("manifest.json", manifest_json)
        zp.writestr("background.js", background_js)

    chrome_option = Options()
    chrome_option.add_extension(pluginfile)

    return chrome_option

def zhima_proxy(method):
    """
    芝麻代理  链接需要去芝麻代理的网站复制
    acc：wootou
    paw：wootou1
    :return:
    """
    url = "http://webapi.http.zhimacangku.com/getip?num=1&type=1&pro=&city=0&yys=0&port=1&time=1&ts=0&ys=0&cs=0&lb=1&sb=0&pb=4&mr=1&regions="
    if method == 'selenium':
        r = requests.get(url)
        result = re.search(r"\d+[.]\d+[.]\d+[.]\d+[:]\d+", r.text)
        PROXY = result.group(0)
        return PROXY
    if method == 'requests':
        r = requests.get(url)
        result = re.search(r"\d+[.]\d+[.]\d+[.]\d+[:]\d+", r.text)
        print("代理:"+str(result))
        PROXY = result.group(0)

        list = PROXY.split(":")
        proxyHost = list[0]
        proxyPort = list[1]

        proxyMeta = "http://%(host)s:%(port)s" % {
            "host": proxyHost,
            "port": proxyPort,
        }

        # pip install -U requests[socks]  socks5代理
        # proxyMeta = "socks5://%(host)s:%(port)s" % {

        #     "host" : proxyHost,

        #     "port" : proxyPort,

        # }
        proxies = {
            "http": proxyMeta,
            "https": proxyMeta,
        }
        return proxies

def abuyun_proxy():
    """
    阿布云代理
    acc：wotou
    psw：756870
    """

    # proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
    proxyMeta = "socks5://%(user)s:%(pass)s@%(host)s:%(port)s" % {
        "host": proxyHost,
        "port": proxyPort,
        "user": proxyUser,
        "pass": proxyPass,
    }

    proxies = {
        # "http": proxyMeta,
        # "https": proxyMeta,
        'socks5':proxyMeta
    }
    return proxies


def wuyou_proxy():
    """
    无忧代理
    """
    order = "2d65cf792190c55c63806da206a6152e"
    apiUrl = "http://api.ip.data5u.com/dynamic/get.html?order=" + order
    res = requests.get(apiUrl).content.strip()
    proxies = {'http': 'http://%s' % res, 'https': 'http://%s' % res}
    return proxies


if __name__ == '__main__':
    proxy = zhima_proxy("selenium")
    print(proxy)



