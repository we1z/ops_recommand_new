# coding:utf-8
from wotou.spiders.patent.patexplorer import patexplorer_search_word_api
from wotou.lib.db_connection import mongodb_connection
import multiprocessing
from wotou.spiders.patent.sooip import get_patent_company
from wotou.spiders.patent.patenthub import patenthub_get_patent
from tqdm import tqdm

from wotou.spiders.patent.sooip_selenium import *
# 使用的专利爬取目标
SOURCE='SOOIP_SELENIUM'


def get_single_patent(i):

    if 'company' in i[0]:
        pass
    elif 'keyword' not in i[0]:
        pass
    else:
        company = []
        stop_word_list = []
        for word in i[0]['keyword']:
            flag = True
            if ' ' in word:
                for stop_word in stop_word_list:
                    if word.startswith(stop_word + ' ') or word.endswith(' ' + stop_word):
                        company.append([])
                        flag = False
                        break
            if flag:
                if SOURCE=='PATEXPLORER':
                    _, raw_company = patexplorer_search_word_api(word, True, 2)
                elif SOURCE=='PATENTHUB':
                    _, raw_company = patenthub_get_patent(word)
                elif SOURCE == "SOOIP":
                    _, raw_company = get_patent_company(word)
                elif SOURCE == "SOOIP_SELENIUM":
                    raw_company = get_filter_singpatent(word)
                if raw_company:
                    company.append(list(set(raw_company)))
                    if ' ' not in word:
                        stop_word_list.append(word)
                elif raw_company is None:
                    if ' ' not in word:
                        stop_word_list.append(word)
                    company.append([])
                else:
                    company.append(raw_company)

        mongodb_connection('news')[i[1]].update({"_id": i[0]['_id']}, {'$set': {'company': company}})


def get_magazine_patent_api(magazine_mark):
    '''
    从媒体关键词，通过专利找到相关公司,并保存在数据库中
    :param media: 媒体名称
    :param start_date: 开始时间
    :param end_date: 结束时间
    :return: 无
    '''
    dbname='crawling_'+magazine_mark
    if dbname in mongodb_connection('news').list_collection_names():
        print('数据库表已存在')
    else:
        move_data(magazine_mark)
    # 找到所有数据库中company为0标识的关键字
    l=[[i,dbname] for i in mongodb_connection('news')[dbname].find({'company':{'$exists':0}})]
    pool = multiprocessing.Pool(processes=32)
    pool.map(get_single_patent,l)
    insert_company(magazine_mark)

def move_data(magazine_mark):
    k=[]
    final=[]
    for i in mongodb_connection('news')['cnki'].find({'company':{'$exists':0},'文献标识': magazine_mark, 'keyword': {'$exists': 1}}):
        k.extend(i['keyword'])
    keyword=list(set(k))
    for i in keyword:
        print(i)
        final.append({'keyword':[i]})
    print(2)
    mongodb_connection('news')['crawling_'+magazine_mark].insert_many(final)


def insert_company(magazine_mark):
    l=[i for i in mongodb_connection('news')['cnki'].find({'文献标识': magazine_mark,'company':{'$exists':0},'keyword':{'$exists':1}})]
    insert_list=[i for i in mongodb_connection('news')['crawling_'+magazine_mark].find() if i['keyword']!=None]
    for i in tqdm(l):
        try:
            company=[]
            keyword=i['keyword']
            for k in keyword:
                # print(k)
                for j in insert_list:
                    if j['keyword'][0]==k:
                        company.extend(j['company'])
            mongodb_connection('news')['cnki'].update({'_id':i['_id']},{'$set':{'company':company}})
            print('insert completed')
        except:
            continue
    mongodb_connection('news').drop_collection('crawling_'+magazine_mark)



if __name__ == '__main__':
    get_magazine_patent_api('zhihu_2018-09-17_2018-09-19')