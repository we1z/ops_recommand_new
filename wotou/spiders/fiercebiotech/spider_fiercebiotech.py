# encoding:UTF-8

"""
从fiercebiotech中爬取生物医药新闻
"""

import urllib.request
from bs4 import BeautifulSoup
import datetime
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.util import proxies
import pymongo
import random
import time

articles_list = []
db = mongodb_connection('news')
collection = db['fiercebiotech']

PROXIES = proxies.abuyun_proxy()


def biotech_spider_api(i):
    """
    爬虫主体，将爬取内容输出到屏幕和数据库中
    :param i: 页码
    :return: 随机爬取下一页
    """
    opener = urllib.request.build_opener()

    urllib.request.install_opener(opener)
    articles_list = []
    url = "http://www.fiercebiotech.com/biotech?page=0%2C" + str(i)
    data = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(data, "lxml")
    print(i)
    for article in soup.find_all('div', class_="card horizontal views-row"):
        if article.time:
            # articles_date = article.time.string
            # articles_date = datetime.datetime.strptime(str(articles_date), "%b %d, %Y %M:%S%p")

            link = article.find('h2', class_="field-content list-title").find('a').get('href')
            articles_list.append(link)
    if random.randint(0, 2) == 1:
        articles_list.reverse()
    for link in articles_list:
        url = "http://www.fiercebiotech.com" + link
        title = link[link.find('/', 1) + 1:]
        try:
            page = urllib.request.urlopen(url).read()
        except urllib.error.HTTPError:
            biotech_spider_api(i + 1)
        soup = BeautifulSoup(page, "lxml")
        name = soup.find('h1', class_="page-title").text.strip()
        try:
            content = soup.find('div', property="schema:articleBody").text
        except Exception:
            continue
        origin_keyword_container = soup.find('div', class_="keywords-topics").find_all('a')
        origin_keyword = []
        for key in origin_keyword_container:
            origin_keyword.append(key.text)

        write_time = soup.find('time').get('datetime')[:10]

        spider_time = datetime.datetime.now().strftime("%Y-%m-%d")

        print({
            'time': write_time,
            "spidertime": spider_time,
            "content": content,
            "url": url,
            "name": name
        })

        try:
            collection.insert({
                'time': write_time,
                "spidertime": spider_time,
                "content": content,
                "url": url,
                "name": name
            })
        except pymongo.errors.DuplicateKeyError:
            return
    biotech_spider_api(i + 1)


if __name__ == '__main__':
    """
    爬取该网页的某一页内容
    """

    biotech_spider_api(1)
