import pymongo
from multiprocessing import Pool
import requests
from selenium import webdriver
import re
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.common.keys import Keys
from wotou.lib.db_connection import mongodb_connection_local

# 由于数据量较大，建议将数据库保存到本地再操作


def get_api(weibohao, ur, pd):
    # 输入新浪微博号，微博账号，微博密码
    # 输出微博号API的页数，数据库的名称，微博号的UID，
    options = Options()
    options.add_argument("--headless")
    browser = webdriver.Chrome(chrome_options=options)
    browser.get(
        'https://passport.weibo.cn/signin/login?entry=mweibo&res=wel&wm=3349&r=http%3A%2F%2Fm.weibo.cn%2Fsearchs')
    usr = browser.find_element_by_xpath('//input[@id="loginName"]')
    time.sleep(1)
    usr.send_keys(ur)
    psd = browser.find_element_by_xpath('//input[@id="loginPassword"]')
    time.sleep(1)
    psd.send_keys(pd)
    psd.send_keys(Keys.ENTER)
    print('登录')
    time.sleep(1)
    sh = browser.find_element_by_xpath('//div[@class="input-box"]//input')
    sh.send_keys(weibohao)
    sh.send_keys(Keys.ENTER)
    print('搜索')
    time.sleep(1)
    browser.find_element_by_xpath('//div[@class="card-main"]').click()
    time.sleep(1)
    url = browser.current_url
    uid = re.findall(re.compile('uid=(.*?)&luicode=10000011', re.S), url)[0]
    print('uid:', uid)
    api_url = 'https://m.weibo.cn/api/container/getIndex?uid=' + uid + '&luicode=10000011&lfid=100103type%3D1%26q%3D' + weibohao + '&featurecode=20000320&type=uid&value=' + uid + '&containerid=107603' + uid + '&page='
    browser.close()
    browser = webdriver.Chrome(chrome_options=options)
    browser.get('https://s.weibo.com/weibo/%s&Refer=index' % weibohao)
    num = \
    browser.find_elements_by_xpath('//a[@suda-data="key=tblog_search_weibo&value=direct_user_num_nologin:_nologin"]')[
        2].text
    num = (int(num) // 10) + 10
    databasename = browser.find_element_by_xpath(
        '//a[@suda-data="key=tblog_search_weibo&value=direct_user_url_nologin:_nologin"]').text
    databasename = 'sina_' + re.findall(re.compile('com/(.*)', re.S), databasename)[0]
    browser.close()
    return [num, databasename, api_url]


def sina_spider(url):
    # 输入微博号API网址
    # 输出该网址下所有微博的信息并上传数据库
    time.sleep(2)
    try:
        r = requests.get(url)
        if r.status_code == 200:
            pass
        else:
            print('连接失败')
            return sina_spider(url)
        info = r.json()
        for i in info['data']['cards']:
            if 'scheme' in i:
                if mongodb_connection_local('news')[database_name].find({'scheme': i['scheme']}).count() == 0:
                    mongodb_connection_local('news')[database_name].insert(i)
                    print('\033[1;32m 录入\033[0m', i['scheme'])
                else:
                    pass
            else:
                pass
    except:
        print('未知错误')
        return sina_spider(url)


if __name__ == '__main__':
    para = get_api('199IT-互联网数据中心', '', '')
    print(para)
    database_name = para[1]
    # 生成所有微博API的网址列表
    API_list = [(para[-1] + str(i)) for i in range(para[0])]
    pool = Pool(processes=12)
    pool.map(sina_spider, API_list)
    # 财经网#
    # 新浪证券#
    # 第一财经周刊#
    # 凤凰网财经#
    # 第一财经#
    # 网易财经#
    # 澎湃新闻#
    # 每日经济新闻#
    # 21世纪经济报道#
    # 经济观察报#
    # 经济之声#
    # 中国经济网#
    # 经济参考报#
    # 中国经济周刊#
    # 证券时报网#
    # 中国证券报#
    # 上海证券报#
    # 新浪科技#
    # 凤凰网科技#
    # 每日医学资讯#
    # 医药手机报#
    # 法制晚报#
    # 观察者网#
    # 大公報-大公網#
    # 界面#
    # 新京报#
    # 蓝鲸TMT网#
    # 钛媒体APP#
    # 东方财富网#
    # 新财富杂志#
    # 环球市场播报
    # 财新网
    # 金融界网站
# 中国企业报
# 科学网
# 麻省理工科技评论
# 新浪健康
# 中国医药联盟
# 中国医学论坛报
# 朗信医药信息
# 199IT-互联网数据中心