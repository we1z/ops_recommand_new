from lxml import etree
from pymongo import MongoClient
import re
import time
from newspaper import Article
from multiprocessing import Pool
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import pymongo
import time
from wotou.lib.db_connection import mongodb_connection_local

# 由于数据量较大，建议将数据库保存到本地再操作


def database(db_name):
    return mongodb_connection_local('corpus')[db_name]


def get_weibo_url_and_text(one):
    '''
    解析出微博文本和来源url
    :param one:
    :return:
    '''
    try:

        if re.match('.*?全文</a>', one['mblog']['text']):
            r = requests.get(one['scheme'],
                             # proxies={'https': 'socks5://127.0.0.1:1080'}
                             )
            if r.status_code != 200:
                print(r.status_code)
                return
            r = r.text
            print(2)
            pattern = re.compile('"text": "(.*?),', re.S)
            dict = {'weibo_url': one['scheme'], 'weibo_text': re.findall(pattern, r)[0]}
            print(re.findall(pattern, r)[0])
        else:
            print(3)
            dict = {'weibo_url': one['scheme'], 'weibo_text': one['mblog']['text']}
        database('sina_12003030902').insert(dict)
        database('sina_12003030901').remove({'scheme': one['scheme']})
        time.sleep(2.5)
    except Exception as e:
        print(e)
        pass


def get_maybe_news_url(one):
    '''
    解析出带有链接的微博，删除不带链接的微博
    :param one:
    :return:
    '''
    try:
        # one = database('sina_thepapernewsapp').find()[num]
        text = one['weibo_text']
        html = etree.HTML(text)
        maybe = html.xpath('//a[@data-url]/@data-url')
        if len(maybe) == 0:
            database('sina_12003030902').remove({'weibo_url': one['weibo_url']})
            print('删除')
        else:
            L = [re.sub(r'"|\\', '', i) for i in maybe][0]
            print(L)
            weibotext = handle_weibotext(text)
            # dict = {'weibo_url': one['weibo_url'], 'weibo_text':weibotext, 'maybe_news_url': L}
            database('sina_12003030902').update_one(one, {
                '$set': {'weibo_url': one['weibo_url'], 'weibo_text': weibotext, 'maybe_news_url': L}})
            # database('sina_cbnweekly01').remove({'weibo_url': one['weibo_url']})
    except:
        pass


def handle_weibotext(text):
    '''
    去除微博正文中无用部分
    :param text:
    :return:
    '''
    text = re.sub('<a data-url.*?</a>', '', text)
    text = re.sub('<.*?>', '', text)
    return text


def get_maybe_news_text(one):
    '''
    通过原文链接获取文章正文内容
    :param one:
    :return:
    '''
    weibo_name = 'sina_120030309'

    database_in = database(weibo_name + '03')
    database_out = database(weibo_name + '02')
    paper = None
    try:
        # one = database('sina_cbnweekly02').find()[num]
        paper = Article(url=one['maybe_news_url'], language='zh', fetch_images=False, memoize_articles=False,
                        http_success_only=True
                        # ,proxies={'http': 'socks5://127.0.0.1:1080'}
                        )
        paper.download()

        paper.parse()
        if paper.title == '新浪财经客户端巨献-密语直播':
            database_out.delete_one({'weibo_url': one['weibo_url']})
            print("无效1")
        if len(paper.text) == 0:
            text = handle_3g163_news(one['maybe_news_url'])
            if len(text) == 0:
                print('抓不到', one['maybe_news_url'])

                pass
            elif text == "-1":
                database_out.delete_one({'weibo_url': one['weibo_url']})
                print("无效2")
            else:
                one.update({'maybe_news_text': text})
                database_in.insert(one)
                database_out.delete_one({'weibo_url': one['weibo_url']})
                print('headless录入', one['maybe_news_url'])
        else:
            text = paper.text
            one.update({'maybe_news_text': text})
            database_in.insert(one)
            database_out.delete_one({'weibo_url': one['weibo_url']})
            print('录入成功')
    except Exception as e:
        print(e, one['maybe_news_url'])
        if paper and paper.download_exception_msg and 'Error' in paper.download_exception_msg:
            database_out.delete_one({'weibo_url': one['weibo_url']})
        pass


browser = None


def handle_3g163_news(url):
    '''
    若正文链接无法解析，改用selenium处理
    :param url:
    :return:
    '''
    global browser

    if not browser:
        options = Options()
        options.add_argument("--headless")
        prefs = {"profile.managed_default_content_settings.images": 2}
        options.add_experimental_option("prefs", prefs)

        browser = webdriver.Chrome(chrome_options=options)
        browser.set_page_load_timeout(10)
    browser.get(url)
    time.sleep(1)
    pageSource = browser.page_source

    if 'miaopai' in browser.current_url or 'special_lives' in browser.current_url or browser.current_url == 'http://www.sina.com.cn/' or browser.current_url == 'http://blog.sina.com.cn/' or 'http://photo.weibo.com' in browser.current_url:
        print(browser.current_url)
        return "-1"
    if '404' == browser.title or '页面没有找到' == browser.title:
        print(browser.title)
        return '-1'

    article = Article(url=url, language='zh', fetch_images=False, memoize_articles=False, http_success_only=True)
    article.download(input_html=pageSource)
    article.parse()
    return article.text


if __name__ == '__main__':
    pool = Pool(processes=15)
    pool.map(get_weibo_url_and_text, database('sina_12003030901').find())
    pool.map(get_maybe_news_url, database('sina_12003030902').find({'maybe_news_url': {'$exists': 0}}))
    pool.map(get_maybe_news_text, database('sina_12003030902').find({'maybe_news_url': {'$exists': 1}}))
