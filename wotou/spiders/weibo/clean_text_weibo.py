from wotou.lib.db_connection import mongodb_connection_local
import re
import jieba

# 由于数据量较大，因此最后在本地数据库处理

col = mongodb_connection_local('corpus')['company_abstract']
abstract_col = mongodb_connection_local('corpus')['abstract']

for i in col.find():
    d = {'abstract': i['abstract'],
         'type': 'abstract',
         'content': i['content'],
         'from': 'company_abstract',
         'url': i['company'],
         'abstract_url': i['company']}
    abstract_col.insert(d)

for times in range(1):
    for i in abstract_col.find({'from':'company_abstract'}):
        weibo_text = i['abstract'].replace('\u200b', '')

        weibo_text = re.sub('^\s*#.*#\s*', '', weibo_text)
        weibo_text = re.sub('^\s*【.*】\s*', '', weibo_text)
        weibo_text = re.sub('^\s*#.*#\s*', '', weibo_text)
        weibo_text = re.sub('\s*#.*#\s*$', '', weibo_text)
        weibo_text = re.sub('\s*【.*】\s*$', '', weibo_text)
        weibo_text = re.sub('^\s*.*】\s*', '', weibo_text)
        weibo_text = re.sub('^\s*（.*）\s*', '', weibo_text)

        weibo_text = re.sub('[\u4e00-\u9fa5_a-zA-Z0-9]{0,10}(&gt;)+$', '', weibo_text)

        if '//@' in weibo_text or len(weibo_text) < 10 or '秒拍' in weibo_text or weibo_text.startswith(
                '回复@') or '我发起了一个投票' in weibo_text or '专题' in weibo_text[-10:] or '报名' in \
                weibo_text[-10:] or '直播' in weibo_text[-10:] or '课程' in weibo_text[-10:] or '视频' in weibo_text[-10:]\
                or '你也快来表态吧' in weibo_text or weibo_text.endswith('?') or weibo_text.endswith('？'):
            print(weibo_text)
            abstract_col.delete_one({'_id': i['_id']})
            continue

        weibo_text = re.sub('^\s*', '', weibo_text)
        weibo_text = re.sub('\s*$', '', weibo_text)
        weibo_text = re.sub('详见：$', '', weibo_text)
        weibo_text = re.sub('（.{1,20}）$', '', weibo_text)
        weibo_text = re.sub('\(.{1,20}\)$', '', weibo_text)
        weibo_text = re.sub('via.{1,20}$', '', weibo_text)
        weibo_text = re.sub('@.{1,15}$', '', weibo_text)

        weibo_text = re.sub('来自$', '', weibo_text)
        weibo_text = re.sub('链接：$', '', weibo_text)
        weibo_text = re.sub('详情$', '', weibo_text)
        weibo_text = re.sub('更多详情查看$', '', weibo_text)
        weibo_text = re.sub('更多内容$', '', weibo_text)
        weibo_text = re.sub('更多精彩内容$', '', weibo_text)
        weibo_text = re.sub('点击链接$', '', weibo_text)
        weibo_text = re.sub('全文$', '', weibo_text)
        weibo_text = re.sub('更多数据$', '', weibo_text)
        weibo_text = re.sub('&gt;&gt;&gt;.{0,10}$', '', weibo_text)
        weibo_text = re.sub('#', '', weibo_text)
        weibo_text = re.sub('&amp;', '&', weibo_text)
        weibo_text = re.sub('&lt;', '<', weibo_text)
        weibo_text = re.sub('&gt;', '>', weibo_text)
        weibo_text = re.sub('&quot;', '"', weibo_text)

        weibo_text=re.sub('\[.*?\]','',weibo_text)

        weibo_text = weibo_text.replace('１', '1').replace('２', '2').replace('３', '3').replace('４', '4').replace('５',
                                                                                                                '5') \
            .replace('６', '6').replace('７', '7').replace('８', '8').replace('９', '9').replace('０', '0').replace('％', '%')
        if weibo_text != i['abstract']:
            abstract_col.update_one({'_id': i['_id']}, {'$set': {'abstract': weibo_text}})
            abstract_col.update_one({'_id': i['_id']}, {
                '$unset': {'abstract_seg': '', 'content_seg': '', 'abstract_list': '', 'content_list': ''}})

            #
        content_text = i['content'].replace('\u200b', '')
        content_text = re.sub('^\s*', '', content_text)
        content_text = re.sub('\s*$', '', content_text)
        content_text = re.sub('^\n*', '', content_text)
        content_text = re.sub('\n*$', '', content_text)
        content_text = re.sub('\n\n', '\n', content_text)
        content_text = re.sub('\n\n', '\n', content_text)
        content_text = re.sub('&amp;', '&', content_text)
        content_text = re.sub('&lt;', '<', content_text)
        content_text = re.sub('&gt;', '>', content_text)
        content_text = content_text.replace('１', '1').replace('２', '2').replace('３', '3').replace('４', '4').replace('５',
                                                                                                                    '5') \
            .replace('６', '6').replace('７', '7').replace('８', '8').replace('９', '9').replace('０', '0').replace('％', '%')
        if len(content_text) < 10 or len(content_text) <= len(weibo_text):
            print(content_text)
            abstract_col.delete_one({'_id': i['_id']})
            continue

        if content_text != i['content']:
            abstract_col.update_one({'_id': i['_id']}, {'$set': {'content': content_text}})
            abstract_col.update_one({'_id': i['_id']}, {
                '$unset': {'abstract_seg': '', 'content_seg': '', 'abstract_list': '', 'content_list': ''}})

for i in abstract_col.find({'content_seg': {'$exists': 0}}).skip(0):
    c = " ".join(jieba.cut(i['content']))
    c = re.sub('\d', '#', c)
    c = c.replace('\n', '<NL>')
    c = c.replace('   ', ' ').replace('  ', ' ').replace('  ', ' ')
    c = c.strip()
    while "<NL> <NL>" in c:
        c = c.replace("<NL> <NL>", "<NL>")
    while c.startswith("<NL> "):
        c = c[5:]
    # print(c)

    s = " ".join(jieba.cut(i['abstract']))
    s = re.sub('\d', '#', s)
    s = s.replace('\n', '<NL>')
    s = s.replace('   ', ' ').replace('  ', ' ').replace('  ', ' ')
    s = s.strip()

    abstract_col.update_one({'_id': i["_id"]}, {'$set': {'content_seg': c, 'abstract_seg': s}})

for i in abstract_col.find({'abstract_list': {'$exists': 0}, 'content_seg': {'$exists': 1}}):
    abstract_list = i['abstract_seg'].split()
    content_list = i['content_seg'].split()
    abstract_col.update_one({'_id': i['_id']}, {'$set': {'abstract_list': abstract_list, 'content_list': content_list}})

abstract_col.update_many({'content_seg': {'$exists': 1}},{'$unset':{'content_seg':'','abstract_seg':''}})

import copy
#
for i in abstract_col.find({'abstract_list': {'$exists': 1}}).skip(1100000).limit(100000):
    tmp_list = copy.copy(i['abstract_list'])
    for j in i['content_list']:
        if j in tmp_list:
            tmp_list.remove(j)
    if len(i['abstract_list']) < len(tmp_list) * 1.5:
        print('###')
        print(i['abstract'])
        print(i['content'])
        abstract_col.delete_one({'_id': i['_id']})
