# coding:utf-8
'''
爬取微信公众号的爬虫
'''

from selenium import webdriver
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import urllib.parse
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from PIL import Image
from selenium.webdriver import DesiredCapabilities
import lxml.html
from wotou.lib.db_connection import *
import datetime
from pymongo.errors import AutoReconnect
from wotou.spiders.util.identify_code import handle_identify_code_api
from newspaper import Article
import platform


# url='https://mp.weixin.qq.com/profile?src=3&timestamp=1495698067&ver=1&signature=awlEuYd6RNvE29b8NZo7sKoi4aJjafIFA3f4Q5w3qPC8SHbbBUGmCiRdUGlQjMGiO8GrwRh*xt79OwBAH6iBCQ=='
caps = DesiredCapabilities.PHANTOMJS
caps["phantomjs.page.settings.userAgent"] = \
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"

driver = webdriver.Chrome(desired_capabilities=caps)
db = mongodb_connection('news')
name_of_gongzonghao = ''


def get_text(elem):
    """提取一个元素的正文内容"""
    rc = []
    for node in elem.itertext():
        rc.append(node.strip())
    return ''.join(rc)


def get_one_page_content(href, times=0):
    """得到一片文章的正文，需要注意的是，要打开一个新的窗口再获取页面，最后要将页面关闭"""
    driver.execute_script('window.open()')

    driver.switch_to_window(driver.window_handles[1])
    driver.get(href)
    wait = WebDriverWait(driver, 10)
    # 首先判断是否是分享文章，如果是，得到连接，抓取来源处文章
    if 'js_share_source' in driver.page_source:
        href = wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="js_share_source"]'))).get_attribute(
            'href')
        driver.execute_script("window.close()")
        driver.switch_to_window(driver.window_handles[0])
        return get_one_page_content(href)
    # 如果不是分享文章，直接提取正文
    else:
        text = ''
        try:
            # wait.until(EC.presence_of_element_located((By.XPATH, '//*[@id="js_content"]')))
            # r = lxml.html.document_fromstring(driver.page_source).xpath('//*[@id="js_content"]')[0]
            # items = r.xpath('//p/span')
            #
            # for item in items[2:]:
            #     itemtext = get_text(item).strip()
            #     if ((not itemtext.startswith('参考资料')) and (not itemtext.startswith('原文出处'))
            #         and (not itemtext.startswith('本文系生物谷原创编译整理')) and (not itemtext.startswith('原始出处'))):
            #         text += (itemtext + '\n')
            paper = Article(url=driver.current_url, language='zh')
            paper.download(input_html=driver.page_source)
            paper.parse()
            text=paper.text
            print(text)
        # 超时重抓
        except TimeoutException:
            print('超时，重新抓取')
            times += 1
            if times < 5:
                get_one_page_content(href, times)
            else:
                print('重新抓取次数超过限制，抓取失败')
        # 没有找到文章正文
        except NoSuchElementException:
            print('未找到文章内容，抓取失败')
        # 最后关闭文章窗口
        finally:
            driver.execute_script("window.close()")
            driver.switch_to_window(driver.window_handles[0])
            return text


def search_mongodb(title, times=0):
    """判断文章是否已经存在于mongodb中"""
    try:
        db = mongodb_connection('news')
        ret = db['weixin_' + name_of_gongzonghao].find_one({'name': title})
        if not ret:
            return True
        else:
            return False
    except AutoReconnect:
        times += 1
        if times <= 5:
            print('连接错误，正在尝试重新连接mongodb')
            search_mongodb(title, times)
        else:
            print('mongodb连接失败')
            return False


def insert_mongodb(item, times=0):
    """文章插入mongodb中"""
    try:
        db = mongodb_connection('news')
        db['weixin_' + name_of_gongzonghao].insert(item)
    except AutoReconnect:
        times += 1
        if times <= 5:
            print('连接错误，正在尝试重新连接mongodb')
            insert_mongodb(item, times)
        else:
            print('mongodb连接失败')


def handle_identify_code():
    if platform.system() == 'Windows':
        img = driver.find_element_by_id("verifycode")
    else:
        img = driver.find_element_by_id("verify_img")

    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=3004)
    input=driver.find_element_by_id("input")
    input.send_keys(result)
    driver.find_element_by_id("bt").click()


def handle_sougou_identify_code():

    img = driver.find_element_by_id("seccodeImage")

    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=1006)
    input=driver.find_element_by_id("seccodeInput")
    input.send_keys(result)
    driver.find_element_by_id("submit").click()

def search_mainpage(url, times=0):
    """函数入口，得到文章列表"""
    driver.get(url)

    wait = WebDriverWait(driver, 10)
    try:
        try:
            items = wait.until(EC.presence_of_all_elements_located((By.XPATH, '//*[@id="history"]/div')))
        except TimeoutException:
            if driver.title=='请输入验证码':
                handle_identify_code()
                return search_mainpage(url)
            else:
                print("出错，找不到控件")
                return
        # items = dom.xpath('//*[@id="history"]/div')
        for item in items:
            subitems = item.find_elements_by_xpath('./div[2]/div')
            for subitem in subitems:
                href = subitem.find_element_by_xpath('./div[1]/h4').get_attribute('hrefs')
                if '://' not in href:
                    href = 'https://mp.weixin.qq.com' + href
                time = subitem.find_element_by_xpath('./div[1]/p[2]').text
                time = datetime.datetime(int(time[:time.find('年')]), int(time[time.find('年') + 1:time.find('月')]),
                                         int(time[time.find('月') + 1:time.find('日')])).strftime('%Y-%m-%d')

                title = subitem.find_element_by_xpath('./div[1]/h4[last()]').get_attribute('innerText')
                # 如果数据库中没有找到对应的文章，表明是一篇新文章，执行插入
                if search_mongodb(title):
                    content = get_one_page_content(href).strip()
                    spidertime = datetime.datetime.now()
                    # 内容不为空则执行插入
                    if content != '':
                        insertitem = {'url': href, 'name': title, 'time': time, 'content': content,
                                      'spidertime': spidertime}
                        insert_mongodb(insertitem)

    # 超时处理，如果超过五次超时，则返回
    except TimeoutException:
        print('超时，重新抓取')
        times += 1
        if times < 5:
            search_mainpage(url, times)
        else:
            print('重新抓取次数超过限制，抓取失败')
            return ''
    # 找不到元素
    except NoSuchElementException:
        print('未找到相关文章，抓取失败')
        return ''


def update_weixin_data_api(query, times=0):
    '''
    爬取微信公众号需要先从搜狗搜索获取公众号链接
    :param query: 公众号名称
    :param times: 查询次数
    :return:
    '''
    global name_of_gongzonghao
    base_url = 'http://weixin.sogou.com/weixin?'
    params = {
        'type': 1,
        'query': query
    }
    url = base_url + urllib.parse.urlencode(params)
    driver.get(url)
    wait = WebDriverWait(driver, 10)
    try:
        href = wait.until(EC.presence_of_element_located(
            (By.XPATH, '//*[@id="sogou_vr_11002301_box_0"]/div/div[2]/p[1]/a'))).get_attribute('href')
        name_of_gongzonghao = driver.find_element_by_name("em_weixinhao").text.lower()
        search_mainpage(href)
    # 超时处理，如果超过五次超时，则返回
    except TimeoutException:
        print('超时，重新抓取')
        if '用户您好，您的访问过于频繁，为确认本次访问为正常用户行为，需要您协助验证' in driver.page_source:
            handle_sougou_identify_code()
            return update_weixin_data_api(query,times)
        times += 1
        if times < 5:
            update_weixin_data_api(query, times)
        else:
            print('重新抓取次数超过限制，抓取失败')
            return ''
    # 找不到元素
    except NoSuchElementException:
        print('未找到该公众号，抓取失败')
        return ''


if __name__ == '__main__':
    update_weixin_data_api('新智元')
    driver.close()