# coding:utf-8
"""
从spectrum.ieee.org中爬取新闻
"""
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.util import proxies
import requests
from lxml import html
import time
import pymongo


def modify_time(time_str):
    """
    时间处理函数
    :param time_list: 发表日期，格式为28 Jul 2017 | 22:49 GMT
    :return:符合格式的日期2017-07-28
    """
    month_dict = {"Jan": '01', "Feb": '02', "Mar": '03', "Apr": '04', "May": '05', "Jun": '06',
                  "Jul": '07', "Aug": '08', "Sep": '09', "Oct": '10', "Nov": '11', "Dec": '12'}
    l = time_str.find('|')
    time_list = time_str[:l].split(' ')
    month = month_dict[time_list[1]]
    time_list[1] = month
    # 对日期小于10的进行处理，例如：由7变为07
    if len(time_list[0]) == 1:
        time_list[0] = '0' + time_list[0]
    time_str = time_list[2] + '-' + time_list[1] + '-' + time_list[0]
    return time_str


def page_spider(new_id, url):
    """
    爬取新闻页面信息
    :param new_id:新闻的id号，用做xpath查找
    :param url: 新闻的url
    :return:
    """
    # 远程数据库表
    collection = mongodb_connection('news')['spectrum_ieee']
    # 本地数据库表
    # collection = mongodb_connection_local()
    # 判断新闻url是否在数据库中，如果存在则不进行爬取
    if collection.find_one({'url': url}):
        print('此页信息已存在，不再存入！\n')
    else:
        try:
            r = requests.get(url=url, timeout=10)
        except requests.exceptions.ReadTimeout:
            print('网络链接错误ReadTimeout,重启中！！')
            return page_spider(new_id, url)
        except requests.exceptions.ConnectionError:
            print('网络链接错误ConnectionError,重启中！！')
            return page_spider(new_id, url)
        web_content = html.fromstring(r.text)
        # 爬虫时间：格式为YY-MM-DD
        spidertime = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        # 内容,多种路径格式，但仍存在爬不到的情况
        content = ''.join(web_content.xpath('.//*[@id=%s]/div[2]/p/text()' % new_id))
        if not content:
            content = ''.join(web_content.xpath('.//*[@id=%s]/div[2]/div/div[1]/p/text()' % new_id))
        elif not content:
            content = ''.join(web_content.xpath('.//*[@id=%s]/div[2]/div/div[1]/div/p/text()' % new_id))
        elif not content:
            content = ''.join(web_content.xpath('.//*[@id=%s]/div[2]/div/div[2]/p/text()' % new_id))
        elif not content:
            content = ''.join(web_content.xpath('.//*[@id=%s]/div[2]/div[3]/p/text()' % new_id))
        # 新闻的标题
        title = ''.join(web_content.xpath('.//*[@id=%s]/h1/text()' % new_id))
        # 发布时间，两种路径格式
        publish_time = ''.join(web_content.xpath('.//*[@id=%s]/div[1]/div/div[1]/div/label/text()' % new_id))
        if not publish_time:
            publish_time = ''.join(web_content.xpath('.//*[@id=%s]/div[1]/div[1]/div/label/text()' % new_id))
        # 判断新闻内容是否为空,为空就不录入
        if content:
            print('论文标题：', title)
            print('论文内容:', content)
            # 有新闻存在没有发表时间的情况
            if publish_time:
                publish_time = modify_time(publish_time)
                print('发表时间：', publish_time)
            print('论文爬取时间：', spidertime)
            print('文章链接：', url)
            print('\n')
            ##写入数据库
            collection.insert({'name': title, 'content': content,
                               'spidertime': spidertime, 'time': publish_time, 'url': url})
        else:
            print(url)


def spectrum_ieee_spider(url):
    """
    爬取页面，大概每页有30条新闻
    :param url: 主页的url
    :return:
    """
    try:
        r = requests.get(url=url, timeout=10)
    except requests.exceptions.ReadTimeout:
        print('网络链接错误ReadTimeout,重启中！！')
        return spectrum_ieee_spider(url)
    except requests.exceptions.ConnectionError:
        print('网络链接错误ConnectionError,重启中！！')
        return spectrum_ieee_spider(url)
    web_content = html.fromstring(r.text)
    print('网页信息：', web_content)
    artiles = web_content.xpath(".//article")
    news_url_dict = {}
    for i in range(1, len(artiles) + 1):
        ##新闻的标题
        title = web_content.xpath(".//article[%d]/a/h3/text()" % i)
        # 新闻的id号
        artile_id = web_content.xpath(".//article[%d]/@id" % i)
        artile_id = ''.join(artile_id)
        # 新闻的链接
        href = web_content.xpath(".//article[%d]/a/@href" % i)
        href = 'http://spectrum.ieee.org/' + ''.join(href)
        if href != 'http://spectrum.ieee.org/':
            print('新闻链接：', href)
            print('新闻标题：', ''.join(title))
            print('新闻的id号：', artile_id)
            print('\n')
            news_url_dict[artile_id] = href
    print(news_url_dict)
    for new_id in news_url_dict:
        page_spider(new_id, news_url_dict[new_id])


def update_spectrum_ieee_api():
    """
    爬虫接口
    :return:
    """
    # 爬取初始页面链接
    url_current = 'http://spectrum.ieee.org/'
    spectrum_ieee_spider(url_current)
    # 爬取后续页面链接，页面不是翻页而是在一页中额外加载
    for i in range(30, 60, 30):  # 1页30条状态，更新时修改中间值即可,现保存5页120
        url = 'http://spectrum.ieee.org/isotope/home/load-more?offset=' \
              + str(i) + '&isotopeFilter=.recent-item'
        print(url)
        spectrum_ieee_spider(url)


if __name__ == '__main__':
    update_spectrum_ieee_api()
