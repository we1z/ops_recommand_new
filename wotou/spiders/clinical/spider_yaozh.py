from lxml import etree
from selenium import webdriver
from wotou.lib.db_connection import mongodb_connection_local, mongodb_connection
import datetime


def update_yaozhi():
    option = webdriver.ChromeOptions()
    # option.add_argument('--headless')
    # option.add_argument("--proxy-server=socks5://" + '114.226.135.180:1546')
    driver = webdriver.Chrome(chrome_options=option)
    for page in range(1, 3):
        url = 'https://db.yaozh.com/linchuangshiyan?p=%s' % page + '&pageSize=30'
        driver.get(url)
        html = etree.HTML(driver.page_source)
        djh = html.xpath('//table[@class="table table-striped responsivetable-clone"]//a[@class="cl-blue"]/text()')
        num = len(djh)
        for i in range(1, num + 1):
            d = {}
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/th/a/text()' % i):
                d['登记号'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/th/a/text()' % i)[0]
            else:
                print('错误')
                break
                return None
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[1]/text()' % i):
                d['实验题目'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[1]/text()' % i)[0]
            else:
                d['实验题目'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[1]/span/text()' % i)[
                    0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[2]/text()' % i):
                d['药物名称'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[2]/text()' % i)[0]
            else:
                d['药物名称'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[2]/span/text()' % i)[
                    0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[3]/text()' % i):
                d['适应症'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[3]/text()' % i)[0]
            else:
                d['适应症'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[3]/span/text()' % i)[0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[4]/text()' % i):
                d['试验状态'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[4]/text()' % i)[0]
            else:
                d['试验状态'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[4]/span/text()' % i)[
                    0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[5]/text()' % i):
                d['试验分期'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[5]/text()' % i)[0]
            else:
                d['试验分期'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[5]/span/text()' % i)[
                    0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[6]/text()' % i):
                d['申办单位'] = [cname.strip() for cname in
                             html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[6]/text()' % i)[
                                 0].split('/') if len(cname) > 3]
            else:
                d['申办单位'] = [cname.strip() for cname in
                             html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[6]/span/text()' % i)[
                                 0].split('/') if len(cname) > 3]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[7]/text()' % i):
                d['试验机构'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[7]/text()' % i)[0]
            else:
                d['试验机构'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[7]/span/text()' % i)[
                    0]
            if html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[8]/text()' % i):
                d['登记时间'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[8]/text()' % i)[0]
            else:
                d['登记时间'] = html.xpath('/html/body/div[5]/div[4]/div/div[2]/table/tbody/tr[%d]/td[8]/span/text()' % i)[
                    0]
            if mongodb_connection('news')['clinical_trial'].find({'登记号': d['登记号'], '登记时间': d['登记时间']}).count() != 0:
                print('信息已存在')
            elif mongodb_connection('news')['clinical_trial'].find({'登记号': d['登记号']}).count() != 0:
                print('信息需要更新')
                mongodb_connection('news')['clinical_trial'].update({'登记号': d['登记号']}, {'$set': d})
                print(d)
            else:
                print('信息不存在')
                mongodb_connection('news')['clinical_trial'].insert(d)
                print(d)


def clinical_trial_filter_api(datestr, dateend, is_update=False):
    """
    输入：起始日期，终止日期
    输出：期间有临床试验申报的
         [中国企业列表，符合条件企业列表]
    """
    update_item_list = get_clinical_data_by_date(datestr, dateend)  # 实际用
    # update_item_list = _one_page_data(datestr, 1)   # 单页测试
    wanted_company_list = []
    apply_company_set = name_filter(update_item_list)
    for com in apply_company_set:
        if '，' in com:
            wanted_company_list.extend(com.split('，'))
        if '、' in com:
            wanted_company_list.extend(com.split('、'))
        else:
            wanted_company_list.append(com)

    return [i for i in wanted_company_list if '公司' in i]


def get_clinical_data_by_date(datestr_str, dateend_str):
    """
    输入：起始日期，终止日期
    输出：该段时间内的临床试验条目信息
    """
    result = []
    datestr = datetime.datetime.strptime(datestr_str, '%Y-%m-%d')
    dateend = datetime.datetime.strptime(dateend_str, '%Y-%m-%d')
    while (dateend - datestr).days > -1:
        one_day_data = get_data_from_db_by_date(datestr.strftime('%Y-%m-%d'))  # 现在数据库中查找该日数据
        result += one_day_data
        datestr = datestr + datetime.timedelta(days=1)
    return result


def get_data_from_db_by_date(datestr):
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    return [item for item in collection.find({'登记时间': datestr})]


def name_filter(item_list):
    '''
    输入：临床试验条目列表
    输出：不包含stop_word的中国公司
    '''
    # blacklist_name_list = [item['stop_word'] for item in mongodb_connection('company')['blacklist_clinical_trial'].find()]
    result_set = set([])
    for item in item_list:
        for cname in item['申办单位']:
            flag = True
            # for blacklist_name in blacklist_name_list:
            #     if blacklist_name in cname:
            #         flag = False
            #         break
            if not check_contain_chinese(cname):
                flag = False
            if flag:
                result_set.add(cname)
    return result_set


def check_contain_chinese(check_str):
    """
    判断字符串是否包含中文
    """
    for ch in check_str:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
    return False


if __name__ == '__main__':
    update_yaozhi()
