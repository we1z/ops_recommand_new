# -*- coding: utf-8 -*-
"""
author: cifer_zhang
date: 2017.5.6
临床试验追踪
数据源：药智网
https://db.yaozh.com/linchuangshiyan/

date:2017.5.12
增加代理
"""
import socket
import datetime
import time
import urllib.parse
import requests
from lxml import html

from wotou.spiders.util import proxies
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.company.spider_company import search_company_api


PROXIES = proxies.abuyun_proxy()



def get_clinical_data_from_db(id):
    '''
    输入：临床试验注册号
    若公司存在于数据库，输出条目
    不存在输出None
    '''
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    ret = collection.find_one({'注册号': id})
    if not ret:
        return None
    return ret


def write_updated_item2mongo(item):
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    item['爬取时间'] = [datetime.datetime.now()]
    ret = collection.find_one({'注册号': item['注册号']})
    if not ret:
        return None
    if item['试验状态'] not in ret['试验状态'] or item['试验分期'] not in ret['试验分期']:
        collection.update({'注册号': item['注册号']}, {'$set': {'试验状态': ret['试验状态'] + item['试验状态'],
                                                          '试验分期': ret['试验分期'] + item['试验分期'],
                                                          '登记时间': ret['登记时间'] + item['登记时间'],
                                                          '爬取时间': ret['爬取时间'] + item['爬取时间']}})


def write2mongo(item):
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    item['爬取时间'] = [datetime.datetime.now()]
    collection.insert(item)


def update_clinical_data_api():
    """
    后台更新：上次爬至今该段时间内的临床试验条目信息
    """
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    date_time = [item['登记时间'][-1] for item in collection.find()]
    latest_date_in_db = datetime.datetime.strptime(sorted(date_time)[-1], '%Y-%m-%d')
    today = datetime.datetime.now()
    # result = get_clinical_data_by_date(latest_date_in_db.strftime('%Y-%m-%d'), today)
    result = []
    while (today - latest_date_in_db).days > -1:
        result.extend(_one_day_clinical_data(latest_date_in_db, 0))
        latest_date_in_db = latest_date_in_db + datetime.timedelta(days=1)
    return result


def get_data_from_db_by_date(datestr):
    db = mongodb_connection('news')
    collection = db['clinical_trial']
    return [item for item in collection.find({'登记时间': datestr})]


def get_clinical_data_by_date(datestr_str, dateend_str):
    """
    输入：起始日期，终止日期
    输出：该段时间内的临床试验条目信息
    """
    result = []
    datestr = datetime.datetime.strptime(datestr_str, '%Y-%m-%d')
    dateend = datetime.datetime.strptime(dateend_str, '%Y-%m-%d')
    while (dateend - datestr).days > -1:
        one_day_data = get_data_from_db_by_date(datestr.strftime('%Y-%m-%d'))  # 现在数据库中查找该日数据
        result += one_day_data
        datestr = datestr + datetime.timedelta(days=1)
    return result


def _one_day_clinical_data(datestr, times=0):
    """
    输入：起始日期，终止日期（一次查询）
    输出：该段时间内的临床试验条目信息
    """
    print(datestr)
    base_url = 'https://db.yaozh.com/linchuangshiyan?'
    data = {'p': 1,
            'sort': '全部',
            'type': '全部',
            'pageSize': '30',
            'phase': '全部',
            'status': '全部',
            'datestr': datestr,
            'dateend': datestr, }
    url = base_url + urllib.parse.urlencode(data)
    try:
        response = requests.get(url)
        root = html.fromstring(response.content)
        total_nums_xpath = '//div[@data-widget="dbPagination"]/@data-total'
        if root.xpath('//div[@data-widget="dbNoData"]'):  # 无查询结果
            print('Blank Page')
            return []
        total_nums = int(root.xpath(total_nums_xpath)[0])  # 条目总数
        total_pages = int(total_nums / 30) + 1
        not_crawled_list = []
        for i in range(1, total_pages + 1):
            not_crawled_list += _one_page_data(datestr, i)
        return not_crawled_list
    except socket.gaierror as e:
        print("网络中断")
    except IndexError as e:
        print("页面信息错误")
    except Exception as e:
        if times < 3:
            times += 1
            return _one_day_clinical_data(datestr, times)
        else:
            return []


def _one_page_data(datestr, current_page, times=0):
    """
    输入：起始日期，终止日期，页数
    输出：该页内临床受审条目信息
    注册号 链接 试验题目 适应症 试验状态 试验分期 [申办单位，] 公司名称
    """
    print('page:', current_page)
    base_url = 'https://db.yaozh.com/linchuangshiyan?'
    data = {'p': current_page,
            'sort': '全部',
            'type': '全部',
            'pageSize': '30',
            'phase': '全部',
            'status': '全部',
            'datestr': datestr,
            'dateend': datestr, }
    url = base_url + urllib.parse.urlencode(data)
    try:
        response = requests.get(url)
        root = html.fromstring(response.content)
        tr_num = 1  # 行数
        items_list = []
        while True:
            if root.xpath('//tbody/tr[%d]/th/a/text()' % tr_num) == [] or tr_num > 30:  # 该页已爬完
                break
            id = root.xpath('//tbody/tr[%d]/th/a/text()' % tr_num)[0]
            link = 'http://db.yaozh.com' + root.xpath('//tbody/tr[%d]/th/a/@href' % tr_num)[0]
            # item = get_clinical_data_from_db(id)
            # if item:     # 跳过之前爬过的
            #     items_list.append(item)
            #     tr_num += 1   # 下一行
            #     continue
            detail_xpath = root.xpath('//tbody/tr[%d]' % tr_num)[0]
            item = {'注册号': id, '链接': link, '试验题目': detail_xpath.xpath('td[1]/text()')[0]}
            try:
                item['适应症'] = detail_xpath.xpath('td[3]')[0].xpath('string(.)')
            except Exception as e:
                item['适应症'] = ''
            try:
                item['试验状态'] = [detail_xpath.xpath('td[4]/text()')[0]]
            except Exception as e:
                item['试验状态'] = []
            try:
                item['试验分期'] = [detail_xpath.xpath('td[5]/text()')[0]]
            except Exception as e:
                item['试验分期'] = []
            try:
                item['申办单位'] = [cname.strip() for cname in detail_xpath.xpath('td[6]/text()')[0].split('/') if
                                len(cname) > 3]
            except Exception as e:
                item['申办单位'] = []
            try:
                item['登记时间'] = [detail_xpath.xpath('td[7]/text()')[0]]
            except Exception as e:
                item['登记时间'] = []
            # if not write_updated_item2mongo(item):  # 更新数据
            write2mongo(item)  # 存入数据库
            tr_num += 1  # 下一行
            items_list.append(item)
            print(item)
        return items_list
    except socket.gaierror as e:
        print("网络中断")
    except IndexError as e:
        print("页面信息错误")
    except Exception as e:
        if times < 3:
            times += 1
            return _one_day_clinical_data(datestr, times)
        else:
            return []


def name_filter(item_list):
    '''
    输入：临床试验条目列表
    输出：不包含stop_word的中国公司
    '''
    # blacklist_name_list = [item['stop_word'] for item in mongodb_connection('company')['blacklist_clinical_trial'].find()]
    result_set = set([])
    for item in item_list:
        for cname in item['申办单位']:
            flag = True
            # for blacklist_name in blacklist_name_list:
            #     if blacklist_name in cname:
            #         flag = False
            #         break
            if not check_contain_chinese(cname):
                flag = False
            if flag:
                result_set.add(cname)
    return result_set


def check_contain_chinese(check_str):
    """
    判断字符串是否包含中文
    """
    for ch in check_str:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
    return False


def in_blacklist(check_str, blacklist_name):
    """
    检查是否包含外国国名
    包含返回True
    """
    for bn in blacklist_name:
        if bn in check_str:
            return True
    return False


def clinical_trial_filter_api(datestr, dateend, is_update=False):
    """
    输入：起始日期，终止日期
    输出：期间有临床试验申报的
         [中国企业列表，符合条件企业列表]
    """
    update_item_list = get_clinical_data_by_date(datestr, dateend)  # 实际用
    # update_item_list = _one_page_data(datestr, 1)   # 单页测试
    wanted_company_list = []
    apply_company_set = name_filter(update_item_list)
    for com in apply_company_set:
        if '，' in com:
            wanted_company_list.extend(com.split('，'))
        if '、' in com:
            wanted_company_list.extend(com.split('、'))
        else:
            wanted_company_list.append(com)

    return [i for i in wanted_company_list if '公司' in i]


if __name__ == '__main__':
    print(update_clinical_data_api())
