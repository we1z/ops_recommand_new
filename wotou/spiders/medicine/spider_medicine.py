# -*- coding: utf-8 -*-

"""
爬取药物批文的爬虫
"""

import requests

from lxml import html
import pymongo
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.util import proxies
import datetime
import socket
from wotou.spiders.company.spider_company import search_company_api



def get_medicine_data_from_db(item):
    """
    输入：药品受理号
    若公司存在于数据库，输出条目
    不存在输出None
    """
    db = mongodb_connection('news')
    collection = db['medicine']
    ret = collection.find_one({'受理号': item['受理号']})
    if not ret:
        return None
    return ret


def write2mongo(item):
    db = mongodb_connection('news')
    collection = db['medicine']
    item['爬取时间'] = datetime.datetime.now()
    collection.insert(item)


def update_medicine_data_api():
    """
    后台更新：上次爬至今该段时间内的药品受审条目信息
    """
    db = mongodb_connection('news')
    collection = db['medicine']
    date_time = [item['承办日期'] for item in collection.find()]
    latest_date_in_db = datetime.datetime.strptime(sorted(date_time)[-1], '%Y-%m-%d')
    # latest_date_in_db= datetime.datetime(2016,1,1)
    today = datetime.datetime.now()
    result = []
    result.extend(get_update_accept_list(latest_date_in_db.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d'), 0))
    return result


def get_data_from_db_by_date(datestr):
    db = mongodb_connection('news')
    collection = db['medicine']
    return [item for item in collection.find({'承办日期': datestr})]


def get_update_accept_list(str_datestr, str_dateend, times=0):
    """
    输入：起始日期，终止日期
    输出：该段日期内药物受理条目
    """
    url = 'http://www.cde.org.cn/transparent.do?method=list'

    try:
        data = {'checktype': '1', 'pagetotal': '', 'statenow': '0', 'year': '2018', 'drugtype': '', 'applytype': '',
                'acceptid': '', 'drugname': '', 'company': '', 'currentPageNumber': '1', 'pageMaxNumber': '80',
                'totalPageCount': '', 'pageroffset': '', 'pageMaxNum': '80', 'pagenum': '1'}
        r = requests.post(url, data=data)
        root = html.fromstring(r.content)
        page_num_xpath = '//td[@id="pageNumber"]/font[2]/text()'  # 页码总数
        page_num = int(root.xpath(page_num_xpath)[0])
        page_total_xpath = '//span[@class="STYLE21"]/text()'  # 表单条目总数
        page_total = int(root.xpath(page_total_xpath)[0])
        not_crawled_list = []
        for i in range(1, page_num + 1):
            one_page_list = accept_list_one_page(page_num, page_total, i, str_datestr, str_dateend)
            not_crawled_list += one_page_list[0]
            if not one_page_list[1]:
                break
        return not_crawled_list
    except socket.gaierror as e:
        print("网络中断")
        if times < 3:
            times += 1
            return get_update_accept_list(str_datestr, str_dateend, times)
        else:
            return []
    except IndexError as e:
        print("页面信息错误")
        if times < 3:
            times += 1
            return get_update_accept_list(str_datestr, str_dateend, times)
        else:
            return []
    except Exception as e:
        print("未知错误")
        if times < 3:
            times += 1
            return get_update_accept_list(str_datestr, str_dateend, times)
        else:
            return []


def accept_list_one_page(page_num, page_total, current_page, str_datestr, str_dateend, times=0):
    """
    输入：总条目数，总页数，当前页，起始日期，终止日期
    输出：受理号，药品名称，药品类型， 申请类型，注册分类，企业名称，承办日期
    """
    data = {'checktype': '1', 'pagetotal': page_total, 'statenow': '0', 'year': '2018', 'drugtype': '', 'applytype': '',
            'acceptid': '',
            'drugname': '', 'company': '', 'currentPageNumber': current_page, 'pageMaxNumber': '80',
            'totalPageCount': page_num, 'pageroffset': '20',
            'pageMaxNum': '80', 'pagenum': current_page}
    url = 'http://www.cde.org.cn/transparent.do?method=list'
    try:
        response = requests.post(url, data=data)
        root = html.fromstring(response.content)
        item_num = 1
        items_list = []
        while (True):
            item = {}
            if not root.xpath('//tr[@height="30"][%d]' % item_num) or item_num > 80:
                break
            detail_xpath = root.xpath('//tr[@height="30"][%d]' % item_num)[0]
            try:
                item['受理号'] = detail_xpath.xpath('td[1]/text()')[0].strip()
            except Exception as e:
                item['受理号'] = ''
            try:
                item['药品名称'] = detail_xpath.xpath('td[2]/text()')[0].strip()
            except Exception as e:
                item['药品名称'] = ''
            try:
                item['药品类型'] = detail_xpath.xpath('td[3]/text()')[0].strip()
            except Exception as e:
                item['药品类型'] = ''
            try:
                item['申请类型'] = detail_xpath.xpath('td[4]/text()')[0].strip()
            except Exception as e:
                item['申请类型'] = ''
            try:
                item['注册分类'] = detail_xpath.xpath('td[5]/text()')[0].strip()
            except Exception as e:
                item['注册分类'] = ''
            try:
                item['企业名称'] = [cname.strip() for cname in detail_xpath.xpath('td[6]/text()')[0].split() if
                                check_contain_chinese(cname)]
            except Exception as e:
                item['企业名称'] = []
            try:
                item['承办日期'] = detail_xpath.xpath('td[7]/text()')[0].strip()
            except Exception as e:
                item['承办日期'] = ''
            print(item)
            if not get_medicine_data_from_db(item):
                write2mongo(item)
            item_num += 1
            item_date = datetime.datetime.strptime(item['承办日期'], '%Y-%m-%d')
            if (item_date - datetime.datetime.strptime(str_datestr, '%Y-%m-%d')).days < 0:  # 承办日期小于起始日期，搜索终止
                return items_list, False
            if (item_date - datetime.datetime.strptime(str_dateend, '%Y-%m-%d')).days < 0:  # 承办日期小于终止日期，添加条目
                items_list.append(item)
        return items_list, True
    except socket.gaierror as e:
        print("网络中断")
    except IndexError as e:
        print("页面信息错误")
    except Exception as e:
        if times < 3:
            times += 1
            return accept_list_one_page(page_num, page_total, current_page, str_datestr, str_dateend, times)
        else:
            return items_list, True


def check_contain_chinese(check_str):
    """
    判断字符串是否包含中文
    """
    for ch in check_str:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True
    return False


def name_filter(item_list):
    '''
    1、筛选新药。筛选时第一位为C（国内注册）,第二个字母是X（筛选出新药），第三个字母筛选H（化药）Z（中药）S（生物制品）
    2、筛选首仿药。第一位是C（国内注册），第二个字母是Y（仿制药），第三个字母筛选H（化药），注册分类为3类（已在国外上市销售，但尚未在国内上市销售）。
    3，不含黑名单字符，不是外国公司
    输入：药品受理条目列表
    输出：输出符合要求的公司列表
    '''

    # blacklist_name_list = [item['stop_word'] for item in mongodb_connection('company')['blacklist_clinical_trial'].find()]
    result_set = set([])
    for item in item_list:

        if (item['受理号'][:2] == 'CX' and (item['受理号'][2] == 'H' or item['受理号'][2] == 'Z' or item['受理号'][2] == 'S')) or (
                        item['受理号'][:3] == 'CYH' and item["注册分类"] == "3"):
            for cname in item['企业名称']:
                # flag = True
                # for blacklist_name in blacklist_name_list:
                #     if blacklist_name in cname:
                #         flag = False
                #         break
                # if flag:
                result_set.add(cname)
    return result_set


def get_medicine_data_by_date(datestr_str, dateend_str):
    """
    输入：起始日期，终止日期
    输出：该段时间内的临床试验条目信息
    """
    result = []
    datestr = datetime.datetime.strptime(datestr_str, '%Y-%m-%d')
    dateend = datetime.datetime.strptime(dateend_str, '%Y-%m-%d')
    while (dateend - datestr).days > -1:
        one_day_data = get_data_from_db_by_date(datestr.strftime('%Y-%m-%d'))  # 现在数据库中查找该日数据
        result += one_day_data
        datestr = datestr + datetime.timedelta(days=1)
    return result


def medicine_filter_api(str_datestr, str_dateend):
    """
    输入：起始日期，终止日期
    输出：期间有药物受审的企业
    """
    update_item_list = get_medicine_data_by_date(str_datestr, str_dateend)  # 正式爬虫
    # update_item_list = accept_list_one_page(0, 0, 1, str_datestr, str_dateend)[0]   # 单页测试
    apply_company_set = name_filter(update_item_list)

    return [a for a in apply_company_set if '公司' in a]


def test(str_datestr, str_dateend):
    result = medicine_filter_api(str_datestr, str_dateend)
    print('结果：')
    print('-----------------所有公司--------------------')
    for r in result[0]:
        print(r)
    print('------------------符合要求-------------------')
    for w in result[1]:
        print(w)


if __name__ == '__main__':
    update_medicine_data_api()  # 定时爬取指令，部署在服务器上时不要注释
    # medicine_filter_api('2017-01-01', '2017-05-02')
    # print get_update_accept_list()
