'''
获取中国知网中，所有杂志的摘要信息
http://navi.cnki.net/knavi
'''

import requests
from lxml import etree
from multiprocessing import Pool
from wotou.lib.db_connection import mongodb_connection
from pymongo.errors import AutoReconnect


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def address(x):
    '''
    生成杂志url,用于遍历所有杂志
    :param x:
    :return:单本杂志url
    '''
    l = []
    while x // 26 != 0:
        l.append(x % 26)
        x = x // 26
    l.append(x)
    while len(l) != 4:
        l.append(0)
    l = [chr(i + 65) for i in l]
    l.reverse()
    url = 'http://navi.cnki.net/knavi/JournalDetail?pcode=CJFD&pykm=' + ''.join(l)
    return url


def get_info(url):
    '''
    获取杂志info
    :param url:杂志url
    :return: 杂志info字典
    '''
    r = requests.get(url).text
    html = etree.HTML(r)
    journal_name_cn = html.xpath('//dd[@class="infobox"]/h3[@class="titbox"]/text()')[0].strip()
    journal_name_en = html.xpath('//dd[@class="infobox"]/p/text()')[0].strip()
    journal_type = html.xpath('//dd[@class="infobox"]/p[@class="journalType"]/span/text()')
    journal_base_info_title = [i[:-1] for i in html.xpath('//ul[@id="JournalBaseInfo"]//p[@class]/text()')]
    journal_base_info_content = html.xpath('//ul[@id="JournalBaseInfo"]//p[@class]//span/text()')
    journal_publish_info_title = [i[:-1] for i in html.xpath('//ul[@id="publishInfo"]//p[@class]/text()')]
    journal_publish_info_content = html.xpath('//ul[@id="publishInfo"]//p[@class]//span/text()')
    journal_evaluate_info_title = [i[:-1] for i in html.xpath('//ul[@id="evaluateInfo"]//p[@class="hostUnit"]/text()')]
    journal_evaluate_info_content = html.xpath('//ul[@id="evaluateInfo"]//p[@class="hostUnit"]//span/text()')
    d_baseinfo = dict(zip(journal_base_info_title, journal_base_info_content))
    d_publishinfo = dict(zip(journal_publish_info_title, journal_publish_info_content))
    d_evaluateinfo = dict(zip(journal_evaluate_info_title, journal_evaluate_info_content))
    d = {}
    d['该刊被以下数据库收录'] = html.xpath('//ul[@id="evaluateInfo"]//p[@class="database"]/text()')
    d['中文期刊名'] = journal_name_cn
    d['英文期刊名'] = journal_name_en
    d['期刊种类'] = journal_type
    d['网址'] = url
    # print(merge_dicts(d, d_baseinfo, d_evaluateinfo, d_publishinfo))
    return merge_dicts(d, d_baseinfo, d_evaluateinfo, d_publishinfo)


def insert_mongodb(item, times=0):
    '''
    链接数据库代码
    :param item: 输入内容
    :param times: 重试次数
    :return:
    '''
    try:
        db = mongodb_connection('news')
        db['test'].insert(item)
    except AutoReconnect:
        times += 1
        if times <= 5:
            print('连接错误，正在尝试重新连接mongodb')
            insert_mongodb(item, times)
        else:
            print('mongodb连接失败')


def main(number):
    try:
        insert_mongodb(get_info(address(number)))
    except IndexError as e:
        print('url not exsit')
        pass

if __name__ == '__main__':
    pool = Pool(processes=100)
    pool.map(main, [i for i in range(0, 26*26*26*26)])
