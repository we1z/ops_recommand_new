'''
爬取对应期刊的所有文章
'''

import requests
from requests.exceptions import ReadTimeout, ConnectTimeout
from lxml import etree
from multiprocessing import Pool
from wotou.lib.db_connection import mongodb_connection
from pymongo.errors import AutoReconnect
import pymongo
import wotou.spiders.util.proxies

db = mongodb_connection('news')


def get_cnki_paper_api(magazine_tag):
    '''
    获取期刊中所有文章，并保存到数据库
    :param magazine_tag:期刊的url
    :return:无
    '''
    global db
    url0 = 'http://navi.cnki.net/knavi/JournalDetail/GetJournalYearList?pcode=CJFD&pykm=%s&pIdx=0' % magazine_tag
    print(url0)
    r = get_requests_info(url0)
    # r = requests.get(url0,proxies=requests_proxies).text
    html = etree.HTML(r)
    try:
        yq = html.xpath('//div[@class="yearissuepage"]//dl//dd//a//@id')
    except:
        return get_cnki_paper_api(magazine_tag)
    # 获取所有文献地址
    yq_links = []
    try:
        paper_list = [i['文献地址'] for i in db['cnki'].find({'文献标识': magazine_tag})]
    except pymongo.errors.AutoReconnect:
        db = mongodb_connection('news')
        get_cnki_paper_api(magazine_tag)
        return
    for i in yq:
        # 先找到杂志所有期的文章目录
        yqlink = 'http://navi.cnki.net/knavi/JournalDetail/GetArticleList?year=%s&issue=%s&pykm=%s&pageIdx=0' % (
            i[2:6], i[6:], magazine_tag)
        print(yqlink)
        yq_links.append(yqlink)
    for i in yq_links:
        # 再找到所有期文章目录所有文章的地址
        i = get_requests_info(i)
        # r = requests.get(i,proxies=requests_proxies,timeout=10).text
        html = etree.HTML(i)
        try:
            links = html.xpath('//dd//span[@class="name"]//a//@href')
        except:
            return get_cnki_paper_api(magazine_tag)
        for j in links:
            paperlink = 'http://kns.cnki.net/kcms/detail/detail.aspx?DBCode=CJFD&DBName=CJFDTEMP&fileName=' + \
                        magazine_tag + j[55:64]
            print('%s准备录入' % paperlink)
            if paperlink in paper_list:
                print('\033[1;32m 文献已录入\033[0m')
            else:
                try:
                    info = extract_article_info(get_requests_info(paperlink), paperlink)
                    insert_mongodb(info)
                    print('%s完成录入' % paperlink)
                except IndexError:
                    print('%文献不存在' % paperlink)


def get_requests_info(paperlinks):
    '''
    获取request的信息
    :param paperlinks:url
    :return:requests的text
    '''
    try:
        r = requests.get(paperlinks, timeout=10
                         # ,proxies=requests_proxies
                         ).text
        return r
    except ReadTimeout:
        print('\033[1;31m %s读取超时\033[0m' % paperlinks)
        return get_requests_info(paperlinks)
    except ConnectTimeout:
        print('\033[1;31m %s连接超时\033[0m' % paperlinks)
        return get_requests_info(paperlinks)
    except requests.exceptions.ConnectionError:
        print('\033[1;31m %s连接错误\033[0m' % paperlinks)
        return get_requests_info(paperlinks)


def extract_article_info(r, paperlinks):
    '''
    提取文章信息
    :param r:html格式字符串
    :param paperlinks:url地址
    :return:文章信息
    '''
    dict = {}
    html = etree.HTML(r)
    try:
        title = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxTitle"]//h2[@class="title"]/text()')[0]
        author = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxTitle"]//div[@class="author"]//a/text()')
    except:
        return extract_article_info(r, paperlinks)
    try:
        organization = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxTitle"]//div[@class="orgn"]//a/text()')
        dict['文献组织'] = organization
    except IndexError:
        dict['文献组织'] = ''
    try:
        summary = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxInfo"]//div[@class="wxBaseinfo"]//span[@id="ChDivSummary"]/text()')[
            0]
        dict['文献摘要'] = summary
    except IndexError:
        dict['文献摘要'] = ''
    try:
        fund = [i.strip()[:-1] for i in html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxInfo"]//div[@class="wxBaseinfo"]//p//label[@id="catalog_FUND"]//following-sibling::*/text()')]
        dict['文献基金'] = fund
    except IndexError:
        dict['文献基金'] = ''

    try:
        keyword = [i.strip()[:-1] for i in html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxInfo"]//div[@class="wxBaseinfo"]//p//label[@id="catalog_KEYWORD"]//following-sibling::*/text()')]
        dict['keyword'] = keyword
    except IndexError:
        dict['keyword'] = ''

    try:
        number = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxInfo"]//div[@class="wxBaseinfo"]//p//label[@id="catalog_ZTCLS"]/parent::*/text()')[
            0]
        dict['文献分类号'] = number
    except IndexError:
        dict['文献分类号'] = ''
    try:
        doi = html.xpath(
            '//body//div[@id="mainArea"]//div[@class="wxmain"]//div[@class="wxInfo"]//div[@class="wxBaseinfo"]//p//label[@id="catalog_ZCDOI"]/parent::*/text()')[
            0]
        dict['文献DOI'] = doi
    except IndexError:
        dict['文献DOI'] = ''

    dict['文献题目'] = title
    dict['文献作者'] = author
    dict['文献地址'] = paperlinks
    dict['文献标识'] = paperlinks[-13:-9]

    return dict


# def handle_keyword(keyword):
#     '''
#     将关键词格式改成我们常用格式
#     :param keyword: 原关键词list
#     :return:修改好格式的关键词list
#     '''
#     newkeyword = []
#     for i in keyword:
#         for j in keyword:
#             if i == j:
#                 pass
#             else:
#                 s = []
#                 s.append(i)
#                 s.append(j)
#                 s.sort()
#                 s = ' '.join(s)
#                 if s not in newkeyword:
#                     newkeyword.append(s)
#                 else:
#                     pass
#     newkeyword = keyword + newkeyword
#     return newkeyword


def insert_mongodb(dict, times=0):
    """文章插入mongodb中"""
    try:
        db = mongodb_connection('news')
        db['cnki'].insert(dict)
    except AutoReconnect:
        times += 1
        if times <= 5:
            print('连接错误，正在尝试重新连接mongodb')
            insert_mongodb(dict, times)
        else:
            print('mongodb连接失败')
    except pymongo.errors.DuplicateKeyError:
        pass


if __name__ == '__main__':
    get_cnki_paper_api('RJXB')
