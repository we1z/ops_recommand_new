import multiprocessing

from selenium import webdriver
import time
from selenium.webdriver import ActionChains

from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options

from wotou.lib.db_connection import mongodb_connection
import datetime


# 判断元素是否存在
from wotou.spiders.util.proxies import get_chrome_option


def isElementExist(driver,ty,element):


    flag = True
    browser = driver
    try:
        if ty == 'css_':
            browser.find_element_by_class_name(element)
        if ty == 'css':
            browser.find_element_by_css_selector(element)
        if ty == 'xpath':
            browser.find_element_by_xpath(element)
        if ty == 'id':
            browser.find_element_by_id(element)
        return flag

    except:
        flag = False
        return flag


# 关闭弹窗广告
def close_adv(driver):
    btn_xpath = "//button[@aria-label='Close']"
    if isElementExist(driver,'xpath',btn_xpath):
        button = driver.find_element_by_xpath(btn_xpath)
        actions = ActionChains(driver)

        actions.move_to_element(button)
        actions.click(button)
        actions.perform()
    else:
        pass

def get_more_art(driver):
    # 获取更多
    print("获取更多")
    more_css = "show_more_art_btn"
    if isElementExist(driver,'css_',more_css):
        btn = driver.find_element_by_class_name(more_css)
        actions = ActionChains(driver)
        actions.move_to_element(btn)
        actions.click(btn)
        actions.perform()
    else:
        print("没有找到更多按钮")

def get_base_info(page_source):
    '''
    获取基本信息，如分类，文章发布时间，文章标题，文章链接，作者名
    若数据库中已经存在文章标题，那么不再进行录入
    使用beautifulsoup
    :return:
    '''
    # 找到所有class为col-xs-12 col-othernews的内容
    # 判断是否存在数据库，若不存在则创建数据库
    db_name = "healthweekly"
    titles = []
    if  db_name in mongodb_connection('news').list_collection_names():
        print('数据库表已存在')
        # 获取title
        title = mongodb_connection('news')[db_name].find({},{'title':1})
        for t in title:
            titles.append(t['title'])

    # 若存在则将其title集合拿出来，用于去重

    print("获取基本信息")
    soup = BeautifulSoup(page_source,'lxml')
    othernews = soup.find_all(class_='col-xs-12 col-othernews')

    articles = []
    for cont in othernews:
        article = {}
        cont_soup = BeautifulSoup(str(cont),'lxml')
        # 获取 标题 page_article_title
        title = cont_soup.select('.page_article_title')[0].text
        print("标题: " + str(title).strip())

        if str(title).strip() in titles:
            print("已录入该文章，不再录入......")
            continue
        article['title'] = str(title.strip())
        # 获取分类 class = post-category
        category = cont_soup.select('.post-category')[0].text
        print("category:" + str(category))
        article['category'] = str(category)
        # 获取发布时间 pagecategorydate
        pagedate = cont_soup.select('.pagecategorydate')[0].text
        print("发布时间: "+str(pagedate))
        article['发布时间'] = str(pagedate)
        # 获取href
        for a in cont_soup.find_all('a', class_='page_article_title',href=True):
            if a.get_text(strip=True):
                print("获取链接: "+a['href'])
                href = a['href']
        #href = cont_soup.select('.page_article_title').href
        #print("获取链接: " + str(href))
        # 获取作者名称 article_authore_more
        article['href'] = str(href)

        author_name = cont_soup.select('.article_authore_more')[0].text
        print("获取作者名称: " + str(author_name))
        article['authorname'] = str(author_name)

        article['爬取时间'] = datetime.datetime.now()
        articles.append(article)
    # 存入数据库
    if articles:
        mongodb_connection('news')[db_name].insert_many(articles)

def get_single_content(id,href):
    # article_post_div

    # option.add_argument(
    #     'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')
    # option.add_argument("--headless")
    driver = webdriver.Chrome()
    driver.get(href)
    # 去广告
    close_adv(driver)
    # 'col-lg-7 col-xs-12 display_con'
    cssname = "[class='col-lg-7 col-xs-12 display_con']"
    if isElementExist(driver,'css',cssname):
        content1 = driver.find_element_by_css_selector(cssname)
        driver.close()
        return id, content1.text
    else:
        print("不是文章链接")
        driver.close()
        db_name = 'healthyweekly'
        id = mongodb_connection('news')[db_name].find_one({"id": ""})["_id"]

    # col-lg-3 col-xs-12 article_mobile_hide
    # content2 = driver.find_element_by_css_selector('.col-lg-3.col-xs-12.article_mobile_hide')





def get_all_aticle_content():
    # 获取所有的文章内容
    db_name = "healthweekly"
    hrefs = []
    if  db_name in mongodb_connection('news').list_collection_names():
        print('数据库表存在')
        # 获取title
        href = mongodb_connection('news')[db_name].find({'content': {'$exists': 0}})
        for h in href:
            # content = get_single_content(driver, h['href'])
            # mongodb_connection('news')[db_name].update({'_id': h['_id']}, {'$set': {'content': content}})
            # print('insert ' + str(h['title']) + ' \'s,content')
            try:
                id,cont = get_single_content(h['_id'],h['href'])
                if cont:
                    mongodb_connection('news')[db_name].update({'_id': id}, {'$set': {'content': cont}})
            except:
                print("程序异常，重启")
                get_all_aticle_content()
    else:
        print("数据库表不存在，请爬取基本信息")

def get_all_article(url,driver,num):
    # num 抓取的页数

    # title = ['digital-disruption','investing-startups','pharma','illinois-healthcare-news']
    title = ['digital-disruption']
    for t in title:
        driver.get(url+t)
        close_adv(driver)
        # show_more_art_btn
        for i in range(num):
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            # onclick="showmoreart()"
            get_more_art(driver)
        time.sleep(1)
        page_source = driver.page_source
        get_base_info(page_source)
        get_single_content(driver, 'https://healthcareweekly.com/renalytix-public-funding/')

if __name__ == "__main__":
    url = 'https://healthcareweekly.com/'

    num = 10
    #get_all_article(url,driver,num)
    # get_all_aticle_content()
    get_all_aticle_content()


