from selenium import webdriver
import time

from selenium.webdriver.common.action_chains import ActionChains

driver = None

def get_patent_num(s):
    '''
    获取专利数量，获取专利页数
    :param s:
    :return: 专利数量
    '''
    # 专利数量
    try:
        list = s.split("|")
        list1 = list[0].split("：")
        print(list1)

        # 专利页数
        list2 = list[1].split("/")
        print(list2)
        return int(list1[1]),int(list2[1])
    except:
        return 0,0


# 判断元素是否存在
def isElementExist(ty,element):
    global driver
    flag = True
    browser = driver
    try:
        if ty == 'css_':
            browser.find_element_by_class_name(element)
        if ty == 'css':
            browser.find_element_by_css_selector(element)
        if ty == 'xpath':
            browser.find_element_by_xpath(element)
        if ty == 'id':
            browser.find_element_by_id(element)
        return flag

    except:
        flag = False
        return flag

def get_page(patent_num_xpath):
    global driver
    cn_num = 0
    page = 0
    exc_time = 0
    try:
        if isElementExist('xpath',patent_num_xpath):
            number = driver.find_element_by_xpath(patent_num_xpath).text
            while number is None:
                if exc_time>3:
                    return cn_num,page
                print(str(number) + "未获取到页数，尝试继续获取")
                time.sleep(2)
                number = driver.find_element_by_xpath(patent_num_xpath).text
                exc_time = exc_time + 1

    except:
        if exc_time>3:
            return cn_num, page
        print("未获取到页数，尝试继续获取")
        exc_time = exc_time +1
        time.sleep(2)
        cn_num,page = get_page(patent_num_xpath)

    cn_num, page = get_patent_num(number)
    return cn_num, page

def restart_driver():
    global driver
    if driver:
        driver.close()
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.set_page_load_timeout(20)
    driver.get("http://www.sooip.com.cn/app/patentlist")

def start_driver():
    global driver
    if driver is None:
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.set_page_load_timeout(20)
        driver.get("http://www.sooip.com.cn/app/patentlist")

def get_filter_singpatent(word):
    '''
    获取过滤后的专利
    :param word:
    :return:
    '''
    try:
        start_driver()
        express1 = word
        twoWord = False
        if ' ' in word:
            # 二次检索
            print('组合词')
            express = word.split(' ')
            express1 = express[0]
            express2 = express[1]
            twoWord = True

        express1 = express1.replace(':', '').replace('~', '').replace('/', '').replace('(', '').replace(')', ''). \
            replace('[', '').replace(']', '').strip()

        print("输入关键字:" + express1)
        driver.find_element_by_id('express2').send_keys(express1)
        css_btn = '[value="重新检索"]'
        if isElementExist('css',css_btn):
            driver.find_element_by_css_selector(css_btn).click()
        time.sleep(1)

        # 如果是两个词
        if twoWord is True:
            # 清空输入框
            driver.find_element_by_id('express2').clear()
            # 输入第二个词，进行二次检索
            print("二次检索" + express2)
            driver.find_element_by_id('express2').send_keys(express2)
            filter_search = "do2search"
            two_search = "defaultSearch"
            if isElementExist('id',filter_search):
                fs = driver.find_element_by_id(filter_search)

                if isElementExist('id',two_search):
                    actions = ActionChains(driver)
                    ts = driver.find_element_by_id(two_search)
                    actions.move_to_element(fs)
                    actions.click(ts)
                    actions.perform()

        time.sleep(1)

        # 过滤条件 找到所有的过滤条件的按钮 中国发明
        element_list = ["[value='CNA0']","[value='CNY0']","[value='CNB0']"]
        flag = False
        for element in element_list:
            if isElementExist('css',element):
                driver.find_element_by_css_selector(element).click()
                flag = True

        # 进行筛选
        css = '[value="筛选"]'
        if flag is True:
            if isElementExist('css',css):
                driver.find_element_by_css_selector(css).click()
                time.sleep(2)
        else:
            print("没有中国专利")
            driver.find_element_by_id('express2').clear()
            return None

        # 查看专利的数量，如果专利数量大于500，那么返回空值
        #patent_num_xpath="/html[@class='ng-scope']/body/div[@class='mainbody gl_content']/div[@class='gl_content_r']/div[@class='zl_box']/div[@class='zhuanli_list']/form[@id='querylist']/div[@id='mGrid_patentGrid']/div[@class='pagebox']/div[@id='patentMainPaginator']/div[@class='modelGridPaginatorContainer']/div[@class='floatl ng-binding']"
        patent_num_xpath = "/html[@class='ng-scope']/body/div[@class='mainbody gl_content']/div[@class='gl_content_r']/div[@class='zl_box']/div[@class='zhuanli_list']/form[@id='querylist']/div[@id='mGrid_patentGrid']/div[@class='pagebox']/div[@id='patentMainPaginator']/div[@class='modelGridPaginatorContainer']/div[@class='floatl ng-binding']"
        cn_num,page = get_page(patent_num_xpath)

        if cn_num>300 or cn_num==0:
            # 专利数目大于500
            print("专利数量大于300或者为0")
            driver.find_element_by_id('express2').clear()
            return None
        company_list = []
        # 获取每一页的专利详情
        for p in range(page):
            for i in range(10):
                i = i + 1
                company_xpath = "/html[@class='ng-scope']/body/div[@class='mainbody gl_content']/div[@class='gl_content_r']/div[@class='zl_box']/div[@class='zhuanli_list']/form[@id='querylist']/div[@id='mGrid_patentGrid']/ul/li[@class='ng-scope']["+str(i)+"]/div[@class='zl_table']/table/tbody/tr[3]/td[1]/span[@class='expressLink ng-binding']/a[@class='expressLink']"
                #company_css = '[field="apo"]'
                company = ''
                if isElementExist('xpath', company_xpath):
                    company = driver.find_element_by_xpath(company_xpath).text
                    #company = driver.find_element_by_css_selector(company_css).text
                # 如果不是公司，那么下一个
                if '公司' not in company:
                    continue
                if company != '':
                    company_list.append(company)
            # 点击下一页
            #xpath = "/html[@class='ng-scope']/body/div[@class='mainbody gl_content']/div[@class='gl_content_r']/div[@class='zl_box']/div[@class='zhuanli_list']/form[@id='querylist']/div[@id='mGrid_patentGrid']/div[@class='pagebox']/div[@id='patentMainPaginator']/div[@class='modelGridPaginatorContainer']/div[@id='mGrid_patentGrid_paginator_0']/ul/li[@class='nextPage']/a"
            css = '[title="下一页"]'
            #if isElementExist('xpath',xpath,driver):
            time.sleep(1)
            if isElementExist('css',css):
                #next_page = driver.find_element_by_xpath(xpath)
                actions = ActionChains(driver)
                next_page = driver.find_element_by_css_selector(css)
                actions.move_to_element(next_page)
                actions.click(next_page)
                actions.perform()
            # 等待1秒钟
            time.sleep(1)

        driver.find_element_by_id('express2').clear()
        return list(set([company_name for company_name in company_list]))
    except:
        print("超时重启浏览器")
        restart_driver()
        get_filter_singpatent(word)

if __name__ == '__main__':
    get_filter_singpatent("电力电子技术 物联网")





