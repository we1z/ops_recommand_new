# coding:utf-8
import time
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from lxml import etree
from selenium.webdriver.common.keys import Keys
from selenium.common import exceptions
import re
from selenium.webdriver.chrome.options import Options
from wotou.spiders.patent.patent_blacklist import patent_code_blacklist
from selenium.common.exceptions import TimeoutException
from wotou.lib.db_connection import mongodb_connection
import datetime
import random
import sys


IP='180.118.128.248:8646'
driver=None
count=0


def restart_driver(driver):
    '''
    重启浏览器
    :return:
    '''
    if driver:
        driver.quit()
    time.sleep(random.randint(0,10))
    option = Options()
    option.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36")
    # option.add_argument('--headless')
    # option.add_argument("--proxy-server=socks5://{}".format(IP))
    prefs = {"profile.managed_default_content_settings.images": 2}
    option.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(chrome_options=option)
    # driver.implicitly_wait(12)
    # driver.set_page_load_timeout(15)
    try:
        driver.get('https://www.patenthub.cn/user/login.html')
        login(driver)
    except exceptions.TimeoutException:
        pass
    return driver


def login(driver):
    '''
    登录操作
    :param driver: 浏览器driver
    :return:
    '''
    global acc
    acc=random.choice([i for i in mongodb_connection('company')['patenthub_acc'].find({'state':'ok','last_used_time':{'$lte':datetime.datetime.now().timestamp()-500}})])
    if not acc:
        print('暂无可用账号')
        time.sleep(300)
        return login(restart_driver(driver))
    mongodb_connection('company')['patenthub_acc'].find_one_and_update({'acc':acc['acc']},{'$set':{'last_used_time':datetime.datetime.now().timestamp()}})
    driver.find_element_by_xpath('//*[@id="account"]').send_keys(acc['acc'])
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="password"]').send_keys(acc['pw'])
    driver.find_element_by_xpath('//*[@id="password"]').send_keys(Keys.ENTER)
    time.sleep(1)
    try:
        if driver.current_url=='https://www.patenthub.cn/':
            pass
        elif driver.find_element_by_xpath('//*[@id="error"]').text=="账号处于冻结状态，请重新注册并验证手机号码":
            mongodb_connection('company')['patenthub_acc'].find_one_and_update({'acc': acc['acc']}, {'$set': {'state': 'ban'}})
            return login(restart_driver(driver))
    except:
        pass


def patenthub_get_patent(word):
    '''
    根据关键字构造搜索页地址进行信息爬取
    :param word: 关键字
    :return:final_list:公司列表
    '''
    try:
        company_list=[]
        patent_info = []
        global count
        global driver
        if not driver:
            driver = restart_driver(driver)
        url='https://www.patenthub.cn/s?p=1&q2=&q='+re.sub(' ','+',word)+'&ps=20&s=score%21&dm=mix&fc=%5B%7B%22type%22%3A%22applicantType%22%2C%22op%22%3A%22include%22%2C%22values%22%3A%5B%22%E4%BC%81%E4%B8%9A%22%5D%7D%2C%7B%22type%22%3A%22applicantCountryCode%22%2C%22op%22%3A%22include%22%2C%22values%22%3A%5B%22CN%22%5D%7D%5D&ds=cn'
        mongodb_connection('company')['patenthub_acc'].find_one_and_update({'acc': acc['acc']}, {
            '$set': {'last_used_time': datetime.datetime.now().timestamp()}})
        driver.get(url)
        time.sleep(1)
        if '没有找到符合搜索条件的专利，请输入有效的搜索条件进行查询' in driver.page_source:
            print('第{}次跳转'.format(count),word,None)
            count=count+1
            return word,None
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[3]/div[7]/div/div/div[4]/div/div[1]')))
        p_num_total=int(driver.find_element_by_xpath('//*[@id="countryCode"]/ul/li/div/label/span[2]').text)
        pat_num=p_num_total
        print('第{}次跳转'.format(count),word,'共有%s个专利'%pat_num)
        if pat_num>500:
            print('第{}次跳转'.format(count),word,company_list)
            count=count+1
            return word, company_list
        pages=int((pat_num - 1) / 20) + 1
        for i in range(1,pages+1):
            count=count+1
            time.sleep(2)
            if i==1:
                pass
            else:
                url0 = 'https://www.patenthub.cn/s?p=%s&q2=&q='%i + re.sub(' ', '+', word) + '&ps=20&s=score%21&dm=mix&fc=%5B%7B%22type%22%3A%22applicantType%22%2C%22op%22%3A%22include%22%2C%22values%22%3A%5B%22%E4%BC%81%E4%B8%9A%22%5D%7D%2C%7B%22type%22%3A%22applicantCountryCode%22%2C%22op%22%3A%22include%22%2C%22values%22%3A%5B%22CN%22%5D%7D%5D&ds=cn'
                driver.get(url0)
            if pat_num-20*i>=0:
                pn=20
            else:
                pn=pat_num-20*i+20
            page_source=driver.page_source
            html=etree.HTML(page_source)
            for patent in range(1,pn+1):
                try:
                    classify_num=html.xpath('//*[@id="mode_1"]/ul[%s]/li/div/ul/li[9]//span/text()'%patent)[0]
                    raw_company_list = html.xpath('//*[@id="mode_1"]/ul[%s]/li/div/ul/li[7]//span/text()' % patent)
                    if not patent_filter(classify_num):
                        pass
                    else:
                        company_list.extend(raw_company_list)
                except:
                    raw_company_list = html.xpath('//*[@id="mode_1"]/ul[%s]/li/div/ul/li[7]//span/text()' % patent)
                    company_list.extend(raw_company_list)
        final_list=list(set([company_name for company_name in company_list if company_name and '公司' in company_name]))
        print(final_list)
        count=count+1
        if count>300:
            print('跳转300次后重启浏览器。')
            count=0
            mongodb_connection('company')['patenthub_acc'].find_one_and_update({'acc': acc['acc']}, {'$set': {'last_used_time': datetime.datetime.now().timestamp()}})
            driver=restart_driver(driver)
            return word,final_list
        return word, final_list
    except TimeoutException:
        print('超时')
        return patenthub_get_patent(word)
    except KeyboardInterrupt:
        mongodb_connection('company')['patenthub_acc'].find_one_and_update({'acc': acc['acc']}, {'$set': {'last_used_time': datetime.datetime.now().timestamp()}})
        sys.exit()
    except ConnectionRefusedError:
        sys.exit()


def patent_filter(classify_num):
    '''
    根据专利分类号黑名单过滤专利
    :param classify_num: 专利分类号
    :return:boolen:True 或者 False
    '''
    if classify_num[:3] in patent_code_blacklist:
        return False
    elif classify_num[:4] in patent_code_blacklist:
        return False
    elif classify_num[:6] in patent_code_blacklist:
        return False
    elif classify_num[:7] in patent_code_blacklist:
        return False
    elif classify_num[:8] in patent_code_blacklist:
        return False
    elif classify_num[:9] in patent_code_blacklist:
        return False
    elif classify_num[:10] in patent_code_blacklist:
        return False
    elif classify_num[:11] in patent_code_blacklist:
        return False
    elif classify_num[:12] in patent_code_blacklist:
        return False
    elif classify_num[:13] in patent_code_blacklist:
        return False
    else:
        return True


if __name__ == '__main__':
    patenthub_get_patent('train')