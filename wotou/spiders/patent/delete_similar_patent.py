# coding:utf-8
'''
删除类似的专利,用于svm专利分类时，训练语料的整理
'''

import Levenshtein
import random
list=[]

with open('./corpus/tmt',encoding='UTF-8') as f:
    for l in f.readlines():
        list.append(l)

i=len(list)-1
for time in range(0,100000000):
    string_a=random.randint(0,i)
    string_b=random.randint(0,i)
    if string_a==string_b:
        continue
    if Levenshtein.distance(list[string_a],list[string_b])<(len(list[string_b])+len(list[string_a]))/2.5:
        print(string_a,string_b)
        print(list[string_b],list[string_a])

        list.remove(list[string_a])
        i-=1

    if time % 50000 ==0:

        with open('./corpus/tmt','w',encoding='UTF-8') as f:
            for j in list:
                f.write(j)