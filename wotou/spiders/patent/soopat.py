# -*- coding:utf-8 -*-

'''
soopat搜索相关专利,目前没有用，用patexplorer_request代替
'''

import time
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from wotou.spiders.util.proxies import service_args
from wotou.lib.db_connection import mongodb_connection
from selenium.common import exceptions
from wotou.spiders.util.identify_code import handle_identify_code_api
from PIL import Image
import re
import urllib
import http
import multiprocessing

from lxml import html

driver = None


def init_phantomjs(is_change_ip):
    """
    初始化pantomjs浏览器
    """
    global driver
    caps = DesiredCapabilities.PHANTOMJS
    caps["phantomjs.page.settings.userAgent"] = \
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0"

    if is_change_ip:
        proxy_service_args = service_args
    else:
        proxy_service_args = []

    try:
        driver = webdriver.PhantomJS(desired_capabilities=caps,
                                     service_args=['--load-images=false', "--web-security=no",
                                                   "--ignore-ssl-errors=yes",
                                                   "--ignore-ssl-errors=true",
                                                   "--ssl-protocol=tlsv1"] + proxy_service_args)
    except exceptions.WebDriverException:
        init_phantomjs(is_change_ip)
    driver.implicitly_wait(20)
    driver.set_page_load_timeout(60)


def restart_driver():
    """
    重启浏览器
    """
    global driver
    driver.quit()
    print("重启浏览器")

    # caps = DesiredCapabilities.PHANTOMJS
    # caps["phantomjs.page.settings.userAgent"] = \
    #     "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0"
    # try:
    #     driver.delete_all_cookies()
    #     driver.save_screenshot("beforequit.png")
    # except Exception as e:
    #     pass
    #
    # driver.quit()
    #
    # driver = webdriver.PhantomJS(desired_capabilities=caps
    #                              ,
    #                              service_args=['--load-images=false', "--web-security=no", "--ignore-ssl-errors=yes",
    #                                            "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1"]
    #                              )
    #
    # driver.implicitly_wait(20)
    # driver.set_page_load_timeout(60)

    driver = webdriver.Chrome()
    driver.implicitly_wait(20)
    driver.set_page_load_timeout(20)


def init_chrome():
    """
    初始化chrome浏览器
    """
    global driver

    driver = webdriver.Chrome()
    driver.implicitly_wait(20)
    driver.set_page_load_timeout(20)


def init_Firefox():
    """
    初始化Firefox浏览器
    """
    global driver
    driver = None
    if driver is None:
        driver = webdriver.Firefox()
        driver.implicitly_wait(30)
        driver.set_page_load_timeout(30)


def judge_pagenum():
    """
    获取专利个数
    """
    global driver
    no = driver.find_element_by_xpath("html/body/div[5]/p/b[1]")
    # print('共有', no.text, '项符合结果')
    return int(no.text)


def soopat_handle_identify_code():
    """
    对验证码图片进行截图并进行识别
    """
    # 确定图片，输入框，确认键在途中的位置
    try:
        line = driver.find_element_by_xpath(".//*[@id='Cd']")
    except exceptions.TimeoutException:
        driver.refresh()
        return soopat_handle_identify_code()
    except exceptions.NoSuchElementException:
        driver.refresh()
        return soopat_handle_identify_code()
    press = driver.find_element_by_xpath(".//*[@id='btn']")

    img = driver.find_element_by_xpath("//img[@src='/Account/ValidateImage']")

    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + 50)
    bottom = int(img.location['y'] + 50)

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=2001)
    try:
        line.send_keys(result)
        press.click()
    except (exceptions.TimeoutException,exceptions.WebDriverException):
        driver.execute_script('window.stop()')
        # soopat_handle_identify_code()

    if '验证码错误' in driver.page_source:
        print('验证码识别错误！！！！！\n')
        soopat_handle_identify_code()


def soopat_search_single_page_content(page, is_restart=True, retry_time=0,current_url=''):
    """
    爬取当页信息，并点击进入下一页
    """
    global driver
    if page != 1:
        try:
            url = re.sub("&PatentIndex=\d+", "", current_url)
            url += '&PatentIndex=' + str(page * 10 - 10)

            driver.get(url)
        except (exceptions.TimeoutException, http.client.RemoteDisconnected):
            print("超时在第" + str(page) + "页")
            if retry_time > 1:
                restart_driver()
            return soopat_search_single_page_content(page, is_restart, retry_time + 1,current_url)
    if '请输入验证码' in driver.page_source:
        print("第" + str(page) + "页输入验证码" + str(is_restart) + url)
        if is_restart:
            restart_driver()
        else:
            soopat_handle_identify_code()
        return soopat_search_single_page_content(page, is_restart, retry_time,current_url)

    if (page != 1 and driver.current_url != url) or (page == 1 and driver.current_url == "about:blank"):
        return soopat_search_single_page_content(page, is_restart, retry_time,current_url)

    if '503 Service Unavailable' in driver.page_source:
        return soopat_search_single_page_content(page, is_restart, retry_time,current_url)

    company_list = []

    root = html.fromstring(driver.page_source)
    contain_list = root.xpath(".//*[@id='PageContent']/div[@class='PatentBlock']")
    # driver.save_screenshot(str(page) + '.png')
    for i in range(1, len(contain_list)):

        status = contain_list[i].xpath("./div[2]/h2/div")[0].text

        title = contain_list[i].xpath("./div[2]/h2/a")[0].xpath('string(.)').split('-')[0]
        abstract = str(contain_list[i].xpath("./div[2]/span[2]")[0].xpath('string(.)')).replace("\n", "")
        abstract = abstract[abstract.find("摘要"):]
        print(title + abstract)

        if '审中' in status or "有权" in status:
            proposer_list = contain_list[i].xpath("./div[2]/span[1]/a")
            proposer_list = proposer_list[:-1]
            company_list.extend([j.text_content() for j in proposer_list])

    return company_list


def soopat_search_word_api(word, is_limit=False, is_restart=True, retry_time=0):
    """
    soopat搜关键词入口
    :param retry_time: 重试次数
    :param is_restart: 遇到验证码时，是否重启浏览器，True则重启，原理是换ip后有时间验证码会消失，False则直接破解验证码
    :param is_limit: 是否限制专利个数
    :param word: 关键词
    """
    global driver
    # if not driver:

    init_chrome()

    raw_company_list = []

    # 打开搜索页面，输入关键词，点击搜索
    try:

        base_url = 'http://www.soopat.com/Home/Result?'
        data = {'SearchWord': word,
                'FMZL': 'Y',
                'FMSQ': 'Y',
                'PatentIndex':'0'}
        url = base_url + urllib.parse.urlencode(data)
        driver.get(url)

    except exceptions.TimeoutException as e:
        print("网络中断，搜索页面无法打开，重试中！！" + word)
        time.sleep(2)
        driver.quit()
        return soopat_search_word_api(word, is_limit, is_restart, retry_time + 1)
    time.sleep(2)
    if '请输入验证码' in driver.page_source:
        print("第" + str(1) + "页输入验证码" + str(is_restart) + url)
        if is_restart:
            if retry_time > 1:
                return word, -1
            restart_driver()
            return soopat_search_word_api(word, is_limit, is_restart, retry_time + 1)
        else:
            soopat_handle_identify_code()

    if driver.current_url == 'about:blank':
        print("空白页" + word)
        driver.quit()
        return soopat_search_word_api(word, is_limit, is_restart, retry_time)
    try:
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "html/body/div[5]/p/b[1]")))
    except exceptions.TimeoutException as e:
        driver.quit()
        return soopat_search_word_api(word, is_limit, is_restart, retry_time + 1)

    no = judge_pagenum()

    if is_limit:
        if no > 500:
            return word, []
    if no == 0:
        return word, []

    for i in range(1, min((int(no) - 1) // 10 + 1, 100) + 1):
        time.sleep(2)
        raw_company_list.extend(soopat_search_single_page_content(i, is_restart,current_url=url))
    raw_company_list = list(set(raw_company_list))

    print(word + str(raw_company_list))
    driver.quit()
    return word, [name for name in raw_company_list if '公司' in name]


if __name__ == '__main__':
    # pool = multiprocessing.Pool(processes=4)
    # process_list = []
    # index = 0
    # with open('list.txt') as f:
    #
    #     for i in f.readlines():
    #         index += 1
    #         if index > 200 or index <150:
    #             continue
    #         i = i.replace("\n", "").strip()
    #         # company_list.append(soopat_search_word_api(i, True))
    #         process_list.append(pool.apply_async(soopat_search_word_api, (i, True, False)))
    #         time.sleep(15)
    #
    # pool.close()
    # pool.join()

    soopat_search_word_api('物联网 信息安全', True, False)
