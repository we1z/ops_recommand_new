# coding:utf-8

'''
破解专利探索者网站的验证码，（通过不停在专利探索者刷新，刷出验证码然后破解，保证本ip有效）
采用生成chrome插件来使用阿布云代理

已无法使用
'''

import os
import re
import zipfile
import string

from wotou.lib.db_connection import mongodb_connection
from PIL import Image
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import urllib
import random
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json
from wotou.spiders.util.identify_code import handle_identify_code_api
from selenium.common.exceptions import UnexpectedAlertPresentException,NoSuchElementException,TimeoutException
import time
from wotou.spiders.util.proxies import get_chrome_option

from wotou.config.config import *

driver = None

# 创建插件代理
def create_proxy_auth_extension(proxy_host, proxy_port,
                                proxy_username, proxy_password,
                                scheme='socks', plugin_path=None):
    if plugin_path is None:
        plugin_path = os.getcwd() + r'/{}_{}@socks-cla.abuyun.com_8030.zip'.format(proxy_username, proxy_password)

    manifest_json = """
       {
           "version": "1.0.0",
           "manifest_version": 2,
           "name": "Abuyun Proxy",
           "permissions": [
               "proxy",
               "tabs",
               "unlimitedStorage",
               "storage",
               "<all_urls>",
               "webRequest",
               "webRequestBlocking"
           ],
           "background": {
               "scripts": ["background.js"]
           },
           "minimum_chrome_version":"22.0.0"
       }
       """
    background_js = string.Template(
        """
        var config = {
            mode: "fixed_servers",
            rules: {
                singleProxy: {
                    scheme: "${scheme}",
                    host: "${host}",
                    port: parseInt(${port})
                },
                bypassList: ["foobar.com"]
            }
          };
        chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});
        function callbackFn(details) {
            return {
                authCredentials: {
                    username: "${username}",
                    password: "${password}"
                }
            };
        }
        chrome.webRequest.onAuthRequired.addListener(
            callbackFn,
            {urls: ["<all_urls>"]},
            ['blocking']
        );
        """
    ).substitute(
        host=proxy_host,
        port=proxy_port,
        username=proxy_username,
        password=proxy_password,
        scheme=scheme,
    )

    with zipfile.ZipFile(plugin_path, 'w') as zp:
        zp.writestr("manifest.json", manifest_json)
        zp.writestr("background.js", background_js)

    return plugin_path

def handle_identify_code():
    '''
    通过云打码破解验证码
    :return:
    '''
    img = driver.find_element_by_id("nocrawler_img")
    print('正在破解验证码...')
    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=3004)
    input=driver.find_element_by_id("code")
    input.send_keys(result)
    try:
        driver.find_element_by_id("Button1").click()
    except TimeoutException:
        pass

def restart_driver(ip):
    '''
    启动浏览器
    :param ip:是否使用代理
    :return:
    '''
    global driver
    if driver:
        driver.quit()
        driver=None
    # option = get_chrome_option()


    # 创建插件代理文件
    proxy_auth_plugin_path = create_proxy_auth_extension(
        proxy_host=PROXYHOST,
        proxy_port=PROXYPORT,
        proxy_username=PROXYUSER,
        proxy_password=PROXYPASS)

    option=webdriver.ChromeOptions()
    option.add_argument('--headless')
    option.add_argument(
        'user-agent="Mozilla/5.0 (iPod; U; CPU iPhone OS 2_1 like Mac OS X; ja-jp) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5F137 Safari/525.20"')

    if ip:
        # option.add_argument("--proxy-server=%s" % ip)
        option.add_extension(proxy_auth_plugin_path)
    else:
        pass
    # d = DesiredCapabilities.CHROME
    # d['loggingPrefs'] = {'performance': 'INFO'}
    # driver = webdriver.Chrome(chrome_options=option,desired_capabilities=d)
    try:
        driver=webdriver.Chrome(chrome_options=option)
        driver.implicitly_wait(5)
        driver.set_page_load_timeout(10)

        driver.get('http://www.patexplorer.com')
    except TimeoutException:
        pass
    except ConnectionResetError:
        pass

def get_patexplorer_cookies(num,ip):
    '''
    获取指定数量的专利探索者cookies
    :param num: 指定数量
    :return:
    '''
    global driver
    for i in range(0, num):

        if not driver or  random.random()<0.3:
            restart_driver(ip)

        word = str(random.randint(0, 1000))
        encode_word = urllib.parse.urlencode({'q': word})
        url = 'http://www.patexplorer.com/results/s.html?sc=7&%s&type=s#/100/1' % encode_word

        try:
            driver.delete_all_cookies()
            driver.get(url)
            while driver.find_element_by_tag_name('h3').text=='抱歉，您的操作过于频繁，请输入验证码以继续操作：':
                # mongodb_connection('company')['patexplorer_account'].delete_many({})
                handle_identify_code()
                time.sleep(0.1)
        except UnexpectedAlertPresentException:
            driver.switch_to.alert.accept()
        except NoSuchElementException:
            pass
        except OSError:
            pass
        except TimeoutException:
            pass
        except AttributeError:
            pass
        time.sleep(1)

        # list_postfix=''
        # for entry in driver.get_log('performance'):
        #     if 'results/list/' in entry['message']:
        #         # print(entry['message'])
        #         list_postfix=entry['message'][entry['message'].find('/results/list/')+14:entry['message'].find('\"',entry['message'].find('/results/list/'))]
        #         if 10<len(list_postfix)<100:
        #             print(list_postfix)
        #             print(driver.get_cookies())
        #             break
        # if list_postfix:
        #     mongodb_connection('company')['patexplorer_account'].insert({'cookies': driver.get_cookies(),'list_postfix':list_postfix})
    driver.quit()
    driver = None


if __name__ == '__main__':
    get_patexplorer_cookies(50000,True)
    driver.quit()
    driver = None
