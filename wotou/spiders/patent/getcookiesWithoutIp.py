# coding:utf-8
from wotu.lib.db_connection import mongodb_connection

from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import urllib
import random

driver = None

def restart_driver():
    global driver
    if driver:
        driver.delete_all_cookies()
        driver.quit()
        driver=None
    option = Options()
    # option.add_argument('--headless')
    # option.add_argument("--proxy-server=socks5://" + '127.0.0.1' + ":" + '1080')
    driver = webdriver.Chrome(chrome_options=option)
    driver.implicitly_wait(12)
    driver.set_page_load_timeout(240)
    driver.get('http://www.patexplorer.com')
    # driver.get('https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=0&rsv_idx=1&tn=baidu&wd=%E4%B8%93%E5%88%A9%E6%8E%A2%E7%B4%A2%E8%80%85&rsv_pq=be53e4e10001df5e&rsv_t=3318SCYbwOnOZhUH%2F%2BURuVUedkbIdS5VnUG%2FD%2Bin%2BAbfN4EWpX4IBWdlLUQ&rqlang=cn&rsv_enter=1&rsv_sug3=13&rsv_sug1=11&rsv_sug7=100')
    # driver.get('https://www.baidu.com/link?url=6odCKj4QtxhVjxaMd9qYTezgYRB3bmlfb6_WaOPHS32KO8r1Fd8nGHdFkVx1DgLt&wd=&eqid=8bbf513300010c96000000065a4b4d11')


def get_patexplorer_cookies(num):
    '''
    获取指定数量的专利探索者cookies
    :param num: 指定数量
    :return:
    '''
    global driver
    for i in range(0, num):
        restart_driver()
        word = str(random.randint(0, 1000))
        encode_word = urllib.parse.urlencode({'q': word})
        url = 'http://www.patexplorer.com/results/s.html?sc=7&%s&type=s#/100/1' % encode_word
        driver.get(url)
        mongodb_connection('company')['patexplorer_account'].insert({'cookies': driver.get_cookies()})
    driver.quit()
    driver = None


if __name__ == '__main__':
    get_patexplorer_cookies(50)
    # driver.quit()
    driver = None