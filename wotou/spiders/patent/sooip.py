import requests
import re
from bs4 import BeautifulSoup
import time
from requests.exceptions import ConnectionError

headers = {     'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding':'gzip, deflate',
                'Accept-Language':'zh-CN,zh;q=0.9',
                'Connection':'keep-alive',
                # 'Content-Length': '328',
                'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
                'Host':'www.so.iptrm.com',
                'Origin': 'http://www.so.iptrm.com',
                'Referer': 'http://www.so.iptrm.com/app/patentlist',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36',
                'X-Requested-With': 'XMLHttpRequest',
                'Upgrade-Insecure-Requests':'1'
                }
cookie = {}

def get_cookie():
    response = requests.get('http://www.so.iptrm.com/app/patentlist')

    cookie['JSESSIONID'] = response.headers.get('Set-Cookie').split(';')[0].split('=')[1]



def get_cn_num(express,express2, categoryIndex):
    get_cookie()
    time.sleep(1)
    response = requests.post('http://www.so.iptrm.com/txnOneCategoryIndex.ajax',
                            data={'select-key:categoryWords': 'pdb;lsscn',
                                    'select-key:express': express,
                                    'select-key:express2':  ''and express2,
                                    'select-key:categoryIndex': categoryIndex,
                                    'select-key:patentLib': '',
                                    'select-key:patentType': 'patent2',
                                    'select-key:categoryNum': '5',
                                    'select-key:categoryStart': '0'},
                             headers=headers,
                             cookies=cookie)

    if 'CN' not in response.text:
        print('没有中国专利')
        cn_num = 0
    else:
        cna_num, cnb_num, cny_num = 0, 0, 0 #中国发明申请，中国发明授权,中国实用新型
        cna_num_re = re.search('<key>CNA0</key>\n<value>(\d+)', response.text)
        cnb_num_re = re.search('<key>CNB0</key>\n<value>(\d+)', response.text)
        cny_num_re = re.search('<key>CNY0</key>\n<value>(\d+)', response.text)
        if cna_num_re:
            cna_num = int(cna_num_re.group(1))
        if cnb_num_re:
            cnb_num = int(cnb_num_re.group(1))
        if cny_num_re:
            cny_num = int(cny_num_re.group(1))
        cn_num = cna_num + cnb_num + cny_num
    print(cn_num)
    return cn_num


def get_patent_company(word):
    """
    :param word: 关键词：'癌症 DNA'
    :return: word, company_list
    """
    try:
        company_list = [] #返回公司列表
        word = word.replace(':', "").replace('~', "").replace('/', '').replace('(', '').replace(')', ''). \
            replace('[', '').replace(']', '').strip()
        word_list = word.split(' ')
        print(word)
        if len(word_list) < 2:
            express = '(名称,摘要,主权项 +=  ( '+word_list[0]+' ) )' #搜索条件一
            express2 = '' #搜索条件二
        else:
            express2 = '(名称,摘要,主权项 +=  ( '+word_list[1]+' ) )'
            express = '(名称,摘要,主权项 +=  ( '+word_list[0]+' ) )'
        categoryIndex = 'CNA0|专利类型^CNY0|专利类型^CNB0|专利类型^' # 过滤条件：中国发明申请，中国实用新型，中国发明授权

        num_cn = get_cn_num(express, express2, categoryIndex) #返回专利数
        if num_cn > 500:
            # print('数目大于500')
            return word, company_list
        elif num_cn == 0:
            # print('未查询到专利')
            return word,company_list
        time.sleep(1)
        for i in range(1,(num_cn-1)//50+2):
            response = requests.post('http://www.so.iptrm.com/txnPatentData01.ajax',
                                     data={'secondKeyWord': '名称+摘要+主权项',
                                           'secondkeyWordVal': '',
                                           'secondSearchType': 'AND',
                                           'express2': ''and express2,
                                           'express': express,
                                           'isFamily': '',
                                           'categoryIndex': categoryIndex,
                                           'selectedCategory': '',
                                           'patentLib': '',
                                           'patentType': 'patent2',
                                           'order': '',
                                           'pdbt': '',
                                           'attribute-node:patent_cache-flag': 'false',
                                           'attribute-node:patent_start-row': (i-1)*50+1,
                                           'attribute-node:patent_page-row': '50',
                                           'attribute-node:patent_sort-column': 'ano',
                                           'attribute-node:patent_page': i},
                                     headers=headers,
                                     cookies=cookie
                                     )
            soup = BeautifulSoup(response.text, 'html.parser')
            patent_list = soup.select('patent')
            for j in patent_list:
                apo = j.select('apo')[0].text.split(';')  #单个专利的申请人列表
                for c in apo:
                    company_list.append(c)
            time.sleep(1)
        print(list(set([company_name for company_name in company_list if company_name and '公司' in company_name])))
        return word, list(set([company_name for company_name in company_list if company_name and '公司' in company_name]))
    except ConnectionError:
        print('报错')
        return get_patent_company(word)


if __name__ == '__main__':
    get_patent_company('公交巴士')
