# coding:utf-8

'''
破解专利探索者网站的验证码，（通过不停在专利探索者刷新，刷出验证码然后破解，保证本ip有效）
使用普通的ip代理
'''

from wotou.lib.db_connection import mongodb_connection
from PIL import Image
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import urllib
import random
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json
from wotou.spiders.util.identify_code import handle_identify_code_api
from selenium.common.exceptions import UnexpectedAlertPresentException,NoSuchElementException,TimeoutException
import time
from wotou.spiders.util.proxies import get_chrome_option
driver = None
import os
import requests
import re

def zhima_proxy():
    """
    芝麻代理
    acc：wootou
    paw：wootou1
    :return:
    """
    url = 'http://webapi.http.zhimacangku.com/getip?num=1&type=1&pro=&city=0&yys=0&port=11&time=1&ts=0&ys=0&cs=0&lb=1&sb=0&pb=4&mr=1&regions='
    r = requests.get(url)
    result = re.search(r"\d+[.]\d+[.]\d+[.]\d+[:]\d+", r.text)
    PROXY = result.group(0)
    return PROXY

def handle_identify_code():
    '''
    通过云打码破解验证码
    :return:
    '''
    img = driver.find_element_by_id("nocrawler_img")

    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=3004)
    input=driver.find_element_by_id("code")
    input.send_keys(result)
    try:
        driver.find_element_by_id("Button1").click()
    except TimeoutException:
        pass

def restart_driver(ip):
    '''
    启动浏览器
    :param ip:是否使用代理
    :return:
    '''
    global driver
    if driver:
        driver.quit()
        driver=None
    # option = get_chrome_option()
    option=webdriver.ChromeOptions()
    option.add_argument('--headless')
    option.add_argument(
        'user-agent="Mozilla/5.0 (iPod; U; CPU iPhone OS 2_1 like Mac OS X; ja-jp) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5F137 Safari/525.20"')
    desired_capabilities = option.to_capabilities()
    if ip:
        # option.add_argument("--proxy-server=%s" % ip)
        proxy = zhima_proxy()
        print(proxy)
        desired_capabilities['proxy'] = {
            "httpProxy": proxy,
            "ftpProxy": proxy,
            "sslProxy": proxy,
            "noProxy": None,
            "proxyType": "MANUAL",
            "class": "org.openqa.selenium.Proxy",
            "autodetect": False
        }
    else:
        pass
    # d = DesiredCapabilities.CHROME
    # d['loggingPrefs'] = {'performance': 'INFO'}
    # driver = webdriver.Chrome(chrome_options=option,desired_capabilities=d)
    try:
        driver=webdriver.Chrome(desired_capabilities = desired_capabilities)
        driver.implicitly_wait(5)
        driver.set_page_load_timeout(10)
        driver.get('http://test.abuyun.com')
        driver.get('http://www.patexplorer.com')
    except TimeoutException:
        pass
    except ConnectionResetError:
        pass

def get_patexplorer_cookies(num,ip):
    '''
    获取指定数量的专利探索者cookies
    :param num: 指定数量
    :return:
    '''
    global driver
    for i in range(0, num):

        if not driver or  random.random()<0.3:
            restart_driver(ip)

        word = str(random.randint(0, 1000))
        encode_word = urllib.parse.urlencode({'q': word})
        url = 'http://www.patexplorer.com/results/s.html?sc=7&%s&type=s#/100/1' % encode_word

        try:
            driver.delete_all_cookies()
            driver.get(url)
            while driver.find_element_by_tag_name('h3').text=='抱歉，您的操作过于频繁，请输入验证码以继续操作：':
                # mongodb_connection('company')['patexplorer_account'].delete_many({})
                handle_identify_code()
                time.sleep(0.1)
        except UnexpectedAlertPresentException:
            driver.switch_to.alert.accept()
        except NoSuchElementException:
            pass
        except OSError:
            pass
        except TimeoutException:
            pass
        except AttributeError:
            pass
        time.sleep(1)
        list_postfix=''
        for entry in driver.get_log('performance'):
            if 'results/list/' in entry['message']:
                # print(entry['message'])
                list_postfix=entry['message'][entry['message'].find('/results/list/')+14:entry['message'].find('\"',entry['message'].find('/results/list/'))]
                if 10<len(list_postfix)<100:
                    print(list_postfix)
                    print(driver.get_cookies())
                    break
        if list_postfix:
            mongodb_connection('company')['patexplorer_account'].insert({'cookies': driver.get_cookies(),'list_postfix':list_postfix})
    driver.quit()
    driver = None


if __name__ == '__main__':
    # 芝麻代理ip 需要保证ip有效  http://webapi.http.zhimacangku.com/getip?num=1&type=1&pro=&city=0&yys=0&port=1&time=1&ts=0&ys=0&cs=0&lb=1&sb=0&pb=4&mr=1&regions=

    get_patexplorer_cookies(50000,ip=True)
    driver.quit()
    driver = None
