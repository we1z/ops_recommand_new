# coding:utf-8
'''
专利探索者爬虫，selenium版，目前使用中
'''
import time
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common import proxy


from wotou.spiders.util.proxies import service_args
from wotou.lib.db_connection import mongodb_connection
from selenium.common import exceptions
from wotou.spiders.util.identify_code import handle_identify_code_api
from PIL import Image
import re
import urllib
import http
import multiprocessing
from selenium.webdriver.chrome.options import Options
from wotou.spiders.patent.patent_classify import PatentClassification
from selenium.common.exceptions import UnexpectedAlertPresentException, NoSuchElementException, TimeoutException
from lxml import html
import jieba
import random
from functools import reduce
from wotou.spiders.patent.patent_blacklist import patent_code_blacklist
import requests
import json
import os

driver = None
patent_classify_model = None


def handle_identify_code(word):
    '''
    通过云打码破解验证码
    :return:
    '''
    img = driver.find_element_by_id("nocrawler_img")

    # 保存验证码网页，确认验证码位置
    driver.save_screenshot('%s.png' % word)
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('%s.png' % word)
    im = im.crop((left, top, right, bottom))
    im.save('code_%s.png' % word)

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code_%s.png' % word, codetype=3004)
    input = driver.find_element_by_id("code")
    input.send_keys(result)
    try:
        driver.find_element_by_id("Button1").click()
    except TimeoutException:
        pass

def restart_driver(driver):
    '''
    重启浏览器
    :return:
    '''
    if driver:
        driver.quit()

    driver = webdriver.PhantomJS(
        service_args=service_args + ['--load-images=false', "--web-security=no", "--ignore-ssl-errors=yes",
                                     "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1"])
    driver.implicitly_wait(12)
    driver.set_page_load_timeout(15)
    try:
        driver.get('https://login.zlbaba.com/login?service=http://www.patexplorer.com/login/cas')
        login(driver)
    except exceptions.TimeoutException:
        pass
    return driver



def login(driver):
    '''
    登录操作
    :param driver: 浏览器driver
    :return:
    '''
    # 获取任意一个账户
    try:
        acc = random.choice([i for i in mongodb_connection('company')['patexplorer_acc'].find()])
    except IndexError:
        print("找不到帐号")
        time.sleep(200)
        return login(driver)
    if acc:
        driver.find_element_by_id('Js_loginId').send_keys(acc['phone'])
        driver.find_element_by_name('password').send_keys(acc['pw'])
        driver.find_element_by_id('loginBtn').click()

def count(word):
    '''
    获取指定关键词的专利个数
    :param word:
    :return:
    '''
    url_encode_word = urllib.parse.urlencode({'q': word})

    headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "*/*", "Accept-Encoding": "gzip, deflate",
               "Accept-Language": "zh-CN,zh;q=0.8", "Connection": "keep-alive",
               "Content-Length": str(19 + len(url_encode_word)),
               # "Cookie": "UM_distinctid=1600a72c449d49-0626dd071c6637-3970065f-1fa400-1600a72c44ab49; yunsuo_session_verify=7e47cad40bf28ed8c303c6cbf3f1ce55; JSESSIONID=4506DADEE82F1849C6FB8DD60EE2059A; CNZZDATA1261546263=2113758036-1496212147-http%253A%252F%252Fwww.baiten.cn%252F%7C1513904387",
               "Cookie": '',
               "Host": "www.patexplorer.com", "Origin": "https://www.patexplorer.com",
               "Referer": "https://www.patexplorer.com/results/s.html?sc=7&" + url_encode_word + "&fq=&type=s&sort=&sortField=",
               "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
               "X-Requested-With": "XMLHttpRequest"}
    data = {"sc": "7", "q": word, "fq": "", "pageSize": ""}
    try:

        r = requests.post('https://www.patexplorer.com/results/filter', headers=headers, data=data,timeout=10,
                          proxies={'https': 'socks5://SO6NG45MQ8C88FQC:79B6CE41BF438EF2@socks-cla.abuyun.com:8030',
                                   'http': 'socks5://SO6NG45MQ8C88FQC:79B6CE41BF438EF2@socks-cla.abuyun.com:8030'})
        if r.status_code == 200:
            j = json.loads(r.text)
        else:
            print('requests 获取专利数失败')
            return -1
    except (requests.exceptions.ReadTimeout, json.decoder.JSONDecodeError, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError):
        print('requests 获取专利数失败')
        return -1
    if j['facetPivots']:
        if j['facetPivots']['country'] == {}:
            return 0
        else:
            c = sum(j['facetPivots']['country']['cn'].values())
            if 'cn_um' in j['facetPivots']['country']['cn']:
                c = c - j['facetPivots']['country']['cn']['cn_um']

            return c
    else:
        return count(word)



def patexplorer_search_word_api(word, is_limit=False, classify_type=2, classify_arg=None):
    '''
    专利搜索入口
    :param classify_arg: 专利分类方法所带参数
    :param classify_type: 专利分类方法，1为svm，2为黑名单法
    :param word:关键词
    :param is_limit: 是否限制只搜索个数小于500的专利
    :return: 关键词，相关公司名称
    '''

    if classify_type == 1:
        global patent_classify_model
        patent_classify_model = PatentClassification()

    word = word.replace(':', "").replace('~', "").replace('/', '').replace('(', '').replace(')', ''). \
        replace('[', '').replace(']', '')
    if len(word) < 2:
        return word, []
    company_list = []
    num = count(word)
    if is_limit and num > 500:
        return word, company_list
    elif num == 0:
        return word, None

    global driver
    if random.random() < 0.01 or not driver:
        driver = restart_driver(driver)
    print("搜索" + word)

    encode_word = urllib.parse.urlencode({'q': word})
    url = 'https://www.patexplorer.com/results/s.html?sc=7&%s&type=s#/100/1' % encode_word
    # print(driver.get_cookies())
    try:
        driver.get(url)
        if random.random() < 0.1:
            driver.refresh()
        while 'vcode.html' in driver.current_url:
            time.sleep(15)
            driver.get(url)
    except exceptions.TimeoutException:
        if random.random() < 0.1:
            driver = restart_driver(driver)
        return patexplorer_search_word_api(word, is_limit, classify_type, classify_arg)

    try:
        r = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located(
                (By.XPATH, './/p[@class="no_data_reason"] | .//div[@class="Js_outerList"]/div')))

    except exceptions.TimeoutException:
        if '没有搜索到相关专利' in driver.page_source:
            return word, None
        elif '抱歉，您的操作过于频繁，请输入验证码以继续操作' in driver.page_source:
            if '二次检索' in driver.page_source:
                try:
                    handle_identify_code(word)
                except:
                    return patexplorer_search_word_api(word, is_limit, classify_type, classify_arg)
                os.remove('%s.png' % word)
                os.remove('code_%s.png' % word)
            else:
                driver = restart_driver(driver)
        else:
            if 'login.zlbaba.com' in driver.current_url:
                login(driver)
            if random.random() < 0.1:
                driver = restart_driver(driver)
            return patexplorer_search_word_api(word, is_limit, classify_type, classify_arg)


    if int(num) == 0:
        return word, None

    try:
        r = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.XPATH, "//div[@class='Js_outerList']/div")))
    except exceptions.TimeoutException:

        if '没有搜索到相关专利' in driver.page_source:
            return word, None
        else:
            driver.save_screenshot("aaa.png")
            return patexplorer_search_word_api(is_limit, classify_type, classify_arg)

    for i in range(1, int((int(num) - 1) / 100) + 2):
        company_list.extend(_get_single_page_result(i, url, classify_type, classify_arg))

    print([company_name for company_name in company_list if company_name and '公司' in company_name])
    return word, list(set([company_name for company_name in company_list if company_name and '公司' in company_name]))


def _get_single_page_result(page, url, classify_type=2, classify_arg=None):
    '''
    获取指定关键词和页数对应的公司
    :param page: 页数
    :param url: 链接
    :param classify_type: 专利分类方法
    :param classify_arg: 专利分类参数
    :return:该页中满足条件的专利对应的公司名称
    '''
    global driver
    raw_company_list = []

    if page != 1:
        try:
            url = re.sub('/\d$', '/' + str(page), url)
            driver.get(url)
            while 'vcode.html' in driver.current_url:
                time.sleep(15)
                driver.get(url)

            WebDriverWait(driver, 60).until(
                EC.presence_of_element_located((By.XPATH, "//div[@class='Js_outerList']/div")))
        except (exceptions.TimeoutException, urllib.error.URLError, ConnectionRefusedError) as e:
            print(e)
            # driver.save_screenshot('bbb.png')
            time.sleep(1)
            if random.random() < 0.1:
                driver = restart_driver(driver)
            return _get_single_page_result(page, url, classify_type, classify_arg)
    root = html.fromstring(driver.page_source)
    contain_list = root.xpath(".//div[@class='Js_outerList']/div")

    patent_info = []

    for i in contain_list:

        try:
            state = i.xpath('.//div[2]/div[2]/div/span/text()')[0]
            if '无权' in state:
                continue
        except IndexError:
            continue

        try:
            title = i.xpath('.//div[2]/div[2]/a[2]')[0].xpath('string(.)')
        except IndexError:
            continue
        try:
            abstract = i.xpath('.//div[3]/div[2]/div[1]/div[4]')[0].xpath('string(.)').replace("\n", "").replace(" ",
                                                                                                                 "").strip()
        except IndexError:
            continue

        try:
            classify_code = i.xpath(".//div[3]/div[2]/div[1]/div[2]/p[2]/a/text()")[0]
        except:
            print('出错了')
            driver.save_screenshot('aaaa.png')
            print(driver.page_source)
            time.sleep(1000)
            print('等待处理错误')

        company_list = i.xpath('.//div[3]/div[2]/div[1]/div[1]/p[2]/a')
        patent_info.append({'state': state, 'title': title, 'abstract': abstract, 'company_list': company_list,
                            'classify_code': classify_code})

    if not patent_info:
        return raw_company_list

    if classify_type == 1:
        array_list = patent_classify_model.get_word_array(
            [" ".join(jieba.cut(a['title'] + " " + a['abstract'])) for a in patent_info])
        flag_list = patent_classify_model.get_predict_result(array_list)

        for i in range(0, len(flag_list)):
            if flag_list[i] in classify_arg:
                raw_company_list.extend(patent_info[i]['company_list'])

            else:
                print(patent_info[i]['title'] + ' ' + patent_info[i]['abstract'])
                # raw_company_list = reduce(lambda a, b: a + [b.xpath('string(.)')], raw_company_list, [])
        return raw_company_list
    elif classify_type == 2:
        for i in patent_info:
            if i['classify_code'][:3] in patent_code_blacklist or i['classify_code'][:4] in patent_code_blacklist or i[
                                                                                                                         'classify_code'][
                                                                                                                     :6] in patent_code_blacklist or \
                    i['classify_code'][:7] in patent_code_blacklist or i['classify_code'][
                                                                       :8] in patent_code_blacklist or \
                    i['classify_code'][:9] in patent_code_blacklist or i['classify_code'][
                                                                       :10] in patent_code_blacklist or i[
                                                                                                            'classify_code'][
                                                                                                        :11] in patent_code_blacklist or \
                    i['classify_code'][:12] in patent_code_blacklist or i['classify_code'][
                                                                        :13] in patent_code_blacklist:
                print(i)
                pass
            else:
                # print(i)
                raw_company_list.extend([j.text for j in i['company_list']])
        return raw_company_list


if __name__ == '__main__':
    print(patexplorer_search_word_api('lam', False))
