# coding:utf-8
import time
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common import proxy
from wotou.lib.db_connection import mongodb_connection
from selenium.common import exceptions
import multiprocessing
from tqdm import tqdm
from selenium.webdriver.chrome.options import Options
from wotou.spiders.patent.patent_blacklist import patent_code_blacklist


def get_patent(word):
    try:
        company_list=[]
        patent_info = []
        option = Options()
        # option.add_argument('--headless')
        prefs = {"profile.managed_default_content_settings.images": 2}
        option.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(chrome_options=option)
        driver.get('https://user.zgg.com/reg/login.aspx?gourl=http%3A%2F%2Fwww.zgg.com%2Flist%2Fzllist.html#passsign')
        driver.find_element_by_xpath('//*[@id="tb_user"]').send_keys('13924855979')
        driver.find_element_by_xpath('//*[@id="tb_password"]').send_keys('asd123456')
        driver.find_element_by_xpath('//*[@id="login1"]').click()
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="search-content"]').send_keys(word)
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="search-button"]').click()
        time.sleep(1)
        windows = driver.window_handles
        driver.switch_to.window(windows[-1])
        if '专利不存在' in driver.page_source:
            print(word,None)
            driver.close()
            windows = driver.window_handles
            driver.switch_to.window(windows[-1])
            driver.close()
            return word,None
        WebDriverWait(driver, 15).until(EC.text_to_be_present_in_element((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/div/div[1]/a[1]/span'),u'申请日'))
        url=driver.current_url
        pat_num=int(driver.find_element_by_xpath('/html/body/div[4]/div/div[2]/div[2]/div/p/span').text)
        if pat_num>500:
            print(word,company_list)
            driver.close()
            windows = driver.window_handles
            driver.switch_to.window(windows[-1])
            driver.close()
            return word, company_list
        pages=int((pat_num - 1) / 10) + 1
        print('搜索',word)
        for i in tqdm(range(1,pages+1)):
            url0=url+'&p='+str(i)
            driver.get(url0)
            if pat_num-10*i>=0:
                pn=10
            else:
                pn=pat_num-10*i+10
            for patent in range(1,pn+1):
                classify_num=driver.find_element_by_xpath('/html/body/div[4]/div/div[2]/div[2]/ul/li[%s]/div[1]/a/div[3]/p[1]/span'%patent).text
                raw_company_list=driver.find_element_by_xpath('/html/body/div[4]/div/div[2]/div[2]/ul/li[%s]/div[1]/a/div[2]/p[3]/span'%patent).text
                abstract= driver.find_element_by_xpath('/html/body/div[4]/div/div[2]/div[2]/ul/li[%s]/div[1]/div/p/b' % patent).text
                raw_company_list=raw_company_list.split(' ')
                if not patent_filter(classify_num):
                    print(classify_num)
                    pass
                else:
                    company_list.extend(raw_company_list)
        final_list=list(set([company_name for company_name in company_list if company_name and '公司' in company_name]))
        print(final_list)
        while driver:
            driver.close()
        return word, final_list
    except Exception as e:
        print(e)
        driver.close()
        windows = driver.window_handles
        driver.switch_to.window(windows[-1])
        driver.close()
        return get_patent(word)


def patent_filter(classify_num):
    if classify_num[:3] in patent_code_blacklist:
        return False
    elif classify_num[:4] in patent_code_blacklist:
        return False
    elif classify_num[:6] in patent_code_blacklist:
        return False
    elif classify_num[:7] in patent_code_blacklist:
        return False
    elif classify_num[:8] in patent_code_blacklist:
        return False
    elif classify_num[:9] in patent_code_blacklist:
        return False
    elif classify_num[:10] in patent_code_blacklist:
        return False
    elif classify_num[:11] in patent_code_blacklist:
        return False
    elif classify_num[:12] in patent_code_blacklist:
        return False
    elif classify_num[:13] in patent_code_blacklist:
        return False
    else:
        return True


if __name__ == '__main__':
    get_patent('数据库 电脑')