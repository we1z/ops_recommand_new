# coding:utf-8
'''
专利探索者爬虫，request版，目前不再使用
'''
import time
from wotou.lib.db_connection import mongodb_connection
import urllib
from wotou.spiders.patent.patent_classify import PatentClassification
from lxml import html
import jieba
import random
from functools import reduce
from wotou.spiders.util.proxies import abuyun_proxy
import requests
import json
from wotou.spiders.patent.get_cookies import get_patexplorer_cookies
from wotou.spiders.patent.patent_blacklist import patent_code_blacklist


patent_classify_model = None
driver = None

cookies = 'UM_distinctid=160b5c9ea2b86e-0f81699eb6d0a6-3a75045d-1fa400-160b5c9ea2ccef; _uab_collina=151487841034773255387992; _umdata=0712F33290AB8A6D0AFA3937044BAD2A94B14BA01AD546BBB32315FABBDE022DDFE77A403FC4AF98CD43AD3E795C914CA04E1DF45472C718EC6C1C4EF32295CB; CNZZDATA1261546263=96610399-1514875873-null%7C1521597388; JSESSIONID=519C43272C58D4B7962D8F945AD78495; PD=ba70cd4c0fbbb14b0d0b06de94282e655b985d7c029c57e8fd3ee49f2150bcdd63ad0dc9584e7e9f; yunsuo_session_verify=0ac7001f6a6b81ed4b9de2cf7a77f305'
list_postfix = 'OTRhZWMwZGE0NDAxMTA4Y2FiYjQwYTYyZjZjMzljNGM='


def get_new_cookies():
    '''
    获取新cookies
    :return:
    '''
    global cookies, list_postfix

    if cookies:
        time.sleep(3)
        mongodb_connection('company')['patexplorer_account'].delete_many({})
        cookies=None

    for i in mongodb_connection('company')['patexplorer_account'].find().limit(1):
        cookies = reduce(lambda a, b: a + b['name'] + '=' + b['value'] + ';', i['cookies'], '')
        list_postfix = i['list_postfix']

    if not cookies:

        # get_patexplorer_cookies(1)
        return get_new_cookies()
        # if random.random() < 0.3:
        #     cookies = None


def detail(word,ip=False,page=1):
    '''
    获取指定关键词指定页数的专利
    :param word:关键词
    :param page:页数
    :return:专利列表
    '''
    global cookies
    url_encode_word = urllib.parse.urlencode({'q': word})

    headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "*/*", "Accept-Encoding": "gzip, deflate",
               "Accept-Language": "zh-CN,zh;q=0.8", "Connection": "keep-alive",
               "Content-Length": str(72 + len(url_encode_word)),
               # "Cookie": "UM_distinctid=1600a72c449d49-0626dd071c6637-3970065f-1fa400-1600a72c44ab49; yunsuo_session_verify=7e47cad40bf28ed8c303c6cbf3f1ce55; JSESSIONID=4506DADEE82F1849C6FB8DD60EE2059A; CNZZDATA1261546263=2113758036-1496212147-http%253A%252F%252Fwww.baiten.cn%252F%7C1513904387",
               "Cookie": cookies,
               "Host": "www.patexplorer.com", "Origin": "http://www.patexplorer.com",
               "Referer": "http://www.patexplorer.com/results/s.html?sc=7&" + url_encode_word + "&fq=&type=s&sort=&sortField=",
               "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
               "X-Requested-With": "XMLHttpRequest"}
    data = {"sc": "7", "q": word, "sort": "", "sortField": "", "fq": "", "pageSize": "100", "pageIndex": str(page),
            "type": "s",
            "merge": "no-merge"}
    try:
        if ip:
            r = requests.post('http://www.patexplorer.com/results/list/' + list_postfix, headers=headers, data=data,
                              proxies={'http':ip},
                              timeout=10)
        else:
            r = requests.post('http://www.patexplorer.com/results/list/' + list_postfix, headers=headers, data=data,timeout=10)
        if r.status_code == 200:
            j = json.loads(r.text)
        else:
            print(1)
            get_new_cookies()
            return detail(word,ip,page)
    except (requests.exceptions.ReadTimeout, json.decoder.JSONDecodeError, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError):
        print(1)
        get_new_cookies()
        return detail(word,ip,page)
    if j['cubePatentSearchResponse']['qTime']:
        patent_list = j['cubePatentSearchResponse']['documents']
        return patent_list
    else:
        return detail(word,ip,page)


def count(word,ip=False):
    '''
    获取指定关键词的专利个数
    :param word:
    :return:
    '''
    global cookies
    url_encode_word = urllib.parse.urlencode({'q': word})

    headers = {"Content-Type": "application/x-www-form-urlencoded", "Accept": "*/*", "Accept-Encoding": "gzip, deflate",
               "Accept-Language": "zh-CN,zh;q=0.8", "Connection": "keep-alive",
               "Content-Length": str(19 + len(url_encode_word)),
               # "Cookie": "UM_distinctid=1600a72c449d49-0626dd071c6637-3970065f-1fa400-1600a72c44ab49; yunsuo_session_verify=7e47cad40bf28ed8c303c6cbf3f1ce55; JSESSIONID=4506DADEE82F1849C6FB8DD60EE2059A; CNZZDATA1261546263=2113758036-1496212147-http%253A%252F%252Fwww.baiten.cn%252F%7C1513904387",
               "Cookie": cookies,
               "Host": "www.patexplorer.com", "Origin": "http://www.patexplorer.com",
               "Referer": "http://www.patexplorer.com/results/s.html?sc=7&" + url_encode_word + "&fq=&type=s&sort=&sortField=",
               "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36",
               "X-Requested-With": "XMLHttpRequest"}
    data = {"sc": "7", "q": word, "fq": "", "pageSize": ""}
    try:
        if ip:
            r = requests.post('http://www.patexplorer.com/results/filter', headers=headers, data=data,
                              proxies={'http': ip},
                              timeout=10)
        else:
            r = requests.post('http://www.patexplorer.com/results/filter', headers=headers, data=data,timeout=10)
        if r.status_code == 200:
            j = json.loads(r.text)
        else:
            get_new_cookies()
            print(0)
            return count(word,ip)
    except (requests.exceptions.ReadTimeout, json.decoder.JSONDecodeError, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError):
        get_new_cookies()
        print(0)
        return count(word,ip)
    if j['facetPivots']:
        if j['facetPivots']['country'] == {}:
            return 0
        else:
            c = sum(j['facetPivots']['country']['cn'].values())
            if 'cn_um' in j['facetPivots']['country']['cn']:
                c = c - j['facetPivots']['country']['cn']['cn_um']

            return c
    else:
        return count(word,ip)


def patexplorer_search_word_api(word, ip=False,is_limit=False, classify_type=1, classify_arg=None):
    '''
    专利搜索入口
    :param classify_arg: 分类方法带的参数
    :param word:关键词
    :param is_limit: 是否限制只搜索个数小于500的专利
    :param classify_type: 专利分类的方法，1为svm，2为分类号黑名单法
    :return: 关键词，相关公司名称
    '''
    global patent_classify_model
    if classify_type==1:
        patent_classify_model=PatentClassification()
    print("搜索" + word)
    company_list = []

    num = count(word,ip)

    if is_limit and int(num) > 500:
        return word, company_list
    if int(num) == 0:
        return word, None

    for i in range(1, min(100, int((int(num) - 1) / 100) + 2)):

        company_list.extend(_get_single_page_result(word, i, classify_type,ip,classify_arg))

    print([company_name for company_name in company_list if '公司' in company_name])
    return word, list(set([company_name for company_name in company_list if '公司' in company_name]))


def _get_single_page_result(word, page, classify_type, ip=False,classify_arg=None):
    '''
    获取指定关键词和页数对应的公司
    :param word: 关键词
    :param page: 页数
    :param is_svm: 所需的分类
    :return:
    '''
    raw_company_list = []

    contain_list = detail(word,ip,page)

    patent_info = []

    for i in contain_list:

        try:
            state = i['field_values']['lsnt']
            if '无权' in state:
                continue
            title = i['field_values']['ti']
            abstract = i['field_values']['ab']
            classify_code = i['field_values']['ic1']
            company_list = i['field_values']['pa']
        except IndexError:
            continue

        patent_info.append({'state': state, 'title': title, 'abstract': abstract, 'company_list': company_list,
                            'classify_code': classify_code})

    if not patent_info:
        return raw_company_list

    if classify_type == 1:
        array_list = patent_classify_model.get_word_array(
            [" ".join(jieba.cut(a['title'] + " " + a['abstract'])) for a in patent_info])
        flag_list = patent_classify_model.get_predict_result(array_list)

        for i in range(0, len(flag_list)):
            if flag_list[i] in classify_arg:
                raw_company_list.extend(patent_info[i]['company_list'])

            else:
                print(patent_info[i]['title'] + ' ' + patent_info[i]['abstract'])
                # raw_company_list = reduce(lambda a, b: a + [b.xpath('string(.)')], raw_company_list, [])
        return raw_company_list
    elif classify_type == 2:
        for i in patent_info:
            if i['classify_code'][:3] in patent_code_blacklist or i['classify_code'][:4] in patent_code_blacklist or  i['classify_code'][:6] in patent_code_blacklist or i['classify_code'][:7] in patent_code_blacklist or i['classify_code'][:8] in patent_code_blacklist or \
                    i['classify_code'][:9] in patent_code_blacklist or i['classify_code'][:10] in patent_code_blacklist or i['classify_code'][:11] in patent_code_blacklist or i['classify_code'][:12] in patent_code_blacklist or i['classify_code'][:13] in patent_code_blacklist:
                print(i)
                pass
            else:
                # print(i)
                raw_company_list.extend(i['company_list'])
        return raw_company_list


#if __name__ == '__main__':
    #print(patexplorer_search_word_api('公司', False, 2))
    # index = 0
    # with open('list',encoding='UTF-8') as f:
    #
    #     for i in f.readlines():
    #         # index += 1
    #         # if index < 300 or index>400:
    #         #     continue
    #         i = i.replace("\n", "").strip()
    #         # company_list.append(soopat_search_word_api(i, True))
    #         patexplorer_search_word_api(i, False, [6])
    #         time.sleep(2)

    # for i in mongodb_connection_local('news')['shengwugu'].find({"time":"2017-08-21"}):
    #     for word in i['keyword']:
    #         patexplorer_search_word_api(word,True)
    #         time.sleep(1)

    # pool = multiprocessing.Pool(processes=4)
    # process_list = []
    # index = 0
    # for i in mongodb_connection_local('news')['shengwugu'].find({"time":"2017-08-21"}):
    #     for word in i['keyword']:
    #
    #         process_list.append(pool.apply_async(patexplorer_search_word_api, (word, True)))
    #         time.sleep(15)
    #
    # pool.close()
    # pool.join()