from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from PIL import Image
from wotou.spiders.util.identify_code import handle_identify_code_api
import random
import string
import time
from selenium.common import exceptions
from wotou.lib.db_connection import mongodb_connection


driver=None
a1=['张','金','李','王','赵','陈','黄','何','史','廖','林','白','蔡']
a2=['玉','明','龙','芳','军','玲','波','冰','一','立','雯','凤','陵','仲']
a3=['','立','玲','','国','','','','','','']
company=['北京拓尔思信息技术股份有限公司', '元亮科技有限公司', '郑州威科姆科技股份有限公司', '多喜爱集团股份有限公司', '山东机客网络技术有限公司', '北京东方广视科技股份有限公司', '西安金源电气股份有限公司', '广州华工百川科技有限公司', '深圳市华润通光电股份有限公司', '深圳市庆丰光电科技有限公司', '陕西凯星电子科技有限责任公司', '湖北百杰瑞新材料股份有限公司', '广州移淘网络科技有限公司', '深圳市标准市场研究有限公司', '湖南东方报业有限公司', '深圳市东信时代信息技术有限公司', '宁波激智科技股份有限公司', '北京速途网络科技股份有限公司', '北京优纳科技有限公司', '湖北鄂信钻石科技股份有限公司', '浙江双飞无油轴承股份有限公司', '东莞市雅路智能家居股份有限公司', '珐玛珈（广州）包装设备有限公司', '深圳市乐普泰科技股份有限公司', '上海西域机电系统有限公司', '众力通源石油天然气工程技术股份有限公司', '广州珐玛珈智能设备股份有限公司', '陕西沃泰科技股份有限公司', '北京诚品快拍物联网科技股份有限公司', '晨光生物科技集团股份有限公司', '惠州亿纬锂能股份有限公司', '恒泰艾普集团股份有限公司', '深圳市金百泽电子科技股份有限公司', '南昌百特生物高新技术股份有限公司', '广州移盟数字传媒科技有限公司', '湖南恒润高科股份有限公司', '深圳市大富豪实业发展有限公司', '深圳市火乐科技发展有限公司', '辽宁优格生物科技股份有限公司', '西安达刚路面机械股份有限公司', '广东昱嘉华讯科技发展有限公司', '哈尔滨安天科技股份有限公司', '郸城财鑫糖业有限责任公司', '南京普朗医疗设备有限公司', '深圳市康尔诺生物技术有限公司', '深圳市中天博日科技有限公司', '江苏徕兹智能装备科技股份有限公司', '深圳扑象文化传播有限公司', '上海嘉洁环保工程有限公司', '江苏通灵电器股份有限公司', '深圳市哲扬科技有限公司', '深圳市万泉河科技股份有限公司', '深圳市德厚科技有限公司', '深圳市力函科技有限公司', '深圳市爱思宝艺术板业有限公司', '深圳洛赛声学技术有限公司', '深圳市汉华安道科技有限责任公司', '深圳市为创科技有限公司', '盛景网联科技股份有限公司', '北京沃尔德金刚石工具股份有限公司', '伟景行科技股份有限公司', '新疆仪尔高新农业开发有限公司', '北京中基联盟管理咨询有限公司', '北京奇步天下科技有限公司', '北京海峰科技有限责任公司', '中电科华云信息技术有限公司', '捷众信息咨询服务有限公司', '深圳刷宝科技有限公司', '济南翼菲自动化科技有限公司', '上海冰鉴信息科技有限公司', '北京停简单信息技术有限公司', '什马互联网金融信息服务（上海）有限公司', '深圳市前海泽金互联网金融服务有限公司', '清陶（昆山）能源发展有限公司', '上海普道财务咨询有限公司', '唯存（上海）网络科技有限公司', '武汉两点十分文化传播有限公司', '胶囊时尚（厦门）网络科技有限公司', '北京快洁筷好味科技有限公司', '北京零零无限科技有限公司', '西安图灵网络技术有限公司', '一飞智控（天津）科技有限公司', '深圳鱼羊美厨网络科技有限公司', '路肯（上海）医疗科技有限公司', '北京凯峰数据科技有限公司', '易信视界（北京）管理咨询有限公司', '苏州比可网络科技有限公司', '北京快服务科技有限公司', '北京吆喝科技有限公司', '舟谱数据技术南京有限公司', '北京蜂巢互保网络科技有限公司', '杭州米谟科技有限公司', '北京费马科技有限公司', '北京方方土动漫科技有限公司', '北京磨刀刻石科技有限公司', '北京希瑞亚斯科技有限公司', '敢玩（北京）科技有限公司', '杭州宜宝康健网络科技有限公司', '北京诺信创联科技有限公司', '深圳盈富斯科技有限公司']


def handle_identify_code(elementID,driver,codetype):
    img = driver.find_element_by_id(elementID)
    driver.save_screenshot('screenshot.png')
    left = int(img.location['x'])
    top = int(img.location['y'])
    right = int(img.location['x'] + img.size['width'])
    bottom = int(img.location['y'] + img.size['height'])

    # 打开验证码网页截图，截图验证码
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')

    # 使用云打码处理图片
    result = handle_identify_code_api(filename='code.png', codetype=codetype)
    return result


def restart_driver(driver):
    if driver:
        driver.quit()
    option = Options()
    option.add_argument("user-agent=Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0")
    # option.add_argument('--headless')
    driver = webdriver.Chrome(chrome_options=option)
    try:
        driver.get('http://www.51ym.me/User/Login.aspx')
        reciver = login_to_ym(driver)
    except exceptions.TimeoutException:
        pass
    return driver,reciver


def login_to_ym(driver):
    reciver = driver.current_window_handle
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="username"]').send_keys('wootou')
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="password"]').send_keys('123456')
    result = handle_identify_code('CaptchaImg', driver, 4004)
    driver.find_element_by_xpath('//*[@id="verify"]').send_keys(result)
    time.sleep(1)
    try:
        driver.find_element_by_xpath('//*[@id="SubmitBtn"]').click()
        time.sleep(1)
    except exceptions.WebDriverException:
        pass
    return reciver


def get_project(driver):
    driver.get('http://www.51ym.me/User/MobileSMSCode.aspx')
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="seachitembtn"]').click()
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="Keywords"]').send_keys('专利汇')
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="seachbtn"]').click()
    time.sleep(1.5)
    driver.find_element_by_xpath('//*[@id="MobileItemModalfooter"]/button').click()
    time.sleep(1.5)
    return driver


def get_identify_phone(driver):
    driver.find_element_by_xpath('//*[@id="GetMobileNoBtn"]').click()
    time.sleep(1)
    phone = driver.find_element_by_xpath('//*[@id="MobileNo"]').get_attribute('value')
    while not phone:
        time.sleep(1)
        phone = driver.find_element_by_xpath('//*[@id="MobileNo"]').get_attribute('value')
    return driver, phone


def register(driver, phone):
    Js = 'window.open("https://www.patenthub.cn/user/register/one.html")'
    driver.execute_script(Js)
    driver.switch_to.window(driver.window_handles[1])
    driver.find_element_by_xpath('//*[@id="account"]').send_keys(phone)
    time.sleep(1)
    real_name = random.choice(a1)+random.choice(a2)+random.choice(a3)
    driver.find_element_by_xpath('//*[@id="realName"]').send_keys(real_name)
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="password"]').send_keys('asd123456')
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="rePass"]').send_keys('asd123456')
    time.sleep(1)
    enterpriseName = random.choice(company)
    driver.find_element_by_xpath('//*[@id="enterpriseName"]').send_keys(enterpriseName)
    result = handle_identify_code('login_VcodeP', driver, 1004)
    driver.find_element_by_id('code').send_keys(result)
    time.sleep(1)
    driver.find_element_by_css_selector('#submit input').click()
    while '验证码错误' in driver.page_source:
        driver.find_element_by_id('login_VcodeP').click()
        result = handle_identify_code('login_VcodeP', driver, 1004)
        driver.find_element_by_id('code').clear()
        driver.find_element_by_id('code').send_keys(result)
        time.sleep(1)
        driver.find_element_by_css_selector('#submit input').click()
    return driver


def get_identify_code(driver,reciver):
    driver.switch_to.window(reciver)
    if '短信获取失败' in driver.page_source:
        return None, driver
    identify_code = ''
    times = 0
    while not identify_code and times <= 75:
        identify_code = driver.find_element_by_id('VerificationCode').get_attribute('value')
        time.sleep(1)
        times += 1
    return identify_code, driver


def register_next(driver, reciver, identify_code):
    for handle in driver.window_handles:
        if handle != reciver:
            register = handle
    driver.switch_to.window(register)
    driver.find_element_by_xpath('//*[@id="signInTwo_code"]').send_keys(identify_code)
    time.sleep(1)
    driver.find_element_by_css_selector('div[onclick="comfirm_signInTwo()"]').click()
    time.sleep(1)
    return driver,register


def write_account(phone,pwd):
    mongodb_connection('company')['patenthub_acc'].insert({'acc': phone},{'pw':pwd},{'state': 'ok'},{'last_used_time': time.time()})


def get_account_api(n):
    global driver
    if not driver:
        driver, reciver = restart_driver(driver)
        driver = get_project(driver)
    while n > 0:
        try:
            driver, phone = get_identify_phone(driver)
            driver = register(driver, phone)
            identify_code, driver = get_identify_code(driver, reciver)
            if not identify_code:
                driver.find_element_by_id('ReleaseMobileNoBtn').click()
                for handle in driver.window_handles:
                    print(handle)
                    registe = None
                    if handle != reciver:
                        registe = handle
                print(registe)
                if registe:
                    driver.switch_to.window(registe)
                    driver.close()
                    driver.switch_to.window(reciver)
                continue
            driver, register_win = register_next(driver, reciver, identify_code)
            print('注册号码:', phone)
        except Exception as e:
            print(e)
            driver.get('http://www.51ym.me/User/MobileSMSCode.aspx')
            continue
        # if '恭喜' in driver.page_source:
        mongodb_connection('company')['patenthub_acc'].insert({'acc': phone, 'pw': 'asd123456', 'state': 'ok',
                                                                  'last_used_time': time.time()})
        driver.close()
        driver.switch_to.window(reciver)
        n -= 1
        # else:
        #     driver.close()
        time.sleep(1)

if __name__ == '__main__':
    get_account_api(10)