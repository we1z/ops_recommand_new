# coding:utf-8
'''
专利分类器：
0 tmt
1 生物医药 医疗器械
2 化工 材料 环境 农业 化肥
3 食品 中药 保健品 化妆品 养殖 种植
4 机械 采掘 传统制造
5 工程 建筑
'''

import jieba
import re
import gc
from sklearn import svm
import random
import Levenshtein
import numpy
import pandas
import hashlib
import shelve
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.externals import joblib

import os
import inspect
import sys

med_list = []
tmt_list = []
hua_list = []
shipin_list = []

sign_list = []
sentence_list = []
med_test_list = []


def tf_idf_cal(content_train_set):
    """计算tfidf值
    返回词典以及每篇文章的tfidf值列表
    """
    vectorizer = CountVectorizer()  # 该类会将文本中的词语转换为词频矩阵，矩阵元素a[i][j] 表示j词在i类文本下的词频
    transformer = TfidfTransformer()  # 该类会统计每个词语的tf-idf权值
    tfidf = transformer.fit_transform(
        vectorizer.fit_transform(content_train_set))  # 第一个fit_transform是计算tf-idf，第二个fit_transform是将文本转为词频矩阵
    word = vectorizer.get_feature_names()  # 获取词袋模型中的所有词语

    del vectorizer
    gc.collect()
    weight = tfidf.toarray()  # 将tf-idf矩阵抽取出来，元素a[i][j]表示j词在i类文本中的tf-idf权重
    weight = weight.astype(numpy.float16)
    del tfidf
    gc.collect()
    return word, weight, transformer.idf_


class PatentClassification:
    def __init__(self, test=False):
        '''
        初始化，test模式为测试模型准确度
        :param test:
        '''
        self.test_result_list = []
        self.test_sentence_list = []

        with open(os.path.abspath(os.path.dirname(__file__)) + '/md', encoding='UTF-8') as f:

            md5_origin = f.read()
            print(md5_origin)

        raw_patent_content = open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/gongcheng',
                                  encoding='UTF-8').read() + \
                             open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/hua', encoding='UTF-8').read() + \
                             open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/jixie',
                                  encoding='UTF-8').read() + \
                             open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/shipin',
                                  encoding='UTF-8').read() + \
                             open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/tmt', encoding='UTF-8').read() + \
                             open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/yiyao', encoding='UTF-8').read()

        md5_now = hashlib.md5(raw_patent_content.encode('utf-8')).hexdigest()
        print(md5_now)
        # 若专利语料已改变,则用重新进行模型计算
        if md5_now != md5_origin or test:

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/tmt', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)

                    if not test:
                        med_list.append(fenci)
                        sign_list.append(0)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(0)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(0)
                            sentence_list.append(l)

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/yiyao', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)
                    if not test:
                        med_list.append(fenci)
                        sign_list.append(1)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(1)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(1)
                            sentence_list.append(l)

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/hua', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)

                    if not test:
                        med_list.append(fenci)
                        sign_list.append(2)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(2)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(2)
                            sentence_list.append(l)

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/shipin', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)

                    if not test:
                        med_list.append(fenci)
                        sign_list.append(3)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(3)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(3)
                            sentence_list.append(l)

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/jixie', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)

                    if not test:
                        med_list.append(fenci)
                        sign_list.append(4)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(4)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(4)
                            sentence_list.append(l)

            with open(os.path.abspath(os.path.dirname(__file__)) + '/corpus/gongcheng', encoding='UTF-8') as f:
                for l in f.readlines():
                    fenci = self.seg_line(l)
                    if not test:
                        med_list.append(fenci)
                        sign_list.append(5)
                        sentence_list.append(l)
                    else:
                        if random.random() < 0.1:
                            self.test_result_list.append(5)
                            self.test_sentence_list.append(l)
                        else:
                            med_list.append(fenci)
                            sign_list.append(5)
                            sentence_list.append(l)

            word_list = med_list + tmt_list + hua_list + shipin_list

            if not test:
                print("等待训练")
                self.word, weight, self.idf = tf_idf_cal(word_list)

                d = shelve.open(os.path.abspath(os.path.dirname(__file__)) + '/tfidfmatrix', flag='c', protocol=2,
                                writeback=True)
                try:
                    d['word'] = self.word
                    d['idf'] = self.idf  ##存tdidf值到shelve
                finally:
                    d.close()

                self.clf = svm.LinearSVC()
                self.clf.fit(weight, sign_list)
                joblib.dump(self.clf, "train_model")
                print("训练完成")
                with open(os.path.abspath(os.path.dirname(__file__)) + '/md', "w", encoding='UTF-8') as f:
                    f.write(md5_now)
            else:
                print("测试中")
                self.word, weight, self.idf = tf_idf_cal(word_list)
                self.clf = svm.LinearSVC()
                self.clf.fit(weight, sign_list)
        # 若专利语料无改变,则直接用以前模型计算
        else:
            print("从模型导入")
            d = shelve.open(os.path.abspath(os.path.dirname(__file__)) + '/tfidfmatrix', flag='r', protocol=2,
                            writeback=False)
            self.word = d['word']
            self.idf = d['idf']
            d.close()
            self.clf = svm.LinearSVC()
            self.clf = joblib.load("train_model")

    def seg_line(self, l):
        '''
        分词和简单预处理
        :param l:
        :return:
        '''
        fenci = " " + " ".join(
            jieba.cut(re.sub('-|：|，|。|\.\.\.|、|:|\(|\)|,|（|）|‑|；|”|“|;|\?|/|~|～|％', "", l.strip()))) + " "
        fenci = re.sub(" \w*\d\.?\w* ", " ", fenci)
        fenci = re.sub(" \w{9,100} ", " ", fenci)
        return fenci

    def get_word_idf(self):
        return self.word, self.idf

    def get_predict_result(self, array):
        '''
        获取预测结果
        :param array:特征化后的专利表示
        :return:预测结果，
        0 tmt
        1 生物医药 医疗器械
        2 化工 材料 环境 农业 化肥
        3 食品 中药 保健品 化妆品 养殖 种植
        4 机械 采掘 传统制造
        5 工程 建筑
        '''
        return self.clf.predict(array)

    def get_word_array(self, fenci_list):
        '''
        将分词后的专利特征化
        :param fenci_list:分词后的专利list
        :return:特征化的list
        '''
        vectorizer = CountVectorizer()  # 该类会将文本中的词语转换为词频矩阵，矩阵元素a[i][j] 表示j词在i类文本下的词频
        transformer = TfidfTransformer(norm=None, use_idf=False)
        tf = transformer.fit_transform(vectorizer.fit_transform(fenci_list))
        word_list = vectorizer.get_feature_names()
        weight_list = tf.toarray()

        array_list = []
        for weight in weight_list:
            array = numpy.array([0] * len(self.idf))

            for i in range(0, len(weight)):
                if weight[i] > 0:
                    try:
                        index = self.word.index(word_list[i])
                    except ValueError:
                        continue
                    if index != -1:
                        array[index] = weight[i] * self.idf[index]

            array_list.append(array)
        return array_list

    def run_test(self):
        '''
        test模式下，测试模型的准确率
        :return:
        '''
        assert self.test_result_list and self.test_sentence_list
        correct_num = 0
        for k, v in dict(zip(self.test_sentence_list, self.test_result_list)).items():

            fenci = self.seg_line(k)
            if fenci == '':
                continue
            result = self.get_predict_result(self.get_word_array([fenci]))[0]
            if result == v:
                correct_num += 1
            else:
                print(k.replace('\n', ''))
                print('预测值:', result, '正确值', v)
                print('\n')

        print(correct_num / len(self.test_result_list))


if __name__ == '__main__':
    patent_classify_model = PatentClassification()
    print(patent_classify_model.get_predict_result(patent_classify_model.get_word_array(
        ['茶 黄素 作为 饲料 添加剂 的 用途 及 相应 饲料   摘要 ： 本发明 提供 的 茶 黄素 作为 饲料 添加剂 的 用途 及 相应 饲料 ， 属于 饲料 添加剂 技术 领域 。',
         '从而 避免 由于 使用 抗生素 而 造成 的 药物 残留 等 问题 ； 可 提高 养殖 动物 抗氧化 能力 ， 从而 避免 添加 化学合成 的 抗氧化剂 而 带来 的 毒副作用 。 可 广泛 用于 饲料 添加剂'])))
