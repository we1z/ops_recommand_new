# coding:utf-8
'''
将天眼查和企查查都找不到企业信息的企业名称加入黑名单
'''

from wotou.lib.db_connection import mongodb_connection
from wotou.lib.db_connection import read_col
import pymongo


def add_not_found_blacklist_api():
    '''
    将天眼查和企查查都找不到企业信息的企业名称加入黑名单,避免重复查找浪费资源
    :return: None
    '''
    db = mongodb_connection('company')
    unfinished_collection = db['unfinished_company']
    blacklist_collection = db['blacklist_company_name']
    blacklist = read_col('company', 'blacklist_company_name', 'company')

    for i in unfinished_collection.find({'状态': 'not found'}):
        if i['企业全称'] not in blacklist:
            try:
                blacklist_collection.insert({'company': i['企业全称']})
                print('加入黑名单', i['企业全称'])
                unfinished_collection.delete_many({'企业全称': i['企业全称']})
            except pymongo.errors.DuplicateKeyError:
                continue


if __name__ == '__main__':
    add_not_found_blacklist_api()
