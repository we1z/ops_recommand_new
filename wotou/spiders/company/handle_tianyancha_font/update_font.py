# coding:utf-8

'''
windows版
'''
import requests
from lxml import etree
import re
from PIL import ImageFont, ImageDraw, Image, ImageChops
from itertools import chain
import os
from fontTools.ttLib import TTFont
from fontTools.unicode import Unicode
import argparse
from multiprocessing import Pool
import numpy
import sys


def read_totalfontdict():
    #读取本地总字库，返回字库字典
    with open('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/total_fontdict.txt','r',encoding='GB2312') as f:
        content=f.read()
        return eval(content)



def find_font_css():
    #找到天眼查字体文件的地址
    r=requests.get('https://www.tianyancha.com/').text
    html=etree.HTML(r)
    href=html.xpath('//@href')
    for i in href:
        if re.match('.*?font\.css',i):
            return i
        else:
            pass


def download_ttf_svg():
    #下载天眼查字体的TTF格式，并返回SVG格式字体的地址
    r=requests.get(find_font_css()).text
    pattern = re.compile('\((.*?)\)', re.S)
    list = re.findall(pattern, r)
    for i in list:
        if re.match('.*?ttf.*?', i):
            r = requests.get(i[1:-1])
            with open("E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/tyc-num.ttf", "wb") as code:
                code.write(r.content)
        elif re.match('.*?svg#tic.*?', i):
            svgfile=i[1:-1]
        else:
            pass
    return svgfile



def ttf2png():
    #将下载得到的TTF文件里存在的字形转换成图片
    parser = argparse.ArgumentParser(description='Turn otf to images.')
    args = parser.parse_args()
    args.otf = 'E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/tyc-num.ttf'
    args.image_size = 128
    args.font_size = 128
    ttf = TTFont(args.otf, 0, verbose=0, allowVID=0, ignoreDecompileErrors=True, fontNumber=-1)
    # Check which word in this font
    chars = chain.from_iterable([y + (Unicode[y[0]],) for y in x.cmap.items()] for x in ttf["cmap"].tables)
    char_list = (list(chars))
    # load font
    font = ImageFont.truetype(args.otf, args.font_size)
    font0=ImageFont.truetype('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/yahei_mono.ttf', args.font_size)
    # draw font
    for i in char_list:
        char_unicode = chr(i[0])
        origin = [0, 0]
        txt = Image.new('RGB', (args.image_size, args.image_size), (255, 255, 255))
        draw = ImageDraw.ImageDraw(txt)
        size = draw.textsize(char_unicode, font=font)
        if args.image_size - size[0] < 0:
            origin[0] = args.image_size - size[0]
        else:
            origin[0] = 0
        if args.image_size - size[1] < 0:
            origin[1] = args.image_size - size[1]
        else:
            origin[1] = 0
        draw.text(tuple(origin), char_unicode, font=font, fill=0)
        # check if the image is whole white
        if not ImageChops.invert(txt).getbbox():
            continue
        name=chr(i[0]) + ".png"
        txt.save('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font/%s'%name)

    for i in char_list:
        char_unicode = chr(i[0])
        if 48<=ord(char_unicode)<=57:
            pass
        else:
            origin = [0, 0]
            txt = Image.new('RGB', (args.image_size, args.image_size), (255, 255, 255))
            draw = ImageDraw.ImageDraw(txt)
            size = draw.textsize(char_unicode, font=font0)
            if args.image_size - size[0] < 0:
                origin[0] = args.image_size - size[0]
            else:
                origin[0] = 0
            if args.image_size - size[1] < 0:
                origin[1] = args.image_size - size[1]
            else:
                origin[1] = 0
            draw.text(tuple(origin), char_unicode, font=font0, fill=0)

            # check if the image is whole white

            if not ImageChops.invert(txt).getbbox():
                continue
            name = chr(i[0]) + ".png"
            txt.save('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0/%s' % name)


def initialization():
    #生成字体转图片所需的文件夹，和本地字库的TXT文件
    print('初始化文件夹')
    if os.path.exists('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font'):
        for file in os.listdir('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font/'):
            os.remove('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font/' + file)
    else:
        os.makedirs('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font')
    if os.path.exists('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0'):
        for file in os.listdir('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0/'):
            os.remove('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0/' + file)
    else:
        os.makedirs('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0')
    if os.path.exists('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/total_fontdict.txt'):
        pass
    else:
        # os.mknod('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/total_fontdict.txt')
        with open('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/total_fontdict.txt', 'w') as f:
            f.write('{}')



def get_svg_path():
    #得到SVG字体的字形路径信息，保存为字典返回
    dict={}
    r=requests.get(download_ttf_svg()).text
    html=etree.HTML(r)
    glyph_names=html.xpath('//glyph/@glyph-name')
    for i in glyph_names:
        dict[i]=html.xpath('//glyph[@glyph-name="%s"]/@d'%i)[0]
    return dict


def distance(a, b):
    '''计算两个矩阵之间的距离'''
    L=[]
    for i in range(128):
        l=list(map(lambda x: abs(x[0]-x[1]), zip(a[i], b[i])))
        L.append(l)
    d = numpy.linalg.norm(L)
    return abs(d)


def img2matrix(file):
    #将字体图片转换为只有1和0的矩阵，尺寸是128X128
    image = Image.open(file)
    pixels = image.load()
    L = []
    for y in range(image.width):
        l = []
        for x in range(image.height):
            if sum(pixels[x, y]) / 3 < 128:
                p = 1
                l.append(p)
            else:
                p = 0
                l.append(p)
        L.append(l)
    return L


def compare(file):
    #将输入的字形文件和其他所有字形对比，并将最匹配的字形写入本地字典
    svg_dict = get_svg_path()
    total_fontdict = read_totalfontdict()
    if svg_dict[file[-5]] not in total_fontdict:
        a = img2matrix(file)  # 图片地址自行替换
        characters = os.listdir("E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0/")  # 图片文件夹地址自行替换
        nums = os.listdir("E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/num/")
        dict = {}
        for i in characters:
            b = "E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font0/" + str(i)
            b = img2matrix(b)
            compare = distance(a, b)
            dict[i[0]] = compare
        for i in nums:
            b = "E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/num/" + str(i)
            b = img2matrix(b)
            compare = distance(a, b)
            dict[i[0]] = compare
        result = sorted(dict.items(), key=lambda item: item[1], reverse=False)[0][0]
        print(file, '完成匹配')
        return [svg_dict[file[-5]],result]

    else:
        print('字形已存在')
        pass


def generate_fontdict():
    initialization()
    svg_dict = get_svg_path()
    total_fontdict = read_totalfontdict()
    #生成本地字典，并更新本地总字库
    ttf2png()
    l=['E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font/'+i for i in os.listdir('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/font/')]
    pool=Pool(processes=8)
    for i in pool.map(compare,l):
        if i:
            total_fontdict[i[0]]=i[1]
        else:
            pass
    with open('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/total_fontdict.txt','w') as f:
        f.write(str(total_fontdict))
    local_fontdict={}
    for i in svg_dict:
        local_fontdict[i]=total_fontdict[svg_dict[i]]
    with open('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/local_fontdict.txt','w') as f:
        f.write(str(local_fontdict))


def generate_fontdict_without_ttf():
    svg_dict = get_svg_path()
    total_fontdict = read_totalfontdict()
    local_fontdict={}
    for i in svg_dict:
        local_fontdict[i]=total_fontdict[svg_dict[i]]
    with open('E:/Work/ops_recommand/wotou/spiders/company/handle_tianyancha_font/local_fontdict.txt','w') as f:
        f.write(str(local_fontdict))

if __name__ == '__main__':
    generate_fontdict()
