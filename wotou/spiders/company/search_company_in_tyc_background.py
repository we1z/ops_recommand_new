# coding:utf-8
'''
用天眼查后台爬取企业信息
'''
from wotou.spiders.company.spider_company import search_company_in_tianyancha_background_api
import time
import multiprocessing
import psutil
from multiprocessing import cpu_count
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.company.handle_tianyancha_font.update_font import generate_fontdict,generate_fontdict_without_ttf

if __name__ == '__main__':
    time.sleep(0)
    # MAX_PROCESS_NUM = int(cpu_count()*2)
    MAX_PROCESS_NUM=9
    p_list = []
    while True:
        try:
            generate_fontdict_without_ttf()
        except:
            generate_fontdict()
        if (psutil.cpu_percent(interval=1) < 70 or len(p_list)==0) and len(p_list) < MAX_PROCESS_NUM and mongodb_connection("company")['unfinished_company'].find(
                {'$or': [{"状态": "not started"}, {'状态': 'qichacha not found'}]}).count()>0:
            p = multiprocessing.Process(target=search_company_in_tianyancha_background_api)
            p.start()
            p_list.append(p)
        for i in p_list:
            if not i.is_alive():
                p_list.remove(i)
        print("当前线程数:"+str(len(p_list)))
        time.sleep(5 + 8 * len(p_list))