# coding:utf-8
"""
更新上市公司列表，需从
http://www.sse.com.cn/assortment/stock/list/share/
http://www.szse.cn/szseWeb/ShowReport.szse?SHOWTYPE=xlsx&CATALOGID=1110&tab1PAGENO=1&ENCODE=1&TABKEY=tab1
下载并放置文件到本文件目录
将更新上市公司的货币资金，后代公司，十大股东，母公司的后代公司，并输出树状图
"""
import xlrd
from wotou.lib.db_connection import mongodb_connection
import pymongo
from wotou.lib.db_connection import read_col
import requests
from lxml import etree
from wotou.spiders.company.spider_company import is_state_own_company
from wotou.spiders.company.spider_company import _company_name_strip
from wotou.spiders.company.spider_company import search_company_api
from wotou.spiders.company.spider_company import search_iteration_son_company_of_listed_company_api
from bs4 import BeautifulSoup
import re
from bson.dbref import DBRef
import csv
from wotou.spiders.company.spider_company import get_company_detail_from_db_api

# B股企业和新浪有错的页面
black_list = ['000053',
              '000054',
              '000152',
              '000160',
              '000168',
              '000468',
              '000512',
              '000706',
              '000771',
              '000986',
              '000992',
              '002910']
# 树状图输出格式
html_content = """
        <!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title> Basic example </title>
    <link rel="stylesheet" href="Treant.css">
    <link rel="stylesheet" href="basic-example.css">

</head>
<body>
    <div class="chart" id="basic-example"></div>
    <script src="raphael.js"></script>
    <script src="Treant.js"></script>

    <script src="%(js)s"></script>
    <script>
        new Treant( chart_config );
    </script>
</body>
</html>
        """


def get_info_from_sina(stock_num):
    '''
    从新浪财经获取公司全称，货币资金和十大股东
    :param stock_num: 上市公司代码
    :return: 公司全称，货币资金，十大股东列表，十大股东持股列表
    '''
    url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vCI_CorpInfo/stockid/" + stock_num + ".phtml"
    response = requests.get(url)
    root = etree.HTML(response.content)
    full_name = root.xpath('//table[@id="comInfo1"]/tr[1]/td[2]')[0].text
    url = "http://money.finance.sina.com.cn/corp/go.php/vFD_BalanceSheet/stockid/" + stock_num + "/ctrl/part/displaytype/4.phtml"
    if not full_name:
        return None
    response = requests.get(url)
    root = etree.HTML(response.content)
    is_bank = root.xpath('//table[@id="BalanceSheetNewTable0"]/tbody/tr[4]/td[1]/a')[0].text
    if is_bank == '货币资金':
        money = float(root.xpath('//table[@id="BalanceSheetNewTable0"]/tbody/tr[4]/td[2]')[0].text.replace(",", ""))
    else:
        money = 0
    url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vCI_StockHolder/stockid/" + stock_num + "/displaytype/30.phtml"
    response = requests.get(url)
    root = etree.HTML(response.content)
    table = root.xpath('//table[@id="Table1"]/tbody/tr')
    holder_list = []
    hold_percent_list = []
    for i in range(6, 16):
        if len(table[i].xpath('./td')) < 5:
            print(stock_num + ' 股东信息有误,更换解析库')
            # 部分新浪页面用lxml库会解析错误，所以换用html5lib库解析
            holder_list, hold_percent_list = _use_html5lib_to_parse(response.content)
            break
        holder = table[i].xpath('./td[2]/div')[0].text
        if not holder:
            holder = table[i].xpath('./td[2]/div/a')[0].text
        holder_list.append(holder)
        hold_percent = table[i].xpath('./td[4]/div')[0].text
        if not hold_percent:
            hold_percent = table[i].xpath('./td[4]/div/a')[0].text
        hold_percent_list.append(float(hold_percent))

    return full_name, money, holder_list, hold_percent_list


def _use_html5lib_to_parse(content):
    '''
    用htmllib库解析html
    :param content: html内容
    :return: 股东列表，股东占比列表
    '''
    soup = BeautifulSoup(content, 'html5lib')

    table = soup.find(id='Table1')

    tr = table.find_all('tr')

    holder_list = []
    hold_percent_list = []
    for i in tr:
        td = i.find_all('td')
        if len(td) < 5:
            if holder_list:
                break
            else:
                continue
        holder = td[1].get_text()
        if holder == '股东名称':
            continue
        holder_percent = re.findall('\d+\.?\d+?', td[3].get_text())[0]
        holder_list.append(holder)
        hold_percent_list.append(holder_percent)
    return holder_list, hold_percent_list


def is_largest_holder(ref, holder_name):
    '''
    判断是否大股东
    :param ref: 公司引用
    :param holder_name:大股东名称
    :return: bool
    '''
    listed_investment_collection = mongodb_connection('company')['listed_investment_company']
    investment_company_detail = listed_investment_collection.find_one({"_id": ref.id})

    raw_company_collection = mongodb_connection('company')['raw_company']
    raw_company_detail = raw_company_collection.find_one({'_id': investment_company_detail['详细信息'].id})

    if not raw_company_detail or '股东状况' not in raw_company_detail or '股东' not in raw_company_detail['股东状况'] or len(
            raw_company_detail['股东状况']['股东']) == 0:
        return False

    else:
        percent_list = [
            float(item.replace("%", "").replace("-", "0").replace("未公开", "0").replace("<", "")) if item else 0 for
            item in raw_company_detail['股东状况']['出资比例']]

        if percent_list[0] != 0 and (percent_list[0] >= 50 or percent_list[0] == max(percent_list)):
            return _company_name_strip(raw_company_detail['股东状况']['股东'][0].strip()) == _company_name_strip(
                holder_name) or get_company_detail_from_db_api(
                raw_company_detail['股东状况']['股东'][0].strip()) == get_company_detail_from_db_api(holder_name)
        elif max(percent_list) == 0:
            return False
        else:
            return _company_name_strip(
                raw_company_detail['股东状况']['股东'][percent_list.index(max(percent_list))].strip()) == _company_name_strip(
                holder_name) or get_company_detail_from_db_api(raw_company_detail['股东状况']['股东'][percent_list.index(
                max(percent_list))].strip()) == get_company_detail_from_db_api(holder_name)


def get_control_son_company_list(ref):
    '''
    获取控股子公司列表
    :param ref: 公司引用
    :return: 控股子公司列表
    '''
    son_company_list = []
    listed_inventment_company_col = mongodb_connection('company')['listed_investment_company']
    detail = listed_inventment_company_col.find_one({"_id": ref.id})
    raw_son_company_list = detail['子公司列表']
    raw_son_company_percent_list = detail['子公司出资比例']
    for i in range(len(raw_son_company_list)):
        try:
            percent = float(raw_son_company_percent_list[i].replace("%", ""))
        except Exception:

            if is_largest_holder(raw_son_company_list[i], detail['企业全称']):
                son_company_list.append(raw_son_company_list[i])
            continue
        if percent > 50:
            son_company_list.append(raw_son_company_list[i])
        else:
            if is_largest_holder(raw_son_company_list[i], detail['企业全称']):
                son_company_list.append(raw_son_company_list[i])

    return son_company_list


def get_son_company_list(ref):
    '''
    获取子公司列表
    :param ref: 公司引用
    :return: 子公司列表
    '''
    son_company_list = []
    son_company_percent_list = []
    detail = mongodb_connection('company')['listed_investment_company'].find_one({"_id": ref.id})
    raw_son_company_list = detail['子公司列表']
    raw_son_company_percent_list = detail['子公司出资比例']
    for i in range(len(raw_son_company_list)):
        son_company_percent_list.append((raw_son_company_list[i], raw_son_company_percent_list[i]))
        son_company_list.append(raw_son_company_list[i])
    return son_company_list, son_company_percent_list


def get_control_offspring_company_list(ref_list):
    '''
    获取控股后代公司列表
    :param ref_list: 公司引用列表
    :return: 控股后代公司列表
    '''
    offspring_company_list = []
    for i in ref_list:
        son_company_list = get_control_son_company_list(i)
        offspring_company_list.extend(son_company_list)
        if son_company_list:
            offspring_company_list.extend(get_control_offspring_company_list(son_company_list))
    return offspring_company_list


def get_all_offspring_list(ref_list):
    '''
    获取所有后代公司列表
    :param ref_list: 公司引用列表
    :return: 后代公司列表
    '''
    offspring_parent_company_son_company_list = []
    for i in ref_list:
        son_company_list, son_company_percent_list = get_son_company_list(i)

        for j in son_company_percent_list:
            offspring_parent_company_son_company_list.append((i, j[0], j[1]))

        if son_company_list:
            offspring_parent_company_son_company = get_all_offspring_list(son_company_list)
            offspring_parent_company_son_company_list.extend(offspring_parent_company_son_company)
        print(son_company_list)
    return offspring_parent_company_son_company_list


def get_investment_offspring_list(ref_list):
    '''
    获取投资后代公司列表
    :param ref_list: 公司引用列表
    :return: 投资后代公司列表
    '''
    offspring_parent_company_son_company_list = get_all_offspring_list(ref_list)
    invest_company_list = []

    tree_list = []

    detail = mongodb_connection('company')['listed_investment_company'].find_one(ref_list[0].id)
    raw_company_detail = mongodb_connection('company')['raw_company'].find_one(detail['详细信息'].id)
    node = {'id': '_' + str(ref_list[0].id), "collapsed": 'false', "text": {"name": detail['企业全称']},
            "link": raw_company_detail['链接']}

    tree_list.append(node)

    for i in offspring_parent_company_son_company_list:
        detail = mongodb_connection('company')['listed_investment_company'].find_one(i[1].id)
        if '投资' in detail['企业全称'] or '基金管理' in detail['企业全称'] or '孵化' in detail['企业全称'] or '资产' in detail[
            '企业全称'] or '资本' in detail['企业全称'] or '创投' in detail['企业全称'] or '有限合伙' in detail['企业全称']:
            invest_company_list.append(detail['企业全称'])
        background = ''
        try:
            percent = float(i[2].replace("%", ""))
        except (ValueError, AttributeError):
            percent = 0

        if percent == 0:
            background = 'yellow'
        elif 0 < percent <= 20:
            background = 'gray1'
        elif 20 < percent <= 40:
            background = 'gray2'
        elif 40 < percent <= 60:
            background = 'gray3'
        elif 60 < percent <= 80:
            background = 'gray4'
        else:
            background = 'gray5'
        print(detail)
        raw_company_detail = mongodb_connection('company')['raw_company'].find_one(detail['详细信息'].id)
        node = {'id': '_' + str(i[1].id), 'HTMLclass': background, "parent": "_" + str(i[0].id), "collapsed": 'true',
                "text": {"name": detail['企业全称']}, "link": raw_company_detail['链接']}
        tree_list.append(node)

    for i in invest_company_list:
        for j in tree_list:
            if i == j['text']['name']:

                node = j
                while 'parent' in node or node['collapsed'] != 'false':
                    node['collapsed'] = 'false'
                    for k in tree_list:
                        if k['id'] == node['parent']:
                            node = k
                            break
                j['image'] = True

    text = ''
    for i in tree_list:
        if i['collapsed'] == 'true':
            continue

        if 'parent' in i:
            if 'image' not in i:
                text += i['id'] + '={HTMLclass:"' + i['HTMLclass'] + '",parent:' + i['parent'] + ',collapsed:' + i[
                    'collapsed'] + ',text:{name:"' + i['text']['name'] + '"}' + ',link:{href:"' + i[
                            'link'] + '"}' + '};\n'
            else:
                text += i['id'] + '={HTMLclass:"' + i['HTMLclass'] + '",parent:' + i['parent'] + ',collapsed:' + i[
                    'collapsed'] + ', image:"' + './star.png' + '",text:{name:"' + \
                        i['text']['name'] + '"}' + ',link:{href:"' + i['link'] + '"}' + '};\n'
        else:
            text += i['id'] + '={' + 'collapsed:' + i['collapsed'] + ',text:{name:"' + i['text'][
                'name'] + '"}' + ',link:{href:"' + i['link'] + '"}' + '};\n'
    text = text + 'chart_config = [config,'
    for i in tree_list:
        if i['collapsed'] == 'true':
            continue
        text += i['id'] + ',\n'
    js_text = 'var config = {container: "#basic-example",rootOrientation:  "WEST",connectors: {type: "step"},node: {HTMLclass: "nodeExample1",collapsable: true}};\n' + text[
                                                                                                                                                                        :-2] + '];'
    return invest_company_list, js_text


stock_list = []
saved_stock_num_list = read_col('company', 'stock_company', '股票代码')

# 更新上交所上市公司列表
f = open('A股.xls', encoding='gbk')
for i in f.readlines():
    if i.startswith("6"):
        info_list = i.split()
        stock_list.append({"股票代码": info_list[0], "简称": info_list[1]})
#
# 更新深交所上市公司列表
oldwb = xlrd.open_workbook('上市公司列表.xlsx')
table = oldwb.sheets()[0]

stock_num_list = table.col_values(0)
stock_name_list = table.col_values(1)

for i in range(1, len(stock_name_list)):
    stock_list.append({"股票代码": stock_num_list[i], "简称": stock_name_list[i]})

db = mongodb_connection('company')
col = db['stock_company']

for i in stock_list:
    if i["股票代码"] not in saved_stock_num_list:
        try:
            col.insert(i)
            print(i)
        except pymongo.errors.DuplicateKeyError:
            continue
#
# 从新浪获取公司信息
for i in col.find():
    if ('十大股东' not in i or len(i['十大股东']) == 0) and i['股票代码'] not in black_list:
        new_item = i.copy()
        try:
            new_item['企业全称'], new_item['货币资金'], new_item['十大股东'], new_item['持股比例'] = get_info_from_sina(i['股票代码'])
        except TypeError:
            print(i['股票代码'] + '无股东信息')
            continue
        col.update(i, new_item)

# 获取上市公司天眼查或企查查资讯
for i in col.find():
    if '详细信息' not in i and i['股票代码'] not in black_list:
        detail = search_company_api(i['企业全称'])
        new_item = i.copy()
        new_item['详细信息'] = DBRef('raw_company', detail['_id'])

        col.update({'_id': i['_id']}, new_item)

stock_info_list = []
for i in col.find({"$and": [{"股票代码": {"$regex": "^60399"}}, {"股票代码": {"$gt": "000000"}}]}):
    stock_info_list.append(i)
# 获取上市公司子公司资讯
for i in stock_info_list:
    if '企业全称' in i and i['股票代码'] not in black_list and '子公司引用' not in i:
        print(i['股票代码'])
        id = search_iteration_son_company_of_listed_company_api(i['企业全称'], None)
        mongodb_connection('company', is_reused=False)['stock_company'].update(i, {
            "$set": {'子公司引用': DBRef('listed_investment_company', id)}})
#
for i in stock_info_list:
    if '企业全称' in i and '子公司引用' in i:
        print(i)
        offspring_company_ref_list = []
        listed_investment_company_ref_list = get_control_offspring_company_list([i['子公司引用']])
        for j in listed_investment_company_ref_list:
            col = mongodb_connection('company')['listed_investment_company']
            offspring_company_ref_list.append(col.find_one({"_id": j.id}, {"_id": 0, "详细信息": 1})['详细信息'])
        mongodb_connection('company')['stock_company'].update(i, {"$set": {'子公司列表': offspring_company_ref_list}})

# 获取上市公司母公司资讯
for i in stock_info_list:
    if i['股票代码'] not in black_list and (i['十大股东'][0].endswith("公司") or '有限合伙' in i['十大股东'][0]) and '母公司引用' not in i:
        print(i['十大股东'][0] + " " + i['股票代码'])
        id = search_iteration_son_company_of_listed_company_api(i['十大股东'][0], None)
        mongodb_connection('company', is_reused=False)['stock_company'].update(i, {
            "$set": {'母公司引用': DBRef('listed_investment_company', id)}})

# 输出上市公司投资行为树状图
investment_info_list = []
for i in stock_info_list:
    # if not i['股票代码'].startswith('300001'):
    #     continue
    print(i)
    if i['股票代码'] in black_list:
        continue

    detail = mongodb_connection('company')['raw_company'].find_one({'_id': i['详细信息'].id})
    investment_info = {}
    investment_info['股票代码'] = i['股票代码']
    investment_info['企业全称'] = i['企业全称']
    investment_info['简称'] = i['简称']
    if i['货币资金'] < 20000:
        continue
        # pass
    investment_info['货币资金'] = i['货币资金']
    investment_info['地区'] = detail['地区']
    investment_info['省份'] = detail['省份']
    investment_info['企业类型'] = detail['企业类型']
    if is_state_own_company(detail):
        continue
        # pass
    if '母公司引用' in i and i['母公司引用'].id:
        mother_detail = search_company_api(i['十大股东'][0])
        if is_state_own_company(mother_detail):
            continue
        investment_info['母公司'] = i['十大股东'][0]
        investment_info['母公司投资公司'], js_text = get_investment_offspring_list([i['母公司引用']])

        if investment_info['母公司投资公司']:
            f = open('pic/' + i['股票代码'] + 'mu.js', mode='w')
            f.writelines(js_text)
            f.close()
            f = open('pic/' + i['股票代码'] + 'mu.html', mode='w')
            f.writelines(html_content % {'js': i['股票代码'] + 'mu.js'})
            f.close()
            investment_info['母公司图'] = 'pic/' + i['股票代码'] + 'mu.html'
        else:
            investment_info['母公司图'] = ''
    else:
        investment_info['母公司'] = ''
        investment_info['母公司投资公司'] = []
        investment_info['母公司图'] = ''
        # print(get_offspring_investment_list([i['子公司引用']]))
        # print(i)
    investment_info['投资公司'], js_text = get_investment_offspring_list([i['子公司引用']])
    if investment_info['投资公司']:
        f = open('pic/' + i['股票代码'] + '.js', mode='w')
        f.writelines(js_text)
        f.close()
        f = open('pic/' + i['股票代码'] + '.html', mode='w')
        f.writelines(html_content % {'js': i['股票代码'] + '.js'})
        f.close()
        investment_info['子公司图'] = 'pic/' + i['股票代码'] + '.html'
    else:
        investment_info['子公司图'] = ''
    investment_info['母公司投资公司'] = list(set(investment_info['母公司投资公司']) - set(investment_info['投资公司']))

    if not investment_info['投资公司'] and not investment_info['母公司投资公司']:
        continue

    investment_info_list.append(investment_info)

with open('上市公司投资行为汇总-深圳主板.csv', 'a') as csvfile:
    fieldnames = ['股票代码', '企业全称', '简称', '货币资金', '地区', '省份', '企业类型', '母公司', '母公司投资公司', '母公司图', '投资公司', '子公司图']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(investment_info_list)
