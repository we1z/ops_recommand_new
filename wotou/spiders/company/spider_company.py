# -*- coding:utf-8 -*-#
"""
爬取公司详细信息,机构跟踪法的爬虫
"""
import http
import re
import urllib.parse
from functools import reduce
import pymongo
import requests
import selenium.webdriver.support.expected_conditions as EC
from bson.dbref import DBRef
from lxml import html, etree
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import os


from wotou.config.config import *
from wotou.lib.db_connection import read_col
from wotou.lib.delay_controller import time_delay_controller_qichacha
from wotou.lib.delay_controller import time_delay_controller_tianyancha
from wotou.lib.error import *
from wotou.lib.region import judge_location
from wotou.spiders.company.login.easy_login import *
from wotou.spiders.util.proxies import service_args
from wotou.spiders.util.proxies import get_chrome_option
from wotou.spiders.company.add_blacklist_from_not_found import add_not_found_blacklist_api
from wotou.spiders.company.handle_tianyancha_font.update_font import generate_fontdict, generate_fontdict_without_ttf
import sys
import multiprocessing
from selenium.webdriver.chrome.options import Options


retry_time = 0
driver = None

cookie_for_request = None
cookie_for_selenium = None
cookie_dict_list_for_request = None
cookie_dict_list_for_selenium = None

address_list = None
industry_list = None
trace_company_list = None
blacklist_company_list = None
blacklist_company_not_in_crm_list = None
blacklist_company_name_list = None
whole_blacklist_company_list = None

# 全部的黑名单企业名称
whole_blacklist_company_list_name = None
proxy_service_args = service_args

# caps = DesiredCapabilities.PHANTOMJS
# caps["phantomjs.page.settings.userAgent"] = \
#     "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"

tianyancha_fontdict = {}


def init(is_change_ip=False, is_chrome=False):
    '''
    初始化浏览器
    :param is_change_ip: 是否使用阿布云代理
    :param is_chrome: 是否用chrome
    :return:
    '''
    global driver
    global proxy_service_args
    global blacklist_company_name_list

    # 初始化selenium + phantomjs 配置

    if is_change_ip:

        option = get_chrome_option()
        option.add_argument(
            'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')
        # option.add_argument("--headless")
        driver = webdriver.Chrome(chrome_options=option)
    else:
        # if not is_chrome:
        #     proxy_service_args = []
        #     driver = webdriver.PhantomJS(desired_capabilities=caps,
        #                                  service_args=['--load-images=false', "--web-security=no",
        #                                                "--ignore-ssl-errors=yes",
        #                                                "--ignore-ssl-errors=true",
        #                                                "--ssl-protocol=tlsv1"] + proxy_service_args)
        # else:
        option = Options()
        option.add_argument(
            'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')
        option.add_argument("--headless")
        driver = webdriver.Chrome(chrome_options=option)

    driver.implicitly_wait(5)
    driver.set_page_load_timeout(60)
    blacklist_company_name_list = read_col('company', 'blacklist_company_name', 'company')


def filter_data_init():
    '''
    初始化黑名单和过滤器所需数据
    :return:
    '''
    global address_list
    global industry_list
    global trace_company_list
    global blacklist_company_list
    global blacklist_company_not_in_crm_list
    global whole_blacklist_company_list
    global blacklist_company_name_list
    global whole_blacklist_company_list_name

    address_list = read_col('company', 'whitelist_district', 'city')
    industry_list = read_col('company', 'whitelist_industry', 'industry')

    trace_company_list = [str(i.id) for i in read_col('company', 'trace_company', '详细信息')]
    blacklist_company_list = [str(i.id) for i in read_col('company', 'blacklist_company', '详细信息')]
    blacklist_company_not_in_crm_list = [str(i.id) for i in read_col('company', 'blacklist_company_not_in_crm', '详细信息')]

    # 测试代码
    trace_company_list_name = [str(i) for i in read_col('company', 'trace_company', '企业全称')]
    blacklist_company_list_name = [str(i) for i in read_col('company', 'blacklist_company', '企业全称')]
    blacklist_company_not_in_crm_list_name = [str(i) for i in read_col('company', 'blacklist_company_not_in_crm', '企业全称')]

    whole_blacklist_company_list = trace_company_list + blacklist_company_list + blacklist_company_not_in_crm_list

    whole_blacklist_company_list_name = trace_company_list_name + blacklist_company_list_name + blacklist_company_not_in_crm_list_name
    blacklist_company_name_list = read_col('company', 'blacklist_company_name', 'company')


def get_cookies_from_database():
    # 从数据库中读取cookies
    global cookie_dict_list_for_request
    global cookie_dict_list_for_selenium
    global cookie_for_request
    global cookie_for_selenium
    cookie_dict_list_for_request = []
    cookie_dict_list_for_selenium = []

    new_cookie_dict_for_selenium = {}
    new_cookie_dict_for_request = {}
    db = mongodb_connection('company')
    cookies_collection = db['cookies']

    for cookies in cookies_collection.find():
        for i in cookies['cookies']:
            new_cookie_dict_for_request[i['name']] = i['value']

        cookie_dict_list_for_request.append(new_cookie_dict_for_request)

        copy_cookies = cookies['cookies'].copy()
        for j in copy_cookies:
            j['domain'] = '.qichacha.com'

        cookie_dict_list_for_selenium.append(copy_cookies)

    cookie_for_request = cookie_dict_list_for_request[random.randint(0, len(cookie_dict_list_for_request) - 1)]
    cookie_for_selenium = cookie_dict_list_for_selenium[random.randint(0, len(cookie_dict_list_for_selenium) - 1)]


def change_qichacha_selenium_cookies():
    """
    更换selenium所用的cookies
    """
    login_qichacha(driver)

def is_investment_company(company_name, mother_company_name_list):
    """
    判断company_name是否投资公司
    :param company_name:公司名
    :param mother_company_name_list:母公司列表
    :return:True or False
    """
    if '有限合伙' in company_name:
        return True
    # 若公司名称包含'投资'或'基金管理'或‘孵化’或‘资本’或‘资产’，则需要判断母公司的占股比例
    elif '投资' in company_name or '基金管理' in company_name or '孵化' in company_name or '资产' in company_name or '资本' in company_name or '创投' in company_name:
        try:
            detail = search_company_in_cluster_api([company_name], 30)[0]
        except IndexError:
            return False
        sum_percent = 0
        for mother_company_name in mother_company_name_list:
            if mother_company_name in detail['股东状况']['股东']:
                percent = detail['股东状况']['出资比例'][detail['股东状况']['股东'].index(mother_company_name)]

                try:
                    percent = percent.replace("%", "")
                    percent = float(percent)
                    sum_percent += percent
                except Exception:
                    print(company_name + "股东占比未知")

                    return True

        if sum_percent >= 5:
            return True
        else:
            return False

    else:
        return False


def _search_single_investment_company_in_db(asset_name):
    """
    从数据库中“对外投资”一列中获取公司的子公司
    """

    detail = get_company_detail_from_db_api(asset_name)

    if not detail or '对外投资' not in detail:
        detail = search_company_api(asset_name, True)

    if detail and '对外投资' in detail and '子公司' in detail['对外投资']:
        return detail['对外投资']['子公司'], detail['对外投资']['出资比例']
    else:
        return [], []


def get_company_detail_from_db_api(company_name):
    """
    输入：公司名称
    若公司存在于数据库，输出条目
    不存在输出None
    """
    company_name = _company_name_strip(company_name)
    collection = mongodb_connection('company')['raw_company']
    ret = collection.find_one({'企业全称': company_name})
    if not ret:
        ret = collection.find_one({'曾用名': {"$in": [company_name]}})
        if not ret:
            return None
    return ret


def _search_mainpage(query):
    """
    通过公司名字，查询天眼查中公司主页的url
    :param query: 公司名字
    :return: url和公司曾用名

    """
    global driver
    # if not proxy_service_args:
    #     restart_driver()
    if 'tianyancha' not in driver.current_url:
        driver.get('https://www.tianyancha.com/')
        login_tianyancha(driver)

    baseurl = 'https://www.tianyancha.com/search?'
    params = {
        'checkForm': 'searchBox',
        'key': query
    }
    url = baseurl + urllib.parse.urlencode(params)
    driver.get(url)

    if not is_tianyancha_cookies_useful(driver.current_url, driver.page_source):
        print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
        # print(driver.page_source)
        login_tianyancha(driver)
        return _search_mainpage(query)
    if 'black' in driver.current_url:
        time.sleep(200)
        restart_driver()
        return _search_mainpage(query)

    href = ''
    history_name = []
    wait = WebDriverWait(driver, 15)
    try:
        temp_driver = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'result-list')))
    except exceptions.TimeoutException:
        if '没有找到相关结果' in driver.page_source or '天眼查为你找到相关公司' in driver.page_source:
            return '', ''
        try:
            driver.find_element_by_xpath('//*[@id="web-content"]/div/div[1]/div/div[3]/div')
        except exceptions.NoSuchElementException:
            return '', ''
        if 'login' in driver.current_url:
            login_tianyancha(driver)
            return _search_mainpage(query)
        else:
            raise exceptions.TimeoutException

    company_num = temp_driver.find_element_by_xpath('//*[@id="search"]/span[2]').text
    print("company_num: " +str(company_num))

    company_num = min(20, int(company_num.replace("+", "")))

    temp_full_name = ''
    temp_href = ''
    page_source=etree.HTML(driver.page_source)
    if company_num==1:
        try:
            print("company为1")
            href_xpath = "//*[@id='web-content']/div[@class='container pt25']/div[@class='container-left']/div[@class='search-block']/div[@class='result-list']/div[@class='search-item'][1]/div[@class='search-result-single  ']/div[@class='content']/div[@class='header']/a[@class='name select-none']/@href"
            #temp_href =page_source.xpath('//*[@id="web-content"]/div/div[1]/div/div[3]/div/div[2]/div[1]/a/@href')[0]
            temp_href = page_source.xpath(href_xpath)[0]
            print("只有一条信息，其href为: "+temp_href)
            name_xpath = "//*[@id='web-content']/div[@class='container pt25']/div[@class='container-left']/div[@class='search-block']/div[@class='result-list']/div[@class='search-item'][{}]/div[@class='search-result-single  ']/div[@class='content']/div[@class='header']/a"
            temp_full_name = page_source.xpath(name_xpath.format(1))[0].xpath('string()')
            temp_full_name0 = deal_with_tianyancha_js_num_trick(temp_full_name)

            if temp_full_name == query:
                pass
            elif temp_full_name0 == query:
                temp_full_name = temp_full_name0
            else:
                pass

            temp_history_name = ''

            if '曾用名' in driver.page_source:
                history_name_list = page_source.xpath(
                    '//*[@id="company_web_top"]/div[2]/div[2]/div[3]/div/div/div/text()')
        except IndexError:
            print("只有一个公司,获取公司信息xpath出现问题")
            temp_history_name = ''
        if temp_full_name == query or temp_history_name == query:
            href = temp_href
            print(href)
            if temp_history_name:
                history_name.append(temp_history_name)
    else:
        for i in range(1, company_num + 1):
            try:

                href_xpath = "//*[@id='web-content']/div[@class='container pt25']/div[@class='container-left']/div[@class='search-block']/div[@class='result-list']/div[@class='search-item'][{}]/div[@class='search-result-single  ']/div[@class='content']/div[@class='header']/a[@class='name select-none']/@href"

                #temp_href = page_source.xpath('//*[@id="web-content"]/div/div[1]/div/div[3]/div[{}]/div[2]/div[1]/a/@href'.format(i))[i-1]
                temp_href = page_source.xpath(href_xpath.format(i))[i - 1]
                print("多条信息，对应信息的href为: " + temp_href)
                # temp_full_name = page_source.xpath('//*[@id="web-content"]/div/div[1]/div/div[3]/div[{}]/div[2]/div[1]/a'.format(i))[i-1].xpath('string()')
                name_xpath = "//*[@id='web-content']/div[@class='container pt25']/div[@class='container-left']/div[@class='search-block']/div[@class='result-list']/div[@class='search-item'][{}]/div[@class='search-result-single  ']/div[@class='content']/div[@class='header']/a"
                temp_full_name = page_source.xpath(name_xpath.format(i))[i - 1].xpath('string()')

                # 通过天眼查字体来将其变为正常显示
                temp_full_name0 = deal_with_tianyancha_js_num_trick(temp_full_name)
                print("公司名为:" + temp_full_name0)
                if temp_full_name == query:
                    pass
                elif temp_full_name0 == query:
                    temp_full_name = temp_full_name0
                else:
                    pass

                temp_history_name = ''

                if '曾用名' in driver.page_source:
                    history_name_list = page_source.xpath(
                        '//*[@id="company_web_top"]/div[2]/div[2]/div[3]/div/div/div/text()')

            except IndexError:
                print("获取公司信息xpath出现问题")
                temp_history_name = ''

            if temp_full_name == query or temp_history_name == query:
                href = temp_href
                print(href)
                if temp_history_name:
                    history_name.append(temp_history_name)
                break
    print(query, temp_full_name)
    if not href:
        history_name_list = []

        url_xpath = "//*[@id='web-content']/div[@class='container pt25']/div[@class='container-left']/div[@class='search-block']/div[@class='result-list']/div[@class='search-item']/div[@class='search-result-single  ']/div[@class='content']/div[@class='header']/a[@class='name select-none']/@href"
        # '//*[@id="web-content"]/div/div/div[1]/div/div[3]/div[1]/div[2]/div[1]/a/@href'
        urlaa = page_source.xpath(url_xpath)[0]
        print("urlaa: " + str(urlaa))
        Js = 'window.open("{}")'.format(urlaa)
        driver.execute_script(Js)
        driver.switch_to.window(driver.window_handles[1])
        page_source1 = etree.HTML(driver.page_source)
        if '曾用名' in driver.page_source:
            history_name_list = page_source1.xpath(
                '//*[@id="company_web_top"]/div[2]/div[2]/div[3]/div/div/div/text()')
            #//*[@id="company_web_top"]/div[2]/div[2]/div[3]/div/div/div[1]
        driver.close()
        driver.switch_to.window(driver.window_handles[0])
        if query in history_name_list:
            href = urlaa
            print(href)
            history_name = history_name_list

    return href, history_name


def _company_detail_by_href(times=0, href=None):
    """
    获取公司主页内信息
    输入：企查查内公司id
    输出：公司全称、曾用名、地址、地区、省份、行业、注册资本
          股东状况[股东, 出资比例] 成立时间 企业资质[专利数,著作权数] 联系方式[电话，邮箱] 上市情况
    """
    global driver

    if not href:
        href = driver.current_url
    else:
        driver.get(href)
    try:

        result = driver.page_source
        if not is_qichacha_cookies_useful(result):
            change_qichacha_selenium_cookies()
            return _company_detail_by_href(times, href)
        wait = WebDriverWait(driver, 15)

        if 'c_logo_xggs' in driver.page_source:
            is_hongkong = True
            return None
        else:
            is_hongkong = False

        if 'c_logo_twgs' in driver.page_source:
            return None
        else:
            is_taiwan = False

        # **顶部信息**
        try:
            top_element = wait.until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="company-top"]/div/div[@class="content"]')))
        except (exceptions.NoSuchElementException, exceptions.TimeoutException):
            print("未找到top_element")
            top_element = None
        # 公司全称
        if top_element:
            try:
                full_name = top_element.find_element_by_xpath('./div[1]/h1').get_attribute('innerHTML').split('\n')[
                    0].strip()
            except exceptions.NoSuchElementException:
                print('为指导全称')
                full_name = ''
            # 公司状态
            try:
                status = top_element.find_element_by_xpath('./div[1]/span[last()]').get_attribute('innerText')
            except exceptions.NoSuchElementException:
                status = ''

            # 是否上市
            try:
                first_small_text = top_element.find_element_by_xpath('./div[2]/span').text
                # 上市公司页面有3个small标签，第一个为"上市详情："
                on_list = '上市' in first_small_text
            except exceptions.NoSuchElementException:
                print('是否上市xpath异常')
                on_list = False
            adjust_num = 3 if on_list else 2  # 上市公司small标签延后
            # 电话
            try:
                phone_str = top_element.find_element_by_xpath('./div[%d]/span[2]' % adjust_num).text.strip()
                phone = re.findall('[\d\-]+', phone_str)[0]
            except exceptions.NoSuchElementException:
                print('电话xpath异常NoSuchElementException')
                phone = ''
            except IndexError:
                print('电话xpath异常IndexError')
                phone = ''

            # 邮箱
            try:
                email = top_element.find_element_by_xpath('./div[%d]/span[2]/a' % (adjust_num + 1)).text
                # email = email_str.get_attribute('innerHTML').strip()
            except exceptions.NoSuchElementException:
                print('邮箱NoSuchElementException')
                email = ''
            except IndexError:
                print('邮箱xpath异常IndexError')
                email = ''

            # 官网
            try:
                website = top_element.find_element_by_xpath('./div[%d]/span[4]' % (adjust_num + 1)).text
            except exceptions.NoSuchElementException:
                print('官网NoSuchElementException')
                website = ''

            # 公司地址
            try:
                location = top_element.find_element_by_xpath('./div[%d]/span[2]/a' % (adjust_num + 2)).text.strip()
            except exceptions.NoSuchElementException:
                location = ''
            except IndexError:
                location = ''
        else:
            location = ''
            website = ''
            email = ''
            on_list = False
            is_hongkong = False
            is_taiwan = False
            phone = ''
            status = ''
            full_name = ''

        # **基本信息**
        if not is_hongkong and not is_taiwan:
            contain_element = wait.until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="Cominfo"]/table[1]/tbody')))

        # 法定代表人
        if not is_hongkong and not is_taiwan:
            try:
                principle = contain_element.find_element_by_xpath('./tr[2]/td[1]/div/div/div[2]/a[1]').text
            except exceptions.NoSuchElementException:
                principle = ''
        else:
            principle = ''

        if not is_hongkong and not is_taiwan:
            contain_element = wait.until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="Cominfo"]/table[2]/tbody')))

        # 曾用名
        history_name_list = []
        if not is_hongkong and not is_taiwan:
            if '曾用名' in contain_element.text:
                try:
                    history_name = contain_element.find_element_by_xpath('./tr[8]/td[2]').text.strip()
                    print(history_name)
                except exceptions.NoSuchElementException:
                    history_name = ''

                if history_name == "-":
                    history_name = ""

                if history_name.find(" "):
                    name_split_list = history_name.split()
                    print(name_split_list)
                    for name in name_split_list:
                        if name:
                            history_name_list.append(_company_name_strip(name))
                else:
                    history_name_list.append(_company_name_strip(history_name))

        # 省份和地区
        province, city = judge_location(location, full_name)

        # 公司类型
        if not is_hongkong and not is_taiwan:
            company_type = contain_element.find_element_by_xpath('./tr[5]/td[2]').text
        else:
            company_type = '香港企业'

        # 公司行业
        if not is_hongkong and not is_taiwan:
            try:
                industry = contain_element.find_element_by_xpath("./tr[5]/td[4]").get_attribute('innerText')
            except exceptions.NoSuchElementException:
                industry = ''
        else:
            industry = ''
        # 公司注册资本
        if not is_hongkong and not is_taiwan:
            try:
                money_str = contain_element.find_element_by_xpath('./tr[1]/td[2]').text
                money = float(re.findall('([\d|\.]+)', money_str)[0])
                if '美元' in money_str:
                    money *= 6.6
            except exceptions.NoSuchElementException:
                money = 0
            except ValueError:
                money = 0
            except IndexError:
                money = 0
        else:
            money = 0

            # 成立时间
        if not is_hongkong and not is_taiwan:
            try:
                founded_time = contain_element.find_element_by_xpath('./tr[2]/td[4]').get_attribute('innerText')
            except exceptions.NoSuchElementException:
                founded_time = ''
        else:
            founded_time = ''

            # **股东信息**
        try:
            holders_element = wait.until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="Sockinfo"]')))
            # 股东数量
            try:
                holder_num = int(holders_element.find_element_by_xpath('./div[1]/span[1]').text)
            except ValueError:
                holder_num = 0
            # 股东信息
            holders = []
            holders_percent = []
            try:
                for idx in range(2, holder_num + 2):
                    holder = holders_element.find_element_by_xpath('./table/tbody/tr[%d]' % idx)
                    holders.append(holder.find_element_by_xpath('./td[2]/a').text.strip())
                    holders_percent.append(holder.find_element_by_xpath('./td[3]').get_attribute('innerText').strip())
            except exceptions.NoSuchElementException:
                holders = []
        except exceptions.TimeoutException:
            holders = []
            holders_percent = []

            # 投资行为信息
        son_company_list = []
        son_company_percent_list = []
        try:
            invest_element = wait.until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="touzilist"]')))
            son_company_num = invest_element.find_element_by_xpath('.//div[1]/span[1]').text

            if son_company_num == '0':
                invest_info = {"子公司": son_company_list, "出资比例": son_company_percent_list}
            else:

                son_company_num = int(son_company_num)
                son_company_pages = (son_company_num - 1) // 10 + 1

                if son_company_pages == 1:
                    son_company_element = invest_element.find_element_by_xpath('.//table/tbody')
                    for i in range(2, son_company_num + 2):
                        son_company_name = son_company_element.find_element_by_xpath(
                            "./tr[%d]/td[1]" % i).text.strip()
                        son_company_percent = son_company_element.find_element_by_xpath(
                            "./tr[%d]/td[4]" % i).get_attribute('innerText').strip()

                        son_company_list.append(son_company_name)
                        son_company_percent_list.append(son_company_percent)
                else:
                    for page in range(2, son_company_pages + 2):

                        son_company_element = invest_element.find_element_by_xpath('.//table/tbody')
                        for j in range(2, min(10, son_company_num - (page - 2) * 10) + 2):
                            son_company_name = son_company_element.find_element_by_xpath(
                                "./tr[%d]/td/a" % j).text.strip()
                            son_company_percent = son_company_element.find_element_by_xpath(
                                "./tr[%d]/td[4]" % j).get_attribute("innerText").strip()
                            son_company_list.append(son_company_name)
                            son_company_percent_list.append(son_company_percent)

                        if page != son_company_pages + 1:
                            # contain_element.find_element_by_xpath('./div[2]/div/div[2]/div[%d]/div[2]//div/ul/li[%d]/a' % (
                            #     int(adjust_num + 4), int(page + 1))).click()
                            driver.execute_script('getTabList(' + str(page) + ',"base","touzi")')
                            time.sleep(0.5)

                invest_info = {"子公司": son_company_list, "出资比例": son_company_percent_list}

        except:
            print(full_name + " 对外投资信息出错")
            invest_info = {"子公司": son_company_list, "出资比例": son_company_percent_list}

        # **知识产权信息**
        # driver.save_screenshot('screenshot.png')
        time.sleep(1)
        driver.execute_script('arguments[0].scrollIntoView(true);', driver.find_element_by_id('assets_title'))
        try:
            driver.find_element_by_id('assets_title').click()
        except exceptions.WebDriverException:
            driver.refresh()
            driver.execute_script('arguments[0].scrollIntoView(true);', driver.find_element_by_id('assets_title'))
            driver.find_element_by_id('assets_title').click()

        time.sleep(1)  # 时间过短可能仍停留在基本信息界面
        wait2 = WebDriverWait(driver, 5)
        asset_element = wait2.until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="assets_div"]/section[1]/div')))
        # 专利数量
        try:
            patent = asset_element.find_element_by_xpath('./a[2]').get_attribute('innerText')
            patent_count = int(patent.split()[-1].replace("+", ""))
        except ValueError:
            patent_count = 0
        except exceptions.NoSuchElementException:
            patent_count = 0
        # 软件著作权
        try:
            # copyright = asset_element.find_element_by_xpath('./a[4]').text
            software_copyright = asset_element.find_element_by_xpath('./a[5]').text
            copyright_count = int(software_copyright.split()[-1].replace("+", ""))
        except ValueError:
            copyright_count = 0
        except exceptions.NoSuchElementException:
            copyright_count = 0

        while len(driver.window_handles) > 1:
            driver.close()
            driver.switch_to_window(driver.window_handles[0])

        return {"企业全称": full_name, '曾用名': history_name_list, "公司地址": location, '省份': province, '地区': city,
                "行业": industry, "注册资本": money, "成立日期": founded_time,
                '联系方式': {'电话': phone, '邮箱': email}, '网站': website,
                '链接': href, "股东状况": {"股东": holders, "出资比例": holders_percent}, "对外投资": invest_info, '主要负责人': principle,
                "企业资质": {'专利数': patent_count, '著作权数': copyright_count}, "是否上市": on_list, "企业状态": status,
                "企业类型": company_type}

    except exceptions.TimeoutException as e:
        if times < 1:
            times += 1
            return _company_detail_by_href(times, href)
        else:
            raise exceptions.TimeoutException
    except exceptions.NoSuchElementException as e:
        if times < 1:
            times += 1
            return _company_detail_by_href(times, href)
        else:
            raise exceptions.NoSuchElementException


def write_raw_company_to_db(item):
    """
    输入公司信息到数据库
    :param item: 公司信息
    """
    item['爬取时间'] = datetime.datetime.now()
    print(item['爬取时间'])
    mongodb_connection('company')['raw_company'].insert(item)
    return item


def update_raw_company_to_db(item):
    """
    更新公司信息
    :param item: 公司信息
    :return: 公司信息
    """
    db = mongodb_connection('company')
    collection = db['raw_company']
    del item['_id']
    old_detail = collection.find_one({"企业全称": item["企业全称"]})
    item['曾用名'] = item['曾用名'] + old_detail['曾用名']
    collection.find_one_and_update({"企业全称": item["企业全称"]}, {"$set": item})


def update_raw_company_used_name(item, com_name):
    """
    更新公司曾用名
    :param item:公司信息
    :param com_name: 用于搜索的公司名称
    :return: 公司信息
    """
    db = mongodb_connection('company')
    collection = db['raw_company']
    if com_name not in item['曾用名']:
        item['曾用名'].append(com_name)
    del item['_id']
    collection.find_one_and_update({"企业全称": item["企业全称"]}, {"$set": item})
    collection.find_one_and_delete({"企业全称": com_name})


def _company_name_strip(name):
    """
    处理公司名称不规范的问题
    :param name: 公司名称
    :return: 规范后的公司名称
    """
    return name.replace("(", "（").replace(")", "）").replace(" ", "").replace("\xa0", "").replace("/", "").replace(
        "<DEL>", "").replace("<DEL/>", "").strip()


@time_delay_controller_qichacha
def _search_detail_in_qichacha(company_name):
    """
    在企查查的公司主页中，获取公司详细信息
    输入：公司名称
    输出：详细信息
    """
    print("进入企查查搜索")
    base_url = 'http://www.qichacha.com/search?'
    data = {'key': company_name}
    url = base_url + urllib.parse.urlencode(data)
    if 'qichacha' not in driver.current_url:
        driver.get('http://www.qichacha.com')
        change_qichacha_selenium_cookies()

    driver.get(url)
    time.sleep(1)

    print(driver.page_source)

    # 企查查网站的bug，cookies会一直增长到出错，因此需要处理
    if len(driver.get_cookies()) > 50:
        cookies_list = driver.get_cookies()
        for i in cookies_list:
            if i['name'][0:3] == "gr_":
                driver.delete_cookie(i['name'])

    if '您的访问被阻断' in driver.page_source:
        print("您的访问被阻断")
        time.sleep(1)
        restart_with_cookies()
        return _search_detail_in_qichacha(company_name)

    if not is_qichacha_cookies_useful(driver.page_source):
        print("当前企查查cookies无效")
        change_qichacha_selenium_cookies()
        return _search_detail_in_qichacha(company_name)

    try:
        if '小查还没找到数据' in driver.page_source or '您的搜索词太宽泛' in driver.page_source:
            return {}

        # while len(driver.window_handles) > 1:
        #     driver.switch_to_window(driver.window_handles[-1])
        #     driver.close()

        print("进入企查查")
        match_company_tag = WebDriverWait(driver, 45).until(
            EC.presence_of_element_located((By.XPATH, '//section[@id="searchlist"]/table/tbody/tr[1]/td[2]/a')))
        company_brief_info = driver.find_element_by_xpath('//section[@id="searchlist"]/table/tbody/tr[1]/td[2]').text
        print("第一个xpath")
        used_name = ''
        if '曾用名' in company_brief_info:
            used_name = company_brief_info[company_brief_info.index("曾用名") + 4:]
        if company_name != _company_name_strip(match_company_tag.text) and company_name != used_name:
            return {}

        while len(driver.window_handles) > 1:
            driver.close()
            driver.switch_to_window(driver.window_handles[0])

        driver.execute_script('arguments[0].scrollIntoView(true);', match_company_tag)

        match_company_tag.click()
        driver.switch_to_window(driver.window_handles[1])

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@id="company-top"]')))

        detail = _company_detail_by_href()
        if not detail:
            return {}
        detail['企业全称'] = _company_name_strip(detail['企业全称'])
        if company_name != detail['企业全称'] and company_name not in detail['曾用名']:
            return {}

    except IndexError:
        print("Index Error")
        return {}
    except KeyError:
        print("Key Error")
        return {}
    except exceptions.TimeoutException:
        print("timeout")
        driver.save_screenshot("qichacha.png")
        return _search_detail_in_qichacha(company_name)
    except exceptions.NoSuchWindowException:
        driver.save_screenshot("qichacha.png")
        restart_driver()
        return _search_detail_in_qichacha(company_name)
    except exceptions.NoSuchElementException:
        print("NoSuchElementException")
        return {}
    except exceptions.ElementNotVisibleException:
        print("ElementNotVisibleException")
        return _search_detail_in_qichacha(company_name)
    return detail


def restart_with_cookies():
    global driver
    cookies = driver.get_cookies()
    driver.quit()

    option = Options()
    option.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=option)
    driver.implicitly_wait(5)
    driver.set_page_load_timeout(60)
    return cookies


def restart_driver():
    """
    重启浏览器
    """
    global driver
    global proxy_service_args
    try:
        driver.delete_all_cookies()
        driver.save_screenshot("beforequit.png")
    except Exception as e:
        pass
    driver.quit()
    print('chrome重启')
    option = Options()
    # option.add_argument("--headless")
    option.add_argument(
        'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')
    driver = webdriver.Chrome(chrome_options=option)
    driver.implicitly_wait(5)
    driver.set_page_load_timeout(60)


@time_delay_controller_tianyancha
def _search_detail_in_tianyancha(href, history_name, retry_time=0):
    """
    在天眼查的企业主页中，获取
    :param href:企业主页具体网址
    :param history_name:企业曾用名
    :return:
    """
    print(history_name)
    global driver
    adjust_num = 2  # 调整上市与非上市公司标签抓取位置
    driver.get(href)

    if '很抱歉！你的页面丢了，试试搜索其他信息吧！' in driver.page_source:
        return None

    wait = WebDriverWait(driver, 15)

    if not is_tianyancha_cookies_useful(driver.current_url, driver.page_source):
        login_tianyancha(driver)
        return _search_detail_in_tianyancha(href, history_name)
    if 'black' in driver.current_url:
        restart_driver()
        return _search_detail_in_tianyancha(href, history_name)


    contain_element = wait.until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="company_web_top"]/div[2]')))
    page_source=etree.HTML(driver.page_source)

    try:
        driver.find_element_by_id('tyc_banner_close').click()
    except Exception as e:
        pass



    # 公司名称
    try:
        company_name=page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[1]/h1/text()')[0]

        # 是否上市
        try:
            if 'nav-main-shangshiitem' in driver.page_source:
                stock_num = contain_element.find_element_by_xpath('//*[@id="nav-main-shangshiitem"]/span').text
                if stock_num == '上市信息':
                    on_list = True
                    adjust_num = 3
                else:
                    on_list = False

            else:
                on_list = False
        except exceptions.NoSuchElementException:
            on_list = False

        # 公司地址
        tag=page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[4]//span/text()')

        if '中概股' in tag or '港股' or 'A股' in tag:
            on_list = True

        is_social_organization = False
        is_hongkong = False
        is_state_institution = False

        if '事业单位' in tag:
            is_state_institution = True
        elif '香港企业' in tag:
            is_hongkong = True
        elif '社会组织' in tag:
            is_social_organization = True

        try:
            location = page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[2]/div[2]/span[2]/@title | //*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[2]/div[2]/text()')[0]

        except IndexError:
            location = ''

            # 网站

        web_site = page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[2]/div[1]/a/@href')
        if len(web_site)==0:
            web_site=''
        else:
            web_site=web_site[0]

            # 电话
        if '查看更多' in page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[1]/div[1]/span[3]/span/text()'):
            try:
                contact_phone =page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[1]/div[1]/span[3]/script/text()')[0].replace('"', '').replace('[', '').replace(']', '')
            except IndexError:
                contact_phone = ''
        else:
            try:
                contact_phone = page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[1]/div[1]/span[2]/text()')[0]
            except IndexError:
                contact_phone = ''

            # 邮箱
        try:
            contact_mail = page_source.xpath('//*[@id="company_web_top"]/div[2]/div[2]/div[5]/div[1]/div[2]/span[2]/text()')[0]
        except IndexError:
            contact_mail = ''

            # 公司行业
        if not (is_state_institution or is_hongkong or is_social_organization):
            try:
                industry = page_source.xpath('//*[@id="_container_baseInfo"]/table[2]/tbody/tr[3]/td[4]/text()')[0]
            except IndexError:
                industry = ''

        else:
            industry = ''

        # 企业类型
        if not (is_state_institution or is_hongkong or is_social_organization):
            try:
                company_type = page_source.xpath('//*[@id="_container_baseInfo"]/table[2]/tbody/tr[2]/td[4]/text()')[0]
            except IndexError:
                company_type = ''
        else:
            if is_state_institution:
                company_type = '事业单位'
            elif is_hongkong:
                company_type = '香港企业'
            elif is_social_organization:
                company_type = '社会组织'


        # 公司注册资本
        if not (is_state_institution or is_hongkong or is_social_organization):
            try:

                money = deal_with_tianyancha_js_num_trick(page_source.xpath('//*[@id="_container_baseInfo"]/table[1]/tbody/tr[1]/td[2]/div[2]/text/text()')[0].strip())
            except IndexError:
                # money = ['','','']
                money = ''

            try:
                if '美元' in money:
                    money = re.findall('(\d+)', money)[0]
                    money = float(money) * 6.3
                else:
                    money = re.findall('(\d+)', money)[0]
                    money = float(money)

            except IndexError:
                money = 0.0
            except ValueError:
                money = 0.0
        else:
            money = 0

            # 成立时间
        if not (is_state_institution or is_hongkong or is_social_organization):
            try:
                founded_time = deal_with_tianyancha_js_num_trick(page_source.xpath('//*[@id="_container_baseInfo"]/table[1]/tbody/tr[2]/td/div[2]/text/text()')[0].strip())
            except exceptions.NoSuchElementException:
                founded_time = ''
        else:
            founded_time = ''

            # 企业状态
        if not (is_state_institution or is_hongkong or is_social_organization):
            status = page_source.xpath('//*[@id="_container_baseInfo"]/table[1]/tbody/tr[3]/td/div[2]/text()')[0]
        else:
            status = ''

        # 专利数量
        # 由于部分该信息处于页面底部，所以会延迟加载
        try:
            patent_element = WebDriverWait(driver, 2).until(
                EC.presence_of_element_located((By.ID, "nav-main-patentCount")))
            patent_count = patent_element.find_element_by_xpath(
                './span').text.strip()

        except exceptions.NoSuchElementException:
            patent_count = 0
        except exceptions.TimeoutException:
            patent_count = 0
            # 著作权
        try:
            copyright_element = WebDriverWait(driver, 1).until(
                (EC.presence_of_element_located((By.ID, "nav-main-cpoyRCount"))))
            copyright_count = copyright_element.find_element_by_xpath(
                './span').text.strip()

        except exceptions.NoSuchElementException:
            copyright_count = 0
        except exceptions.TimeoutException:
            copyright_count = 0

            # 地区
        province, city = judge_location(location, company_name)

        # 主要负责人
        try:
            principal = page_source.xpath('//*[@id="_container_baseInfo"]/table[1]/tbody/tr[1]/td[1]/div/div[1]/div[2]/div[1]/a/text()')[0]
        except IndexError:
            principal = ''

        try:
            holders=[i.replace('\n','').replace('  ','') for i in page_source.xpath('//a[@tyc-event-ch="CompangyDetail.gudong.gongsi"]/text() | //a[@tyc-event-ch="CompangyDetail.gudong.ziranren"]/text()')]
            holders_invest_par = page_source.xpath('//*[@id="_container_holder"]/table/tbody//tr/td[3]/div/div/span/text()')

        except Exception as e:
            if 'id="nav-main-holderCount">股东信息' in page_source:
                print(company_name + ' 没有股东信息')
            else:
                print(company_name + ' 股东信息出错')

        son_company_list = []
        son_company_invest_list = []

        try:
            son_company_num = contain_element.find_element_by_xpath('//div[@id="nav-main-inverstCount"]/span').text
            '//*[@id="nav-main-inverstCount"]/span'
            son_company_num = int(son_company_num)
        except exceptions.NoSuchElementException:
            son_company_num = 0

        if son_company_num == 0:
            pass
        else:
            try:
                try:
                    son_company_element = driver.find_element_by_xpath(
                        '//*[@id="_container_invest"]/table/tbody')
                except exceptions.NoSuchElementException:
                    adjust_num -= 1
                son_company_pages = (son_company_num - 1) // 20 + 1

                if son_company_pages <= 1:
                    son_company_list = page_source.xpath('//div[@id="_container_invest"]/table/tbody/tr/td[2]/a/text()')
                    if not son_company_list:
                        son_company_list = page_source.xpath('//*[@id="_container_holder"]/table/tbody/tr/td[2]/div/div[2]/a/text()')
                    #//*[@id="_container_holder"]/table/tbody/tr/td[2]/div/div[2]/a
                    #//*[@id="_container_invest"]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/a
                    #//*[@id="_container_invest"]/table/tbody/tr[11]/td[2]/table/tbody/tr/td[2]/a
                    son_company_invest_list = page_source.xpath('//*[@id="_container_invest"]/table/tbody//tr/td[5]/span/text()')
                else:
                    for page in range(1, son_company_pages + 1):

                        time.sleep(2)
                        for j in range(1, min(20, son_company_num - (page - 1) * 20) + 1):
                            son_company_name = driver.find_element_by_xpath(
                                '//*[@id="_container_invest"]/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[2]/a' % j).text.strip()

                            if son_company_name == '':
                                son_company_name = driver.find_element_by_xpath(
                                    '//*[@id="_container_invest"]/table/tbody/tr[%d]/td[2]/table/tbody/tr/td[2]/a' % j).get_attribute('innerHTML').text.strip()
                            try:
                                son_company_invest_percent = driver.find_element_by_xpath(
                                    '//*[@id="_container_invest"]/table/tbody/tr[%d]/td[5]/span' % j).text

                            except:
                                son_company_invest_percent = None
                            son_company_list.append(son_company_name)
                            son_company_invest_list.append(son_company_invest_percent)

                        driver.find_element_by_xpath(
                            '//*[@id="_container_invest"]/div/ul/li[last()]/a').click()


            except Exception as e:
                if '对外投资</div><div class="company-nav-item-disable position-rel' in driver.page_source:
                    print(company_name + ' 没有对外投资信息')
                else:
                    print(company_name + ' 对外投资信息出错')


        # companyJingpin = []
        # Js = 'window.open("{}")'
        # try:
        #     jinpin_element = driver.find_element_by_xpath('//*[@id="_container_jingpin"]')
        # except exceptions.NoSuchElementException:
        #     pass
        # else:
        #     count_jinpin = int(driver.find_element_by_xpath('//*[@id="nav-main-companyJingpin"]/span').text)
        #     page_jinpin = (count_jinpin-1)//10 + 1
        #     if page_jinpin <= 1:
        #         for i in range(1,count_jinpin+1):
        #             try:
        #                 url9 = driver.find_element_by_xpath(
        #                     '//*[@id="_container_jingpin"]/div/table/tbody/tr[{}]/td[2]/table/tbody/tr/td[2]/a'.format(
        #                         i)).get_attribute('href')
        #                 driver.execute_script(Js.format(url9))
        #                 driver.switch_to.window(driver.window_handles[1])
        #                 companyJingpin.append(
        #                     driver.find_element_by_xpath('//*[@id="project_web_top"]/div[2]/div[1]/a').text)
        #                 driver.close()
        #                 driver.switch_to.window(driver.window_handles[0])
        #                 #//*[@id="_container_jingpin"]/div/table/tbody/tr[9]/td[2]/table/tbody/tr/td[2]/a
        #             except exceptions.NoSuchElementException:
        #                 continue
        #     else:
        #         for page in range(1, page_jinpin+1):
        #             time.sleep(2)
        #             for i in range(1, min(10, count_jinpin - (page - 1) * 10) + 1):
        #                 try:
        #                     url9 = driver.find_element_by_xpath(
        #                         '//*[@id="_container_jingpin"]/div/table/tbody/tr[{}]/td[2]/table/tbody/tr/td[2]/a'.format(i)).get_attribute('href')
        #                     driver.execute_script(Js.format(url9))
        #                     driver.switch_to.window(driver.window_handles[1])
        #                     companyJingpin.append(
        #                         driver.find_element_by_xpath('//*[@id="project_web_top"]/div[2]/div[1]/a').text)
        #                     driver.close()
        #                     driver.switch_to.window(driver.window_handles[0])
        #                 except exceptions.NoSuchElementException:
        #                     continue
        #             print(companyJingpin)
        #             driver.find_element_by_xpath('//*[@id="_container_jingpin"]/div/div/ul/li[last()]/a').click()
        #             #
        #             # elif page == page_jinpin:
        #             #     pass
        #             # else:
        #             #     driver.find_element_by_xpath(
        #             #         '//*[@id="_container_jingpin"]/div/div/ul/li[{}]/a'.format(page + 2)).click()
        # companyJingpin = list(set(companyJingpin))

    except exceptions.NoSuchElementException:
        retry_time += 1
        if retry_time < 3:
            return _search_detail_in_tianyancha(href, history_name, retry_time)
        else:
            return {}
        # 曾用名

    if not history_name:
        page_source1 = etree.HTML(driver.page_source)
        if '曾用名' in driver.page_source:
            history_name_list = page_source1.xpath(
                '//*[@id="company_web_top"]/div[2]/div[2]/div[3]/div/div/div/text()')
            history_name = history_name_list
            # print('----888:', history_name)

    patent_count = int(patent_count)
    copyright_count = int(copyright_count)
    contact_way = {"电话": contact_phone, "邮箱": contact_mail}
    qualification = {"专利数": patent_count, "著作权数": copyright_count}
    holders_info = {"股东": holders, "出资比例": holders_invest_par}
    search_link = href

    invest_info = {"子公司": son_company_list, "出资比例": son_company_invest_list}
    # print(history_name)

    return {"企业全称": company_name, "曾用名": history_name, "成立日期": founded_time, "地区": city, "省份": province,
            "注册资本": money, "联系方式": contact_way, "网站": web_site, "主要负责人": principal, "股东状况": holders_info,
            "对外投资": invest_info, "企业类型": company_type,
            "企业资质": qualification, "链接": search_link, "公司地址": location, "行业": industry, "是否上市": on_list,
            "企业状态": status
        # , '竞品信息':companyJingpin
            }


def deal_with_tianyancha_js_num_trick(str):
    '''
    应付天眼查js变换数字
    :param str: 字符串
    :return: 处理后的字符串
    '''

    output = []
    str = str.strip()
    with open('./handle_tianyancha_font/local_fontdict.txt', 'r') as f:
        fontdict = f.read()
    tianyancha_fontdict = eval(fontdict)
    for i in str:
        if i in tianyancha_fontdict:
            output.append(tianyancha_fontdict[i])
        else:
            output.append(i)
    new_str = ''.join(output)
    return new_str


def filter_company_by_blacklist_api(detail_list,is_keep_crm=False):
    """
    根据黑名单法过滤公司
    :param detail: 公司详细信息
    :param is_keep_crm  是否保存crm中的公司
    :return: 过滤后的公司
    """
    # 需要初始化

    if whole_blacklist_company_list_name is None:
        filter_data_init()
    print("黑名单法:"+str(whole_blacklist_company_list_name))
    if is_keep_crm:
        return detail_list
    else:
        # return [i for i in detail_list if str(i['_id']) not in whole_blacklist_company_list]
        return [i for i in detail_list if str(i['企业全称']) not in whole_blacklist_company_list_name]
        # for i in detail_list:
        #     if str(i['企业全称'])  in whole_blacklist_company_list_name:
        #         print("黑名单法淘汰公司: " + str(i['企业全称']))




def is_state_own_company(detail):
    '''
    判断企业是否国企
    :param detail:企业详细信息
    :return: 是否国企
    '''
    if '政府' in detail['企业全称'] or '国务院' in detail['企业全称'] or detail['企业全称'].endswith('大学') or '国有资' in detail[
        '企业全称'] or '国资委' in detail['企业全称']:
        return True

    if '股东状况' in detail and '股东' in detail['股东状况']:
        for holder in detail['股东状况']['股东']:
            if '国务院' in holder or '国有资' in holder or '国资委' in holder:
                return True

    if '企业类型' in detail:
        if '全民' in detail['企业类型'] or '国有' in detail['企业类型'] or '事业' in detail['企业类型']:
            return True

    if '股东状况' in detail:
        largest_holder = get_largest_holder(detail)
        if not largest_holder:
            return False
        if '公司' not in largest_holder and len(largest_holder) < 5:
            return False
        else:
            mother_company_detail = search_company_in_cluster_api([largest_holder])
            if mother_company_detail:
                try:
                    return is_state_own_company(mother_company_detail[0])
                except RecursionError:
                    return False
            else:
                return False
    return False


def is_stock_relate_company(detail):
    '''
    判断企业是否上市公司控股企业
    :param detail: 企业详细信息
    :return: 是否
    '''
    if detail['是否上市']:
        return True
    else:
        largest_holder = get_largest_holder(detail)
        if not largest_holder:
            return False
        if '公司' not in largest_holder and len(largest_holder) < 5:
            return False
        else:
            mother_company_detail = search_company_in_cluster_api([largest_holder])
            if mother_company_detail:
                try:
                    return is_stock_relate_company(mother_company_detail[0])
                except RecursionError:
                    return False
            else:
                return False


def get_largest_holder(detail):
    '''
    获取公司最大控股股东，若所有股东占比均小于20%，则返回None
    :param detail: 企业详细信息
    :return: 最大股东名称
    '''
    if '股东状况' in detail and len(detail['股东状况']['股东']) == 1:
        return detail['股东状况']['股东'][0]
    else:
        if not detail['股东状况']['出资比例']:
            return None
        percent_list = [
            float(item.replace("%", "").replace("-", "0").replace("未公开", "0").replace("<", "")) if item else 0 for
            item in detail['股东状况']['出资比例']]
        max_percent = max(percent_list)
        if max_percent < 20:
            return None
        else:
            index = percent_list.index(max_percent)
            return detail['股东状况']['股东'][index]


def filter_company_by_detail_api(detail):
    """
    筛选公司入口
    根据地区，注册资本，行业，成立日期，股东，是否上市，是否有专利或著作权这七个维度筛选公司
    :param detail: 公司详细信息
    :return: True or False
    """
    if address_list is None:
        filter_data_init()
    if not detail:
        return False

    flag_dict = {'district': False, 'money': False, 'industry': False, 'build_date': False, 'holders': False,
                 'listed': False, "patent": False}
    # 地点
    # address_list = _read_whitelist('whitelist_district', 'city')
    province = detail['省份']
    city = detail['地区']
    # for ad in address_list:
    #     if ad in province or ad in city:
    #         flag_dict['district'] = True
    #         break
    if province in address_list and city in address_list:
        flag_dict['district'] = True
    elif province in address_list and city == '':
        flag_dict['district'] = True
    elif city in address_list:
        flag_dict['district']=True
    else:
        flag_dict['district'] = False

    # if not province and not city:
    #     flag_dict['district'] = False

    # 行业
    # industry_list = _read_whitelist('whitelist_industry', 'industry')
    industry = detail['行业']
    for ind in industry_list:
        # 不比较全部字符串，可能是有意为之
        if ind in industry:
            flag_dict['industry'] = True
            break
    # 资金 取消大于500w要求
    flag_dict['money'] = True
    # try:
    #     if FILTER_COMPANY_MONEY_LOWER_LIMIT <= detail['注册资本'] <= FILTER_COMPANY_MONEY_UPPER_LIMIT:
    #         flag_dict['money'] = True
    # except TypeError:
    #     flag_dict['money'] = False

    # 注册时间
    build_date = detail['成立日期']
    try:
        date = datetime.datetime.strptime(build_date, '%Y-%m-%d')
        diff = datetime.datetime.now() - date
        flag_dict['build_date'] = (diff.days <= FILTER_COMPANY_YEAR * 365)
    except:
        flag_dict['build_date'] = True

    # 是否有机构股东
    flag_dict['holders'] = True
    # for s in detail['股东状况']['股东']:
    #     if '有限' in s or '公司' in s:
    #         flag_dict['holders'] = False
    # #
    # 企业资质[专利数，著作权数]
    # flag_dict['patent'] = sum(detail['企业资质'].values()) > 0
    flag_dict['patent'] = True
    # 是否上市
    flag_dict['listed'] = not detail['是否上市']

    # 过滤已注销公司
    if '企业状态' in detail and ('注销' in detail['企业状态'] or '吊销' in detail['企业状态']):
        print(detail['企业全称'], '企业状态异常')
        return False

    for flag in flag_dict.values():
        if not flag:
            # print(detail['企业全称'], flag_dict)
            return False

    # write_filtered_company_to_db(detail)
    return True


def search_company_api(company_name, is_update=False, search_type=1):
    """
    查找公司详细消息入口：
    先从数据库查找公司名字，数据库没有的话就去天眼查找，如果没有去企查查查找，找后输入到数据库中
    返回公司详细信息
    :param is_update True代表获取网站最新数据，False代表获取数据库数据
    :param search_type 1代表
    """
    global retry_time, driver

    try:
        generate_fontdict_without_ttf()
    except:
        generate_fontdict()
    company_name = _company_name_strip(company_name)
    try:
        detail = get_company_detail_from_db_api(company_name)
        if not detail or is_update:
            print("从网页获取详细信息")

            if search_type == 1:

                # if isinstance(driver, webdriver.phantomjs.webdriver.WebDriver):
                #     pass
                # else:
                if driver:
                    driver.quit()
                init(is_change_ip=False)

                href, history_name = _search_mainpage(company_name)
                if href:
                    detail = _search_detail_in_tianyancha(href, history_name)

                if not href or not detail:
                    driver.quit()
                    init(is_chrome=True)
                    detail = _search_detail_in_qichacha(company_name)
            elif search_type == 2:
                if driver is None:
                    init(is_change_ip=False)
                href, history_name = _search_mainpage(company_name)
                if href:
                    detail = _search_detail_in_tianyancha(href, history_name)
            elif search_type == 3:
                if driver is None:
                    init(is_chrome=True)
                detail = _search_detail_in_qichacha(company_name)

            if not detail:
                detail = {}
            if '企业全称' in detail and company_name != detail["企业全称"] and company_name not in detail[
                '曾用名'] and company_name + '（有限合伙）' != detail["企业全称"]:
                detail = {}

            # 天眼查经常会缺少 有限合伙 这几个字
            if '企业全称' in detail and company_name + '（有限合伙）' == detail["企业全称"]:
                detail['曾用名'].append(company_name)
            if detail:
                retry_time = max(0, retry_time - 2)
                try:
                    detail = write_raw_company_to_db(detail)
                except pymongo.errors.DuplicateKeyError:
                    if detail['企业全称'] != company_name:
                        update_raw_company_used_name(detail, company_name)
                    else:
                        update_raw_company_to_db(detail)
            print(detail)
    except pymongo.errors.ServerSelectionTimeoutError:

        print("网络中断,重试中a")
        return search_company_api(company_name, is_update, search_type)
    except pymongo.errors.NetworkTimeout:
        print("网络中断,重试中b")
        return search_company_api(company_name, is_update, search_type)
    except pymongo.errors.AutoReconnect:
        print("网络中断,重试中c")
        return search_company_api(company_name, is_update, search_type)
    except ConnectionResetError:
        print("网络中断，重试中d")
        return search_company_api(company_name, is_update, search_type)
    except exceptions.TimeoutException:
        # driver.save_screenshot("ddd.png")
        retry_time += 1
        if retry_time > MAX_RETRY_TIME - 2:
            restart_driver()
        if retry_time > MAX_RETRY_TIME:
            driver.quit()
            raise RetryTimesTooMuchError
        print("网络中断,重试中e")

        return search_company_api(company_name, is_update, search_type)
    except requests.ConnectionError:
        print("网络中断，重试中f")
        return search_company_api(company_name, is_update, search_type)

    except urllib.error.URLError:
        print('链接错误，重启浏览器g')
        retry_time += 1
        if retry_time > MAX_RETRY_TIME - 2:
            restart_driver()
        if retry_time > MAX_RETRY_TIME:
            raise RetryTimesTooMuchError
        return search_company_api(company_name, is_update, search_type)
    except requests.exceptions.ChunkedEncodingError:
        print("网络中断，重试中h")
        return search_company_api(company_name, is_update, search_type)
    except exceptions.StaleElementReferenceException:
        print("网络中断，重试中i")
        restart_driver()
        return search_company_api(company_name, is_update, search_type)
    if not detail:
        print("找不到公司" + company_name)
        # return search_company_api(company_name, is_update)
        # raise CompanyNotFoundError
    return detail


def search_iteration_investment_company_api(company_name, mother_company_list=None, is_update=False):
    """
    机构跟踪法入口
    :param company_name:机构名称
    :param mother_company_list:机构母公司列表
    :param is_update: 是否更新机构持股信息
    :return:
    """
    global retry_time
    # if driver is None:
    #     init()
    print("查找公司 " + company_name + " 中")
    if mother_company_list is None:
        mother_company_list = []
    mother_company_list.append(company_name)

    detail = {'企业全称': company_name}
    db = mongodb_connection('company')
    collection = db['investment_company']
    ret = collection.find_one(detail)

    non_investment_company_id_list = []
    investment_company_id_list = []

    try:
        as_raw_company_detail = search_company_in_cluster_api([company_name], 30)[0]
    except IndexError:
        return None

    if not ret:
        db = mongodb_connection('company')
        collection = db['investment_company']
        _id = collection.insert(detail)
    else:
        _id = ret['_id']

    if (not ret) or ('公司列表' not in ret) or is_update:
        try:
            # company_list = _search_single_investment_company_in_qichacha(company_name)
            try:
                company_list = as_raw_company_detail['对外投资']['子公司']
            except KeyError:
                company_list = []

            investment_company_list = []
            non_investment_company_list = []
            for i in company_list:
                if is_investment_company(i, mother_company_list):
                    investment_company_list.append(i)
                else:
                    non_investment_company_list.append(i)

            non_investment_company_detail_list = search_company_in_cluster_api(non_investment_company_list)
            for i in non_investment_company_detail_list:
                if i:
                    non_investment_company_id_list.append(DBRef("raw_company", i['_id']))

            for i in investment_company_list:
                if i not in mother_company_list:
                    res = search_iteration_investment_company_api(i, mother_company_list, is_update)
                    if res:
                        investment_company_id_list.append(DBRef("investment_company", res))
            if ret:
                if '基金公司列表' in ret:
                    investment_company_id_list = list(set(investment_company_id_list + ret['基金公司列表']))
                if '公司列表' in ret:
                    non_investment_company_id_list = list(set(non_investment_company_id_list + ret['公司列表']))

            mongodb_connection('company', False)['investment_company'].update({"企业全称": company_name}, {
                "$set": {"详细信息": DBRef('raw_company', as_raw_company_detail['_id']),
                         "基金公司列表": investment_company_id_list, "公司列表": non_investment_company_id_list,
                         "更新时间": time.strftime('%Y-%m-%d', time.localtime(time.time()))}})

        except requests.exceptions.ProxyError as e:
            print("网络中断，重试中1")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except requests.exceptions.ConnectionError as e:
            print("网络中断，重试中2")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except pymongo.errors.ServerSelectionTimeoutError:
            print("网络中断,重试中3")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except pymongo.errors.NetworkTimeout:
            print("网络中断,重试中4")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except pymongo.errors.AutoReconnect:
            print("网络中断,重试中5")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except exceptions.TimeoutException as e:
            print("网络中断，重试中6")
            retry_time += 1
            if retry_time > MAX_RETRY_TIME - 2:
                restart_driver()
            if retry_time > MAX_RETRY_TIME:
                raise RetryTimesTooMuchError
            time.sleep(10)
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except urllib.error.URLError as e:
            print("网络中断，重试中7")
            retry_time += 1
            if retry_time > MAX_RETRY_TIME - 2:
                restart_driver()
            if retry_time > MAX_RETRY_TIME:
                raise RetryTimesTooMuchError
            time.sleep(15)
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except http.client.RemoteDisconnected as e:
            print("网络中断，重试中8")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        except ConnectionResetError as e:
            print("网络中断，重试中9")
            return search_iteration_investment_company_api(company_name, mother_company_list, is_update)
        if not _id:
            print("找不到公司" + company_name)
            # raise CompanyNotFoundError
    print("结束公司 " + company_name + "的查找")
    return _id


def is_son_company_of_listed_company(company_name, mother_company_name_list):
    """
        判断company是否上市公司子公司
        :param company_name:公司名
        :param mother_company_name_list:母公司列表
        :return:True or False
        """
    # if '有限合伙' in company_name:
    #     return True
    # else:
    #     try:
    #         detail = get_company_detail_from_db_api(company_name)
    #     except IndexError:
    #         return False
    #     sum_percent = 0
    #     for mother_company_name in mother_company_name_list:
    #         if mother_company_name in detail['股东状况']['股东']:
    #             percent = detail['股东状况']['出资比例'][detail['股东状况']['股东'].index(mother_company_name)]
    #
    #             try:
    #                 percent = percent.replace("%", "")
    #                 percent = float(percent)
    #                 sum_percent += percent
    #             except Exception:
    #                 print(company_name + "股东占比未知")
    #
    #     if sum_percent >= 50:
    #         return True
    #     else:
    #         return False
    return True


def search_iteration_son_company_of_listed_company_api(company_name, mother_company_list=None, is_update=False):
    """
    查找上市公司子公司
    :param company_name:公司名称
    :param mother_company_list:母公司列表
    :param is_update: 是否更新机构持股信息
    :return:
    """
    global retry_time
    if driver is None:
        init()
    print("查找公司 " + company_name + " 中")
    if mother_company_list is None:
        mother_company_list = []
    mother_company_list.append(company_name)

    detail = {'企业全称': company_name}
    db = mongodb_connection('company')
    collection = db['listed_investment_company']
    ret = collection.find_one(detail)

    son_company_id_list = []

    if not ret:
        db = mongodb_connection('company', False)
        collection = db['listed_investment_company']
        _id = collection.insert(detail)
    else:
        _id = ret['_id']

    if (not ret) or ('子公司列表' not in ret) or is_update:
        try:
            as_raw_company_detail = search_company_in_cluster_api([company_name])[0]
        except IndexError:
            return None

        try:
            company_list, company_percent_list = _search_single_investment_company_in_db(company_name)
            if company_list:
                retry_time = max(retry_time - 2, 0)
            son_company_list = []
            son_company_percent_list = []
            search_company_in_cluster_api(company_list)
            for i in company_list:
                if is_son_company_of_listed_company(i, mother_company_list):
                    son_company_list.append(i)

            for i in son_company_list:
                if i not in mother_company_list:
                    res = search_iteration_son_company_of_listed_company_api(i, mother_company_list, is_update)
                    if res:
                        son_company_id_list.append(DBRef("listed_investment_company", res))
                        son_company_percent_list.append(company_percent_list[company_list.index(i)])
            if ret:
                if '子公司列表' in ret:
                    son_company_id_list = list(set(son_company_id_list))

            mongodb_connection('company', False)['listed_investment_company'].update_many({"企业全称": company_name}, {
                "$set": {"详细信息": DBRef('raw_company', as_raw_company_detail['_id']),
                         "子公司列表": son_company_id_list, "子公司出资比例": son_company_percent_list,
                         "更新时间": time.strftime('%Y-%m-%d', time.localtime(time.time()))}})

        except requests.exceptions.ProxyError as e:
            print("网络中断，重试中1")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except requests.exceptions.ConnectionError as e:
            print("网络中断，重试中2")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except pymongo.errors.ServerSelectionTimeoutError:
            print("网络中断,重试中3")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except pymongo.errors.NetworkTimeout:
            print("网络中断,重试中4")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except pymongo.errors.AutoReconnect:
            print("网络中断,重试中5")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except exceptions.TimeoutException as e:
            print("网络中断，重试中6")
            retry_time += 1
            if retry_time == MAX_RETRY_TIME - 2:
                restart_driver()
            if retry_time > MAX_RETRY_TIME:
                raise RetryTimesTooMuchError
            time.sleep(10)
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except urllib.error.URLError as e:
            print("网络中断，重试中7")
            retry_time += 1
            if retry_time > MAX_RETRY_TIME - 2:
                restart_driver()
            if retry_time > MAX_RETRY_TIME:
                raise RetryTimesTooMuchError
            time.sleep(15)
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except http.client.RemoteDisconnected as e:
            print("网络中断，重试中8")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        except ConnectionResetError as e:
            print("网络中断，重试中9")
            return search_iteration_son_company_of_listed_company_api(company_name, mother_company_list.remove(
                company_name) if mother_company_list else [], is_update)
        if not _id:
            print("找不到公司" + company_name)
            # raise CompanyNotFoundError
    print("结束公司 " + company_name + "的查找")
    return _id


def search_company_in_tianyancha_background_api():
    """
    从mongodb读取未完成的公司查询列表，并在后台查询，将查询好的结果存回数据库
    可在服务器或本地机器后台多线程运行
    """

    try:
        generate_fontdict_without_ttf()
    except:
        print("获取天眼查字符异常")
        sys.exit()
    if driver is None:
        init(is_change_ip=False)
    try:
        unfinished_company_collection = mongodb_connection("company")['unfinished_company']
        while True:
            count = unfinished_company_collection.find(
                {'$or': [{"状态": "not started"}, {'状态': 'qichacha not found'}]}).count()
            magic_number = random.randint(0, 200)

            print("magic_num: "+str(magic_number))
            if count == 0:
                # 定期清理unfinished_company表的已完成
                if magic_number == 88:
                    unfinished_company_collection.delete_many(
                        {"状态": "finished", "更新时间": {'$lt': datetime.datetime.now() - datetime.timedelta(days=0.1)}})
                elif magic_number == 98:
                    add_not_found_blacklist_api()

                elif magic_number < 8:
                    driver.quit()
                    return

                time.sleep(10 + random.randint(0, 30))


            else:
                if 98 < magic_number < 113 and 'tianyancha' in driver.current_url:
                    cookies = restart_with_cookies()
                    try:
                        driver.get('https://www.tianyancha.com/')

                    except exceptions.TimeoutException:
                        return search_company_in_tianyancha_background_api()
                    driver.delete_all_cookies()
                    for i in cookies:
                        if 'www' not in i['domain']:
                            if 'expires' in i:
                                del i['expires']
                            driver.add_cookie(i)
                com_detail = unfinished_company_collection.find_one_and_update(
                    {'$or': [{"状态": "not started"}, {'状态': 'qichacha not found'}]},
                    {"$set": {"状态": "not finished",
                              "更新时间": datetime.datetime.now()}})
                if com_detail:
                    com_name = com_detail['企业全称']

                    d = search_company_api(com_name, search_type=2, is_update=True)
                    if d:
                        unfinished_company_collection.update_many({"企业全称": com_name},
                                                                  {'$set': {"状态": "finished",
                                                                            "更新时间": datetime.datetime.now()}})
                    else:
                        if com_detail['状态'] == 'qichacha not found':
                            unfinished_company_collection.update_many({"企业全称": com_name},
                                                                      {'$set': {"状态": "not found",
                                                                                "更新时间": datetime.datetime.now()}})
                        else:
                            unfinished_company_collection.update_many({"企业全称": com_name},
                                                                      {'$set': {"状态": "tianyancha not found",
                                                                                "更新时间": datetime.datetime.now()}})
    except pymongo.errors.AutoReconnect:
        search_company_in_tianyancha_background_api()


def search_company_in_qichacha_background_api():
    """
    从mongodb读取未完成的公司查询列表，并在后台查询，将查询好的结果存回数据库
    可在服务器或本地机器后台多线程运行
    """
    if driver is None:
        init(is_chrome=True)
    try:
        while True:
            count = mongodb_connection("company")['unfinished_company'].find(
                {'$or': [{'状态': 'tianyancha not found'}, {'状态': 'not started'}]}).count()
            magic_number = random.randint(0, 200)
            print('magic_num'+str(magic_number))

            if count == 0:
                # 定期清理unfinished_company表的已完成
                if magic_number == 88:
                    mongodb_connection("company")['unfinished_company'].delete_many(
                        {"状态": "finished", "更新时间": {'$lt': datetime.datetime.now() - datetime.timedelta(days=0.1)}})
                elif magic_number == 98:
                    add_not_found_blacklist_api()

                elif magic_number < 8:
                    driver.quit()
                    return
                time.sleep(10 + random.randint(0, 10))
            else:

                if 98 < magic_number < 113 and 'qichacha' in driver.current_url:
                    cookies = restart_with_cookies()
                    try:
                        driver.get('https://www.qichacha.com/')
                    except exceptions.TimeoutException:
                        return search_company_in_qichacha_background_api()
                    driver.delete_all_cookies()
                    for i in cookies:
                        if 'www' not in i['domain']:
                            if 'expires' in i:
                                del i['expires']
                            driver.add_cookie(i)

                com_detail = mongodb_connection("company")['unfinished_company'].find_one_and_update(
                    {'$or': [{'状态': 'tianyancha not found'}]},
                    {"$set": {"状态": "not finished",
                              "更新时间": datetime.datetime.now()}})
                if com_detail:
                    com_name = com_detail['企业全称']

                    d = search_company_api(com_name, search_type=3, is_update=True)
                    if d:
                        mongodb_connection("company")['unfinished_company'].update_many({"企业全称": com_name},
                                                                                        {'$set': {"状态": "finished",
                                                                                                  "更新时间": datetime.datetime.now()}})
                    else:
                        if com_detail['状态'] == 'tianyancha not found':
                            mongodb_connection("company")['unfinished_company'].update_many({"企业全称": com_name},
                                                                                            {'$set': {"状态": "not found",
                                                                                                      "更新时间": datetime.datetime.now()}})
                        else:
                            mongodb_connection("company")['unfinished_company'].update_many({"企业全称": com_name},
                                                                                            {'$set': {
                                                                                                "状态": "not found",
                                                                                                "更新时间": datetime.datetime.now()}})
    except pymongo.errors.AutoReconnect:
        search_company_in_qichacha_background_api()


def search_company_in_cluster_api(company_list, allow_day=None):
    """
    利用分布式多机器查找公司信息，建议当company_list预计达到10以上再用此方法，否则用search_company_api的方法
    用此方法时需确保有机器在运行search_company_in_qcc_background.py和search_company_in_tyc_background.py
    :param company_list:公司名称列表
    :param allow_day: 允许的时间，若公司的爬取时间在该时间间隔内，则不爬取，若不是，重新爬取
    :return:公司详细信息列表
    """
    if not driver:
        init()
    if not company_list:
        return []
    print("need" + str(company_list))

    unfinished_list = []
    detail_list = []
    insert_list = []
    # 如果企业名字不在黑名单里
    company_name_list = [_company_name_strip(a) for a in company_list if
                         a not in blacklist_company_name_list and _company_name_strip(
                             a) not in blacklist_company_name_list]

    if company_name_list:
        # 从企业库找到企业细节信息
        if not allow_day:
            detail = mongodb_connection('company')['raw_company'].find({'企业全称': {'$in': company_name_list}})
        else:
            detail = mongodb_connection('company')['raw_company'].find({'企业全称': {'$in': company_name_list}, '爬取时间': {
                '$gte': datetime.datetime.now() - datetime.timedelta(days=allow_day)}})

        detail_list.extend(detail)
        # 使用企业名单减去企业库里有的企业名单，得到现在的企业名单
        company_name_list = list(set(company_name_list) - set([a['企业全称'] for a in detail_list]))

    else:
        return detail_list

    if company_name_list:
        # 查看是否是企业曾用名
        if not allow_day:
            detail_for_used_name = mongodb_connection('company')['raw_company'].find(
                {'曾用名': {'$in': company_name_list}})
        else:
            detail_for_used_name = mongodb_connection('company')['raw_company'].find(
                {'曾用名': {'$in': company_name_list},
                 '爬取时间': {'$gte': datetime.datetime.now() - datetime.timedelta(days=allow_day)}})
        detail_list.extend(detail_for_used_name)
        # 除去这一部分企业
        company_name_list = list(set(company_name_list) - set(reduce(lambda a, b: a + b['曾用名'], detail_list, [])))
    else:
        return detail_list

    # 剩下的便是未处理的企业
    for company_name in company_name_list:

        for i in insert_list:
            if i["企业全称"]==company_name:
                pass
            else:
                insert_list.append({"企业全称": company_name, "状态": "not started", "更新时间": datetime.datetime.now()})
        if {"企业全称": company_name} in unfinished_list:
            pass
        else:
          unfinished_list.append({"企业全称": company_name})


    if insert_list:
        mongodb_connection('company')['unfinished_company'].insert_many(insert_list)

    if not unfinished_list:
        print("get" + str(detail_list))
        return detail_list
    # collection = mongodb_connection_local('company')['unfinished_company']

    while True:
        flag = True
        for i in mongodb_connection('company')['unfinished_company'].find({"$or": unfinished_list}):
            if i['状态'] == "not started":
                flag = False
                break
            if i['状态'] == 'not finished':
                if (datetime.datetime.now() - i['更新时间']).seconds > 2 * 60:
                    mongodb_connection('company')['unfinished_company'].update({"企业全称": i['企业全称']},
                                                                               {"$set": {"状态": "not started"}})
                flag = False
                break
            if i['状态'] == 'tianyancha not found' or i['状态'] == 'qichacha not found':
                flag = False
                break
        if flag:
            for com in unfinished_list:
                company_name = com['企业全称']
                detail = get_company_detail_from_db_api(company_name)
                if detail:
                    detail_list.append(detail)
            print("get" + str(detail_list))
            return detail_list
        else:
            print("未找到企业信息，请更新未处理的企业库")
            time.sleep(20)


if __name__ == '__main__':
    # search_iteration_investment_company_api("'北京风云际会投资管理有限公司'", None, True)
    # search_company_api('江苏汇博机器人技术有限公司', True, search_type=2)
    # filter_data_init()
    # print(str(whole_blacklist_company_list))
    # search_company_in_background_api()
    # _search_detail_in_qichacha('南京优科生物医药集团股份有限公司')
    # search_company_in_qichacha_background_api()
    search_company_in_tianyancha_background_api()

