# coding:utf-8
'''
用企查查后台爬取企业信息
'''
from wotou.spiders.company.spider_company import search_company_in_qichacha_background_api
import time
import multiprocessing
import psutil
from multiprocessing import cpu_count
from wotou.lib.db_connection import mongodb_connection

if __name__ == '__main__':
    time.sleep(0)
    # MAX_PROCESS_NUM = int(cpu_count()*0.8)
    MAX_PROCESS_NUM=1
    p_list = []
    while True:
        if (psutil.cpu_percent(interval=1) < 70 or len(p_list)==0) and len(p_list) < MAX_PROCESS_NUM and mongodb_connection("company")['unfinished_company'].find(
                {'$or': [{'状态':'tianyancha not found'}]}).count()>0:
            p = multiprocessing.Process(target=search_company_in_qichacha_background_api)
            p.start()
            p_list.append(p)
        for i in p_list:
            if not i.is_alive():
                p_list.remove(i)
        print("当前线程数:"+str(len(p_list)))
        time.sleep(15+10*len(p_list))
