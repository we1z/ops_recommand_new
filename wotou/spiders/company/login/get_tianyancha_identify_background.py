# coding:utf-8
'''
获取天眼查验证码的背景图
'''

import cv2
import os
import shutil
from PIL import Image

info_list = []
deal_filename_list = []
png_h = 102
png_w = 322

for filename in os.listdir('../tianyancha_identify'):
    if filename.endswith(".png"):
        image = cv2.imread("../tianyancha_identify/" + filename)
        cv2.imshow("Original", image)

        hist = cv2.calcHist([image], [0], None, [256], [0, 256])
        info_list.append({"filename": filename, "hist": hist})

for i in info_list:

    filename1 = i['filename']
    hist1 = i['hist']
    if filename1 in deal_filename_list:
        continue
    similar_list = []
    for j in info_list:
        filename2 = j['filename']
        hist2 = j['hist']
        similar = 0
        sum = 0
        for k in range(0, 256):
            similar += abs(hist1[k][0] - hist2[k][0])
            sum += max(hist2[k][0], hist1[k][0])
        similar = 1 - similar / sum
        similar_list.append({"similar": similar, "filename1": filename1, "filename2": filename2})
    similar_list.sort(key=lambda item: item['similar'], reverse=True)
    for l in range(0, len(similar_list)):
        if l > 0 and similar_list[l - 1]['similar'] - similar_list[l]['similar'] > 0.1:
            break
        if similar_list[l]['filename2'] in deal_filename_list:
            continue
        deal_filename_list.append(similar_list[l]['filename2'])
        os.makedirs('../tianyancha_identify/' + filename1.split(".")[0], exist_ok=True)
        shutil.copy('../tianyancha_identify/' + similar_list[l]['filename2'],
                    '../tianyancha_identify/' + filename1.split(".")[0])

for filename in os.listdir('../tianyancha_identify'):
    background_pic = None
    if not filename.endswith(".png"):
        png_list = []
        for png_name in os.listdir('../tianyancha_identify/' + filename):
            img = Image.open('../tianyancha_identify/' + filename + "/" + png_name)
            png_list.append(img.load())
            if not background_pic:
                background_pic = img.copy()

        for w in range(0, png_w):
            for h in range(0, png_h):
                pix_list = [item[w, h] for item in png_list]
                pix_set = list(set(pix_list))
                if len(pix_set) != 1:
                    pix_set.sort(key=pix_list.count,reverse=True)

                    background_pic.putpixel((w, h), pix_set[0])

        background_pic.save('../tianyancha_background/'+filename+".png")
