# coding:utf-8
'''
通过破解验证码
获取企查查cookies
'''
from selenium import webdriver
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.company.login.login import login_qichacha_by_mobile
from wotou.spiders.company.login.login import login_qichacha_by_qq_for_pool

if __name__ == '__main__':

    db = mongodb_connection('company')
    col = db['qichacha_account']

    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    # driver = webdriver.Chrome(chrome_options=options)

    for i in col.find().sort('used_time'):
        if 'mobilepw' in i and 'used_time' in i:

            driver = webdriver.Chrome()
            driver.implicitly_wait(2)
            driver.set_page_load_timeout(1000)

            driver.delete_all_cookies()
            cookies = login_qichacha_by_mobile(driver, i)
            # cookies=login_qichacha_by_qq_for_pool(driver,i)
            if cookies:
                del i['used_time']
                i['cookies'] = cookies
                mongodb_connection('company', False)['qichacha_account'].update({"mobile": i['mobile']},i)

                print(driver.get_cookies())

            driver.quit()
