# coding:utf-8
"""
用于天眼查和企查查的模拟登录（cookies登录）
"""

import datetime
import random
from wotou.lib.delay_controller import time_delay_controller_tianyancha
from wotou.lib.db_connection import mongodb_connection
import time
import selenium

@time_delay_controller_tianyancha
def login_tianyancha(driver):
    '''
    用cookies登录天眼查
    :return:
    '''

    db = mongodb_connection('company',False)
    cookies_collection = db['tianyancha_account']

    if 'antirobot' in driver.current_url:    cookies_collection.update(
        {'cookies.0.name': 'RTYCID', 'cookies.0.value': driver.get_cookie('RTYCID')['value']},
        {'$set': {'state': 'expired'}})

    driver.delete_all_cookies()

    copy_cookies = ''
    cookies_list = []
    for cookies in cookies_collection.find({'state':{'$ne':'expired'}}):

        if 'used_time' not in cookies:
            copy_cookies = cookies['cookies'].copy()
            cookies_collection.update({'phone': cookies['phone']}, {'$set': {'used_time': datetime.datetime.now()
                ,'state':'used'}})
            break
        cookies_list.append(cookies['cookies'].copy())
    if len(cookies_list)==0:
        time.sleep(60)
        return login_tianyancha(driver)
    if not copy_cookies:
        copy_cookies = cookies_list[random.randint(0, len(cookies_list) - 1)]

    for j in copy_cookies:
        j['domain'] = '.tianyancha.com'

    for j in copy_cookies:
        driver.add_cookie(j)





def login_qichacha(driver):
    '''
    用cookies登录企查查
    :return:
    '''
    time.sleep(1)
    driver.delete_all_cookies()
    db = mongodb_connection('company',False)
    cookies_collection = db['qichacha_account']

    copy_cookies = ''
    cookies_list = []
    for cookies in cookies_collection.find():

        if 'used_time' not in cookies:
            copy_cookies = cookies['cookies'].copy()
            cookies_collection.update({'_id':cookies['_id']}, {'$set': {'used_time': datetime.datetime.now()}})
            break
        if 'cookies' in cookies:
            cookies_list.append(cookies['cookies'].copy())

    if not copy_cookies:
        copy_cookies = cookies_list[random.randint(0, len(cookies_list) - 1)]

    # for j in copy_cookies:
    #     j['domain'] = '.qichacha.com'

    for j in copy_cookies:
        # 出错原因未知
        try:
            driver.add_cookie(j)
        except selenium.common.exceptions.WebDriverException:
            print("Cookie登录企查查selenium.common.exceptions.WebDriverException")
            pass


def is_qichacha_cookies_useful(result):
    """
    判断当前企查查cookies是否有效
    :param result: 网页内容
    :return: True or False
    """
    # 显示未登录或需要输入验证码

    if "亲，小查告诉你个秘密:-D" in str(result):
        print("亲，小查告诉你个秘密:-D  在网页中")
        return False

    elif "您的操作过于频繁，验证后再操作"  in str(result):
        print("您的操作过于频繁，验证后再操作")
        return False
    elif "一周内保持登录状态" in str(result):
        print("一周内保持登录状态")
        return False
        # or "您的操作过于频繁，验证后再操作" in str(result) or "一周内保持登录状态" in str(result) or len(
        #     str(result)) < 300:
        #print(result)
    elif len(str(result)) < 300:
        print("网页信息太少")
        return False
    else:
        return True


def is_tianyancha_cookies_useful(url,page_source):
    '''
    判断当前天眼查cookies是否有效
    :param url: 当前driver的url
    :param page_source: 网页内容
    :return: True or False
    '''

    if 'login' in url or 'antirobot' in url or '你已在其他地点登录,请重新登录' in page_source:
        print(page_source)
        return False
    else:
        return True
