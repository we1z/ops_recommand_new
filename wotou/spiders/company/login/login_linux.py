# coding:utf-8
"""
用于天眼查和企查查的登录操作，区别于easy_login，这是破解验证码的登录操作
"""
import time
import numpy as np
import stats as sts

import os
import pygame
import cv2
import random
import datetime
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt

from selenium.webdriver.common.action_chains import ActionChains
import datetime
import http
import random
import re
import time

from selenium.common import exceptions
from selenium.webdriver.chrome.options import Options

from wotou.lib.db_connection import mongodb_connection

from PIL import Image
from wotou.spiders.util.identify_code import handle_identify_code_api
from selenium import webdriver

driver = None

import sys

sys.path.append('/home/yuanyuan/ops_recommand')

list_trace_raw_list = \
    [[(434, 564, 1512961047123), (436, 565, 1512961047754), (437, 566, 1512961047772), (438, 566, 1512961047791),
      (439, 566, 1512961047827), (441, 566, 1512961047852), (442, 567, 1512961047873), (444, 567, 1512961047902),
      (446, 567, 1512961047918), (447, 567, 1512961047932), (449, 567, 1512961047945),
      (455, 567, 1512961047957), (457, 567, 1512961047971), (463, 567, 1512961047983), (469, 567, 1512961048004),
      (478, 567, 1512961048020), (481, 567, 1512961048034), (486, 567, 1512961048046), (488, 567, 1512961048055),
      (489, 567, 1512961048068), (494, 567, 1512961048080), (501, 567, 1512961048094),
      (504, 567, 1512961048106), (512, 567, 1512961048120), (518, 567, 1512961048133),
      (521, 567, 1512961048146), (527, 567, 1512961048158),
      (529, 567, 1512961048171), (536, 567, 1512961048185), (551, 567, 1512961048205), (562, 567, 1512961048221),
      (567, 567, 1512961048235),
      (576, 567, 1512961048247), (583, 567, 1512961048258), (589, 567, 1512961048270), (592, 567, 1512961048284),
      (603, 567, 1512961048297), (610, 567, 1512961048310), (611, 567, 1512961048322), (620, 567, 1512961048345),
      (625, 567, 1512961048361), (631, 567, 1512961048374), (632, 567, 1512961048387), (637, 567, 1512961048400),
      (644, 567, 1512961048421), (651, 567, 1512961048437), (656, 567, 1512961048451),
      (667, 567, 1512961048463), (672, 567, 1512961048476), (680, 567, 1512961048488),
      (685, 567, 1512961048502), (686, 567, 1512961048515), (689, 567, 1512961048528),
      (694, 567, 1512961048540), (696, 567, 1512961048553), (710, 567, 1512961048574), (719, 567, 1512961048590),
      (722, 567, 1512961048603),
      (726, 567, 1512961048616), (727, 567, 1512961048629), (728, 567, 1512961048637), (729, 567, 1512961048649),
      (731, 567, 1512961048663), (732, 567, 1512961048675), (736, 567, 1512961048687), (739, 567, 1512961048700),
      (740, 567, 1512961048713), (741, 567, 1512961048723), (745, 567, 1512961048737), (749, 567, 1512961048749),
      (751, 567, 1512961048761), (756, 567, 1512961048774), (759, 567, 1512961048787), (761, 567, 1512961048800),
      (764, 567, 1512961048812), (765, 567, 1512961048825), (766, 567, 1512961048851),
      (767, 567, 1512961048901)],
     [[170, 538, 1513144224383], [172, 539, 1513144228792], [177, 539, 1513144228808],
      [183, 540, 1513144228822], [199, 541, 1513144228837], [208, 543, 1513144228849],
      [226, 545, 1513144228862], [234, 547, 1513144228874], [247, 547, 1513144228887],
      [256, 548, 1513144228899], [269, 551, 1513144228920], [284, 551, 1513144228937],
      [308, 552, 1513144228951], [322, 552, 1513144228963], [354, 552, 1513144228975],
      [387, 552, 1513144228987], [402, 552, 1513144229001], [429, 557, 1513144229014],
      [450, 557, 1513144229028], [459, 557, 1513144229041], [478, 561, 1513144229058],
      [498, 561, 1513144229069], [518, 561, 1513144229087], [533, 561, 1513144229101],
      [539, 561, 1513144229117], [544, 561, 1513144229126], [545, 561, 1513144229139],
      [549, 562, 1513144229152], [554, 562, 1513144229165], [556, 562, 1513144229173],
      [559, 562, 1513144229186], [571, 564, 1513144229223], [579, 564, 1513144229259],
      [585, 566, 1513144229279], [595, 567, 1513144229313], [607, 567, 1513144229349],
      [610, 568, 1513144229383]],
     [[176, 541, 1513144724795], [179, 541, 1513144727228], [180, 541, 1513144727243],
      [182, 541, 1513144727257], [186, 541, 1513144727283], [189, 541, 1513144727298],
      [191, 541, 1513144727311], [194, 541, 1513144727325], [199, 541, 1513144727346],
      [200, 541, 1513144727363], [201, 541, 1513144727426], [202, 541, 1513144727446],
      [205, 541, 1513144727463], [210, 541, 1513144727483], [214, 541, 1513144727500],
      [219, 541, 1513144727514], [220, 541, 1513144727527], [225, 541, 1513144727542],
      [227, 541, 1513144727556], [231, 541, 1513144727568], [234, 541, 1513144727577],
      [235, 541, 1513144727605], [236, 541, 1513144727632], [237, 541, 1513144727653],
      [240, 542, 1513144727669], [244, 542, 1513144727685], [245, 542, 1513144727695],
      [250, 542, 1513144727706], [252, 542, 1513144727720], [256, 542, 1513144727733],
      [261, 542, 1513144727745], [262, 542, 1513144727758], [266, 542, 1513144727782],
      [269, 542, 1513144727800], [271, 542, 1513144727819], [275, 542, 1513144727837],
      [280, 542, 1513144727855], [287, 544, 1513144727873], [294, 545, 1513144727890],
      [297, 545, 1513144727907], [304, 546, 1513144727942], [306, 546, 1513144727955],
      [307, 546, 1513144727969], [309, 546, 1513144727982], [311, 547, 1513144727994],
      [315, 547, 1513144728009], [319, 549, 1513144728024], [321, 549, 1513144728036],
      [328, 549, 1513144728049], [331, 549, 1513144728062], [339, 549, 1513144728074],
      [342, 549, 1513144728087], [350, 549, 1513144728103], [356, 549, 1513144728124],
      [357, 549, 1513144728140], [362, 549, 1513144728153], [366, 549, 1513144728167],
      [375, 549, 1513144728183], [384, 549, 1513144728195], [389, 549, 1513144728208],
      [392, 549, 1513144728221], [396, 549, 1513144728236], [402, 549, 1513144728248],
      [405, 549, 1513144728263], [412, 549, 1513144728276], [420, 549, 1513144728289],
      [422, 549, 1513144728303], [430, 549, 1513144728319], [439, 549, 1513144728330],
      [449, 549, 1513144728344], [454, 549, 1513144728357], [462, 549, 1513144728369],
      [464, 549, 1513144728382], [466, 549, 1513144728394], [467, 549, 1513144728407],
      [469, 549, 1513144728424], [471, 549, 1513144728437], [472, 549, 1513144728449],
      [474, 549, 1513144728463], [477, 550, 1513144728475], [480, 550, 1513144728485],
      [484, 550, 1513144728499], [486, 550, 1513144728510], [491, 550, 1513144728524],
      [492, 550, 1513144728536], [497, 550, 1513144728549], [499, 550, 1513144728562],
      [501, 550, 1513144728620], [502, 550, 1513144728637], [505, 550, 1513144728672],
      [507, 550, 1513144728777], [511, 550, 1513144728812], [519, 550, 1513144728884],
      [521, 550, 1513144728919], [522, 550, 1513144728992], [525, 551, 1513144728992],
      [532, 552, 1513144730246]],
     [[180, 540, 1513144812597], [183, 540, 1513144815058], [189, 540, 1513144815078],
      [196, 540, 1513144815095], [206, 540, 1513144815116], [211, 540, 1513144815132],
      [214, 540, 1513144815145], [221, 540, 1513144815157], [225, 540, 1513144815165],
      [230, 540, 1513144815178], [239, 540, 1513144815190], [250, 540, 1513144815205],
      [255, 540, 1513144815216], [260, 540, 1513144815226], [277, 540, 1513144815237],
      [286, 540, 1513144815250], [309, 540, 1513144815262], [333, 540, 1513144815276],
      [345, 540, 1513144815287], [355, 540, 1513144815299], [376, 540, 1513144815312],
      [399, 540, 1513144815325], [414, 540, 1513144815338], [440, 540, 1513144815350],
      [467, 540, 1513144815371], [489, 540, 1513144815389], [508, 540, 1513144815416],
      [513, 540, 1513144815429], [520, 540, 1513144815446], [521, 540, 1513144815454],
      [526, 540, 1513144815493], [527, 540, 1513144815507], [532, 540, 1513144815517],
      [534, 540, 1513144815547], [536, 540, 1513144815591], [541, 540, 1513144815604],
      [545, 540, 1513144815612], [549, 540, 1513144815625], [555, 540, 1513144815637],
      [560, 540, 1513144815657], [565, 540, 1513144815673], [578, 540, 1513144815693],
      [589, 540, 1513144815710], [607, 540, 1513144815745], [610, 540, 1513144815781]],
     [[187, 543, 1513144941486], [188, 545, 1513144943331], [189, 546, 1513144943347],
      [192, 548, 1513144943368], [193, 548, 1513144943384], [199, 549, 1513144943405],
      [202, 549, 1513144943422], [204, 549, 1513144943435], [212, 549, 1513144943456],
      [218, 549, 1513144943472], [223, 549, 1513144943485], [224, 549, 1513144943498],
      [228, 549, 1513144943519], [232, 549, 1513144943535], [234, 549, 1513144943549],
      [239, 549, 1513144943562], [247, 549, 1513144943582], [252, 549, 1513144943599],
      [254, 549, 1513144943612], [259, 549, 1513144943625], [268, 549, 1513144943646],
      [274, 549, 1513144943662], [278, 549, 1513144943675], [288, 549, 1513144943688],
      [306, 549, 1513144943709], [320, 549, 1513144943725], [325, 549, 1513144943738],
      [333, 549, 1513144943751], [334, 549, 1513144943759], [336, 549, 1513144943772],
      [337, 549, 1513144943786], [339, 549, 1513144943799], [342, 549, 1513144943811],
      [346, 549, 1513144943822], [348, 549, 1513144943836], [350, 550, 1513144943848],
      [352, 551, 1513144943866], [353, 551, 1513144943878], [354, 551, 1513144943892],
      [357, 551, 1513144943905], [361, 551, 1513144943918], [363, 551, 1513144943931],
      [367, 551, 1513144943946], [371, 551, 1513144943958], [372, 551, 1513144943971],
      [376, 551, 1513144943984], [378, 551, 1513144943997], [379, 551, 1513144944010],
      [382, 551, 1513144944026], [388, 551, 1513144944046], [392, 551, 1513144944063],
      [394, 551, 1513144944076], [397, 551, 1513144944089], [399, 551, 1513144944105],
      [402, 551, 1513144944118], [403, 551, 1513144944131], [406, 551, 1513144944144],
      [408, 551, 1513144944153], [413, 551, 1513144944167], [414, 551, 1513144944179],
      [419, 551, 1513144944192], [423, 551, 1513144944205], [426, 551, 1513144944219],
      [429, 551, 1513144944231], [432, 551, 1513144944247], [434, 551, 1513144944256],
      [436, 551, 1513144944269], [438, 551, 1513144944281], [441, 551, 1513144944302],
      [444, 551, 1513144944318], [446, 551, 1513144944331], [449, 551, 1513144944346],
      [454, 551, 1513144944367], [457, 551, 1513144944383], [459, 551, 1513144944396],
      [461, 551, 1513144944410], [464, 550, 1513144944431], [467, 550, 1513144944447],
      [468, 550, 1513144944460], [478, 550, 1513144944477], [486, 550, 1513144944497],
      [494, 550, 1513144944514], [497, 550, 1513144944529], [498, 550, 1513144944606],
      [499, 550, 1513144944619], [501, 549, 1513144944640], [507, 549, 1513144944692],
      [511, 549, 1513144944706], [512, 549, 1513144944727], [513, 549, 1513144944779]],
     [[183, 545, 1513145005861], [186, 545, 1513145007559], [191, 545, 1513145007576],
      [196, 544, 1513145007596], [198, 544, 1513145007613], [202, 544, 1513145007633],
      [207, 544, 1513145007651], [212, 544, 1513145007686], [214, 544, 1513145007700],
      [216, 544, 1513145007721], [217, 544, 1513145007733], [219, 544, 1513145007746],
      [221, 544, 1513145007772], [224, 544, 1513145007787], [226, 544, 1513145007800],
      [231, 544, 1513145007812], [233, 544, 1513145007825], [239, 544, 1513145007838],
      [248, 544, 1513145007851], [253, 544, 1513145007872], [258, 544, 1513145007888],
      [259, 544, 1513145007901], [261, 544, 1513145007951], [264, 544, 1513145008026],
      [268, 544, 1513145008061], [269, 544, 1513145008074], [271, 544, 1513145008087],
      [274, 544, 1513145008100], [279, 544, 1513145008115], [282, 544, 1513145008129],
      [289, 544, 1513145008141], [294, 544, 1513145008155], [297, 544, 1513145008170],
      [301, 544, 1513145008181], [303, 544, 1513145008190], [308, 544, 1513145008203],
      [309, 544, 1513145008217], [313, 544, 1513145008229], [317, 544, 1513145008242],
      [321, 544, 1513145008266], [328, 544, 1513145008282], [331, 543, 1513145008311],
      [333, 543, 1513145008325], [334, 543, 1513145008336], [339, 543, 1513145008349],
      [345, 543, 1513145008362], [347, 542, 1513145008375], [352, 542, 1513145008389],
      [354, 542, 1513145008401], [358, 542, 1513145008413], [362, 542, 1513145008427],
      [364, 542, 1513145008440], [374, 542, 1513145008460], [381, 542, 1513145008477],
      [384, 542, 1513145008490], [389, 542, 1513145008506], [392, 542, 1513145008527],
      [394, 542, 1513145008543], [398, 542, 1513145008556], [401, 542, 1513145008569],
      [409, 542, 1513145008579], [413, 542, 1513145008593], [421, 542, 1513145008605],
      [423, 542, 1513145008618], [426, 542, 1513145008631], [429, 542, 1513145008652],
      [433, 542, 1513145008668], [437, 542, 1513145008694], [441, 542, 1513145008715],
      [442, 542, 1513145008746], [444, 542, 1513145008762], [447, 542, 1513145008775],
      [453, 542, 1513145008787], [456, 542, 1513145008801], [461, 542, 1513145008814],
      [463, 542, 1513145008826], [464, 542, 1513145008841], [467, 542, 1513145008853],
      [469, 542, 1513145008867], [471, 542, 1513145008879], [473, 542, 1513145008892],
      [476, 542, 1513145008913], [479, 542, 1513145008929], [482, 542, 1513145008945],
      [486, 542, 1513145008956], [487, 542, 1513145008966], [489, 542, 1513145008978],
      [491, 542, 1513145008993], [493, 542, 1513145009003], [494, 542, 1513145009027],
      [496, 542, 1513145009049], [497, 542, 1513145009064], [501, 542, 1513145009077],
      [502, 542, 1513145009090], [506, 542, 1513145009105], [508, 542, 1513145009116],
      [509, 542, 1513145009128], [511, 542, 1513145009138], [512, 542, 1513145009150],
      [513, 542, 1513145009163], [517, 542, 1513145009221], [518, 542, 1513145009229],
      [522, 542, 1513145009245], [523, 542, 1513145009257]]
     ]


def login_qichacha_by_qq_for_current(driver):
    '''
    用qq登录方式登录企查查，用于当前操作
    :return: 无
    '''
    url_qichacha = 'https://www.qichacha.com/'
    login = 'user_login'
    url_login = url_qichacha + login
    if '您的操作过于频繁，验证后再操作' in driver.page_source:
        driver.delete_all_cookies()

    driver.get(url_login)
    time.sleep(2)

    if driver.current_url == "https://www.qichacha.com/":
        print('登录成功，成功进入企查查搜索页面')
        return

    account_collection = mongodb_connection('company')['qichacha_account']
    account_cursor = account_collection.find()
    accout_list = []
    qq_num = ''
    qq_pw = ''
    for i in account_cursor:
        if 'used_time' not in i:
            qq_num = i['qq']
            qq_pw = i['pw']

            account_collection.update({'qq': qq_num}, {'$set': {'used_time': datetime.datetime.now()}})
            break
        accout_list.append(i)

    if not qq_num:
        min_time = accout_list[0]['used_time']
        min_index = 0
        for i in accout_list:
            if i['used_time'] < min_time:
                min_time = i['used_time']
                min_index = accout_list.index(i)

        qq_num = accout_list[min_index]['qq']
        qq_pw = accout_list[min_index]['pw']
        account_collection.update({'qq': qq_num}, {'$set': {'used_time': datetime.datetime.now()}})

    try:
        # 点击页面进入QQ登录
        url_login_qq = driver.find_element_by_xpath(".//*[@id='user_login_verify']/div[9]/div[2]/a")
        print('企查查登录界面', url_login)
        print('企查查使用QQ登录链接', url_login_qq.get_attribute('href'))
        url_login_qq.click()  # 进入QQ登陆界面
        time.sleep(random.randint(1, 3))

        # 二维码登录页面，转到帐号密码登录页面
        driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
        login_qq_QRcode = driver.find_element_by_xpath(".//*/body/div[1]/div[9]/a[1]")
        print('二维码登录界面的帐号密码登录：', login_qq_QRcode.text)
        login_qq_QRcode.click()
        time.sleep(random.randint(1, 3))
        print('现在位于的登录是：', driver.current_url)

        # 帐号和密码登录页面
        login_qq_name = driver.find_element_by_xpath(".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[1]/div/input")
        login_qq_passwd = driver.find_element_by_xpath(
            ".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[2]/div/input")
        login_qq_submit = driver.find_element_by_xpath(".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[4]/a/input")
        print('输入帐号的地方：', login_qq_name.get_attribute('class'))
        login_qq_name.clear()
        login_qq_name.send_keys(qq_num)
        print('QQ帐号：', qq_num)
        print('输入密码的地方：', login_qq_passwd.get_attribute('class'))
        login_qq_passwd.clear()
        login_qq_passwd.send_keys(qq_pw)
        print('QQ密码：', qq_pw)
        time.sleep(3)
        print('点击确认的地方', login_qq_submit.get_attribute('value'))
        login_qq_submit.click()
        print('现在driver位于的网页是：', driver.current_url)
        time.sleep(random.randint(1, 3))
        print(driver.current_url)
        # 登录成功，进入企查查搜索页面
        if "https://www.qichacha.com/" in driver.current_url:
            print('登录成功，成功进入企查查搜索页面')
        # 进入QQ帐号登录验证码页面
        elif '安全验证' == driver.find_element_by_xpath(".//*/body/div[1]/div[11]/div[1]/div/div[2]").text:

            iframe_element = driver.find_element_by_tag_name('iframe')
            iframe_x = iframe_element.location_once_scrolled_into_view['x']
            iframe_y = iframe_element.location_once_scrolled_into_view['y']
            driver.switch_to.frame(iframe_element)
            # 处理图片
            element = driver.find_element_by_xpath('//img')
            left = element.location['x'] + iframe_x
            top = element.location['y'] + iframe_y
            right = element.location['x'] + element.size['width'] + iframe_x + 10
            bottom = element.location['y'] + element.size['height'] + iframe_y + 10
            print((left, top, right, bottom))
            # 通过Image处理图像
            driver.save_screenshot('screenshot.png')
            im = Image.open('screenshot.png')
            im = im.crop((left, top, right, bottom))
            im.save('code.png')
            print('图片保存完成！！！！！！')
            result = handle_identify_code_api('code.png')
            input_identify_code = driver.find_element_by_xpath(
                './/*/body/div[1]/div/div[2]/div[1]/div[1]/div[3]/div[2]/input')
            input_identify_code.send_keys(result)
            confirm = driver.find_element_by_xpath('.//*/body/div[1]/div/div[2]/div[1]/div[2]/span')
            confirm.click()
            driver.switch_to.default_content()
            print('点击验证！！！！！')
            time.sleep(random.randint(1, 3))
            # 输入验证码后判断是否登录成功
            if driver.current_url == "https://www.qichacha.com/" or driver.current_url == "https://www.qichacha.com/":
                print('登录成功，成功进入企查查搜索页面')

            else:
                driver.save_screenshot('screenshot.png')
                try:
                    if '安全验证' == driver.find_element_by_xpath(".//*/body/div[1]/div[11]/div[1]/div/div[2]").text:
                        print('验证码错误！！！！！\n')
                        pass
                except exceptions.NoSuchElementException as e:
                    try:
                        if '帐号密码登录' == driver.find_element_by_xpath(".//*[@id='title_2']").text:
                            print('帐号错误,请更换帐号\n')
                            pass
                    except exceptions.NoSuchElementException as e:
                        print('错误未知')
                        pass
    except exceptions.NoSuchElementException:
        pass


def login_qichacha_by_qq_for_pool(driver, item):
    '''
    用qq登录方式登录企查查，用于cookies池
    :return: 无
    '''
    url_qichacha = 'https://www.qichacha.com/'
    login = 'user_login'
    url_login = url_qichacha + login

    driver.get(url_login)
    time.sleep(2)

    qq_num = item['qq']
    qq_pw = item['pw']

    try:
        # 点击页面进入QQ登录
        url_login_qq = driver.find_element_by_xpath(".//*[@id='user_login_verify']/div[9]/div[2]/a")
        print('企查查登录界面', url_login)
        print('企查查使用QQ登录链接', url_login_qq.get_attribute('href'))
        url_login_qq.click()  # 进入QQ登陆界面
        time.sleep(random.randint(1, 3))

        # 二维码登录页面，转到帐号密码登录页面
        driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
        login_qq_QRcode = driver.find_element_by_xpath(".//*/body/div[1]/div[9]/a[1]")
        print('二维码登录界面的帐号密码登录：', login_qq_QRcode.text)
        login_qq_QRcode.click()
        time.sleep(random.randint(1, 3))
        print('现在位于的登录是：', driver.current_url)

        # 帐号和密码登录页面
        login_qq_name = driver.find_element_by_xpath(".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[1]/div/input")
        login_qq_passwd = driver.find_element_by_xpath(
            ".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[2]/div/input")
        login_qq_submit = driver.find_element_by_xpath(".//*/body/div[1]/div[5]/div/div[1]/div[3]/form/div[4]/a/input")
        print('输入帐号的地方：', login_qq_name.get_attribute('class'))
        login_qq_name.clear()
        login_qq_name.send_keys(qq_num)
        print('QQ帐号：', qq_num)
        print('输入密码的地方：', login_qq_passwd.get_attribute('class'))
        login_qq_passwd.clear()
        login_qq_passwd.send_keys(qq_pw)
        print('QQ密码：', qq_pw)
        time.sleep(3)
        print('点击确认的地方', login_qq_submit.get_attribute('value'))
        login_qq_submit.click()
        print('现在driver位于的网页是：', driver.current_url)
        time.sleep(random.randint(1, 3))
        print(driver.current_url)
        # 登录成功，进入企查查搜索页面
        if "https://www.qichacha.com/" in driver.current_url or 'https://www.qichacha.com/' in driver.current_url:
            driver.get("https://www.qichacha.com/firm_5cdd709796eb259e58bd67a46b8aa108.html")

            deal_with_second_identify(driver)

            if driver.current_url == 'https://www.qichacha.com/firm_5cdd709796eb259e58bd67a46b8aa108.html':
                print('登录成功，成功进入企查查搜索页面')
                return driver.get_cookies()
        # 进入QQ帐号登录验证码页面
        elif '安全验证' == driver.find_element_by_xpath(".//*/body/div[1]/div[11]/div[1]/div/div[2]").text:

            # iframe_element = driver.find_element_by_tag_name('iframe')
            # iframe_x = iframe_element.location_once_scrolled_into_view['x']
            # iframe_y = iframe_element.location_once_scrolled_into_view['y']
            # driver.switch_to.frame(iframe_element)
            # # 处理图片
            # element = driver.find_element_by_xpath('//img')
            # left = element.location['x'] + iframe_x
            # top = element.location['y'] + iframe_y
            # right = element.location['x'] + element.size['width'] + iframe_x + 10
            # bottom = element.location['y'] + element.size['height'] + iframe_y + 10
            # print((left, top, right, bottom))
            # # 通过Image处理图像
            # driver.save_screenshot('screenshot.png')
            # im = Image.open('screenshot.png')
            # im = im.crop((left, top, right, bottom))
            # im.save('code.png')
            # print('图片保存完成！！！！！！')
            # result = handle_identify_code_api('code.png')
            # input_identify_code = driver.find_element_by_xpath(
            #     './/*/body/div[1]/div/div[2]/div[1]/div[1]/div[3]/div[2]/input')
            # input_identify_code.send_keys(result)
            # confirm = driver.find_element_by_xpath('.//*/body/div[1]/div/div[2]/div[1]/div[2]/span')
            # confirm.click()
            # driver.switch_to.default_content()
            # print('点击验证！！！！！')
            # time.sleep(random.randint(1, 3))
            current_url = driver.current_url

            while current_url == driver.current_url:
                deal_with_qq_identify_code(driver)

            # 输入验证码后判断是否登录成功
            if driver.current_url == "https://www.qichacha.com/" or driver.current_url == "https://www.qichacha.com/":
                driver.get("https://www.qichacha.com/firm_5cdd709796eb259e58bd67a46b8aa108.html")
                deal_with_second_identify(driver)
                if driver.current_url == 'https://www.qichacha.com/firm_5cdd709796eb259e58bd67a46b8aa108.html':
                    print('登录成功，成功进入企查查搜索页面')
                    return driver.get_cookies()
            else:
                driver.save_screenshot('screenshot.png')



    except exceptions.NoSuchElementException:
        return None


def deal_with_second_identify(driver):
    if 'index_verify' in driver.current_url:
        print('进行第二步验证！！！！')
        # .find_element_by_xpath(
        # ".//*[@class='container']/div/div/div/div[2]/div/div").text:
        slipping_block2 = driver.find_element_by_xpath(".//*[@id='nc_1_n1z']")
        print("第一步,移动到滑块相应位置，点击元素")
        ActionChains(driver).click_and_hold(slipping_block2).perform()
        print("第二步，拖动元素")
        print(datetime.datetime.now())
        list_trace_raw = random.choice(list_trace_raw_list)
        list_trace = []
        for j in range(1, len(list_trace_raw) - 1):
            list_trace.append((list_trace_raw[j][0] - list_trace_raw[j - 1][0],
                               list_trace_raw[j][1] - list_trace_raw[j - 1][1],
                               list_trace_raw[j][2] - list_trace_raw[j - 1][2]))
        for track in list_trace:
            # starttime = datetime.datetime.now()
            ActionChains(driver).move_by_offset(track[0], track[1]).perform()
            print(datetime.datetime.now())
            # endtime = datetime.datetime.now()
            # totaltime = endtime - starttime
            # print(totaltime)
        print(datetime.datetime.now())
        print("第三步，释放鼠标")
        try:
            ActionChains(driver).release(slipping_block2).perform()
        except exceptions.StaleElementReferenceException:
            driver.refresh()
            time.sleep(10)
            return deal_with_second_identify(driver)
        time.sleep(3)
        new_slipping_bar = driver.find_element_by_xpath(".//*[@id='nc_1__scale_text']")
        if "请点击图中" in new_slipping_bar.text:
            print('点击汉字验证码')
            handle_chinese_code(driver, '1')
            time.sleep(2)
            while "请点击图中" in driver.find_element_by_xpath(
                    ".//*[@id='nc_1__scale_text']").text:
                print('汉字点击错误，重新处理')
                handle_chinese_code(driver, '1')
                time.sleep(5)
                if '验证通过' in driver.find_element_by_xpath(
                        ".//*[@id='nc_1__scale_text']").text:
                    break
            btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
            print(btn_login.text)
            btn_login.click()
            time.sleep(5)
        elif "请在下方输入验证码" in new_slipping_bar.text:
            print("输入小验证码")
            # 获取小验证码的图片位置信息
            identify_block = driver.find_element_by_xpath(".//*[@id='nc_1_captcha_input']")
            element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
            screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
            # 验证码识别
            result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
            #       输入验证码值
            identify_block.send_keys(result)
            time.sleep(2)
            # 验证码提交按钮位置
            btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
            time.sleep(2)
            # 验证码点击提交
            btn_submit.click()
            time.sleep(5)
            if '验证通过' in driver.find_element_by_xpath(
                    ".//*[@id='nc_1__scale_text']").text:
                btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
                print(btn_login.text)
                btn_login.click()
                time.sleep(5)
            else:
                while '验证码输入错误' in driver.find_element_by_xpath(
                        ".//*[@id='nc_1__captcha_img_text']/span").text:
                    print('数字验证码错误，请重新输入！')
                    element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
                    screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
                    result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
                    # 输入验证码值
                    identify_block.send_keys(result)
                    time.sleep(2)
                    # 验证码提交按钮位置
                    btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
                    time.sleep(2)
                    # 验证码点击提交
                    btn_submit.click()
                    time.sleep(5)
                    if '验证通过' in driver.find_element_by_xpath(
                            ".//*[@id='nc_1__scale_text']").text:
                        btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
                        print(btn_login.text)
                        btn_login.click()
                        time.sleep(5)
                        break
        # 滑动成功，点击进入
        elif '验证通过' in new_slipping_bar.text:
            btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
            print(btn_login.text)
            btn_login.click()
            time.sleep(5)


def deal_with_qq_identify_code(driver):
    iframe_element = driver.find_element_by_tag_name('iframe')
    iframe_x = iframe_element.location_once_scrolled_into_view['x']
    iframe_y = iframe_element.location_once_scrolled_into_view['y']
    driver.switch_to.frame(iframe_element)
    # 处理图片
    element = driver.find_element_by_xpath('//img[@id="capImg"]')
    left = element.location['x'] + iframe_x
    top = element.location['y'] + iframe_y
    right = element.location['x'] + element.size['width'] + iframe_x + 10
    bottom = element.location['y'] + element.size['height'] + iframe_y + 10
    print((left, top, right, bottom))
    # 通过Image处理图像
    driver.save_screenshot('screenshot.png')
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save('code.png')
    print('图片保存完成！！！！！！')
    result = handle_identify_code_api('code.png', codetype=3004)
    input_identify_code = driver.find_element_by_xpath(
        './/*/body/div[1]/div/div[2]/div[1]/div[1]/div[3]/div[2]/input')
    input_identify_code.send_keys(result)
    confirm = driver.find_element_by_xpath('.//*/body/div[1]/div/div[2]/div[1]/div[2]/span')
    confirm.click()
    driver.switch_to.parent_frame()
    print('点击验证！！！！！')
    time.sleep(random.randint(1, 3))


def manage_corrdinate(points):
    """
    对坐标点进行处理，去除异常点
    :param points: x,y的坐标list
    :return: x,y坐标的均值
    """
    # 上下四分位数
    upper_adjacent_value = sts.quantile(points, p=0.75)
    lower_adjacent_value = sts.quantile(points, p=0.25)

    upper_level = upper_adjacent_value + (upper_adjacent_value - lower_adjacent_value) * 1.5
    lower_level = lower_adjacent_value - (upper_adjacent_value - lower_adjacent_value) * 1.5
    for i in points:
        if i < lower_level or i > upper_level:
            points.remove(i)

    median = np.median(points, axis=0)
    return median


def k_means(coodrinate, k):
    """
    对坐标点进行聚类处理
    :param coodrinate:坐标点集合
    :param k:类的数量
    :return: 类数量最多的那类坐标的均值
    """
    print("\n")
    print('进行聚类，输出聚类结果')
    if len(coodrinate) == 2 or len(coodrinate) == 1:
        return coodrinate
    else:
        X = np.array(coodrinate)
        kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
        print(kmeans.labels_)
        # 类标志形成一个list
        lists = []
        for i in kmeans.labels_:
            lists.append(i)
        print(lists)
        # 建立类标志和数量的字典
        dic = {}
        for i in range(k):
            dic[i] = lists.count(i)
        print(dic)
        sequence = sorted(dic.values(), reverse=True)
        print(sequence)
        if sequence[0] == sequence[1]:
            print('聚类最大个数相同,重新选择聚类个数')
            if k == 2:
                return coodrinate
            else:
                coodrinate = k_means(coodrinate, k - 1)
                return coodrinate
        else:
            # 选出数量最多的类的key
            label = sorted(dic, key=lambda x: dic[x])[-1]
            # 输出这类点的坐标
            x = []
            y = []
            cluster_coordinate = []
            for i in range(len(kmeans.labels_)):
                if kmeans.labels_[i] == label:
                    # x.append(coodrinate[i][0])
                    # y.append(coodrinate[i][1])
                    cluster_coordinate.append(coodrinate[i])
                    print(coodrinate[i])
            print(cluster_coordinate)
            return cluster_coordinate


def feature_matching(character_filename, identify_code_filename):
    """
    对汉字和验证码图片进行
    :param character_filename: 文字文件
    :param identify_code_filename: 验证码文件
    :return: 文字在验证码中的坐标
    """
    # img1 = cv2.imread(character_filename, 0)  # queryImage       ##字的图片
    #     # img2 = cv2.imread(identify_code_filename, 0)  # trainImage     ##文件图片
    img1 = cv2.imdecode(np.fromfile(character_filename, dtype=np.uint8), -1)
    img2 = cv2.imdecode(np.fromfile(identify_code_filename, dtype=np.uint8), -1)
    # 图像灰度处理
    print(img1)
    gray = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)

    # Initiate SIFT detector  SIFT特征提取
    sift = cv2.xfeatures2d.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1, None)  # 获取文字的特征点
    # des1是特征，kp1是点
    # 在字上画特征点
    img = cv2.drawKeypoints(image=img1, outImage=None, keypoints=kp1,
                            flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    kp2, des2 = sift.detectAndCompute(img2, None)  ##获取图片的特征点
    # cv2.imshow('image', img)  # 展示图片
    # cv2.waitKey(0)
    # BFMatcher with default params
    bf = cv2.BFMatcher()  ##进行匹配
    matches = bf.knnMatch(des1, des2, k=2)
    # Apply ratio test
    good = []
    # matches.sort(key=lambda mat: mat[0].distance / mat[1].distance)
    for m, n in matches:  ##判断特征像
        if m.distance < 0.8 * n.distance:
            good.append(m)
    # 获取点的坐标
    list_point = []
    for i in good:
        list_point.append(list(kp2[i.trainIdx].pt))
    print('原坐标点集合：', list_point)
    print('去重后坐标点集合:', len(list_point))
    ##对无特征点的情况进行处理
    if len(list_point) == 0:
        print('无特征点，随机返回位置!!!!')
        x = random.randint(0, 1000)
        y = random.randint(0, 1000)
        return x / (1000 / 230), y / (1000 / 230)
    # #去除重复点的影响
    # resultList = []
    # for item in list_point:
    #     if not item in resultList:
    #         resultList.append(item)
    # list_point = resultList
    print("去重后坐标点集合：", list_point)
    print('去重后坐标点个数：', len(list_point))
    if len(list_point) > 3:
        coordinate = k_means(list_point, 3)
        coordinate = k_means(coordinate, 2)
    else:
        coordinate = k_means(list_point, 2)
    print('输出点个数', len(coordinate))
    if len(coordinate) >= 2:
        p = random.randint(0, len(coordinate) - 1)
        print('选择点坐标为：', coordinate[p])
        img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, flags=2)
        img3 = cv2.circle(img3, (100 + int(coordinate[p][0]), int(coordinate[p][1])), 10, (255, 0, 0), -1)
        # plt.imshow(img3)
        cv2.imwrite('SIFT.png', img3)
        # plt.show()
        return coordinate[p][0] / (1000 / 230), coordinate[p][1] / (1000 / 230)
    else:
        print('选择点坐标为：', coordinate)
        img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, flags=2)
        img3 = cv2.circle(img3, (100 + int(coordinate[0][0]), int(coordinate[0][1])), 10, (255, 0, 0), -1)
        # plt.imshow(img3)
        cv2.imwrite('SIFT.png', img3)
        # plt.show()
        return coordinate[0][0] / (1000 / 230), coordinate[0][1] / (1000 / 230)


def generate_chinese(word):
    """
    生成所要查找的汉字图片
    :return:
    """
    chinese_dir = 'chinese'  ###创建中文的字符文件夹
    if not os.path.exists(chinese_dir):
        os.mkdir(chinese_dir)

    pygame.init()
    pygame.mixer.init()
    # start, end = (0x4E00, 0x9FA5)  # 汉字编码范围
    # for codepoint in range(int(start), int(end)):
    #     word = chr(codepoint)
    font = pygame.font.Font("./STHeiti-Light黑体繁简细体.ttc", 100)
    rtext = font.render(word, True, (255, 255, 255), (0, 0, 0))
    pygame.image.save(rtext, os.path.join(chinese_dir, word + ".bmp"))  ###保存字体


def binaryzation(filename):
    """
    将图片二值化处理
    :return: 以保存的二值化图片
    """
    SIZE = 1000  ##图片大小
    NUM_OF_STRING = 3
    img = Image.open(filename)  ###打开图片
    img = img.resize((SIZE, SIZE), Image.ANTIALIAS)  ###图片大小重新设定

    img.save('testbefore.png')  ###保存改变大小的图片
    r_sum = 0
    g_sum = 0
    b_sum = 0

    img = Image.open('testbefore.png')
    pimg = img.load()
    for w in range(0, SIZE):  ###从宽和高遍历图片的每一个像素点
        for h in range(0, SIZE):
            r, g, b = pimg[w, h][:3]  ##返回该点的像素值
            r_sum += r  ###r,g,b三种颜色想家
            g_sum += g
            b_sum += b

    r_sum = int(r_sum / SIZE / SIZE)
    g_sum = int(g_sum / SIZE / SIZE)
    b_sum = int(b_sum / SIZE / SIZE)

    for w in range(0, SIZE):
        for h in range(0, SIZE):  ###从宽和高遍历图片的每一个像素点
            r, g, b = pimg[w, h][:3]
            r = abs(r - r_sum) % 256  ##二值化处理
            g = abs(g - g_sum) % 256
            b = abs(b - b_sum) % 256
            if r + g + b < 150:
                r, g, b = 0, 0, 0
            else:
                r, g, b = 255, 255, 255
            pimg[w, h] = (r, g, b)  ##修改给定位置的像素值

    img = img.convert("1")
    img.save('test.png')


def handle_chinese_code(driver, index):
    '''

    :param driver:
    :param index:
    :return:
    '''
    character = driver.find_element_by_xpath(".//*[@id='nc_" + index + "__scale_text']/i").text[1]
    print("点击图中：" + character + '字')
    # 生成汉字的图片
    generate_chinese(character)
    # 爬取验证码位置
    chinese_code = driver.find_element_by_xpath(".//*[@id='nc_" + index + "_clickCaptcha']/div[2]/img")
    # 截图验证码，保存为jpg图片
    screenshoot_identify_code(driver, chinese_code, 'code_dayanzhengma.png')
    # 二值化处理，图片保存为test.png
    binaryzation('code_dayanzhengma.png')
    # 验证码进行处理   + character + '.bmp'
    code_x, code_y = feature_matching('chinese/'+ character + '.bmp', 'test.png')
    # x = int(chinese_code.location['x']) + code_x
    # y = int(chinese_code.location['y']) + code_y
    print("汉字在网页中的位置", code_x, code_y)
    ActionChains(driver).move_to_element_with_offset(chinese_code, code_x, code_y).click().perform()


def screenshoot_identify_code(driver, element, save_filename):
    """
    对验证码进行截图，并返回位置
    :param element: 要截图提取的元素
    :param save_filename:要保存的图片名字
    :return: 元素的四个位置的坐标
    """
    driver.save_screenshot('screenshot.png')
    left = int(element.location['x'])
    top = int(element.location['y'])
    right = int(element.location['x'] + element.size['width'])
    bottom = int(element.location['y'] + element.size['height'])
    # print('函数输出',left, top, right, bottom)
    # 通过Image处理图像
    im = Image.open('screenshot.png')
    im = im.crop((left, top, right, bottom))
    im.save(save_filename)
    time.sleep(2)
    print(save_filename + '图片保存完成！！！！！！')
    return left, top, right, bottom


# 已无用，暂时保留
# 根据缺口的位置模拟x轴移动的轨迹
def get_track(length):
    pass
    list = []
    # 间隔通过随机范围函数来获得,每次移动一步或者两步
    x = random.randint(1, 3)
    # 生成轨迹并保存到list内
    while length - x >= 5:
        list.append(x)
        length = length - x
        x = random.randint(1, 3)
    # 最后五步都是一步步移动
    for i in range(length):
        list.append(1)
    return list


def login_qichacha_by_mobile(driver, item):
    url_qichacha = 'https://www.qichacha.com/'
    driver.get(url_qichacha)
    login = 'user_login'
    url_login = url_qichacha + login
    try:
        driver.get(url_login)
    except exceptions.TimeoutException as e:
        print('登录网页加载过慢！！！！')
        return login_qichacha_by_mobile(driver, item)
    except exceptions.WebDriverException:
        print('WebDriverException!!')
        pass
    driver.maximize_window()
    # driver.delete_all_cookies()
    time.sleep(2)

    driver.find_element_by_xpath(".//a[@id='normalLogin']").click()

    try:
        slipping_block = driver.find_element_by_xpath(".//*[@id='nc_1_n1z']")
    except exceptions.NoSuchElementException:
        print("NoSuchElementException")
        return None
    # 登录手机号位置
    login_phone = driver.find_element_by_xpath(
        ".//*[@id='user_login_normal']/div[1]/input[2]")
    print(login_phone.text)
    login_phone.send_keys(item['mobile'])
    # 登录密码位置
    login_code = driver.find_element_by_xpath(
        ".//*[@id='user_login_normal']/div[2]/input")
    print(login_code.text)
    login_code.send_keys(item['mobilepw'])
    time.sleep(2)

    print("第一步,移动到滑块相应位置，点击元素")
    time.sleep(1)
    ActionChains(driver).move_to_element_with_offset(slipping_block, random.randint(1, 10),
                                                     random.randint(1, 10)).perform()
    ActionChains(driver).click_and_hold().perform()
    # 拖动元素，按轨迹执行
    print("第二步，拖动元素")
    print(datetime.datetime.now())
    list_trace_raw = random.choice(list_trace_raw_list)
    list_trace = []
    for j in range(1, len(list_trace_raw) - 1):
        list_trace.append((list_trace_raw[j][0] - list_trace_raw[j - 1][0],
                           list_trace_raw[j][1] - list_trace_raw[j - 1][1],
                           list_trace_raw[j][2] - list_trace_raw[j - 1][2]))

    print("变换后：", list_trace)

    for track in list_trace:
        ActionChains(driver).move_by_offset(track[0], track[1]).perform()

    print("第三步，释放鼠标")
    try:
        ActionChains(driver).release(slipping_block).perform()
        print(1)
    except exceptions.StaleElementReferenceException:
        # 出现此错误，大多数是因为登录次数太多，ip被发现
        print("StaleElementReferenceException")
        time.sleep(300)
        driver.quit()
        driver = webdriver.Chrome()
        driver.implicitly_wait(2)
        driver.set_page_load_timeout(60)
        return login_qichacha_by_mobile(driver, item)
    time.sleep(1)
    #
    try:
        new_slipping_bar = driver.find_element_by_xpath(".//*[@id='nc_1__scale_text']")
    except exceptions.NoSuchElementException:
        print("NoSuchElementException,刷新浏览器")
        driver.refresh()
        return login_qichacha_by_mobile(driver, item)

    # 点击汉字验证码出现
    if "请点击图中" in new_slipping_bar.text:
        print('点击汉字验证码')
        handle_chinese_code(driver, '1')
        time.sleep(2)
        while "请点击图中" in driver.find_element_by_xpath(
                ".//*[@id='nc_1__scale_text']").text:
            handle_chinese_code(driver, '1')
            time.sleep(8)
            if '验证通过' in driver.find_element_by_xpath(
                    ".//*[@id='nc_1__scale_text']").text:
                break
        btn_login = driver.find_element_by_xpath(".//*[@id='user_login_normal']/button")
        print(btn_login.text)
        btn_login.click()
        time.sleep(1)

    # 拖动失败，数字和英文验证码出现
    elif "请在下方输入验证码" in new_slipping_bar.text:
        print("输入小验证码")
        # 获取小验证码的图片位置信息
        identify_block = driver.find_element_by_xpath(".//*[@id='nc_1_captcha_input']")
        element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
        screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
        # 验证码识别
        result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
        # 输入验证码值
        identify_block.send_keys(result)
        time.sleep(2)
        # 验证码提交按钮位置
        btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
        time.sleep(2)
        # 验证码点击提交
        btn_submit.click()
        time.sleep(5)
        if '验证通过' in driver.find_element_by_xpath(
                ".//*[@id='nc_1__scale_text']").text:
            btn_login = driver.find_element_by_xpath(".//*[@id='user_login_normal']/button")
            print(btn_login.text)
            btn_login.click()
            time.sleep(5)
        else:
            try:
                while '验证码输入错误' in driver.find_element_by_xpath(
                        ".//*[@id='nc_1__captcha_img_text']/span").text:
                    print('数字验证码错误，请重新输入！')
                    element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
                    screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
                    result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
                    # 输入验证码值
                    identify_block.send_keys(result)
                    time.sleep(2)
                    # 验证码提交按钮位置
                    btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
                    time.sleep(2)
                    # 验证码点击提交
                    btn_submit.click()
                    time.sleep(5)
                    if '验证通过' in driver.find_element_by_xpath(
                            ".//*[@id='nc_1__scale_text']").text:
                        btn_login = driver.find_element_by_xpath(".//*[@id='user_login_normal']/button")
                        print(btn_login.text)
                        btn_login.click()
                        time.sleep(5)
                        break
                        # btn_login = driver.find_element_by_xpath(".//*[@id='user_login_normal']/div[5]/button")
                        # print(btn_login.text)
                        # btn_login.click()
                        # time.sleep(5)
            except exceptions.NoSuchElementException:
                print("验证时出现NoSuchElementException")
                return login_qichacha_by_mobile(driver, item)
    # 滑动成功，点击进入
    else:
        btn_login = driver.find_element_by_xpath(".//*[@id='user_login_normal']/button")
        print(btn_login.text)
        btn_login.click()
        time.sleep(5)

    if '免费注册' in driver.page_source:
        # return login_qichacha_by_mobile(driver,item)
        return None

    driver.get('https://www.qichacha.com/firm_7b1f5f817a50aea17056db0af60fdb89.shtml')
    time.sleep(2)
    print(driver.current_url)
    if driver.current_url == 'https://www.qichacha.com/firm_7b1f5f817a50aea17056db0af60fdb89.shtml':
        return driver.get_cookies()


    ########进行第二步验证##
    elif 'index_verify' in driver.current_url:
        print('进行第二步验证！！！！')
        # .find_element_by_xpath(
        # ".//*[@class='container']/div/div/div/div[2]/div/div").text:
        slipping_block2 = driver.find_element_by_xpath(".//*[@id='nc_1_n1z']")
        print("第一步,移动到滑块相应位置，点击元素")
        ActionChains(driver).click_and_hold(slipping_block2).perform()
        print("第二步，拖动元素")
        for track in list_trace:
            # starttime = datetime.datetime.now()
            time.sleep(0.09)
            ActionChains(driver).move_by_offset(track[0], track[1]).perform()
            # endtime = datetime.datetime.now()
            # totaltime = endtime - starttime
            # print(totaltime)
        print("第三步，释放鼠标")
        ActionChains(driver).release(slipping_block2).perform()
        time.sleep(2)
        new_slipping_bar = driver.find_element_by_xpath(".//*[@id='nc_1__scale_text']")
        if "请点击图中" in new_slipping_bar.text:
            print('点击汉字验证码')
            handle_chinese_code(driver, '1')
            time.sleep(2)
            while "请点击图中" in driver.find_element_by_xpath(
                    ".//*[@id='nc_1__scale_text']").text:
                print('汉字点击错误，重新处理')
                handle_chinese_code(driver, '1')
                time.sleep(5)
                if '验证通过' in driver.find_element_by_xpath(
                        ".//*[@id='nc_1__scale_text']").text:
                    break
            btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
            print(btn_login.text)
            btn_login.click()
            time.sleep(5)
        elif "请在下方输入验证码" in new_slipping_bar.text:
            print("输入小验证码")
            # 获取小验证码的图片位置信息
            identify_block = driver.find_element_by_xpath(".//*[@id='nc_1_captcha_input']")
            element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
            screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
            # 验证码识别
            result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
            #       输入验证码值
            identify_block.send_keys(result)
            time.sleep(2)
            # 验证码提交按钮位置
            btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
            time.sleep(2)
            # 验证码点击提交
            btn_submit.click()
            time.sleep(5)
            if '验证通过' in driver.find_element_by_xpath(
                    ".//*[@id='nc_1__scale_text']").text:
                btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
                print(btn_login.text)
                btn_login.click()
                time.sleep(5)
            else:
                while '验证码输入错误' in driver.find_element_by_xpath(
                        ".//*[@id='nc_1__captcha_img_text']/span").text:
                    print('数字验证码错误，请重新输入！')
                    element = driver.find_element_by_xpath(".//*[@id='nc_1__imgCaptcha_img']/img")
                    screenshoot_identify_code(driver, element, 'code_xiaoyanzhengma.png')
                    result = handle_identify_code_api(filename='code_xiaoyanzhengma.png', codetype=5001)
                    # 输入验证码值
                    identify_block.send_keys(result)
                    time.sleep(2)
                    # 验证码提交按钮位置
                    btn_submit = driver.find_element_by_xpath(".//*[@id='nc_1_scale_submit']")
                    time.sleep(2)
                    # 验证码点击提交
                    btn_submit.click()
                    time.sleep(5)
                    if '验证通过' in driver.find_element_by_xpath(
                            ".//*[@id='nc_1__scale_text']").text:
                        btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
                        print(btn_login.text)
                        btn_login.click()
                        time.sleep(5)
                        break
        # 滑动成功，点击进入
        elif '验证通过' in new_slipping_bar.text:
            btn_login = driver.find_element_by_xpath(".//*[@id='verify']")
            print(btn_login.text)
            btn_login.click()
            time.sleep(5)

        if driver.current_url == 'https://www.qichacha.com/firm_7b1f5f817a50aea17056db0af60fdb89.shtml':
            return driver.get_cookies()
            # else:
            #     return ''
