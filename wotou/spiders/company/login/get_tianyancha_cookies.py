# coding:utf-8
'''
获取天眼查cookies
含验证码破解代码
big代表待点击的图片(大图片)相关，small代表不需点击的图片（小图片）相关
'''
import selenium
from selenium import webdriver
import time
import os
from wotou.lib.db_connection import mongodb_connection
from selenium.webdriver.chrome.options import Options
from PIL import Image, ImageFile

import cv2
import os
from selenium.webdriver.common.action_chains import ActionChains
import multiprocessing
import zipfile
import numpy
import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from collections import Counter
from scipy import sparse
import math

ImageFile.LOAD_TRUNCATED_IMAGES = True

ratio = 3

png_h = int(100) * ratio
png_w = int(320) * ratio

char_png_w = int(320 * 1.4) * ratio
char_png_h = int(30 * 1.4) * ratio

is_debug = False


def _save_big_png(driver):
    '''
    通过截图获取大图片
    :param driver: 浏览器对象
    :return: 图片名称
    '''
    driver.save_screenshot("antirobot.png")
    canvas_element = driver.find_element_by_id('bgImgie')
    left = canvas_element.location['x']
    top = canvas_element.location['y']
    right = canvas_element.location['x'] + canvas_element.size['width']
    bottom = canvas_element.location['y'] + canvas_element.size['height']

    big_pil = Image.open('antirobot.png')
    big_pil = big_pil.crop((left, top, right, bottom))

    # 若背景图已经很清晰，则下两句不用运行
    # new_file_name = str(time.time()) + '.png'
    # big_pil.save('./tianyancha_identify/' + new_file_name)
    print((left, top, right, bottom))
    try:
        big_pil.save("final_before.png")
    except SystemError:
        driver.refresh()
        return _save_big_png(driver)
    return 'final_before.png'


def _get_central_distance_list(char_pix_list):
    '''
    获取文字中心点
    :param char_pix_list:文字像素列表
    :return:带中心点的文字像素列表
    '''
    center_point_and_char_pix_list = []
    # 若出现叠字，则刷新
    for pix_list in char_pix_list:
        if len(pix_list) < 200 * ratio or len(pix_list) > int(500 * ratio ** 2.5) or pix_list[-1][0] - pix_list[0][
            0] > 35 * ratio:
            print("出现叠字，放弃")
            print(len(pix_list))
            driver.find_element_by_id("refeshie").click()
            time.sleep(2)
            return False

        pix_list.sort(key=lambda a: a[0])
        max_w = pix_list[-1][0]
        min_w = pix_list[0][0]

        pix_list.sort(key=lambda a: a[1])
        max_h = pix_list[-1][1]
        min_h = pix_list[0][1]
        central_point = (int(round((max_w + min_w) / 2)), int(round((max_h + min_h)) / 2))

        center_point_and_char_pix_list.append(
            {"center": central_point,
             "point": [(i[0] - central_point[0], i[1] - central_point[1]) for i in pix_list]})
        if is_debug:
            plt.plot(central_point[0], central_point[1], "^", markersize=3)

    if is_debug:
        # plt.show()
        plt.close()
    return center_point_and_char_pix_list


def _get_clean_small_char_pix_list(driver):
    # char_image_data = driver.find_element_by_id('targetImgie').get_attribute('src')
    # char_image_data = re.sub('^data:image/.+;base64,', '', char_image_data)

    canvas_element = driver.find_element_by_id('targetImgie')
    left = canvas_element.location['x']
    top = canvas_element.location['y']
    right = canvas_element.location['x'] + canvas_element.size['width']
    bottom = canvas_element.location['y'] + canvas_element.size['height']

    big_pil = Image.open('antirobot.png')
    char_png = big_pil.crop((left, top, right, bottom))

    # char_png = Image.open(BytesIO(base64.b64decode(char_image_data)))
    char_png = char_png.resize((char_png_w, char_png_h), Image.ANTIALIAS)
    char_png_load = char_png.load()
    char_png_pix_list = Counter([char_png_load[w, h] for w in range(0, char_png_w) for h in
                                 range(0, char_png_h)])

    _r, _g, _b = char_png_pix_list.most_common(1)[0][0][:3]

    raw_char_point_list = []

    for w in range(0, char_png_w):
        for h in range(0, char_png_h):
            r, g, b = char_png_load[w, h][:3]
            if abs(r - _r) + abs(g - _g) + abs(b - _b) > 165:
                char_png_load[w, h] = (255, 255, 255)
                raw_char_point_list.append([w, h])
            else:
                char_png_load[w, h] = (0, 0, 0)
    char_png.save("final_char.png")

    return raw_char_point_list


def _get_clean_big_char_pix_list(big_file_name):
    '''
    清洗大图片，获得图片中文字所处像素
    :param big_file_name:大图片文件名
    :return:文字像素列表
    '''
    big_opencv = cv2.imread("./" + big_file_name)
    cv2.imshow("Original", big_opencv)
    # 获取图片的hist
    hist = cv2.calcHist([big_opencv], [0], None, [256], [0, 256])
    max_similar = 0
    max_index = 0
    # 通过与背景图的hist对比，选出最接近的背景图
    for j in range(0, len(background_hist_info_list)):
        hist_background = background_hist_info_list[j]['hist']
        similar = 0
        sum = 0
        for k in range(0, 256):
            similar += abs(hist_background[k][0] - hist[k][0])
            sum += max(hist[k][0], hist_background[k][0])
        similar = 1 - similar / sum
        print(background_hist_info_list[j]['filename'], similar)
        if similar > max_similar:
            max_similar = similar
            max_index = j
    big_pil = Image.open("./" + big_file_name).resize((png_w, png_h), Image.ANTIALIAS)
    big_pil_load = big_pil.load()
    background_png = Image.open(
        '../tianyancha_background/' + background_hist_info_list[max_index]['filename']).resize((png_w, png_h),
                                                                                                Image.ANTIALIAS).load()

    print("最相似背景图:../tianyancha_background/" + background_hist_info_list[max_index]['filename'])
    raw_char_point_list = []
    # 除去背景图和文字白边
    for w in range(0, png_w):
        for h in range(0, png_h):
            r, g, b = big_pil_load[w, h][:3]
            _r, _g, _b = background_png[w, h][:3]
            if abs(r - _r) + abs(g - _g) + abs(b - _b) > 80 and r + g + b < 655:
                big_pil_load[w, h] = (255, 255, 255)
                raw_char_point_list.append([w, h])
            else:
                big_pil_load[w, h] = (0, 0, 0)

    big_pil = big_pil.resize((png_w, png_h), Image.ANTIALIAS)
    big_pil.save("final.png")
    return raw_char_point_list


def DBSACN_for_raw_char(raw_char_point_list, eps, min_samples):
    '''
    通过DBSACN聚类，将像素列表按字分开
    :param raw_char_point_list:原始像素列表
    :param eps:距离
    :param min_samples:最小密度
    :return:已经分开的列表
    '''
    raw_char_point_list = numpy.array(raw_char_point_list)

    X = StandardScaler().fit_transform(raw_char_point_list)
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each)
              for each in np.linspace(0, 1, len(unique_labels))]

    char_pix_list = []

    for k, col in zip(unique_labels, colors):
        char_list = []
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = raw_char_point_list[class_member_mask & core_samples_mask]
        if is_debug:
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=6)
        if k != -1:
            char_list.extend(xy)
        xy = raw_char_point_list[class_member_mask & ~core_samples_mask]
        if k != -1:
            char_list.extend(xy)
        if is_debug:
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=4)
        if len(char_list) > 100:
            char_pix_list.append(char_list)
    if is_debug:
        plt.title('Estimated number of clusters: %d' % n_clusters_)
    # plt.show()
    return char_pix_list


def _get_nearest_pix_sort_list(p, round_p):
    '''
    获取附近像素列表，按距离排序
    :param p:
    :param round_p:
    :return: 附近像素列表
    '''
    nearest_pix_list = [(w, h) for w in range(round_p[0] - 1, round_p[0] + 2) for h in
                        range(round_p[1] - 1, round_p[1] + 2)]
    nearest_pix_list.remove(round_p)
    nearest_pix_list.sort(key=lambda a: abs(a[0] - p[0]) + abs(a[1] - p[1]))
    return nearest_pix_list


def _get_array_distance_with_route(small_array, big_pix_list, theta):
    '''
    计算小文字和旋转后的大文子的相似性
    :param small_array: 小文字的稀疏矩阵
    :param big_pix_list: 大文字的像素列表
    :param theta: 大文字旋转角度
    :return: 相似性（越低说明相似性越高）
    '''
    r = theta / 180 * math.pi

    route_big_pix_list = []
    # 计算大文字旋转后的像素列表
    for p in big_pix_list['point']:
        p = (math.cos(r) * p[0] - math.sin(r) * p[1], round(math.sin(r) * p[0] + math.cos(r) * p[1]))
        round_p = (int(round(p[0])), int(round(p[1])))
        if round_p not in route_big_pix_list:
            route_big_pix_list.append(round_p)

        else:
            pix_list = _get_nearest_pix_sort_list(p, round_p)
            for round_p in pix_list:
                if round_p not in route_big_pix_list:
                    route_big_pix_list.append(round_p)
                    break
    route_big_pix_list = list(set(route_big_pix_list))
    # 获取大文字稀疏矩阵
    big_array = _get_array_from_pix_list(route_big_pix_list)

    # 计算两文字之间的距离
    distance = _cal_array_distance(small_array, big_array)

    if is_debug:
        axes = plt.subplot(111)
        x_list = [i for i in range(0, 70 * ratio) for j in range(0, 70 * ratio) if big_array[i, j] != 0]
        y_list = [70 * ratio - j for i in range(0, 70 * ratio) for j in range(0, 70 * ratio) if
                  big_array[i, j] != 0]
        axes.scatter(x_list, y_list, marker='o', s=10, c='b')

        xx_list = [i for i in range(0, 70 * ratio) for j in range(0, 70 * ratio) if
                   small_array[i, j] != 0]
        yy_list = [70 * ratio - j for i in range(0, 70 * ratio) for j in range(0, 70 * ratio) if
                   small_array[i, j] != 0]

        axes.scatter(xx_list, yy_list, marker='.', s=10, c='r')

        # plt.show()
        plt.savefig('./array_pic/' + str(distance) + ".png")
        plt.close()

    print(distance)
    return distance, theta


def deal_with_tianyancha_identify(driver):
    # 刷不出验证码的情况，刷新浏览器
    if "Service Temporarily Unavailable" in driver.page_source:
        driver.refresh()
        time.sleep(2)
        return deal_with_tianyancha_identify(driver)

    # 保存大图片
    big_file_name = _save_big_png(driver)

    # 获取大图片中文字像素列表
    raw_char_point_list = _get_clean_big_char_pix_list(big_file_name)

    # 通过DBSACN聚类，分出每个字的像素
    big_char_pix_list = DBSACN_for_raw_char(raw_char_point_list, 0.10, 10 * ratio)

    # 获取每个字的中心点
    big_singele_pix_list = _get_central_distance_list(big_char_pix_list)

    if not big_singele_pix_list:
        return deal_with_tianyancha_identify(driver)
    # 获取小图片中文字像素列表
    raw_char_point_list = _get_clean_small_char_pix_list(driver)
    # 通过DBSACN聚类，分出每个字的像素
    small_char_pix_list = DBSACN_for_raw_char(raw_char_point_list, 0.25, 15 * ratio)
    # 获取每个字的中心点
    small_single_pix_list = _get_central_distance_list(small_char_pix_list)

    if not small_single_pix_list:
        return deal_with_tianyancha_identify(driver)
    else:
        sure_point_list = []
        small_index = 0

        for small_pix_list in small_single_pix_list:
            small_index += 1
            small_array = _get_array_from_pix_list(small_pix_list['point'])
            print("///")

            for big_pix_list in big_singele_pix_list:
                min_distance = 9999
                process_list = []
                # 用多线程计算大图片中文字和小图片中文字的相似性
                pool = multiprocessing.Pool(processes=8)
                for theta in range(-48, 48, 22):
                    process_list.append(
                        pool.apply_async(_get_array_distance_with_route, (small_array, big_pix_list, theta)))
                pool.close()
                pool.join()

                raw_distance_list = []
                for p in process_list:
                    try:
                        d, theta = p.get()
                    except ValueError:
                        print("未知错误")
                        time.sleep(1)
                        driver.find_element_by_id('refesh').click()
                        time.sleep(1)
                        return deal_with_tianyancha_identify(driver)
                    raw_distance_list.append({"distance": d, "theta": theta})

                raw_distance_list.sort(key=lambda a: a['theta'])
                distance_list = [item['distance'] for item in raw_distance_list]

                for i in range(0, len(distance_list) - 2):
                    sum_distance = sum(distance_list[i:i + 3])
                    if min_distance > sum_distance:
                        min_distance = sum_distance

                sure_point_list.append(
                    {"small_index": small_index, "big_point": big_pix_list['center'], "distance": min_distance})

        sure_point_list.sort(key=lambda a: a['distance'])
        true_sure_point_list = []

        for item in sure_point_list:
            if item['small_index'] not in [a['small_index'] for a in true_sure_point_list] and item[
                'big_point'] not in [a['big_point'] for a in true_sure_point_list]:
                true_sure_point_list.append(item)
        true_sure_point_list.sort(key=lambda a: a['small_index'])

        true_true_sure_point_list = [(a['big_point'][0] / png_w * 322, a['big_point'][1] / png_h * 102) for a in
                                     true_sure_point_list]
        big_png_element = driver.find_element_by_id('bgImgie')
        for w, h in true_true_sure_point_list:
            ActionChains(driver).move_to_element_with_offset(big_png_element, w, h).click().perform()
            # time.sleep(0.5)

        driver.find_element_by_id('submitie').click()
        time.sleep(9)
        try:
            if "antirobot" not in driver.current_url:
                return True
            else:
                # driver.find_element_by_id('refesh').click()
                time.sleep(10)
                return False
        except selenium.common.exceptions.TimeoutException:
            return False


def _cal_array_distance(a, b):
    '''计算两个矩阵之间的距离'''
    c = a - b
    d = numpy.linalg.norm(c, 2)
    return d


def _get_array_from_pix_list(pix_list):
    '''
    通过像素点列表建立稀疏矩阵
    :param pix_list:像素点列表
    :return:稀疏矩阵
    '''
    w_list = [item[0] + 35 * ratio for item in pix_list if
              item[0] + 35 * ratio < 70 * ratio and item[1] + 35 * ratio < 70 * ratio]
    h_list = [item[1] + 35 * ratio for item in pix_list if
              item[0] + 35 * ratio < 70 * ratio and item[1] + 35 * ratio < 70 * ratio]

    return sparse.coo_matrix(([1] * len(w_list), (w_list, h_list)), shape=(70 * ratio, 70 * ratio)).toarray()


if __name__ == '__main__':
    background_hist_info_list = []
    deal_filename_list = []

    for filename in os.listdir('../tianyancha_background'):
        if filename.endswith(".png"):
            image = cv2.imread("../tianyancha_background/" + filename)
            cv2.imshow("Original", image)

            hist = cv2.calcHist([image], [0], None, [256], [0, 256])
            background_hist_info_list.append({"filename": filename, "hist": hist})

    db = mongodb_connection('company')
    col = db['tianyancha_account']

    url_login = "https://www.tianyancha.com/login"
    while col.find({'used_time': {'$exists': 1}}).count() > 5:
        for i in col.find({'used_time': {'$exists': 1}}).sort('used_time'):

            options = webdriver.ChromeOptions()
            options.add_argument("--headless")
            options.add_argument('user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')
            driver = webdriver.Chrome(chrome_options=options)
            # driver = webdriver.Chrome()
            driver.implicitly_wait(30)
            driver.set_page_load_timeout(100)
            driver.delete_all_cookies()
            driver.get(url_login)

            try:
                login_phone_name = driver.find_element_by_xpath(
                    "html/body/div[2]/div[1]/div/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/input")
            except selenium.common.exceptions.NoSuchElementException:
                continue
            login_phone_name.send_keys(i['phone'])
            login_phone_passwd = driver.find_element_by_xpath(
                "html/body/div[2]/div[1]/div/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[3]/input")
            login_phone_passwd.send_keys(i['pw'])

            time.sleep(0.5)

            btn_login = driver.find_element_by_xpath(
                "html/body/div[2]/div[1]/div/div/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[5]")
            print(btn_login.text)
            btn_login.click()
            time.sleep(1)
            if driver.current_url == "https://www.tianyancha.com/":
                print('登录成功，成功进入天眼查搜索页面')
                driver.get('https://www.tianyancha.com/company/13157554')
                time.sleep(1)
                if 'antirobot' in driver.current_url:

                    if not os.path.exists('tianyancha_identify'):
                        os.mkdir('tianyancha_identify')
                    time.sleep(1)
                    is_deal_identify_code = deal_with_tianyancha_identify(driver)
                    # is_deal_identify_code = True
                    if not is_deal_identify_code:
                        continue

                if 'black' in driver.current_url:
                    time.sleep(200)
                    continue

                db = mongodb_connection('company', False)
                collection = db['tianyancha_account']
                collection.update({"phone": i['phone']},
                                  {"from": "天眼查", "phone": i['phone'], "pw": i['pw'], "cookies": driver.get_cookies(),
                                   'state': 'not_used'})
                print("写入:" + str(driver.get_cookies()))
                print('\n')
            else:
                print("登录超时")
                print("手机", i['phone'], "未被写入")
            driver.quit()
