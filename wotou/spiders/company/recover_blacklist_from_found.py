# coding:utf-8
'''
将已经找到的企业从黑名单剔除
'''

from wotou.lib.db_connection import mongodb_connection
from wotou.lib.db_connection import read_col

db = mongodb_connection('company')
unfinished_collection = db['unfinished_company']
blacklist_collection = db['blacklist_company_name']
repeat_list = []
blacklist = read_col('company', 'blacklist_company_name', 'company')
collection = mongodb_connection('company')['raw_company']
blacklist.reverse()
for i in blacklist:

    company_name = i
    ret = collection.find_one({'企业全称': company_name})

    if ret:
        blacklist_collection.delete_one({"company":i})
        print(i)