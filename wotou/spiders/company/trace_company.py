# -*- coding:utf-8 -*-
'''
动态跟踪crm系统中的企业，当企业出现注册资金变动时或投资人变动时，将输出相关信息
'''

import os
import re
import time

import selenium.webdriver.support.expected_conditions as EC
import xlrd
import xlwt
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from xlutils.copy import copy

from wotou.spiders.company.login.easy_login import *
from wotou.spiders.company.spider_company import get_company_detail_from_db_api

driver = None
sheet = None
row_cursor = 0
line_cursor = 0


def init_chrome():
    """
    初始化chrome浏览器
    """
    global driver
    if driver is None:
        options = webdriver.ChromeOptions()
        options.add_argument("-no-sandbox")
        driver = webdriver.Chrome(chrome_options=options)
        driver.implicitly_wait(30)
        driver.set_page_load_timeout(60)
        driver.maximize_window()


def init_phantomjs():
    """
    初始化pantomjs浏览器
    """
    global driver
    caps = DesiredCapabilities.PHANTOMJS
    caps["phantomjs.page.settings.userAgent"] = \
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36"
    if driver is None:
        # driver = webdriver.PhantomJS(desired_capabilities=caps,
        #                              service_args=['--load-images=false', "--web-security=no",
        #                                            "--ignore-ssl-errors=yes",
        #                                            "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1"])
        driver = webdriver.PhantomJS(desired_capabilities=caps)
        driver.implicitly_wait(10)
        driver.set_page_load_timeout(60)


def spider_content(num, change_table, date):
    """
    爬取天眼查公司变更记录的内容
    :param num: 页面中变更记录的数量，一般为5个
    :param change_table: xpath之后得到的变更记录的部分
    :return:
    """
    result_list = []
    for i in range(1, num):
        try:
            change_time = change_table.find_element_by_xpath(
                "./tr[%d]/td[1]/div" % i)

            if change_time.text > date:
                change_title = change_table.find_element_by_xpath(
                    "./tr[%d]/td[2]/div" % i)
                if "注册资本" in change_title.text or "投资人(股权)" in change_title.text:
                    change_content_before = change_table.find_element_by_xpath(
                        "./tr[%d]/td[3]/div" % i)
                    change_content_after = change_table.find_element_by_xpath(
                        "./tr[%d]/td[4]/div" % i)
                    print("变更时间:", change_time.text)
                    print("变更条目:", change_title.text)
                    print("变更前:", change_content_before.text)
                    print("变更后:", change_content_after.text)
                    result_list.append(
                        {"变更时间": change_time.text, "变更条目": change_title.text, "变更前": change_content_before.text,
                         "变更后": change_content_after.text})
            else:
                result_list.append(-1)
                return result_list
        except exceptions.StaleElementReferenceException as e:
            return result_list

    return result_list


def handle_content_tianyancha(company_name, url, date):
    """
    爬取天眼查内容页面
    :param url: 公司的网页链接
    :return:
    """
    global row_cursor
    id = url.split("/")[-1]
    result_list = []
    time.sleep(random.randint(1, 4))
    try:
        driver.get(url)

    except exceptions.TimeoutException as e:
        print('链接超时，正在重新链接')
        return handle_content_tianyancha(company_name, url, date)

    if not is_tianyancha_cookies_useful(driver.current_url,driver.page_source):
        login_tianyancha(driver)
        return handle_content_tianyancha(company_name, url, date)

    contain_element = WebDriverWait(driver, 15).until(
        EC.presence_of_element_located((By.XPATH, '//body/div[2]/div[1]/div[1]/div[1]/div[1]')))
    time.sleep(1)
    if contain_element.text == '':
        contain_element = driver.find_element_by_xpath('//body/div[2]/div[1]/div[1]/div[2]/div[1]')

    # 是否上市
    try:
        stock_num = contain_element.find_element_by_xpath('//*[@id="nav-main-shangshiitem"]/span').text
        if stock_num == '上市信息':
            on_list = True
            adjust_num = 3
        else:
            on_list = False
            # print(on_list)

    except exceptions.NoSuchElementException:
        on_list = False

    if on_list:
        print(company_name + " 已上市")

    try:
        try:
            change_no = driver.find_element_by_xpath(".//*[@id='nav-main-changeCount']/span")
        except exceptions.NoSuchElementException as e:
            print('无变更记录！！！！')
            return
        change_no = int(change_no.text)

        if change_no > 5:
            change_page = driver.find_element_by_xpath(".//*[@id='_container_changeinfo']/div/div[2]/div")
            change_page_num = int(re.sub("\D", "", change_page.text))
            for i in range(1, change_page_num + 1):
                change_table = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH,
                                                    ".//*[@id='_container_changeinfo']/div/div[1]/table/tbody")))
                if i == 1:
                    result_list.extend(spider_content(6, change_table, date))
                    if result_list and result_list[-1] == -1:
                        break
                else:
                    # 由于天眼查网页的bug，翻页的时候偶尔会出错，造成服务器错误，所以以此方式翻页
                    # driver.execute_script(
                    #     "window.open('https://www.tianyancha.com/pagination/changeinfo.xhtml?ps=5&pn=" + str(
                    #         i) + "&id=" + id + "')")
                    # time.sleep(random.randint(1, 4))
                    # driver.switch_to_window(driver.window_handles[-1])
                    # change_table = WebDriverWait(driver, 30).until(
                    #     EC.presence_of_element_located((By.XPATH, '//table/tbody')))
                    if i == change_page_num:
                        result_list.extend(spider_content((change_no % 5) + 1, change_table, date))
                    else:
                        result_list.extend(spider_content(6, change_table, date))
                        if result_list and result_list[-1] == -1:
                            break

                            # if len(driver.window_handles) > 1:
                            #     driver.execute_script("window.close()")
                            #
                            # driver.switch_to_window(driver.window_handles[0])
                # next_page = driver.find_element_by_xpath(
                #     ".//*[@id='_container_changeinfo']/div/div[2]/ul/li[last()]/a")
                # a = driver.find_element_by_xpath(".//*[@id='_container_changeinfo']/div/div/table/tbody/tr[last()-1]")
                # driver.execute_script("arguments[0].scrollIntoView();", a)
                # next_page.click()

                driver.execute_script("companyPageChange("+str(i+1)+",this)")
                time.sleep(1)

        else:
            change_table = driver.find_element_by_xpath(
                ".//*[@id='_container_changeinfo']/div/div[1]/table/tbody")
            result_list.extend(spider_content(change_no + 1, change_table, date))

        for i in result_list:
            if i != -1:
                row_cursor += 1
                sheet.write(row_cursor, 0, company_name)
                sheet.write(row_cursor, 1, url)
                sheet.write(row_cursor, 2, i['变更条目'])
                sheet.write(row_cursor, 3, i['变更时间'])
                sheet.write(row_cursor, 4, i['变更前'])
                sheet.write(row_cursor, 5, i['变更后'])
    except exceptions.TimeoutException as e:
        print('爬取信息超时，正在重新链接')
        return handle_content_tianyancha(company_name, url, date)


def handle_content_qichacha(company_name, url, date):
    """
    爬取企查查公司内容
    :param url: 企查查公司的链接
    :return:
    """
    global row_cursor
    try:
        driver.get(url)
    except exceptions.TimeoutException as e:
        print('链接超时，正在重新链接')
        return handle_content_qichacha(company_name, url, date)

    if not is_qichacha_cookies_useful(driver.page_source):
        login_qichacha(driver)
        return handle_content_qichacha(company_name, url, date)

    wait = WebDriverWait(driver, 15)

    top_element = wait.until(
        EC.presence_of_element_located((By.XPATH, '//*[@id="company-top"]/div/div[1]/span[2]')))
    try:
        first_small_text = top_element.find_element_by_xpath('./small[1]').text
        # 上市公司页面有3个small标签，第一个为"上市详情："
        on_list = '上市详情' in first_small_text
    except exceptions.NoSuchElementException:
        on_list = False

    if on_list:
        print(company_name + ' 已上市')

    try:

        change_no = wait.until(EC.presence_of_element_located(
            (By.XPATH, "//section[@id='Changelist']/div/span[2]")))
        change_content = wait.until(EC.presence_of_element_located(
            (By.XPATH, "//section[@id='Changelist']/table/tbody")))
    except exceptions.TimeoutException:
        return
    for i in range(1, int(change_no.text) * 4):
        try:
            change_title = change_content.find_element_by_xpath('./tr[%d]' % i)
            if '注册资本' in change_title.text or '投资人' in change_title.text:
                change_time = change_title.find_element_by_xpath('./following-sibling::tr[2]/td[2]')
                if change_time.text > date:
                    before = change_title.find_element_by_xpath('./following-sibling::tr[2]/td[3]')
                    after = change_title.find_element_by_xpath('./following-sibling::tr[2]/td[4]')
                    print('变更条目：', change_title.text)
                    print('变更时间：', change_time.text)
                    print('变更前：', before.text.replace('\n', ' '))
                    print('变更后:', after.text.replace('\n', ' '))
                    row_cursor += 1
                    sheet.write(row_cursor, 0, company_name)
                    sheet.write(row_cursor, 1, url)
                    sheet.write(row_cursor, 2, change_title.text)
                    sheet.write(row_cursor, 3, change_time.text)
                    sheet.write(row_cursor, 4, before.text.replace('\n', ' '))
                    sheet.write(row_cursor, 5, after.text.replace('\n', ' '))

        except exceptions.NoSuchElementException as e:
            break
            # collection_local_trace.update({'企业全称': name},
            # {'$push': {j: {j+"前": before, j+"后":after,'变更时间': shijian.text,'爬取时间': spider_time}}})


def trace_crm_company_api(date):
    """
    crm系统动态跟踪调用api，结果以名为 crm_trace_company.xls 的文件输出，由于公司个数较大，大概需要几个小时才能完成，当任何情况让程序停止时，可重新运行，程序支持断点执行
    :param date:开始检测日期
    :return:无
    """
    global sheet
    global row_cursor
    global line_cursor

    if not os.path.exists('crm_trace_result.xls'):
        f = xlwt.Workbook(encoding='utf-8')
        f.add_sheet('1')
        f.save("crm_trace_result.xls")

    oldwb = xlrd.open_workbook('crm_trace_result.xls')
    newwb = copy(oldwb)
    sheet = newwb.get_sheet(0)
    row_cursor = oldwb.sheets()[0].nrows
    line_cursor = 0

    table = oldwb.sheets()[0]
    if row_cursor == 0:
        sheet.write(row_cursor, line_cursor, "企业名称")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "企业链接")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更条目")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更时间")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更前")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更后")

    # 推荐用chrome运行
    init_chrome()
    finished_company_list = table.col_values(0)
    if '公司' in finished_company_list[-1]:
        start_flag = False
    else:
        start_flag = True
    db_company = mongodb_connection('company')
    collection_trace = db_company['trace_company']
    collection_raw = db_company['raw_company']
    company_ref_list = collection_trace.find({})
    for ref in company_ref_list:
        id = ref['详细信息'].id
        company_name = ref['企业全称']
        if not start_flag:
            if company_name == finished_company_list[-1]:
                start_flag = True
            continue
        company_url = collection_raw.find_one({'_id': id}, {'_id': 0, '链接': 1})

        company_url = company_url['链接']

        if 'qichacha' in company_url:
            print(u'企业链接：', company_url)
            handle_content_qichacha(company_name, company_url, date)
        elif 'tianyancha' in company_url:
            print(u'企业链接：', company_url)
            handle_content_tianyancha(company_name, company_url, date)
        newwb.save('crm_trace_result.xls')


def trace_list_company_api(date, filename):
    """
    列表法动态跟踪调用api，结果以名为 list_trace_company.xls 的文件输出，由于公司个数较大，大概需要几个小时才能完成，当任何情况让程序停止时，可重新运行，程序支持断点执行
    :param date:开始检测日期
    :return:无
    """
    global sheet
    global row_cursor
    global line_cursor

    company_name_list = []

    with open(filename,encoding='UTF-8') as f:
        for l in f.readlines():
            company_name_list.append(l.replace("\n", "").strip())

    if not os.path.exists('list_trace_result.xls'):
        f = xlwt.Workbook(encoding='utf-8')
        f.add_sheet('1')
        f.save("list_trace_result.xls")

    oldwb = xlrd.open_workbook('list_trace_result.xls')
    newwb = copy(oldwb)
    sheet = newwb.get_sheet(0)
    row_cursor = oldwb.sheets()[0].nrows
    line_cursor = 0

    table = oldwb.sheets()[0]
    if row_cursor == 0:
        sheet.write(row_cursor, line_cursor, "企业名称")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "企业链接")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更条目")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更时间")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更前")
        line_cursor += 1
        sheet.write(row_cursor, line_cursor, "变更后")

    # 推荐用chrome运行
    init_chrome()
    finished_company_list = table.col_values(0)
    if finished_company_list:
        if '公司' in finished_company_list[-1]:
            start_flag = False
        else:
            start_flag = True
    else:
        start_flag = True
    db_company = mongodb_connection('company')
    for company_name in company_name_list:

        if not start_flag:
            if company_name == finished_company_list[-1]:
                start_flag = True
            continue
        company_url = get_company_detail_from_db_api(company_name)['链接']

        if 'qichacha' in company_url:
            print(u'企业链接：', company_url)
            handle_content_qichacha(company_name, company_url, date)
        elif 'tianyancha' in company_url:
            print(u'企业链接：', company_url)
            handle_content_tianyancha(company_name, company_url, date)
        newwb.save('list_trace_result.xls')


if __name__ == '__main__':
    trace_list_company_api('2016-07-01', 'list.txt')
