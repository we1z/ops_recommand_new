# coding=utf-8
"""
爬取公司联系方式，通过百度搜索前三页，输入为in.txt,输出为以in.txt文件的第一行命名的xls文件
"""
import jieba.posseg as pseg
from itertools import chain
import requests
import re
import urllib3
import urllib.parse
import time
import pymongo
import xlwt
import xlrd
import os
import datetime
from xlutils.copy import copy
from selenium import webdriver
from selenium.common import exceptions
import random
from lxml import html
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options
import multiprocessing
from wotou.spiders.baidu.baidu_text import query
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.company.spider_company import _company_name_strip,get_company_detail_from_db_api

black_list = ['www.1688.com',
              'www.qianlima.com', 'page.1688.com', 'yqk.39.net', 'tecenet.com', 'www.jobui.com', 'p.tgnet.com',
              'www.jianke.com', 'bidcenter.com', 'atobo.com.cn', 'nuomi.com', 'b2b.tfsb.cn', 'maigoo.com', '39.net',
              'a-hospital.com', 'nongjitong.com', 'youbian.com', 'xaecong.com'
              ]


# Structural class for contact
class ContactRecord:
    """
    联系方式类
    """

    def __init__(self, tel=None, name=None, url=None, text=None, origin_url=None):
        if tel:
            self.tel = tel.replace(' ', '').replace('-', '').replace(")", "")
        else:
            self.tel = tel
        self.name = name
        self.url = url
        self.text = text
        self.origin_url = origin_url

    def validate(self, number=None):
        """Verify the validation of the tel num"""
        if not number:
            number = self.tel
        if len(number) == 8:  # date detect
            if all([1970 < int(number[:4]) < 2050, 0 <= int(number[4:6]) <= 23, 0 <= int(number[6:8]) <= 59]):
                return False
        return True

    def __eq__(self, x):
        return self.tel == x.tel

    def __ne__(self, x):
        return self.tel != x.tel

    def __setitem__(self, name, value):
        if name == 'tel':
            self.tel = value.replace(' ', '').replace('-', '').replace(")", "")
        elif name == 'name':
            self.name = value
        elif name == 'url':
            self.url = value

    def __str__(self):
        if self.name:
            return ' '.join([self.name, self.tel])
        else:
            return self.tel


class Company:
    """
    公司类
    """

    def __init__(self, name):
        self.name = name.strip()
        self.contacts = []

    def __len__(self):
        return len(self.contacts)

    def __iter__(self):
        return iter(self.contacts)

    def add(self, contact):
        """
        向公司中添加新找到的联系方式
        :param contact: 联系方式类的实例
        :return:
        """
        if contact not in self.contacts:
            self.contacts.append(contact)
        else:
            for c in self.contacts:
                if c == contact:
                    if c.name is not None:
                        break
                    elif contact.name is not None:
                        c.name = contact.name
                        break

    def __str__(self):
        return '\n'.join(
            ['origin_url:' + str(x.origin_url) + '\n' + 'tel:' + str(x.tel) + '\n' + 'url:' + str(
                x.url + '\n' + 'text:' + str(x.text + '\n')) for x in
             self.contacts])


class ContactHunter:
    """
    爬虫主体
    """

    def __init__(self):
        self.header = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393'}
        self.TEL_REGEX = re.compile(
            r'(?<!\d)010(?:\s?-|\)\s?|\s?)[123456789]\d{7}(?!\d)'
            r'|(?<!\d)02[12345789](?:\s?-|\)\s?|\s?)[123456789]\d{7}(?!\d)'
            r'|(?<!\d)0[3456789]\d{2}(?:\s?-|\)\s?|\s?)[123456789]\d{6,7}(?!\d)'
            r'|(?<!\d)1[3456789]\d{9}(?!\d)')  # |(?<!\d)\d{8}(?!\d)

    def findContact(self, comname, debug=False):
        """
        Method to search for contacts from Baidu results and their childrens.
        Searching is based on match phone number by rigorous regex,
        and the names are according to their contexts.
        """

        # 在百度搜索输入：公司名，公司名+联系人，公司名+手机，公司名+电话，公司名+联系方式
        driver = None

        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument(
            'user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36')

        # options.add_argument("--proxy-server=socks5://" + '127.0.0.1' + ":" + '1080')
        driver = webdriver.Chrome(chrome_options=options)

        search_result = query('"' + comname + '"', driver)
        search_result.extend(query('"' + comname + '"' + ' ' + '联系人', driver))
        search_result.extend(query('"' + comname + '"' + ' ' + '手机', driver))
        search_result.extend(query('"' + comname + '"' + ' ' + '电话', driver))
        search_result.extend(query('"' + comname + '"' + '联系方式', driver))

        driver.quit()

        if debug > 1:
            print(search_result)
        result = Company(comname)
        try:
            for item in search_result:
                black_flag = False
                for black in black_list:
                    if black in item[3] or item[3] in black:
                        black_flag = True
                        break
                if black_flag:
                    continue

                tel_iter = self.TEL_REGEX.finditer(item[2])
                # flag: if in the Baidu abstract, phone nums have been found,
                # then do not visit the link totally again
                containflag = False
                for _tel in tel_iter:
                    sub_result = ContactRecord()
                    sub_result['tel'] = _tel.group().replace(' ', '')
                    if not sub_result.validate():
                        continue
                    containflag = True
                    prefix = item[2][max(
                        0, _tel.span()[0] - 10):_tel.span()[0] - 1]
                    suffix = item[2][_tel.span()[1] +
                                     1:min(len(item[2]), _tel.span()[0] + 10)]
                    # reduce memory use, traverse parts of words by itertools
                    for pair in chain(pseg.cut(prefix), pseg.cut(suffix)):
                        if 'nr' in pair.flag:
                            result.add(ContactRecord(
                                _tel.group().replace(' ', ''), url=item[0], text=item[2], origin_url=item[3],
                                name=pair.word))
                            break
                    else:
                        result.add(ContactRecord(
                            _tel.group().replace(' ', ''), url=item[0], text=item[2], origin_url=item[3]))
                if containflag is False and ("联系" in item[2] or "电话" in item[2]):
                    deeper_url_text = ''
                    try:
                        deeper_url_text = requests.get(item[0], headers=self.header, timeout=10).text
                    except Exception:
                        print(item[0])
                        print("detail connection error")
                        try:
                            time.sleep(2)
                            deeper_url_text = requests.get(item[0], headers=self.header, timeout=5).text
                        except Exception:
                            print("second detail connection error. ignore this information")

                    __tel = self.TEL_REGEX.findall(deeper_url_text)
                    __tel = list(set(__tel))  # remove repeated items

                    for telnum in __tel:
                        c = ContactRecord(telnum, url=item[0], text=item[2], origin_url=item[3])
                        if c.validate():
                            result.add(c)
        except ConnectionError:
            print('ConnectionError')
            return self.findContact(comname, debug)
        except requests.exceptions.ConnectionError:
            print('ConnectionError')
            return self.findContact(comname, debug)
        except requests.exceptions.ChunkedEncodingError:
            print("ConnectionError")
            return self.findContact(comname, debug)
        except UnicodeDecodeError:
            print("UnicodeError")
            return self.findContact(comname, debug)
        return result


def create_xls(file_name):
    """创建工作表
       输入:工作表的name,表的字段
    """
    f = xlwt.Workbook(encoding='utf-8')
    f.add_sheet('1', cell_overwrite_ok=True)
    f.save(file_name)


def get_company_phone_number_api(input_file,output_file):
    """
    读取in.txt的公司名称，分别爬取，结果将同时输出到屏幕和xls文件中
    """

    testlist = []
    testresult = []
    count = 0

    # 若in.txt文件的第一行为#开头，则以此命名输出的文件
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            testlist.append(line)

    q = ContactHunter()

    if not output_file:
        output_file = str(datetime.datetime.now()) + '.xls'

    if not os.path.exists(output_file):
        create_xls(output_file)

    oldwb = xlrd.open_workbook(output_file)
    newwb = copy(oldwb)
    sheet = newwb.get_sheet(0)
    row_cursor = oldwb.sheets()[0].nrows + 1
    line_cursor = 0

    for com in testlist:
        count += 1
        if not count % 5:
            print('已完成' + str(count) + '间公司')
        testresult = q.findContact(com.replace("\n", ""))

        print(testresult.name)
        print(str(testresult))

        detail = get_company_detail_from_db_api(testresult.name)

        if detail['联系方式']['电话']:
            sheet.write(row_cursor, line_cursor, testresult.name)
            line_cursor += 1
            sheet.write(row_cursor, line_cursor, detail['联系方式']['电话'])
            line_cursor += 1
            sheet.write(row_cursor, line_cursor, "天眼查")

            row_cursor += 1
            line_cursor = 0
        for con in testresult.contacts:
            if con.name:
                sheet.write(row_cursor, line_cursor, testresult.name + "联系人：" + con.name)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.tel)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.text)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.url)

            else:

                sheet.write(row_cursor, line_cursor, testresult.name)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.tel)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.text)
                line_cursor += 1
                sheet.write(row_cursor, line_cursor, con.url)
            row_cursor += 1
            line_cursor = 0
        newwb.save(output_file)
    # 设置列宽
    sheet.col(0).width = 256 * 40
    sheet.col(1).width = 256 * 15
    sheet.col(2).width = 256 * 90
    sheet.col(3).width = 256 * 90

    newwb.save(output_file)


if __name__ == '__main__':
    get_company_phone_number_api('in.txt','#北京0320')