# coding=utf-8
'''
爬取百度的百度搜索信息
'''

import urllib.parse
import time

from selenium import webdriver
from selenium.common import exceptions
import random
from lxml import html

from newspaper import Article
import newspaper
import Levenshtein
import urllib3


def query(word, driver):
    """
    搜索入口
    :param word:搜索关键字
    :param debug: 调试模式开关，True则输出调试文档
    :return: 百度搜索结果列表
    """
    print("百度查询")
    url = r'https://www.baidu.com/s?wd='
    words = urllib.parse.quote(word)

    try:
        driver.get(url + words)
        driver.refresh()
        response_data = driver.page_source

        # 搜百度搜索第二页内容

        driver.execute_script('var q=document.documentElement.scrollTop=100000')
        driver.find_element_by_xpath('//span[@class="fk fkd"]').click()
        time.sleep(1)
        if response_data!=driver.page_source:
            response_data = response_data + driver.page_source
        else:
            time.sleep(0.5)
            response_data = response_data + driver.page_source
        # 搜百度第三页内容

        # r = 0
        # while r == 0:
        #     driver.execute_script('var q=document.documentElement.scrollTop=100000')
        #     time.sleep(0.4)
        #     r = driver.execute_script('return document.body.scrollTop')
        #     try:
        #         driver.find_element_by_xpath('//div[@id="page"]/a[last()]').click()
        #     except exceptions.WebDriverException:
        #
        #         continue
        #     break
        # response_data = response_data + driver.page_source


    except exceptions.NoSuchElementException:
        print("NoSuchElementException")
        pass
    except (exceptions.StaleElementReferenceException, exceptions.TimeoutException):
        print("TimeOutException")
        driver = webdriver.Chrome()
        return query(word, driver)

    result = []
    root = html.fromstring(response_data)
    list = root.xpath('//div[@class="result c-container "]')

    for i in list:
        item = []
        try:
            item.append(i.xpath('./h3/a/@href')[0])
            item.append(i.xpath('./h3/a')[0].xpath('string(.)').replace("\xa0", ""))
            item.append(i.xpath('.//div[@class="c-abstract"]')[0].xpath('string(.)').replace("\xa0", ""))
            item.append(i.xpath('.//div[@class="f13"]/a')[0].xpath('string(.)').replace("\xa0", ""))
            result.append(item)
        except IndexError:
            pass

    return result


def is_similar_text(text_list, text):
    '''
    判断类似的信息是否已经爬取过
    :param text_list: 已有信息
    :param text: 待判断信息
    :return: bool
    '''
    if not text_list:
        return False
    for i in text_list:
        distance = Levenshtein.distance(i, text)
        if distance < max(len(text), len(i)) - min(len(text), len(i)) * 1.2 or distance < 0.4 * max(len(text), len(i)):
            return True
    return False


def search_company_baidu_info_api(keyword):
    '''
    获取企业百度搜索信息的api
    :param keyword:公司名称
    :return:百度搜索结果文本
    '''

    driver=None

    options = webdriver.ChromeOptions()
    # options.add_argument("--headless")
    options.add_argument(
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/53')

    # options.add_argument("--proxy-server=socks5://" + '127.0.0.1' + ":" + '1080')
    try:
        driver = webdriver.Chrome(chrome_options=options)
        baidu_list = query(keyword, driver)
    except ConnectionResetError:
        print("连接重置！！！")
        if driver:
            driver.quit()
        return search_company_baidu_info_api(keyword)

    baidu_list.sort(key=lambda a: a[2])

    # baidu_list_len = len(baidu_list)
    # i = 0
    # 过滤一样的链接
    # while i < baidu_list_len:
    #     if baidu_list[i][2] == baidu_list[i - 1][2]:
    #         baidu_list.remove(baidu_list[i])
    #         baidu_list_len -= 1
    #         i -= 1
    #     i += 1

    text_list = []

    return_text = ''
    print(baidu_list)

    for i in baidu_list:
        # 提取文本

        paper = Article(url=i[0], language='zh')
        paper.download()
        try:
            paper.parse()
        except (newspaper.article.ArticleException, TimeoutError, ValueError, urllib3.exceptions.ReadTimeoutError):
            print(i[0]+'出错')
            continue
        if paper.text and len(paper.text) > len(i[2]):
            text = paper.text
        else:
            text = i[2]

        if is_similar_text(text_list, text):
            continue

        text_list.append(text)
        return_text += '\n'+text
    driver.quit()
    return return_text

# search_company_baidu_info_api('深圳市海目星激光科技有限公司')
