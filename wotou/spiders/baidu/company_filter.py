from wotou.spiders.company.spider_company import get_company_detail_from_db_api, is_state_own_company, \
    is_stock_relate_company
from wotou.lib.db_connection import mongodb_connection, mongodb_connection_local
from multiprocessing import Pool
import os
from wotou.spiders.baidu.baidu_text import search_company_baidu_info_api
import shutil

medicine = ['医药制造业',
            '卫生',
            '卫生和社会工作',
            '社会工作']

technology = ['计算机、通信和其他电子设备制造业',
              '专用设备制造业',
              '专用仪器仪表制造',
              '互联网和相关服务',
              '软件和信息技术服务业',
              '研究和试验发展',
              '专业技术服务业',
              '科技推广和应用服务业',
              '信息传输、软件和信息技术服务业',
              '科学研究和技术服务业',
              '汽车制造业',
              '其他制造业']

media = ['新闻和出版业',
         '广播、电视、电影和录音制作业',
         '文化艺术业',
         '体育',
         '娱乐业',
         '文化、体育和娱乐业']

communication = ['电信、广播电视和卫星传输服务']


def txt2list(filepath):
    # 将生成的黑白企业名单txt文件转换成列表
    with open(filepath, 'r') as f:
        content = f.read()
        l = [i for i in content.split('\n') if len(i) > 2]
        return l


black_company_list = list(set(txt2list('企业黑名单.txt')))
white_company_list = list(set(txt2list('企业白名单.txt')))


def get_black_company_txt():
    # 生成企业黑名单
    with open('企业黑名单.txt', 'w') as f:
        for i in mongodb_connection('company')['blacklist_company_not_in_crm'].find():
            f.write(i['企业全称'])
            f.write('\n')


def company_filter(data):
    # 判断一个公司是否国企或者上市公司子公司，是的话就加入本地数据库的washed_list表，同时从原数据库表中删除，不是的话就保留
    detail = get_company_detail_from_db_api(data[0])
    try:
        if is_stock_relate_company(detail):
            mongodb_connection_local('cnki')['washed_list'].insert({'企业全称': data[0]})
            mongodb_connection_local('cnki')[data[1]].remove({'企业全称': data[0]})
        elif is_state_own_company(detail):
            mongodb_connection_local('cnki')['washed_list'].insert({'企业全称': data[0]})
            mongodb_connection_local('cnki')[data[1]].remove({'企业全称': data[0]})
        else:
            pass
    except TypeError as e:
        print(e)


def multi_company_filter():
    # 将原始黑白名单加入本地数据库，然后多进程过滤
    wl = [{'企业全称': i} for i in white_company_list]
    bl = [{'企业全称': i} for i in black_company_list]
    mongodb_connection_local('cnki')['white_list'].insert_many(wl)
    mongodb_connection_local('cnki')['black_list'].insert_many(bl)
    white_list = [[i, 'white_list'] for i in white_company_list]
    black_list = [[i, 'black_list'] for i in black_company_list]
    pool = Pool(processes=8)
    pool.map(company_filter, black_list)
    pool.map(company_filter, white_list)


def check_and_download_company_introduction():
    # 检查公司简介是否已经下载，如果没下载就用百度爬虫下载
    l = [i[:-4] for i in os.listdir('/home/j/文档/ops_recommand/wotou/output/baidu_raw_text')]
    for i in mongodb_connection_local('cnki')['white_list'].find():
        if i['企业全称'] in l:
            print('已存在')
            pass
        else:
            text = search_company_baidu_info_api(i['企业全称'])
            with open('/home/j/文档/ops_recommand/wotou/output/baidu_raw_text/%s.txt' % i['企业全称'], 'w') as f:
                f.write(text)
    for i in mongodb_connection_local('cnki')['black_list'].find():
        if i['企业全称'] in l:
            print('已存在')
            pass
        else:
            text = search_company_baidu_info_api(i['企业全称'])
            with open('/home/j/文档/ops_recommand/wotou/output/baidu_raw_text/%s.txt' % i['企业全称'], 'w') as f:
                f.write(text)


def add_industry(onedata):
    # 将本地数据库的企业加上行业
    data = mongodb_connection('company')['raw_company'].find({'企业全称': onedata[0]})
    if data.count() == 0:
        mongodb_connection_local('cnki')[onedata[1]].remove({'企业全称': onedata[0]})
        print('企业不存在，从本地数据库删掉')
    else:
        detail = [i for i in data][0]
        mongodb_connection_local('cnki')[onedata[1]].find_one_and_update({'企业全称': onedata[0]},
                                                                         {'$set': {'行业': detail['行业']}})
        print(onedata[0] + detail['行业'])


def multi_add_industry():
    # 多进程给本地数据库加上行业
    total = [[i['企业全称'], 'white_list'] for i in
             mongodb_connection_local('cnki')['white_list'].find({'行业': {'$exists': 0}})] + [[i['企业全称'], 'black_list']
                                                                                             for i in
                                                                                             mongodb_connection_local(
                                                                                                 'cnki')[
                                                                                                 'black_list'].find({
                                                                                                 '行业': {
                                                                                                     '$exists': 0}})]
    pool = Pool(processes=16)
    pool.map(add_industry, total)


if __name__ == '__main__':
    filedir = [[i[:-4], '/home/j/文档/ops_recommand/wotou/output/baidu_raw_text/' + i] for i in
               os.listdir('/home/j/文档/ops_recommand/wotou/output/baidu_raw_text/')]
    d={}
    for i in filedir:
        d[i[0]]=i[1]
    print(d)
    # total = [[i['企业全称'], i['行业'], 'white_list'] for i in mongodb_connection_local('cnki')['white_list'].find()] + [
    #     [i['企业全称'], i['行业'], 'black_list'] for i in mongodb_connection_local('cnki')['black_list'].find()]
    # for i in total:
    #     if i[1] in medicine:
    #         print('medicine')
    #     elif i[1] in media:
    #         print('media')
    #     elif i[1] in communication:
    #         print('communication')
    #     elif i[1] in technology:
    #         print('technology')
    #     else:
    #         print('else')
