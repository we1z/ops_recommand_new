# coding:utf-8
'''
生物谷爬虫
'''
from selenium import webdriver
from selenium.common import exceptions
import time
import datetime
import re
import pymongo
from wotou.lib.db_connection import mongodb_connection
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from wotou.spiders.util.proxies import service_args
from selenium.webdriver import DesiredCapabilities

driver = None


def init_phantomjs():
    """
    初始化pantomjs浏览器
    """
    global driver
    caps = DesiredCapabilities.PHANTOMJS
    caps["phantomjs.page.settings.userAgent"] = \
        "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0"
    if driver is None:
        driver = webdriver.Chrome(desired_capabilities=caps,
                                     service_args=['--load-images=false', "--web-security=no",
                                                   "--ignore-ssl-errors=yes",
                                                   "--ignore-ssl-errors=true", "--ssl-protocol=tlsv1"] + service_args)
        # driver = webdriver.PhantomJS(desired_capabilities=caps)
        driver.implicitly_wait(10)
        driver.set_page_load_timeout(120)


def spider_list(url):
    """
    生物谷分支页面列表，获取新闻文章的链接，发布日期，名字
    :param url:生物谷分支链接
    :return:
    """
    try:
        time.sleep(3)
        driver.get(url)
        # 分支的类型
        type = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//li[@class="select"]/a'))).get_attribute("innerHTML").strip()
        print(type)
        art_len = WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.XPATH, '//ul[@id="cms_list"]/li')))
        print(len(art_len))
        news_urls = []
        # 更改部分，page_title和news_url的xpath路径，由div[2]改成div[@class='cntx']
        for i in range(1, len(art_len) + 1):
            # 新闻的标题
            page_title = WebDriverWait(driver, 10).until \
                (EC.presence_of_element_located((By.XPATH,
                                                 '//ul[@id="cms_list"]/li[%d]/div[@class="cntx"]/h4/a' % i))).text
            print('新闻标题：', page_title)
            # 新闻的链接
            news_url = WebDriverWait(driver, 10).until \
                (EC.presence_of_element_located((By.XPATH,
                                                 '//ul[@id="cms_list"]/li[%d]/div[@class="cntx"]/h4/a' % i))).get_attribute(
                'href')
            print('新闻链接：', news_url)
            news_urls.append(news_url)
    except exceptions.TimeoutException as e:
        print('专区网页加载过慢！！！！')
        return spider_list(url)
    print(news_urls)
    # return news_urls
    # 每个新闻分开进行爬取
    for new_url in news_urls:
        spider_news(new_url, type)


def spider_news(url, type):
    """
    爬取新闻页面，获取相应信息
    :param url: 新闻页面url
    :param type: 新闻文章类型
    :return:
    """
    # 本地数据库表
    # collection = mongodb_connection_local()
    # 远程数据库表，写入news的shengwugu表
    db = mongodb_connection('news')
    collection = db['shengwugu']
    # 判断链接是否存在数据库中，不存在则爬取新闻信息，写入数据库
    if collection.find_one({'url': url}):
        print('此页信息已存在，不再存入！\n')
    else:
        try:
            driver.get(url)

            # 爬虫时间
            spidertime = datetime.datetime.now()
            print('爬虫时间：', spidertime)
            # 发布时间
            news_time = WebDriverWait(driver, 10).until \
                (EC.presence_of_element_located((By.XPATH,
                                                 '//div[@class="title5"]/p'))).text
            time_list = re.findall(r'(\w*[0-9]+)\w*', news_time)
            news_time = '-'.join(time_list[:3])
            print('发布时间：', news_time)
            # 新闻的标题
            news_title = WebDriverWait(driver, 10).until \
                (EC.presence_of_element_located((By.XPATH,
                                                 '//div[@class="title5"]/h1'))).text
            print('新闻标题：', news_title)
            # 新闻内容
            news_content = WebDriverWait(driver, 10).until \
                (EC.presence_of_all_elements_located((By.XPATH,
                                                      '//div[@class="text3"]')))
            dr = re.compile(r'<[^>]+>', re.S)
            for i in news_content:
                text = ''.join(i.text)
                text = dr.sub('', text)
                print("新闻内容：")
                print(text)
                print('\n')
                time.sleep(2)
        except exceptions.TimeoutException as e:
            print('新闻网页加载过慢！！！！')
            if driver.current_url!=url:
                return
            else:
                return spider_news(url, type)
        # 插入数据库
        collection.insert({'url': url, 'name': news_title, 'type': type, 'content': text,
                           'time': news_time, 'spidertime': spidertime})


def update_shengwugu_data_api():
    # 原来的type类型，总结起来为转化医学和制药方向
    types = [u'肿瘤免疫治疗', u'疫苗', u'CAR-T/TCR-T细胞治疗', u'新药', u'生物仿制药', u'罕见病和孤儿药', u'单抗药物', u'生物反应器',
             u'神经科学', u'癌症研究', u'代谢组学', u'生物信息学', u'基因治疗', u'炎症与疾病', u'生物标志物', u'肿瘤免疫治疗',
             u'微环境', u'微生物组', u'细胞治疗', u'临床研究', u'干细胞&iPS', u'组学', u'糖尿病', u'免疫学', u'肿瘤转化医学', u'高分辨率成像']
    # type的链接，phantomjs的首页爬不出来，从分支爬取
    types_urls = ['http://news.bioon.com/tumor-immunotherapy/', 'http://news.bioon.com/vaccines/',
                  'http://news.bioon.com/CAR-T/', 'http://news.bioon.com/drug/',
                  'http://news.bioon.com/Biosimilar/', 'http://news.bioon.com/rareDiseases/',
                  'http://news.bioon.com/monoclonalDrugs/', 'http://news.bioon.com/bioreactor/',
                  'http://news.bioon.com/neuroscience/', 'http://news.bioon.com/m_omics/',
                  'http://news.bioon.com/Biologicalinformation/', 'http://news.bioon.com/cancerstemcell/',
                  'http://news.bioon.com/genetherapy/', 'http://news.bioon.com/Inflammation/',
                  'http://news.bioon.com/biomarkers/', 'http://news.bioon.com/tumor-immunotherapy/',
                  'http://news.bioon.com/niche/', 'http://news.bioon.com/microbiology/',
                  'http://news.bioon.com/celltherapy/', 'http://news.bioon.com/trials/',
                  'http://news.bioon.com/stemcell/', 'http://news.bioon.com/omics/',
                  'http://news.bioon.com/diabetesTM/', 'http://news.bioon.com/immunology/',
                  'http://news.bioon.com/cancerTM/', 'http://news.bioon.com/HRI/']
    # init_chrome()
    init_phantomjs()
    # init_Firefox()
    driver.delete_all_cookies()
    for types_url in types_urls:
        spider_list(types_url)
    driver.quit()


if __name__ == '__main__':
    update_shengwugu_data_api()
