# coding=utf-8
import time
from wotou.lib.db_connection import mongodb_connection
from newspaper import Article
import requests


kopf = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'}


def Zhihu_Artikel_Bekommt():
    Autore = {
        "久谦": "jiu-qian-95-59",
        "飞总": "fei-zong-55",
        "彭哲夫": "CMGS",
        "韦易笑": "skywind3000",
        "图灵Don": "TuringDon",
    }
    for Autor  in Autore:
        print("####----{}----####".format(Autor))
        col=mongodb_connection('news')['zhihu']
        url="https://www.zhihu.com/api/v4/members/{}/articles?include=data%5B*%5D.comment_count%2Csuggest_edit%2" \
            "Cis_normal%2Cthumbnail_extra_info%2Cthumbnail%2Ccan_comment%2Ccomment_permission%2Cadmin_closed_comment%2Ccontent%2Cvoteup_count%2" \
            "Ccreated%2Cupdated%2Cupvoted_followees%2Cvoting%2Creview_info%3Bdata%5B*%5D.author.badge%5B%3F(type%3Dbest_answerer)%5D.topics&offset=0&" \
            "limit=20&sort_by=created".format(Autore[Autor])
        Inhalt =requests.get(url,headers=kopf).json()
        text_total=Inhalt ['paging']['totals']
        for i in Inhalt ['data']:
            if col.count({'url':i['url']})==0:
                Artikel = Article(url='', language='zh', fetch_images=False, memoize_articles=False)
                Artikel.download(input_html=i['content'])
                Artikel.parse()
                text={
                    'url':i['url'],
                    'content':Artikel.text,
                    'title':i['title'],
                    'time':time.strftime("%Y-%m-%d", time.localtime(i['created'])),
                    '爬取时间':time.strftime("%Y-%m-%d", time.localtime(time.time())),
                    'author': Autor
                }
                col.insert(text)
                print(text)
                time.sleep(1)
            else:
                print("已存在该文章！")
        c=20
        while c<text_total:
            url = "https://www.zhihu.com/api/v4/members/{}/articles?include=data%5B*%5D.comment_count%2Csuggest_edit%2Cis_normal%2Cthumbnail_extra_info%2Cthumbnail%2Ccan_comment%2Ccomment_permission%2Cadmin_closed_comment%2Ccontent%2Cvoteup_count%2Ccreated%2Cupdated%2Cupvoted_followees%2Cvoting%2Creview_info%3Bdata%5B*%5D.author.badge%5B%3F(type%3Dbest_answerer)%5D.topics&offset={}&limit=20&sort_by=created".format(
                Autore[Autor],c)
            Inhalt = requests.get(url, headers=kopf).json()
            for i in Inhalt ['data']:
                if col.count({'url': i['url']}) == 0:
                    Artikel = Article(url='', language='zh', fetch_images=False, memoize_articles=False)
                    Artikel.download(input_html=i['content'])
                    Artikel.parse()
                    text = {
                        'url': i['url'],
                        'content': Artikel.text,
                        'title': i['title'],
                        'time': time.strftime("%Y-%m-%d", time.localtime(i['created'])),
                        '爬取时间': time.strftime("%Y-%m-%d", time.localtime(time.time())),
                        'author':Autor
                    }
                    col.insert(text)
                    print(text)
                    time.sleep(1)
                else:
                    print("已存在该文章！")
            c=c+20

if __name__ == '__main__':
    Zhihu_Artikel_Bekommt()