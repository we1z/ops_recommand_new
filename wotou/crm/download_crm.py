# coding:utf-8
'''
crm系统企业导出,自动将企业添加到动态跟踪数据库或黑名单数据库
'''
import urllib.request
import urllib
import json
import re
from wotou.lib.db_connection import mongodb_connection
from wotou.spiders.company.spider_company import get_company_detail_from_db_api
import pymongo
from bson.dbref import DBRef
from wotou.lib.db_connection import read_col
from collections import Counter


def download_crm_api():
    # 连接company数据库
    db = mongodb_connection('company')
    # 追踪数据库
    trace_collection = db['trace_company']
    # 连接公司黑名单数据库
    blacklist_collection = db['blacklist_company']
    # raw未处理的公司
    raw_collection = db['raw_company']
    # 连接到crm系统的
    module_name = 'Leads'
    authtoken = 'a558ee830032f6faa159120cad8eeac3'

    # 需要随crm企业数增长
    MAX_COMPANY_NUM = 7000

    company_list = []
    trace_company_list = []
    black_company_list = []

    # crm系统一次最多导出200个企业，因此需要分批导出
    for i in range(0, MAX_COMPANY_NUM, 200):

        params = {'authtoken': authtoken, 'scope': 'crmapi', 'fromIndex': str(i), 'toIndex': str(i + 199)}
        final_URL = "https://crm.zoho.com.cn/crm/private/json/" + "Leads" + "/getRecords"
        data = urllib.parse.urlencode(params).encode(encoding='UTF8')
        response = urllib.request.urlopen(final_URL, data)
        json_response = response.read()

        json_data = json.loads(bytes.decode(json_response))

        if 'result' in json_data['response']:
            for j in json_data['response']['result']['Leads']['row']:
                company = {}
                for k in j['FL']:
                    company[k['val']] = k['content']
                company_list.append(company)
        else:
            break

    with open('企业白名单.txt','w',encoding='utf-8') as f:
        for i in company_list:
            # if '初评人' in i and i['初评人']=='OPS':
                f.write(i['Last Name'])
                f.write('\n')
                print(i['Last Name'])

    # 获取原来的追踪数据库
    origin_trace_list = read_col('company', 'trace_company', '企业全称')

    # 判断企业二评或三评是不是为0,若二评或三评有其中一项标为0，则进入黑名单，否则进入跟踪企业名单
    for i in company_list:
        flag = True
        if '二评' in i:
            raw_erping = i['二评']
            try:
                raw_erping = re.compile("\d+").findall(raw_erping)[0]
                erping = int(raw_erping)
                if erping == 0:
                    flag = False
            except IndexError:
                pass
            except ValueError:
                pass

        if flag and '三评' in i:
            raw_sanping = i['三评']
            try:
                raw_sanping = re.compile("\d+").findall(raw_sanping)[0]
                sanping = int(raw_sanping)
                if sanping == 0:
                    flag = False
            except IndexError:
                pass
            except ValueError:
                pass

        if flag and '三评建议（实习生选）' in i:
            raw_sanping = i['三评建议（实习生选）']
            try:
                raw_sanping = re.compile("\d+").findall(raw_sanping)[0]
                sanping = int(raw_sanping)
                if sanping == 0:
                    flag = False
            except IndexError:
                pass
            except ValueError:
                pass

        if flag:
            trace_company_list.append(i)
            if i['Last Name'] not in origin_trace_list:
                detail = get_company_detail_from_db_api(i['Last Name'])
                try:
                    if detail:
                        trace_collection.insert({"企业全称": detail['企业全称'], "详细信息": DBRef('raw_company', detail['_id'])})
                        print('新增企业' + i['Last Name'])
                    else:
                        print('找不到企业 '+i['Last Name'])
                except pymongo.errors.DuplicateKeyError:
                    pass
                except KeyError:
                    pass
        else:

            black_company_list.append(i)
            if not blacklist_collection.find_one(i['Last Name']):
                detail = get_company_detail_from_db_api(i['Last Name'])
                try:
                    if detail:
                        blacklist_collection.insert(
                            {"企业全称": detail['企业全称'], "详细信息": DBRef('raw_company', detail['_id'])})
                    else:
                        print('找不到企业 '+i['Last Name']+'')
                except pymongo.errors.DuplicateKeyError:
                    pass
                if i['Last Name'] in origin_trace_list:
                    trace_collection.delete_many({"企业全称": detail['企业全称']})
                    print('减少企业' + i['Last Name'])


if __name__ == '__main__':
    download_crm_api()
