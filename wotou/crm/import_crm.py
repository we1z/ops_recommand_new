# coding:utf-8
'''
crm系统企业导入，自动将当前目录的所有xls文件中的有价值的企业导入crm系统，将放弃的企业放到二评黑名单中
crm系统部分字段不容许特殊字符，但是crm的文档并没有注明什么是他的保留字符，所以遇到导入不成功，可以检查相关文件是不是有特殊字符
'''
import urllib.request
import urllib
from xml.dom.minidom import Document
import xlrd
import os
import pymongo
from wotou.lib.db_connection import mongodb_connection
from bson.dbref import DBRef
from bson.objectid import ObjectId


def import_crm_api(filename):
    module_name = 'Leads'
    authtoken = 'a558ee830032f6faa159120cad8eeac3'

    db = mongodb_connection('company')
    collection = db['blacklist_company_not_in_crm']

    company_list = []
    # 读取当前文件夹里面所有xls文件

    if filename.endswith(".xls") or filename.endswith(".xlsx"):

        workbook = xlrd.open_workbook(filename)
        table = workbook.sheets()[0]
        nrows = table.nrows
        key_list = table.row_values(0)

        for i in range(1, nrows):
            company = {}
            if table.cell(i,0).value=='' and table.cell(i,10).value=='' :
                break
            for j in range(0, len(key_list)):
                # 注册资本只能是整数
                if key_list[j] == '注册资本':
                    try:
                        company[key_list[j]] = int(float(table.cell(i, j).value))
                    except ValueError:
                        company[key_list[j]]=0
                else:
                    company[key_list[j]] = table.cell(i, j).value
            company_list.append(company)
    if len(company_list)==0:
        os.remove(filename)
        return None
    tree = Document()
    leads = tree.createElement("Leads")
    tree.appendChild(leads)
    index = 0
    for i in company_list:
        # 无价值的企业放到黑名单
        if '0' in str(i['二评']):
            try:
                collection.insert({"企业全称": i['企业全称'], "详细信息": DBRef('raw_company', ObjectId(i['_id']))})
            except pymongo.errors.DuplicateKeyError:
                continue
        # 有价值的放到crm系统
        elif '1' in str(i['二评']):
            print(i['企业全称'])
            collection.delete_one({'企业全称':i['企业全称']})
            index += 1
            row = tree.createElement('row')
            row.setAttribute('no', str(index))
            leads.appendChild(row)

            for k, v in i.items():
                if k:
                    fl = tree.createElement("FL")
                    fl.setAttribute("val", str(k))
                    fl.appendChild(tree.createTextNode(str(v)))
                    row.appendChild(fl)

            # 由于crm系统的诡异设定，一定要有Last Name字段
            fl = tree.createElement("FL")
            fl.setAttribute("val", "Last Name")
            fl.appendChild(tree.createTextNode(i['企业全称']))
            row.appendChild(fl)

            # 网站字段只能由Website为名称提交
            fl = tree.createElement("FL")
            fl.setAttribute("val", "Website")
            fl.appendChild(tree.createTextNode(i['网站']))
            row.appendChild(fl)

    params = {'authtoken': authtoken, 'duplicateCheck': "1", 'scope': 'crmapi', 'newFormat': 1,
              "xmlData": tree.toprettyxml()}
    final_url = "https://crm.zoho.com.cn/crm/private/xml/" + "Leads" + "/insertRecords"
    data = urllib.parse.urlencode(params).encode(encoding='UTF8')
    response = urllib.request.urlopen(final_url, data)
    json_response = response.read()
    if 'Problem occured while processing the request' in str(json_response):
        if '余0' in filename:
            os.remove(filename)
        else:
            print('导入失败')
    elif 'Record(s) added successfully' in str(json_response):
        print('成功导入')
        os.remove(filename)
    elif 'Duplicate record(s) with same' in str(json_response):
        print('重复导入')
        os.remove(filename)



if __name__ == '__main__':
    for i in [filename for filename in os.listdir('.') if filename.endswith('xls') or filename.endswith('xlsx')]:
        print(i)
        import_crm_api(i)

